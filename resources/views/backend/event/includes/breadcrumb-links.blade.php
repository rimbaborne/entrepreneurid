<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.events.index') }}">Data</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.events.create') }}">@lang('backend_events.menus.create')</a> --}}
                {{-- <a class="dropdown-item" href="{{ route('admin.events.deactivated') }}">@lang('backed_events.menus.deactivated')</a> --}}
                {{-- <a class="dropdown-item" href="{{ route('admin.events.deleted') }}">@lang('backend_events.menus.deleted')</a> --}}
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
