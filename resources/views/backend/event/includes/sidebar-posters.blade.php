<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/posters*')) }}" href="{{ route('admin.posters.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_posters.sidebar.title')
    </a>
</li>