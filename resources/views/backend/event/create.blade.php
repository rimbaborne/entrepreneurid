@extends('backend.layouts.app')

@section('title', __('backend_event.labels.management') . ' | ' . __('backend_event.labels.create'))

@section('breadcrumb-links')
    @include('backend.event.includes.breadcrumb-links')
@endsection

@section('content')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Pilih Produk
                        <small class="text-muted">Buat Event</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>
            @if (request()->produk == null)
                <div class="row" action="">
                    <label class="col-md-2 col-form-label">
                        Pilih Produk
                    </label>
                    <div class="col-md-10">
                        <form action="">
                            <select class="form-control"name="produk">
                                @foreach ($dataproduk as $data)
                                    <option value="{{ $data->id }}">{{ $data->nama }} - {{ $data->inisial }}</option>
                                @endforeach
                            </select>
                            <button class="mt-2 btn btn-primary"><i class="fas fa-chevron-right"></i> Pilih Produk </button>
                        </form>
                    </div>
                </div><!--form-group-->
            @else
            <form class="form-horizontal" action="{{ route('admin.events.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="produkid" value="{{ $produk->id }}">
            <div class="row mt-4 mb-4">
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            ID Event
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="idevent" value="{{ $idevent }}" class="form-control" placeholder="Id Event" readonly>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Produk
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="nama" value="{{ $produk->nama }}" class="form-control" placeholder="Nama Produk" disabled>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Harga
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="harga" value="50000" class="form-control" placeholder="Harga Produk" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Komisi
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="komisi" value="17000" class="form-control" placeholder="Komisi Produk" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Mulai Event
                        </label>
                        <div class="col-md-10">
                            <input type="text" value="2021-07-01 00:00:01" name="start" class="form-control" placeholder="Mulai Event" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Akhir Event
                        </label>
                        <div class="col-md-10">
                            <input type="text" value="2021-07-01 16:00:01" name="end" class="form-control" placeholder="Akhir Event" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Link Sales
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="linksales" value="https://{{ $produk->domain }}/sales/" class="form-control" placeholder="Link Sales" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Link Form
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="linkform" value="https://konfirmasi.{{ $produk->domain }}/daftar/" class="form-control" placeholder="Link Form" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Status Pemesanan
                        </label>
                        <div class="col-md-10">
                            <select class="form-control" name="open">
                                <option value="0">TIDAK AKTIF</option>
                                <option value="1">AKTIF</option>
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Tampilkan (Publish)
                        </label>
                        <div class="col-md-10">
                            <select class="form-control" name="publish">
                                <option value="0">TIDAK</option>
                                <option value="1">YA</option>
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Foto Produk
                        </label>
                        <div class="col-md-10">
                            <input type="file" name="gambar">
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
            @if (null != request()->produk)
            <div class="">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.events.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                    <div class="col text-right">
                        {{ form_submit(__('buttons.general.crud.create')) }}
                    </div><!--row-->
                </div><!--row-->
            </div><!---->
            @else
            <div class="">
                <div class="row">
                    <div class="col">
                        {{ form_cancel(route('admin.events.index'), __('buttons.general.cancel')) }}
                    </div><!--col-->

                </div><!--row-->
            </div><!---->
            @endif
            </form>

            @endif
        </div><!--card-body-->



    </div><!--card-->
@endsection
