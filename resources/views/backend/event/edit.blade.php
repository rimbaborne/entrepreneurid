@extends('backend.layouts.app')

@section('title', 'Event | Edit')

@section('breadcrumb-links')
    @include('backend.event.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <form action="{{ route('admin.events.update') }}" method="POST">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Edit Event
                        <br>
                        <small class="text-muted" style="font-weight: 600">{{ data_get($data->produk, 'nama') }}, {{ $data->id_event }}
                        </small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>
            @csrf
            <input type="hidden" name="id" value="{{ $data->id }}">
            <div class="row mt-4 mb-4">
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            ID Event
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="idevent" value="{{ $data->id_event }}" class="form-control" placeholder="Id Event" readonly>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Produk
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="nama" value="{{ $data->produk->nama }}" class="form-control" placeholder="Nama Produk" disabled>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Harga
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="harga" value="{{ $data->harga }}" class="form-control" placeholder="Harga Produk" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Komisi
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="komisi" value="{{ $data->komisi }}" class="form-control" placeholder="Komisi Produk" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Komisi Sub Agen
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="komisi_jv" value="{{ $data->komisi_jv }}" class="form-control" placeholder="Komisi Produk Sub Agen" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Mulai Event
                        </label>
                        <div class="col-md-10">
                            <input type="text" value="{{ $data->start }}" name="start" class="form-control" placeholder="Mulai Event" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Akhir Event
                        </label>
                        <div class="col-md-10">
                            <input type="text" value="{{ $data->end }}" name="end" class="form-control" placeholder="Akhir Event" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Link Sales
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="linksales" value="{{ $data->link_sales }}" class="form-control" placeholder="Link Sales">
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Link Form
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="linkform" value="{{ $data->link_form }}" class="form-control" placeholder="Link Form" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Status Pemesanan
                        </label>
                        <div class="col-md-10">
                            <select class="form-control" name="open" id="open">
                                <option value="0">TIDAK AKTIF</option>
                                <option value="1">AKTIF</option>
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Tampilkan (Publish)
                        </label>
                        <div class="col-md-10">
                            <select class="form-control" name="publish" id="publish">
                                <option value="0">TIDAK</option>
                                <option value="1">YA</option>
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Status Pemesanan Sub Agen
                        </label>
                        <div class="col-md-10">
                            <select class="form-control" name="open_jv" id="openjv">
                                <option value="0">TIDAK AKTIF</option>
                                <option value="1">AKTIF</option>
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Tampilkan (Publish) Sub Agen
                        </label>
                        <div class="col-md-10">
                            <select class="form-control" name="publish_jv" id="publishjv">
                                <option value="0">TIDAK</option>
                                <option value="1">YA</option>
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">

                </div><!--col-->

                <div class="col text-right">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
          $("#open").val("{!! $data->open !!}");
          $("#publish").val("{!! $data->publish !!}");
          $("#openjv").val("{!! $data->open_jv !!}");
          $("#publishjv").val("{!! $data->publish_jv !!}");
    });
  </script>
@endsection
