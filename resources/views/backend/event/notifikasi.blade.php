@extends('backend.layouts.app')

@section('title', app_name() . ' | Notifikasi Event')

@section('breadcrumb-links')
    @include('backend.produk.includes.breadcrumb-links')
@endsection

@section('content')
@include('backend.event.js')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Notifikasi Event
                    <br>
                    <small class="text-muted" style="font-weight: 600">{{ data_get($data->produk, 'nama') }}, {{ $data->id_event }}
                    </small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                {{-- @include('backend.produk.includes.header-buttons') --}}
            </div><!--col-->
        </div><!--row-->
        {{-- <div class="row">
            <div class="col-12">
                <table class="table borderless table-kecil" style="font-size: 12px; font-family: 'Roboto Mono', monospace;">
                    <thead>
                        <tr>
                            <th width="150"></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID Event</td>
                            <td>:</td>
                            <td class="font-weight-bold text-uppercase">
                                {{ $data->id_event }}
                            </td>
                        </tr>
                        <tr>
                            <td>Produk</td>
                            <td>:</td>
                            <td class="font-weight-bold text-uppercase">
                                {{ data_get($data->produk, 'nama') }}
                            </td>
                        </tr>
                        <tr>
                            <td>Harga</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                RP {{ $data->harga }}
                            </td>
                        </tr>
                        <tr>
                            <td>Komisi Agen</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                RP {{ $data->komisi }}
                            </td>
                        </tr>
                        <tr>
                            <td>Mulai Event</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                {{ $data->start }}
                            </td>
                        </tr>
                        <tr>
                            <td>Akhir Event</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                {{ $data->end }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div> --}}

        <div class="row">
            <div class="col-7">
                <div class="float-left">
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
<div class="card">
    <div class="card-body">
        <div class="row bd-example2">
            <div class="col-md-3">
                <h5 class="card-header">
                    Notifikasi E-mail
                </h5>
                <div class="list-group">
                    <a href="
                        {{ route('admin.events.notifikasi') }}?id={{ request()->id }}&notif=email-pendaftaran"
                        class="list-group-item list-group-item-action
                        {{ request()->notif == 'email-pendaftaran' ? 'active' : '' }}">
                        Pendaftaran
                    </a>
                    <a href="
                        {{ route('admin.events.notifikasi') }}?id={{ request()->id }}&notif=email-upload-pembayaran"
                        class="list-group-item list-group-item-action
                        {{ request()->notif == 'email-upload-pembayaran' ? 'active' : '' }}">
                        Upload Pembayaran
                    </a>
                    <a href="
                        {{ route('admin.events.notifikasi') }}?id={{ request()->id }}&notif=email-konfirmasi-pembayaran"
                        class="list-group-item list-group-item-action
                        {{ request()->notif == 'email-konfirmasi-pembayaran' ? 'active' : '' }}">
                        Konfirmasi Pembayaran
                    </a>
                    <a href="
                        {{ route('admin.events.notifikasi') }}?id={{ request()->id }}&notif=email-upload-ulang"
                        class="list-group-item list-group-item-action
                        {{ request()->notif == 'email-upload-ulang' ? 'active' : '' }}">
                        Reminder
                    </a>
                </div>
                <h5 class="card-header mt-4">
                    Notifikasi WhatsApp
                </h5>
                <div class="list-group">
                    <a href="
                        {{ route('admin.events.notifikasi') }}?id={{ request()->id }}&notif=wa-pendaftaran"
                        class="list-group-item list-group-item-action
                        {{ request()->notif == 'wa-pendaftaran' ? 'active' : '' }}">
                        Pendaftaran
                    </a>
                    <a href="
                        {{ route('admin.events.notifikasi') }}?id={{ request()->id }}&notif=wa-konfirmasi-pembayaran"
                        class="list-group-item list-group-item-action
                        {{ request()->notif == 'wa-konfirmasi-pembayaran' ? 'active' : '' }}">
                        Konfirmasi Pembayaran
                    </a>
                    <a href="
                        {{ route('admin.events.notifikasi') }}?id={{ request()->id }}&notif=wa-belum-upload"
                        class="list-group-item list-group-item-action
                        {{ request()->notif == 'wa-belum-upload' ? 'active' : '' }}">
                        Pengingat Belum Upload
                    </a>
                    <a href="
                        {{ route('admin.events.notifikasi') }}?id={{ request()->id }}&notif=wa-upload-ulang"
                        class="list-group-item list-group-item-action
                        {{ request()->notif == 'wa-upload-ulang' ? 'active' : '' }}">
                        Reminder
                    </a>
                </div>
            </div>
            <div class="col-md-9">
                @isset(request()->notif)
                    <form action="{{ route('admin.events.notifikasi.simpan') }}" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{ $data->id }}">

                        @if (request()->notif == 'email-pendaftaran')
                            @php $data = json_decode($datanotif->email_pendaftaran); @endphp
                            <input type="hidden" name="jenisnotif" value="email_pendaftaran">
                        @elseif (request()->notif == 'email-upload-pembayaran')
                            @php $data = json_decode($datanotif->email_upload_pembayaran); @endphp
                            <input type="hidden" name="jenisnotif" value="email_upload_pembayaran">
                        @elseif (request()->notif == 'email-konfirmasi-pembayaran')
                            @php $data = json_decode($datanotif->email_konfirmasi_pembayaran); @endphp
                            <input type="hidden" name="jenisnotif" value="email_konfirmasi_pembayaran">
                        @elseif (request()->notif == 'email-upload-ulang')
                            @php $data = json_decode($datanotif->email_upload_ulang); @endphp
                            <input type="hidden" name="jenisnotif" value="email_upload_ulang">
                        @elseif (request()->notif == 'wa-pendaftaran')
                            @php $data = json_decode($datanotif->wa_pendaftaran); @endphp
                            <input type="hidden" name="jenisnotif" value="wa_pendaftaran">
                        @elseif (request()->notif == 'wa-konfirmasi-pembayaran')
                            @php $data = json_decode($datanotif->wa_konfirmasi_pembayaran); @endphp
                            <input type="hidden" name="jenisnotif" value="wa_konfirmasi_pembayaran">
                        @elseif (request()->notif == 'wa-belum-upload')
                            @php $data = json_decode($datanotif->wa_belum_upload); @endphp
                            <input type="hidden" name="jenisnotif" value="wa_belum_upload">
                        @elseif (request()->notif == 'wa-upload-ulang')
                            @php $data = json_decode($datanotif->wa_upload_ulang); @endphp
                            <input type="hidden" name="jenisnotif" value="wa_upload_ulang">
                        @endif

                        @if (strpos(request()->notif, 'email') !== false)
                            <div class="mb-3 row">
                                <label class="col-sm-2 col-form-label">Email Pengirim</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="email" value="{{  $data->email ?? '' }}">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-2 col-form-label">Subject Pengirim</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="subject" value="{{  $data->subject ?? '' }}">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-2 col-form-label">Nama Pengirim</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="name" value="{{  $data->name ?? '' }}">
                                </div>
                            </div>
                            <textarea name="isi" id="data">{{  $data->isi ?? '' }}</textarea>
                        @else
                            <textarea name="isi" class="form-control" rows="20">{{  $data->isi ?? '' }}</textarea>
                        @endif

                        <button class="mt-2 btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
                    </form>
                @endisset
            </div>
        </div>
    </div>
</div>
<script>
    var lazyLoadInstance = new LazyLoad();
</script>
@endsection
