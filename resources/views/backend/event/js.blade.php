<script type = "text/javascript" >

    tinymce.init({
        selector: '#data',
        plugins: 'link lists advlist',
        toolbar: 'mybutton | undo redo | forecolor backcolor | bullist numlist | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | outdent indent | link',
        menubar: false,
        height : "480",
        setup: function(editor) {
            editor.ui.registry.addSplitButton('mybutton', {
                text: 'Menu Data',
                onAction: function() {},
                onItemAction: function(api, value) {
                    editor.insertContent(value);
                },
                fetch: function(callback) {
                    if (editor.getContent() !== "") {

                        var items = [
                                        {
                                            "type":"choiceitem",
                                            "text":"Nama",
                                            "value":" <strong>{Nama}<\/strong> ",
                                        },
                                        {
                                            "type":"choiceitem",
                                            "text":"Panggilan",
                                            "value":" <strong>{Panggilan}<\/strong> ",
                                        },
                                        {
                                            "type":"choiceitem",
                                            "text":"Gender",
                                            "value":" <strong>{Gender}<\/strong> ",
                                        },
                                        {
                                            "type":"choiceitem",
                                            "text":"E-mail",
                                            "value":" <strong>{E-mail}<\/strong> ",
                                        },
                                        {
                                            "type":"choiceitem",
                                            "text":"No. Telepon",
                                            "value":" <strong>{No. Telepon}<\/strong> ",
                                        },
                                        {
                                            "type":"choiceitem",
                                            "text":"Alamat",
                                            "value":" <strong>{Alamat}<\/strong> ",
                                        },
                                        {
                                            "type":"choiceitem",
                                            "text":"Kota",
                                            "value":" <strong>{Kota}<\/strong> ",
                                        },
                                        {
                                            "type":"choiceitem",
                                            "text":"Nominal Transfer",
                                            "value":" <strong>{Nominal Transfer}<\/strong> ",
                                        },
                                        {
                                            "type":"choiceitem",
                                            "text":"Ekspedisi",
                                            "value":" <strong>{Ekspedisi}<\/strong> ",
                                        },
                                        {
                                            "type":"choiceitem",
                                            "text":"Jumlah Buku",
                                            "value":" <strong>{Jumlah Buku}<\/strong> ",
                                        }
                                    ];

                        callback(items);
                    };
                }
            });
        }
    });

</script>
