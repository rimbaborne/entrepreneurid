@extends('backend.layouts.app')

@section('title', app_name() . ' | Produk')

@section('breadcrumb-links')
    @include('backend.produk.includes.breadcrumb-links')
@endsection

@section('content')
<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.3.0/dist/lazyload.min.js"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script> --}}
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@500&display=swap" rel="stylesheet">

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Data Event
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                {{-- @include('backend.produk.includes.header-buttons') --}}
            </div><!--col-->
        </div><!--row-->


        {{-- <div class="row mt-4" data-masonry='{"percentPosition": true }'>

            @php
            $first  = 0;
            $end    = 0;
            @endphp
            @foreach($produks as  $key => $produk)
            <div class="col-xl-3 col-lg-4 col-md-4 col-6 mb-4">
                <div class="bg-white rounded shadow">
                    <a href="{{ $produk->link }}" target="_blank">
                        <img alt="CNL" class="img img-responsive full-width img-fluid card-img-top lazy" data-src="https://app.copywritingnextlevel.com/produk/{{ $produk->file }}" />
                    </a>
                    <div class="p-0 row">
                        <div class="pt-2 pl-4 col-6">
                            <h5>{{ $key + $produks->firstItem() }}</h5>
                        </div>
                        <div class="col-6 text-right">
                            <div class="btn-group btn-sm" role="group" aria-label="Action">
                                <a href="/admin/produks/?status=hapus&id={{ $produk->id }}" class="btn btn-danger btn-sm" type="button"><i class="fas fa-trash"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @php
              $first  = $produks->firstItem();
              $end    = $key + $produks->firstItem();
            @endphp
            @endforeach
        </div><!--row--> --}}
        <hr>
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    <a href="{{ route('admin.events.create') }}">
                        <button class="btn btn-info"><i class="fas fa-plus"></i> Buat Event Baru</button>
                    </a>
                    {{-- {!! $first !!} - {!! $end !!} From {!! $produks->total() !!} Data --}}
                </div>
            </div><!--col-->

            <div class="col-5 float-right">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-search"></i> </span>
                    </div>
                    <input wire:model.debounce.1000ms="search" class="form-control" type="text" placeholder="Cari Produk (Inisial)"
                    autocomplete="password" width="100">
                </div>
                {{-- {!! $produks->appends(request()->query())->links() !!} --}}
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@php
    $first  = 0;
    $end    = 0;
@endphp

@foreach ($dataevent as  $key => $data)
<div class="card">
    <div class="display-4 position-absolute" style="color: #626e8047; font-size: 40px; font-weight: bold">
        {{ $key+ $dataevent->firstItem() }}
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-8">
                <table class="table borderless table-kecil" style="font-size: 12px; font-family: 'Roboto Mono', monospace;">
                    <thead>
                        <tr>
                            <th width="250"></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID Event</td>
                            <td>:</td>
                            <td class="font-weight-bold text-uppercase">
                                {{ $data->id_event }}
                            </td>
                        </tr>
                        <tr>
                            <td>Produk</td>
                            <td>:</td>
                            <td class="font-weight-bold text-uppercase">
                                {{ data_get($data->produk, 'nama') }}
                            </td>
                        </tr>
                        <tr>
                            <td>Harga</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                RP {{ $data->harga }}
                            </td>
                        </tr>
                        <tr>
                            <td>Komisi Agen</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                RP {{ $data->komisi }}
                            </td>
                        </tr>
                        <tr>
                            <td>Komisi Sub Agen</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                RP {{ $data->komisi_jv }}
                            </td>
                        </tr>
                        <tr>
                            <td>Mulai Event</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                {{ $data->start }}
                            </td>
                        </tr>
                        <tr>
                            <td>Akhir Event</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                {{ $data->end }}
                            </td>
                        </tr>
                        <tr>
                            <td>Kode Akses</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                {{ $data->kode_akses }}
                            </td>
                        </tr>
                        <tr>
                            <td>Link Sales</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                {{ $data->link_sales }}
                            </td>
                        </tr>
                        <tr>
                            <td>Link Form</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                {{ $data->link_form }}
                            </td>
                        </tr>
                        <tr>
                            <td>Open Pemesanan</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                @if ($data->open == true)
                                    <div class="text-success">AKTIF</div>
                                @else
                                    <div class="text-danger">NONAKTIF</div>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Tampilkan (Publish)</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                @if ($data->publish == true)
                                    <div class="text-success">OPEN</div>
                                @else
                                    <div class="text-danger">CLOSED</div>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Open Pemesanan Sub Agen</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                @if ($data->open_jv == true)
                                    <div class="text-success">AKTIF</div>
                                @else
                                    <div class="text-danger">NONAKTIF</div>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Tampilkan (Publish) Sub Agen</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                @if ($data->publish_jv == true)
                                    <div class="text-success">OPEN</div>
                                @else
                                    <div class="text-danger">CLOSED</div>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Total Transaksi</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                {{ DB::table('transactions')->where('id_event', $data->id)->count() }}
                            </td>
                        </tr>
                        <tr>
                            <td>Total Closing</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                {{ DB::table('transactions')->where('id_event', $data->id)->where('status', 3)->count() }}
                            </td>
                        </tr>
                        <tr>
                            <td>Keikutsertaan Agen</td>
                            <td>:</td>
                            <td class="font-weight-bold">
                                {{ DB::table('transactions')->where('id_event', $data->id)->select('id_agen')->groupBy('id_agen')->get()->count() }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-4">
                <div class="text-center">
                    <img src="/img/produk/{{ $data->gambar }}" alt="" width="150" srcset="">
                </div>
                <div class="">
                    <input type="file">
                    <button class="btn btn-primary btn-sm">Upload</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    <div class="btn-group" role="group" aria-label="labels.backend.access.users.user_actions">
                        <a href="{{ route('admin.events.notifikasi') }}?id={{ $data->id }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-success"><i class="fas fa-paper-plane"></i> Notifikasi</a>
                        <a href="{{ route('admin.events.edit') }}?id={{ $data->id }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-primary"><i class="fas fa-edit"></i> Edit</a>
                        <div class="btn-group btn-group-sm" role="group">
                            <button id="userActions" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Lainnya
                            </button>
                            <div class="dropdown-menu" aria-labelledby="userActions" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 35px, 0px);">
                                <a href="#" class="dropdown-item">Download Excel</a>
                                @if ($data->open == true)
                                    <a href="/admin/events?id={{ $data->id }}&status=0&mode=open" class="dropdown-item">
                                        Non Aktifkan
                                    </a>
                                @else
                                    <a href="/admin/events?id={{ $data->id }}&status=1&mode=open" class="dropdown-item">
                                        Aktifkan
                                    </a>
                                @endif

                                @if ($data->publish == true)
                                    <a href="/admin/events?id={{ $data->id }}&status=0&mode=publish" class="dropdown-item">
                                        Closed
                                    </a>
                                @else
                                    <a href="/admin/events?id={{ $data->id }}&status=1&mode=publish" class="dropdown-item">
                                        Publish
                                    </a>
                                @endif

                                <a href="#" data-method="delete" data-trans-button-cancel="Batal" data-trans-button-confirm="Hapus" data-trans-title="Anda yakin?" class="dropdown-item" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">Hapus
                                    <form action="#" method="POST" name="delete_item" style="display:none">
                                        <input type="hidden" name="_method" value="delete">
                                    </form>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-12">
                <small class="float-right text-muted">
                    <strong>Dibuat :</strong> Minggu, 7-5-2021,
                    <strong>Diperbarui:</strong> Selasa,15-5-2021
                </small>
            </div>
        </div>
    </div>
</div>
@php
    $first  = $dataevent->firstItem();
    $end    = $key + $dataevent->firstItem();
@endphp
@endforeach
<div class="row">
    <div class="col-7">
        <div class="float-left">
            {!! $first !!} - {!! $end !!} From {!! $dataevent->total() !!} Data
        </div>
    </div><!--col-->

    <div class="col-5">
        <div class="float-right">
            {!! $dataevent->appends(request()->query())->links() !!}
        </div>
    </div><!--col-->
</div><!--row-->
<script>
    var lazyLoadInstance = new LazyLoad();
</script>
@endsection
