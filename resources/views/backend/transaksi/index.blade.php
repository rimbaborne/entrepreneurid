@extends('backend.layouts.app')

@section('title', app_name() . ' | Transaksi')

@section('breadcrumb-links')
@include('backend.transaksi.includes.breadcrumb-links')
@endsection

@section('content')

@if (request()->metode == 'edit')
    @include('backend.transaksi.includes.edit-transaksi')
@elseif (request()->metode == 'upload')
    @include('backend.transaksi.includes.upload')
@else
    @include('backend.transaksi.includes.buat-transaksi')

    <div class="modal fade" id="exportdata" tabindex="-1" role="dialog" aria-labelledby="exportdataLabel" aria-hidden="true">
        <form action="/admin/transaksi/exportdata" target="_blank">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exportdataLabel">Export Data Transaksi</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                @csrf
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">Pilih Event</label>
                                    <div class="col-sm-8">
                                        <select name="event" class="form-control">
                                            <option value="">Pilih</option>
                                            @foreach ($produklist as $data)
                                                <option value="{{ $data->id_event }}">{{ $data->event->produk->nama }} ({{ $data->event->id_event }})</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-success">Export <i class="fas fa-download"></i></button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    {{-- @include('backend.transaksi.includes.statistik-2') --}}
    @include('backend.transaksi.includes.statistik')

    <div class="card" style="min-width: 1000px">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <h4 class="card-title mb-0">
                        Data Transaksi
                    </h4>
                </div><!--col-->

                <div class="col-sm-7 ">
                    <div class="float-right">
                        <button type="button" class="btn btn-primary"  data-toggle="collapse" href="#buattransaksi" aria-expanded="false">
                            Tambah Data Transaksi
                        </button>
                    </div>
                </div><!--col-->
            </div><!--row-->
            {{-- <div class="row" style=" text-align: end;">
                <div style=" position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 9999; background: rgba(0,0,0,.4); margin: 0 1; ">
                <div style=" position: relative; border-radius: .3125em; font-family: inherit; top: 50%; left: 50%; transform: translate(-100%, 100%); color: #fff; ">
                <span class="spinner-border" role="status" aria-hidden="true"></span>
            </div> --}}

            <form class="row mt-4" action="{{ url()->current() }}">
                <div class="col-12">
                    <div class="d-print-none row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="pb-2 col-6" >
                                    <div style="padding-top: 5px; padding-bottom: 5px">
                                        <label class="form-check-label">Nama / Email</label>
                                    </div>
                                    <input class="form-control" type="text" value="{{ request()->nama ?? '' }}" name="nama" style="font-size: small" />
                                </div>
                                <div class="pb-2 col-6" >
                                    <div style="padding-top: 5px; padding-bottom: 5px">
                                        <label class="form-check-label">Nama / Email Agen</label>
                                    </div>
                                    <input class="form-control" type="text" value="{{ request()->agen ?? '' }}" name="agen" style="font-size: small" />
                                </div>
                            </div>
                        </div>

                        <div class="pb-2 col">
                            <div  style="padding-top: 5px; padding-bottom: 5px">
                                <label class="form-check-label">Produk</label>
                            </div>
                            <select id="produk" name="produk" class="form-control">
                                <option value="">Semua</option>
                                @foreach ($produklist as $data)
                                <option value="{{ $data->id_event }}">{{ $data->event->produk->nama }} ({{ $data->event->id_event }})</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="pb-2 col">
                            <div  style="padding-top: 5px; padding-bottom: 5px">
                                <label class="form-check-label">Status</label>
                            </div>
                            <select id="status" name="status" class="form-control">
                                <option value="">Semua</option>
                                <option value="1">Belum Aktif</option>
                                <option value="2">Konfirmasi</option>
                                <option value="3">Aktif</option>
                            </select>
                        </div>
                        <div class="pb-2 col">
                            <div  style="padding-top: 5px; padding-bottom: 5px">
                                <label class="form-check-label">Jenis</label>
                            </div>
                            <select id="jenis" name="jenis" class="form-control">
                                <option value="">Semua</option>
                                <option value="1">Transaksi Manual</option>
                                <option value="2">Transaksi Afiliasi</option>
                            </select>
                        </div>
                        <div class="pb-2 col">
                            <div  style="padding-top: 5px; padding-bottom: 5px">
                                <label class="form-check-label">Data</label>
                            </div>
                            <select id="roder" name="roder" class="form-control">
                                <option value="">Semua</option>
                                <option value="1">Repeat Order</option>
                                {{-- <option value="2">Non Repeat Order</option> --}}
                            </select>
                            <small class="text-muted">*Pilih Produk Dulu</small>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-2">
                    <div class="row">
                        <div class="col-1">
                            <div  style="padding-bottom: 5px">
                                <label class="form-check-label">Per Page</label>
                            </div>
                            <select class="form-control form-control-sm" name="perpage" id="perpage">
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </div>
                        <div class="col-3 mb-4">
                            <div  style="padding-bottom: 5px">
                                <label class="form-check-label">Tanggal</label>
                            </div>
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control form-control-sm datepicker" name="start" value="{{ request()->start ?? '2019-01-01' }}">
                                <div style="padding-top: 2px">-</div>
                                <input type="text" class="form-control form-control-sm datepicker" name="end" value="{{ request()->end ? \Carbon\Carbon::parse(request()->end)->format('Y-m-d') : \Carbon\Carbon::now()->format('Y-m-d') }}">
                            </div>
                            {{-- <input class="form-control datepicker form-control-sm" value="" type="text" name="tanggal" id="" maxlength="11" size="11" > --}}
                        </div>
                        <div class="col-3 mb-4">
                            <div  style="padding-bottom: 5px">
                                <label class="form-check-label">Download Data</label>
                            </div>
                            <a href="#" data-toggle="modal" data-target="#exportdata" class="btn btn-sm btn-primary btn-success">
                                <i class="fas fa-download"></i> Transaksi
                            </a>
                            <a href="{{ route('admin.transaksi.leadmagnetexport') }}" target="_blank" class="btn btn-sm btn-outline-warning">
                                Lead Magnet
                                <i class="fas fa-download"></i>
                            </a>
                            {{-- <a href="/admin/transaksi/cnl/resi" class="btn btn-sm btn-primary ">
                                <i class="fas fa-book"></i> Resi CNL
                            </a> --}}
                        </div>

                        <div class="col text-right">
                        </div>
                        <div class="col-4 text-right pt-4">
                            <div class="align-middle">
                                <button type="submit" class="btn btn-primary btn-sm btn-block btn-pill" > <i class="fas fa-search"></i> Cari</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div class="row kotak-atas" style="font-size: 11px; text-transform: uppercase; font-weight: 600">
                <div class="col-2">
                    Nama
                </div>
                <div class="col-3">
                    Email
                </div>
                <div class="col-2">
                    Agen
                </div>
                <div class="col-2">
                    Produk
                </div>
                <div class="col">
                    Jenis
                </div>
            </div>

            <div class="row mt-4" style="margin-top: 0.35rem!important;">
                <div class="col">
                    @php
                    $first  = 0;
                    $end    = 0;
                    @endphp
                    @foreach ($datatransaksi as $key => $data)
                        <div class="legend">
                            <div class="row kotak">
                                <div class="col-2">
                                    <a data-toggle="collapse" href="#detail{{ $key + $datatransaksi->firstItem() }}"
                                        aria-expanded="false" style="color: rgb(56, 56, 56);">
                                        <div><strong>{{ $data->nama }}</strong></div>
                                        <div class="small text-muted">
                                            {{-- @php
                                                $birth = !empty($data->tgllahir) ? $data->tgllahir : \Carbon\Carbon::now()->format('Y-m-d');
                                                $umur = !empty(\Carbon\Carbon::parse($birth)->age) ? \Carbon\Carbon::parse($birth)->age : '0';
                                            @endphp
                                                {{ $data->gender }} | {{ $umur ?? 0 }} Tahun --}}
                                                {{  $data->gender  }} | {{ $data->tgllahir }}
                                        </div>
                                    </a>
                                </div>
                                <div class="col-2">
                                    {{ $data->email }}
                                    <div class="small text-muted">
                                        Daftar : {{ $data->created_at }}
                                    </div>
                                </div>
                                <div class="col-3" style="margin-left: 0px">
                                    <div class="row">
                                        @if (!null == $data->agen)
                                            <div class="col pr-0" style="text-align: center;">
                                                <img class="card-img-top" src="{{ $data->agen->foto ? 'https://dashboard.agen-entrepreneurid.com/foto/'.$data->agen->foto : 'https://ui-avatars.com/api/?background=random&name='.$data->agen->name }}" alt="Profile Picture"
                                                style="
                                                object-fit: cover;
                                                height: 40px;
                                                width: 40px;
                                                border-radius: 50%;
                                                ">
                                            </div>
                                            <div class="col-9 pl-0">
                                                <a href="{{ route('admin.transaksi.dataagen') }}?idagen={{ $data->id_agen }}&produk={{ $data->id_event }}">
                                                    <strong>{{ $data->agen->name }}</strong>
                                                </a>
                                                <div class="small text-muted" style="text-transform: lowercase;">
                                                    {{ $data->agen->email }}
                                                </div>
                                            </div>
                                        @else
                                            -
                                        @endif
                                    </div>
                                </div>
                                <div class="col-2">
                                    <a data-toggle="collapse" href="#detail{{ $key + $datatransaksi->firstItem() }}" aria-expanded="false" style="color: rgb(56, 56, 56);">
                                        <div style="font-size: 11px; text-transform: uppercase;">
                                            <strong>{{ $data->event->produk->nama }}</strong>
                                            <div class="text-muted">
                                                {{ $data->event->id_event }}
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col">
                                    <div class="row">
                                        <div class="col-3">
                                            <a data-toggle="collapse" href="#detail{{ $key + $datatransaksi->firstItem() }}" aria-expanded="false">
                                                <div class="text-muted position-absolute" style="font-size: 10px; text-transform: uppercase;">
                                                    @if ($data->jenis == 1)
                                                        Transaksi Manual
                                                    @elseif ($data->jenis == 2)
                                                        Transaksi Afiliasi
                                                    @endif
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-7">
                                            @include('backend.transaksi.includes.status')
                                        </div>
                                        <div class="col-2" style="padding: 0px">

                                            <div class="btn-group dropleft">
                                                <button class="btn btn-sm btn-outline-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                                                    Opsi
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a href="/admin/transaksi?id={{ $data->id }}&metode=upload" class="dropdown-item">
                                                        Upload Bukti Transfer
                                                    </a>
                                                    {{-- @if($data->status == 2) --}}
                                                        <a href="/admin/transaksi?id={{ $data->id }}&metode=reminder" class="dropdown-item">
                                                            Reminder
                                                        </a>
                                                    {{-- @endif --}}
                                                    @if($data->status == 1)
                                                        <a href="/admin/transaksi?id={{ $data->id }}&metode=belum-upload" class="dropdown-item">
                                                            Belum Upload TF <span class="badge bg-warning badge-sm">notif</span>
                                                        </a>
                                                    @endif
                                                    <a href="/admin/transaksi?id={{ $data->id }}&metode=edit" class="dropdown-item">
                                                        Edit
                                                    </a>
                                                    <a href="#" title="Hapus" data-method="delete" data-trans-button-cancel="Batal" data-trans-button-confirm="Hapus"
                                                    data-trans-title="{{ $data->email }} dihapus?" class=" dropdown-item" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
                                                        <form action=""  onsubmit="return confirm('Apakah Anda yakin data {{ $data->event->produk->nama }} - {{ $data->email }} dihapus ?');" style="display:none">
                                                            <input type="hidden" name="metode" value="hapus">
                                                            <input type="hidden" name="id" value="{{ $data->id }}">
                                                        </form>
                                                        Hapus
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" style="color: #4e4e4e">
                                    <div class="col">
                                        <div class="collapse" id="detail{{ $key + $datatransaksi->firstItem() }}">
                                            <hr>
                                            <table class="table table-sm table-borderless" style="margin-bottom: 0rem">
                                                <thead style="font-weight: 400;">
                                                    <th width="100">Tgl. Lahir</th>
                                                    <th>No. Telp</th>
                                                    <th>Kode Unik</th>
                                                    <th>Bukti Transfer</th>
                                                    <th>Rek. Transfer</th>

                                                </thead>
                                                <tbody style="font-weight: 300;">
                                                    <tr>
                                                        <td>
                                                            {{ $data->tgllahir }}
                                                        </td>
                                                        <td>
                                                            +{{ $data->kode_nohp }}{{ $data->nohp }}
                                                        </td>
                                                        <td>
                                                            <strong>{{ $data->kodeunik }}</strong>
                                                        </td>
                                                        <td>
                                                            <div>
                                                                {{ $data->waktu_upload ?? '-' }}
                                                            </div>
                                                            <div style="position; fixed">
                                                                @if ($data->jenis == 1)
                                                                    <img class="zoom" src="{{ 'https://dashboard.agen-entrepreneurid.com/app/public/bukti-transfer/'.$data->bukti_tf ?? '404.jpg' }}" alt="" height="50">
                                                                @elseif ($data->jenis == 2)
                                                                    <img class="zoom" src="{{ $data->event->link_form.'app/public/bukti-transfer/'.$data->bukti_tf ?? '404.jpg' }}" alt="" height="50">
                                                                @endif
                                                            </div>
                                                        </td>
                                                        <td>
                                                            {{ $data->keterangan }}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            @if ($data->event->produk->jenis == 'BUKU')
                                            <table class="table table-sm table-borderless" style="margin-bottom: 0rem">
                                                <thead style="font-weight: 400;">
                                                    <th>Lokasi Pengiriman</th>
                                                    <th>Kodepos</th>
                                                    <th>No. Resi</th>
                                                    <th>Kurir</th>
                                                    <th>Jumlah</th>
                                                    <th>Harga</th>
                                                    <th>Ongkir</th>
                                                    <th>Total</th>
                                                </thead>
                                                <tbody style="font-weight: 300;">
                                                    <tr>
                                                        <td>
                                                            Prov. {{ $data->book->provinsi ?? 'NO DATA' }}, {{ $data->book->kota ?? 'NO DATA' }}, Kec. {{ $data->book->kecamatan ?? 'NO DATA' }}
                                                        </td>
                                                        <td>
                                                            {{ $data->book->kodepos ?? 'NO DATA' }}
                                                        </td>
                                                        <td>
                                                            <form action="" method="get">
                                                                <input type="hidden" name="method" value="update-resi">
                                                                <input type="hidden" name="id" value="{{ $data->book->id ?? 'NO DATA' }}">
                                                                <input type="text" value="{{ $data->book->no_resi ?? 'NO DATA' }}" name="noresi">
                                                                <button class="btn btn-sm btn-warning" style="padding: 0.2rem 0.5rem;"><i class="fas fa-edit pr-2 pl-2"></i></button>
                                                            </form>
                                                        </td>
                                                        <td>
                                                            {{ $data->book->kurir ?? 'NO DATA' }}
                                                        </td>
                                                        <td>
                                                            {{ $data->book->jumlah_buku ?? 'NO DATA' }} Buku
                                                        </td>
                                                        <td>
                                                            Rp {{ strrev(implode('.',str_split(strrev(strval($data->book->harga ?? '0' )),3))) }}
                                                        </td>
                                                        <td>
                                                            Rp {{ strrev(implode('.',str_split(strrev(strval($data->book->ongkir ?? '0' )),3))) }}
                                                        </td>
                                                        <td>
                                                            Rp {{ strrev(implode('.',str_split(strrev(strval($data->book->total ?? '0' )),3))) }}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @if (!null == $data->agen)
                                    @if ( null != $data->rorder($data->id_event, $data->email, $data->nohp))
                                        <div class="col-12 text-muted pl-4 pt-2">
                                            <div class="row">
                                                <div class="col-6">
                                                    <table class="table table-sm table-borderless text-muted mb-0">
                                                        <tbody>
                                                            <tr>
                                                                <td> <i> Repeat Order </i></td>
                                                                <td>
                                                                    <table class="table table-sm table-borderless text-muted mb-0" >
                                                                        <tbody>
                                                                            @foreach ($data->rorder($data->id_event, $data->email, $data->nohp) as $item)
                                                                                @isset($item['nama_agen'])
                                                                                    <tr style="color: #af6e6e; font-size: 11px">
                                                                                        {{-- <td>{{ !null == $item['created_at'] ? \Carbon\Carbon::parse($item['created_at'])->format('Y-m-d') : '-' }} <strong class="float-right">{{ $item['idevent'] }}</strong></td> --}}
                                                                                        <td>{{ $item['created_at'] }} <strong class="float-right">{{ $item['idevent'] }}</strong></td>
                                                                                        <td>
                                                                                            <a href="{{ route('admin.transaksi.dataagen') }}?idagen={{ $item['idagen'] }}&produk={{ $item['id_event'] }}&nama={{ $item['email'] }}">
                                                                                                {{ $item['nama_agen'] }} - {{ $item['email_agen'] }}
                                                                                            </a>
                                                                                        </td>
                                                                                    </tr>
                                                                                @endisset
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    @php
                    $first  = $datatransaksi->firstItem();
                    $end    = $key + $datatransaksi->firstItem();
                    @endphp
                    @endforeach
                </div>
            </div>


            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                        {!! $first !!} - {!! $end !!} From {!! $datatransaksi->total() !!} Data
                    </div>
                </div><!--col-->

                <div class="col-5">
                    <div class="float-right">
                        {!! $datatransaksi->appends(request()->query())->links() !!}
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
    <script type="text/javascript">
        $(document).ready(function(){
            $("#status").val("{!! request()->status !!}");
            $("#jenis").val("{!! request()->jenis !!}");
            $("#produk").val("{!! request()->produk !!}");
            $("#roder").val("{!! request()->roder !!}");
            $("#perpage").val("{!! request()->perpage ?? 5 !!}");
        });
    </script>
@endif

@endsection
