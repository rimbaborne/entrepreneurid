@extends('backend.layouts.app')

@section('title', __('backend_produks.labels.management') . ' | ' . __('backend_produks.labels.create'))

@section('breadcrumb-links')
    @include('backend.transaksi.includes.breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.produks.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Produk
                        <br>
                        <small class="text-muted">Buat Produk Baru</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Nama
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="nama" class="form-control" placeholder="Nama Produk" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Inisial
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="inisial" class="form-control" placeholder="Inisial Produk" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Domain
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="domain" class="form-control" placeholder="Domain Produk" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 col-form-label">
                            Jenis
                        </label>
                        <div class="col-md-10">
                            <input type="text" name="jenis" class="form-control" placeholder="Jenis Produk" required>
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->

        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.produks.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
