@extends('backend.layouts.app')

@section('title', app_name() . ' | Transaksi')

@section('breadcrumb-links')
@include('backend.transaksi.includes.breadcrumb-links')
@endsection

@section('content')
<style>
    .table-sm-x td, .table-sm-x th {
        padding: 0rem;
    }
</style>
<div class="row">
    <div class="col-md-3">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col text-center">
                        <img class="card-img-top" src="{{ $agen->foto ? 'https://dashboard.agen-entrepreneurid.com/foto/'.$agen->foto : 'https://ui-avatars.com/api/?background=random&name='.$agen->name }}" alt="Profile Picture"
                            style="
                            object-fit: cover;
                            height: 200px;
                            width: 200px;
                            border-radius: 50%;
                            ">
                    </div>
                </div>
                <div class="row pt-4">
                    <div class="col">
                        <a href="#" target="_blank">
                            <strong>{{ $agen->name }}</strong>
                        </a>
                        <div class="small text-muted" style="text-transform: lowercase;">
                            {{ $agen->email }}
                        </div>
                        <table class="mt-2 table table-sm table-borderless text-muted table-sm-x" style="font-size: 11px">
                            <tbody>
                                <tr>
                                    <td> <i> Gender </i></td>
                                    <td>{{ $agen->gender }}</td>
                                </tr>
                                <tr>
                                    <td> <i> Tanggal Lahir </i></td>
                                    <td>{{ $agen->tgl_lahir }}</td>
                                </tr>
                                <tr>
                                    <td> <i> Domisili </i></td>
                                    <td>{{ $agen->kota }}</td>
                                </tr>
                                <tr>
                                    <td> <i> Terdaftar Sejak </i></td>
                                    <td>{{ $agen->created_at }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <table class="mt-2 table table-sm table-borderless text-muted table-sm-x" style="font-size: 11px">
                            <tbody>
                                <tr class="font-weight-bold">
                                    <td> EVENT</td>
                                    <td>{{ $event->id_event ?? 'Semua' }}</td>
                                </tr>
                                <tr class="text-dark">
                                    <td> TOTAL</td>
                                    <td>{{ $datasta['belum_aktif'] + $datasta['menunggu_konfirmasi'] + $datasta['aktif'] }}</td>
                                </tr>
                                <tr class="text-info">
                                    <td> AFILIASI</td>
                                    <td>{{ $datasta['afiliasi'] }}</td>
                                </tr>
                                <tr>
                                    <td> MANUAL</td>
                                    <td>{{ $datasta['manual'] }}</td>
                                </tr>
                                <tr class="text-danger">
                                    <td> BELUM UPLOAD</td>
                                    <td>{{ $datasta['belum_aktif'] }}</td>
                                </tr>
                                <tr class="text-warning">
                                    <td> KONFIRMASI</td>
                                    <td>{{ $datasta['menunggu_konfirmasi'] }}</td>
                                </tr>
                                <tr class="text-success">
                                    <td> AKTIF</td>
                                    <td>{{ $datasta['aktif'] }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h4 class="card-title mb-0">
                            List Data Transaksi Agen
                            @if ($event)
                                - <strong>{{ $event->produk->nama }} ({{ $event->id_event }})</strong>
                            @endif
                        </h4>
                    </div><!--col-->
                </div><!--row-->

                <form class="row mt-4" action="{{ url()->current() }}">
                    <input name="idagen" value="{{ request()->idagen }}" type="hidden">
                    <div class="col-12">
                        <div class="d-print-none row mt-4">
                            <div class="pb-2 col">
                                <div style="padding-top: 5px; padding-bottom: 5px">
                                    <label class="form-check-label">Nama / Email</label>
                                </div>
                                <input class="form-control" type="text" value="{{ request()->nama ?? '' }}" name="nama" style="font-size: small" />
                            </div>
                            <div class="pb-2 col">
                                <div  style="padding-top: 5px; padding-bottom: 5px">
                                    <label class="form-check-label">Produk</label>
                                </div>
                                <select id="produk" name="produk" class="form-control">
                                    <option value="">Semua</option>
                                    @foreach ($produklist as $data)
                                    <option value="{{ $data->id_event }}">{{ $data->event->produk->nama }} ({{ $data->event->id_event }})</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="pb-2 col">
                                <div  style="padding-top: 5px; padding-bottom: 5px">
                                    <label class="form-check-label">Status</label>
                                </div>
                                <select id="status" name="status" class="form-control">
                                    <option value="">Semua</option>
                                    <option value="1">Belum Aktif</option>
                                    <option value="2">Konfirmasi</option>
                                    <option value="3">Aktif</option>
                                </select>
                            </div>
                            <div class="pb-2 col">
                                <div  style="padding-top: 5px; padding-bottom: 5px">
                                    <label class="form-check-label">Jenis</label>
                                </div>
                                <select id="jenis" name="jenis" class="form-control">
                                    <option value="">Semua</option>
                                    <option value="1">Transaksi Manual</option>
                                    <option value="2">Transaksi Afiliasi</option>
                                </select>
                            </div>
                            <div class="pb-2 col">
                                <div  style="padding-top: 5px; padding-bottom: 5px">
                                    <label class="form-check-label">Data</label>
                                </div>
                                <select id="roder" name="roder" class="form-control">
                                    <option value="">Semua</option>
                                    <option value="1">Repeat Order</option>
                                    {{-- <option value="2">Non Repeat Order</option> --}}
                                </select>
                                <small class="text-muted">*Pilih Produk Dulu</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-2">
                        <div class="row">
                            <div class="col text-right">
                            </div>
                            <div class="col-4 text-right" style="padding-bottom:20px;">
                                <div class="align-middle">
                                    <button type="submit" class="btn btn-primary btn-sm btn-block btn-pill" > <i class="fas fa-search"></i> Cari</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="row kotak-atas" style="font-size: 11px; text-transform: uppercase; font-weight: 600">
                    <div class="col-3">
                        Nama
                    </div>
                    <div class="col-3">
                        Email
                    </div>
                    <div class="col-3">
                        Produk
                    </div>
                    <div class="col">
                        Jenis
                    </div>
                </div>

                <div class="row mt-4" style="margin-top: 0.35rem!important;">
                    <div class="col">
                        @php
                        $first  = 0;
                        $end    = 0;
                        @endphp
                        @foreach ($datatransaksi as $key => $data)
                        <div class="legend">
                            <div class="row kotak">
                                <div class="col-3">
                                    <a data-toggle="collapse" href="#detail{{ $key + $datatransaksi->firstItem() }}"
                                        aria-expanded="false" style="color: rgb(56, 56, 56);">
                                        <div><strong>{{ $data->nama }}</strong></div>
                                        <div class="small text-muted">
                                            @php
                                                $birth = !empty($data->tgllahir) ? $data->tgllahir : \Carbon\Carbon::now()->format('Y-m-d');
                                                $umur = !empty(\Carbon\Carbon::parse($birth)->age) ? \Carbon\Carbon::parse($birth)->age : '0';
                                            @endphp
                                                {{ $data->gender }} | {{ $umur ?? 0 }} Tahun
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3">
                                    {{ $data->email }}
                                    <div class="small text-muted">
                                        Daftar : {{ $data->created_at }}
                                    </div>
                                </div>
                                <div class="col-3">
                                    <a data-toggle="collapse" href="#detail{{ $key + $datatransaksi->firstItem() }}" aria-expanded="false" style="color: rgb(56, 56, 56);">
                                        <div style="font-size: 11px; text-transform: uppercase;">
                                            <strong>{{ $data->event->produk->nama }}</strong>
                                            <div class="text-muted">
                                                {{ $data->event->id_event }}
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col">
                                    <div class="row">
                                        <div class="col-3">
                                            <a data-toggle="collapse" href="#detail{{ $key + $datatransaksi->firstItem() }}" aria-expanded="false">
                                                <div class="text-muted position-absolute" style="font-size: 10px; text-transform: uppercase;">
                                                    @if ($data->jenis == 1)
                                                        Transaksi Manual
                                                    @elseif ($data->jenis == 2)
                                                        Transaksi Afiliasi
                                                    @endif
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-7 text-right">
                                            @include('backend.transaksi.includes.status')
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" style="color: #4e4e4e">
                                    <div class="col">
                                        <div class="collapse" id="detail{{ $key + $datatransaksi->firstItem() }}">
                                            <hr>
                                            <table class="table table-sm table-borderless" style="margin-bottom: 0rem">
                                                <thead style="font-weight: 400;">
                                                    <th width="100">Tgl. Lahir</th>
                                                    <th>No. Telp</th>
                                                    {{-- <th>Alamat</th>
                                                    <th>Kota</th> --}}
                                                    <th>Kode Unik</th>
                                                    <th>Bukti Transfer</th>
                                                    <th>Rek. Transfer</th>

                                                </thead>
                                                <tbody style="font-weight: 300;">
                                                    <tr>
                                                        <td>
                                                            {{ $data->tgllahir }}
                                                        </td>
                                                        <td>
                                                            +{{ $data->kode_nohp }}{{ $data->nohp }}
                                                        </td>
                                                        {{-- <td>
                                                            {{ $data->alamat }}
                                                        </td>
                                                        <td>
                                                            {{ $data->kota }}
                                                        </td> --}}
                                                        <td>
                                                            <strong>{{ $data->kodeunik }}</strong>
                                                        </td>
                                                        <td>
                                                            <div>
                                                                {{ $data->waktu_upload ?? '-' }}
                                                            </div>
                                                            <div style="position; fixed">
                                                                @if ($data->jenis == 1)
                                                                    <img class="zoom" src="{{ 'https://dashboard.agen-entrepreneurid.com/app/public/bukti-transfer/'.$data->bukti_tf ?? '404.jpg' }}" alt="" height="50">
                                                                @elseif ($data->jenis == 2)
                                                                    <img class="zoom" src="{{ $data->event->link_form.'app/public/bukti-transfer/'.$data->bukti_tf ?? '404.jpg' }}" alt="" height="50">
                                                                @endif
                                                            </div>
                                                        </td>
                                                        <td>
                                                            {{ $data->keterangan }}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            @if ($data->event->produk->jenis == 'BUKU')
                                            <table class="table table-sm table-borderless" style="margin-bottom: 0rem">
                                                <thead style="font-weight: 400;">
                                                    <th>Lokasi Pengiriman</th>
                                                    <th>Kodepos</th>
                                                    <th>No. Resi</th>
                                                    <th>Kurir</th>
                                                    <th>Jumlah</th>
                                                    <th>Harga</th>
                                                    <th>Ongkir</th>
                                                    <th>Total</th>
                                                </thead>
                                                <tbody style="font-weight: 300;">
                                                    <tr>
                                                        <td>
                                                            Prov. {{ $data->book->provinsi }}, {{ $data->book->kota }}, Kec. {{ $data->book->kecamatan }}
                                                        </td>
                                                        <td>
                                                            {{ $data->book->kodepos }}
                                                        </td>
                                                        <td>
                                                            <form action="" method="get">
                                                                <input type="hidden" name="method" value="update-resi">
                                                                <input type="hidden" name="id" value="{{ $data->book->id }}">
                                                                <input type="text" value="{{ $data->book->no_resi }}" name="noresi">
                                                                <button class="btn btn-sm btn-warning" style="padding: 0.2rem 0.5rem;"><i class="fas fa-edit pr-2 pl-2"></i></button>
                                                            </form>
                                                        </td>
                                                        <td>
                                                            {{ $data->book->kurir }}
                                                        </td>
                                                        <td>
                                                            {{ $data->book->jumlah_buku }} Buku
                                                        </td>
                                                        <td>
                                                            Rp {{ strrev(implode('.',str_split(strrev(strval($data->book->harga)),3))) }}
                                                        </td>
                                                        <td>
                                                            Rp {{ strrev(implode('.',str_split(strrev(strval($data->book->ongkir)),3))) }}
                                                        </td>
                                                        <td>
                                                            Rp {{ strrev(implode('.',str_split(strrev(strval($data->book->total)),3))) }}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @if ( null != $data->rorder($data->id_event, $data->email, $data->nohp))
                                    <div class="col-12 text-muted pl-4 pt-2">
                                        <div class="row">
                                            <div class="col-8">
                                                <table class="table table-sm table-borderless text-muted mb-0">
                                                    <tbody>
                                                        <tr>
                                                            <td> <i> Repeat Order </i></td>
                                                            <td>
                                                                <table class="table table-sm table-borderless text-muted mb-0" >
                                                                    <tbody>
                                                                        @foreach ($data->rorder($data->id_event, $data->email, $data->nohp) as $item)
                                                                        <tr style="color: #af6e6e; font-size: 11px">
                                                                            <td>{{ \Carbon\Carbon::parse($item['created_at'])->format('Y-m-d') }} <strong class="float-right">{{ $item['idevent'] }}</strong></td>
                                                                            <td>
                                                                                <a href="{{ route('admin.transaksi.dataagen') }}?idagen={{ $item['idagen'] }}&produk={{ $item['id_event'] }}&nama={{ $item['email'] }}">
                                                                                    {{ $item['nama_agen'] }} - {{ $item['email_agen'] }}
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        @php
                        $first  = $datatransaksi->firstItem();
                        $end    = $key + $datatransaksi->firstItem();
                        @endphp
                        @endforeach
                    </div>
                </div>

                <div class="row">
                    <div class="col-7">
                        <div class="float-left">
                            {!! $first !!} - {!! $end !!} From {!! $datatransaksi->total() !!} Data
                        </div>
                    </div><!--col-->

                    <div class="col-5">
                        <div class="float-right">
                            {!! $datatransaksi->appends(request()->query())->links() !!}
                        </div>
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->
        </div><!--card-->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#status").val("{!! request()->status !!}");
        $("#jenis").val("{!! request()->jenis !!}");
        $("#produk").val("{!! request()->produk !!}");
        $("#roder").val("{!! request()->roder !!}");
        $("#perpage").val("{!! request()->perpage ?? 5 !!}");
    });
</script>

@endsection
