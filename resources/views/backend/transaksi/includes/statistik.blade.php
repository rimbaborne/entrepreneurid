<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <h5 class="text-center">
                    Transaksi Dalam 10 Hari Terakhir
                </h5>
                <canvas class="chart" id="myChart" style="max-height: 350px"></canvas>
            </div>
            <div class="col-md-4">
                <div class="row pb-2">
                    <div class="col-md-12 text-center">
                        <h5>{{ $dataevent->produk->nama }}</h5>
                        <div class="text-muted text-uppercase font-weight-bold">
                            TOTAL : {{ $datasta['belum_aktif'] + $datasta['menunggu_konfirmasi'] + $datasta['aktif'] }}
                        </div>
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-6 col-sm-4" style="padding: 3px">
                        <div class="card border-info" style="box-shadow: none; margin-bottom: 2px">
                            <div class="text-center">
                                <div class="text-info text-uppercase font-weight-bold">AFILIASI</div>
                                <div class="text-value-xl py-2">{{ $datasta['afiliasi'] }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-4" style="padding: 3px">
                        <div class="card border-danger" style="box-shadow: none; margin-bottom: 2px">
                            <div class="text-center">
                                <div class="text-danger text-uppercase font-weight-bold">MANUAL</div>
                                <div class="text-value-xl py-2">{{ $datasta['manual'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4" style="padding: 3px">
                        <div class="card" style="box-shadow: none; margin-bottom: 2px">
                            <div class="text-center">
                                <div class="text-muted small text-uppercase font-weight-bold">BLM UPLOAD</div>
                                <div class="text-value-xl py-2">{{ $datasta['belum_aktif'] }}</div>
                            </div>
                            <div class="row text-center">
                                <div class="col">
                                    <div class="text-value-xl text-info font-weight-bold">{{ $datasta['belum_aktif_afiliasi'] }}</div>
                                </div>
                                <div class="c-vr"></div>
                                <div class="col">
                                    <div class="text-value-xl text-danger font-weight-bold">{{ $datasta['belum_aktif_manual'] }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4" style="padding: 3px" >
                        <div class="card" style="box-shadow: none; margin-bottom: 2px">
                            <div class="text-center">
                                <div class="text-muted small text-uppercase font-weight-bold">Confirm</div>
                                <div class="text-value-xl py-2">{{ $datasta['menunggu_konfirmasi'] }}</div>
                            </div>
                            <div class="row text-center">
                                <div class="col">
                                    <div class="text-value-xl text-info font-weight-bold">{{ $datasta['menunggu_konfirmasi_afiliasi'] }}</div>
                                </div>
                                <div class="c-vr"></div>
                                <div class="col">
                                    <div class="text-value-xl text-danger font-weight-bold">{{ $datasta['menunggu_konfirmasi_manual'] }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4" style="padding: 3px" >
                        <div class="card" style="box-shadow: none; margin-bottom: 2px">
                            <div class="text-center">
                                <div class="text-muted small text-uppercase font-weight-bold">Aktif</div>
                                <div class="text-value-xl py-2">{{ $datasta['aktif'] }}</div>
                            </div>
                            <div class="row text-center">
                                <div class="col">
                                    <div class="text-value-xl text-info font-weight-bold">{{ $datasta['aktif_afiliasi'] }}</div>
                                </div>
                                <div class="c-vr"></div>
                                <div class="col">
                                    <div class="text-value-xl text-danger font-weight-bold">{{ $datasta['aktif_manual'] }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-12 col-sm-12" style="padding: 3px">
                        <div class="card border-warning" style="box-shadow: none; margin-bottom: 2px">
                            <div class="text-center">
                                <div class="text-muted text-uppercase font-weight-bold">TOTAL AGEN - {{ $datasta['agen'] }}</div>
                                <div class="row pt-2">
                                    <div class="col-md-4 col-sm-4" style="padding: 0px" >
                                        <div class="text-muted text-uppercase" style="font-size: 8px;">
                                            Agen Closing Blm Upload
                                        </div>
                                        {{ $datasta['agen_closing_belum_aktif'] }}
                                        <div class="row text-center">
                                            <div class="col">
                                                <div class="text-value-xl text-info font-weight-bold">{{ $datasta['agen_belum_aktif_afiliasi'] }}</div>
                                            </div>
                                            <div class="c-vr"></div>
                                            <div class="col">
                                                <div class="text-value-xl text-danger font-weight-bold">{{ $datasta['agen_belum_aktif_manual'] }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4" style="padding: 0px" >
                                        <div class="text-muted text-uppercase" style="font-size: 8px;">
                                            Agen Closing Confirm
                                        </div>
                                        {{ $datasta['agen_closing_menunggu_konfirmasi'] }}
                                        <div class="row text-center">
                                            <div class="col">
                                                <div class="text-value-xl text-info font-weight-bold">{{ $datasta['agen_menunggu_konfirmasi_afiliasi'] }}</div>
                                            </div>
                                            <div class="c-vr"></div>
                                            <div class="col">
                                                <div class="text-value-xl text-danger font-weight-bold">{{ $datasta['agen_menunggu_konfirmasi_manual'] }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4" style="padding: 0px" >
                                        <div class="text-muted text-uppercase" style="font-size: 8px;">
                                            Agen Closing Aktif
                                        </div>
                                        {{ $datasta['agen_closing_aktif'] }}
                                        <div class="row text-center">
                                            <div class="col">
                                                <div class="text-value-xl text-info font-weight-bold">{{ $datasta['agen_aktif_afiliasi'] }}</div>
                                            </div>
                                            <div class="c-vr"></div>
                                            <div class="col">
                                                <div class="text-value-xl text-danger font-weight-bold">{{ $datasta['agen_aktif_manual'] }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    const ctx = document.getElementById('myChart').getContext('2d');
    const myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: {{ $tanggal_sta }},
            datasets: [{
                borderWidth: 1,
                data: {{ $total_tran }},
                label: "Total Transaksi",
                borderColor: "#3e95cd",
                fill: false,
            }, {
                borderWidth: 1,
                data: {{ $tran_akt }},
                label: "Transaksi Aktif",
                borderColor: "#cf1418",
                fill: false,
            }, {
                borderWidth: 1,
                data: {{ $tran_afi }},
                label: "Transaksi Afiliasi",
                borderColor: "#8e5ea2",
                fill: false,
            }, {
                borderWidth: 1,
                data: {{ $tran_man }},
                label: "Transaksi Manual",
                borderColor: "#3cba9f",
                fill: false,
            }, {
                borderWidth: 1,
                data: {{ $total_agen }},
                label: "Total Agen",
                borderColor: "#ffc107",
                fill: true,
                backgroundColor: '#ffc10730'
            }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Data Transaksi 10 Hari Terakhir'
            }
        }
    });
</script>
