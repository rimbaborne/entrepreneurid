{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/filepond/4.30.3/filepond.min.css" integrity="sha512-Zgp/CdUqOMnAY0ReSgoyZ2rk7CBVP0TqF+nTxDRo/mS0WEfQ+GOAaQDgHemhvd/C4rNrACYF/wyDqEYxhSN9dQ==" crossorigin="anonymous" referrerpolicy="no-referrer" /> --}}
<div class="card">
    <div class="card-body">
        <h4>Upload Bukti Transfer</h4>
        <form action="{{ route('admin.transaksi.updateuploadbuktitf') }}" method="POST" enctype="multipart/form-data">
            <div class="row">
                <div class="col-12">
                    @csrf
                    <input type="hidden" name="metode" value="update-upload">
                    <input type="hidden" name="id" value="{{ $edittransaksi->id }}">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Produk</label>
                        <div class="col-sm-9">
                            <input value="{{ $edittransaksi->event->id_event }} - {{ $edittransaksi->event->produk->nama }}" class="form-control" type="text" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Status</label>
                        <div class="col-sm-9">
                            <div class="col-form-label">
                                @include('backend.transaksi.includes.status')
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Agen</label>
                        <div class="col-sm-9">
                            <input value="{{ $edittransaksi->agen->name }} - {{ $edittransaksi->agen->email }}" class="form-control" type="text" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                            <input name="nama" value="{{ $edittransaksi->nama }}" class="form-control" type="text" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-9">
                            <input name="email" value="{{ $edittransaksi->email }}" class="form-control" type="text" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Panggilan</label>
                        <div class="col-sm-9">
                            <input name="panggilan" value="{{ $edittransaksi->panggilan }}" class="form-control" type="text" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nomor Handphone</label>
                        <div class="col-sm-9">
                            <input name="nohp" value="{{ $edittransaksi->nohp }}" class="form-control" type="text" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                        <div class="col-sm-9">
                            <input name="tgllahir" value="{{ $edittransaksi->tgllahir }}" class="form-control" type="text" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Gender</label>
                        <div class="col-sm-9">
                            <select id="gender" name="gender" class="form-control" disabled>
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Rekening Tujuan</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="rekening" name="rekening">
                                <option value="Mandiri - 1360007600528">Mandiri - 1360007600528 a/n Wulandari Tri</option>
                                <option value="BCA - 1911488766">BCA - 1911488766 a/n Wulandari Tri</option>
                                <option value="BRI - 012101009502532">BRI - 012101009502532 a/n Wulandari Tri</option>
                                <option value="BNI - 1238568920">BNI - 1238568920 a/n Wulandari Tri</option>
                            </select>
                        </div><!--col-->
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Bukti Transfer</label>
                        <div class="col-sm-9">
                            {{-- <input type="file" class="upload-buktitransfer"/> --}}
                            <input type="file" class="form-control" name="buktitf"/>
                        </div><!--col-->
                    </div>
                    <div class="form-group row">
                        <div class="col text-center">
                            @if ($edittransaksi->jenis == 1)
                                <img class="img img-fluid rounded" src="{{ 'https://dashboard.agen-entrepreneurid.com/app/public/bukti-transfer/'.$edittransaksi->bukti_tf ?? '404.jpg' }}" alt="" width="250">
                            @elseif ($edittransaksi->jenis == 2)
                                <img class="img img-fluid rounded" src="{{ $edittransaksi->event->link_form.'app/public/bukti-transfer/'.$edittransaksi->bukti_tf ?? '404.jpg' }}" alt="" width="250">
                            @endif
                        </div><!--col-->
                    </div>
                </div>
                <div class="col-12">
                    <div class="float-left">
                        <button type="button" class="btn btn btn-light" onclick="history.back();">Kembali</button>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Update <i class="fas fa-check"></i></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
          $("#gender").val("{!! $edittransaksi->gender !!}");
          $("#rekening").val("{!! $edittransaksi->keterangan !!}");
    });
</script>

<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
<script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.js"></script>
<script src="https://unpkg.com/filepond-plugin-image-resize/dist/filepond-plugin-image-resize.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/filepond/4.30.3/filepond.js" integrity="sha512-v6/Pv0rkeNFENOwv7dYFC0nidwx0omSRD2pEL+hCBZ3Zl0v6cRJwd2aC0SiHp2AkyzYPE1y8l6UVC3w/bIEXjA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>
<script>
    $(function(){
        $.fn.filepond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginFileValidateType,
            FilePondPluginFileValidateSize,
            FilePondPluginImageResize
        );
    });

    $(function(){
        $('.upload-buktitransfer').filepond({
            labelIdle: '<span class="filepond--label-action"> Upload Bukti Transfer Atau Perbaruhi Bukti Transfer.</span>',
            allowMultiple: false,
            acceptedFileTypes: "image/png, image/jpeg",
            allowFileSizeValidation: true,
            maxFileSize: '10MB',
            server: {
                url: '/admin/transaksi/upload',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: (response) => response.key,
                    onerror: (response) => response.data,
                    ondata: (formData) => {
                        return console.log('sukses');
                    }
                }
            }
        });

        $('.upload-buktitransfer').on('FilePond:processfile', function(e) {
            document.getElementById("btn-submit").classList.remove('d-none');
        });

    });
    $(document).ready(function () {
        $('#fform').on('submit',function(e) {
            if (pond.status != 4) {
                return false;
            }
            $(this).find(':input[type=submit]').hide();
            return true;
        });
    });

</script>
