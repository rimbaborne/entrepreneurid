<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/transaksi*')) }}" href="{{ route('admin.transaksi.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_transaksi.sidebar.title')
    </a>
</li>
