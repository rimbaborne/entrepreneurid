<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<div class="row">
    <div class="col-md-8 collapse hide" id="buattransaksi">
        <div class="card">
            <div class="card-body m-4">
                <form action="">
                    <div class="row">
                        <div class="col-12">
                            @csrf
                            <input type="hidden" name="metode" value="buat">
                            <input type="hidden" name="agen" value="buat">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Produk</label>
                                <div class="col-sm-9">
                                    <select name="produk" class="form-control">
                                        <option value="">Pilih...</option>
                                        @foreach ($produklist as $data)
                                        <option value="{{ $data->id_event }}">{{ $data->event->produk->nama }} ({{ $data->event->id_event }})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Agen</label>
                                <div class="col-sm-9">
                                    <select class="livesearch form-control p-3" name="ref" style="width: 100%"></select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Nama</label>
                                <div class="col-sm-9">
                                    <input name="nama" class="form-control" type="text" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Email</label>
                                <div class="col-sm-9">
                                    <input name="email" class="form-control" type="email" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Panggilan</label>
                                <div class="col-sm-9">
                                    <input name="panggilan" class="form-control" type="text" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Kode Nomor</label>
                                <div class="col-sm-9">
                                    <input name="kode_nohp" class="form-control" type="text" placeholder="cont: 62" value="62" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Nomor Handphone</label>
                                <div class="col-sm-9">
                                    <input name="nohp" class="form-control" type="text" placeholder="cont: 8123456789" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                <div class="col-sm-9">
                                    <input name="tgllahir" class="form-control" type="text" placeholder="(tgl-bln-thn) 28-12-1999" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Gender</label>
                                <div class="col-sm-9">
                                    <select name="gender" class="form-control">
                                        <option value="Laki-laki">Laki-laki</option>
                                        <option value="Perempuan">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-3 row">
                        <div class="col-12 text-right">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.livesearch').select2({
        placeholder: 'Cari Agen entrepreneurID',
        ajax: {
            url: '/admin/transaksi/agen',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name+' - '+item.email,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>
