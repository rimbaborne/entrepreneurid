<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<div class="modal fade" id="buattransaksi" tabindex="-1" role="dialog" aria-labelledby="buattransaksiLabel" aria-hidden="true">
    <form action="">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="buattransaksiLabel">Ganti Agen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            @csrf
                            <input type="hidden" name="metode" value="ganti-agen">
                            <input type="hidden" name="id" value="{{ request()->id }}">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Agen</label>
                                <div class="col-sm-9">
                                    <select class="livesearch form-control p-3" name="ref"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Update <i class="fas fa-plus-circle"></i></button>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $('.livesearch').select2({
        placeholder: 'Cari Agen entrepreneurID',
        ajax: {
            url: '/admin/transaksi/agen',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name+' - '+item.email,
                            id: item.id
                        }
                    })
                };
            },
            cache: true
        }
    });
</script>
