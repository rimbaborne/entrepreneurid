@if (request()->metode == 'edit' || request()->metode == 'upload')
    @if ($edittransaksi->jenis == 1)
        <button class="btn btn-outline-dark btn-sm">
            Transaksi Manual
        </button>
    @elseif ($edittransaksi->jenis == 2)
        <button class="btn btn-outline-dark btn-sm">
            Transaksi Afiliasi
        </button>
    @endif
    -
    @if($edittransaksi->status == 1)
        <button class="btn btn-danger btn-sm">
            <i class="fas fa-exclamation-triangle"></i> Belum Aktif
        </button>
    @elseif($edittransaksi->status == 2)
        <button class="btn btn-warning btn-sm">
            <i class="fas fa-edit"></i> Konfirmasi
        </button>
    @elseif($edittransaksi->status == 3)
        <button class="btn btn-info btn-sm">
            <i class="fas fa-check"></i> Aktif
        </button>
    @endif
@else
    @if($data->status == 1)
        <button class="btn btn-danger btn-sm">
            <i class="fas fa-exclamation-triangle"></i> Belum Aktif
        </button>
    @elseif($data->status == 2)
        <form action="">
            <input type="hidden" name="metode" value="konfirmasi">
            <input type="hidden" name="id" value="{{ $data->id }}">
            <button class="btn btn-warning btn-sm">
                <i class="fas fa-edit"></i> Konfirmasi
            </button>
        </form>
    @elseif($data->status == 3)
        <form action="">
            <input type="hidden" name="metode" value="konfirmasi">
            <input type="hidden" name="id" value="{{ $data->id }}">
            <button class="btn btn-info btn-sm">
                <i class="fas fa-check"></i> Aktif
            </button>
        </form>
    @endif
@endif
