<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<div class="collapse"  id="buattransaksi">
    <div class="card-body">
        <div class="row">
            <div class="col-12 ">
                <form action="">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="buattransaksiLabel">Ganti Agen</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        @csrf
                                        <input type="hidden" name="metode" value="ganti-agen">
                                        <input type="hidden" name="id" value="{{ request()->id }}">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Agen</label>
                                            <div class="col-sm-9">
                                                <select class="livesearch form-control p-3" name="ref" style="width: 100%"></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-toggle="collapse" href="#buattransaksi">Tutup</button>
                                <button type="submit" class="btn btn-primary">Update <i class="fas fa-plus-circle"></i></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <script type="text/javascript">
                $('.livesearch').select2({
                    placeholder: 'Cari Agen entrepreneurID',
                    ajax: {
                        url: '/admin/transaksi/agen',
                        dataType: 'json',
                        delay: 250,
                        processResults: function (data) {
                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.name+' - '+item.email,
                                        id: item.id
                                    }
                                })
                            };
                        },
                        cache: true
                    }
                });
            </script>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <h4>Edit Transaksi</h4>
        <form action="">
            <div class="row">
                <div class="col-12">
                    @csrf
                    <input type="hidden" name="metode" value="update">
                    <input type="hidden" name="id" value="{{ $edittransaksi->id }}">
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Produk</label>
                        <div class="col-sm-9">
                            <input value="{{ $edittransaksi->event->id_event }} - {{ $edittransaksi->event->produk->nama }}" class="form-control" type="text" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Status</label>
                        <div class="col-sm-9">
                            <div class="col-form-label">
                                @include('backend.transaksi.includes.status')
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Agen</label>
                        <label class="col-sm-9 col-form-label font-weight-bold">
                            {{ $edittransaksi->agen->name }} - {{ $edittransaksi->agen->email }}
                            <div class="float-right">
                                <label class="btn btn-sm btn-light" data-toggle="collapse" href="#buattransaksi">Ganti Agen</label>
                            </div>
                        </label>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nama</label>
                        <div class="col-sm-9">
                            <input name="nama" value="{{ $edittransaksi->nama }}" class="form-control" type="text" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Email</label>
                        <div class="col-sm-9">
                            <input name="email" value="{{ $edittransaksi->email }}" class="form-control" type="text" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Panggilan</label>
                        <div class="col-sm-9">
                            <input name="panggilan" value="{{ $edittransaksi->panggilan }}" class="form-control" type="text" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">kode Nomor</label>
                        <div class="col-sm-9">
                            <input name="kode_nohp" value="{{ $edittransaksi->kode_nohp }}" class="form-control" type="text" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Nomor Handphone</label>
                        <div class="col-sm-9">
                            <input name="nohp" value="{{ $edittransaksi->nohp }}" class="form-control" type="text" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Tanggal Lahir</label>
                        <div class="col-sm-9">
                            <input name="tgllahir" value="{{ $edittransaksi->tgllahir }}" class="form-control" type="text" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Gender</label>
                        <div class="col-sm-9">
                            <select id="gender" name="gender" class="form-control">
                                <option value="Laki-laki">Laki-laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    {{-- <div class="float-left">
                        <a href="/admin/transaksi" class="btn btn btn-light" onclick="history.back();">Kembali</button>
                    </div> --}}
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Update <i class="fas fa-check"></i></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
          $("#gender").val("{!! $edittransaksi->gender !!}");
          $("#rekening").val("{!! $edittransaksi->keterangan !!}");
    });
</script>
