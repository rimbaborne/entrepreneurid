<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <h5 class="text-center">
                    Statistik Transaksi Dalam 120 Hari
                </h5>
                <canvas class="chart" id="myChartAll" style="max-height: 350px"></canvas>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    const ctxa = document.getElementById('myChartAll').getContext('2d');
    const myCharta = new Chart(ctxa, {
        type: 'line',
        data: {
            labels: {{ $tanggal_sta_ }},
            datasets: [{
                borderWidth: 1,
                data: {{ $total_tran_ }},
                label: "Total Transaksi",
                borderColor: "#3e95cd",
                fill: false,
            }, {
                borderWidth: 1,
                data: {{ $tran_akt_ }},
                label: "Transaksi Aktif",
                borderColor: "#cf1418",
                fill: false,
            }, {
                borderWidth: 1,
                data: {{ $tran_afi_ }},
                label: "Transaksi Afiliasi",
                borderColor: "#8e5ea2",
                fill: false,
            }, {
                borderWidth: 1,
                data: {{ $tran_man_ }},
                label: "Transaksi Manual",
                borderColor: "#3cba9f",
                fill: false,
            }, {
                borderWidth: 1,
                data: {{ $total_agen_ }},
                label: "Total Agen",
                borderColor: "#ffc107",
                fill: true,
                backgroundColor: '#ffc10730'
            }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Data Dari Awal System Baru'
            }
        }
    });
</script>
