@extends('backend.layouts.app')

@section('title', app_name() . ' | Leaderboard')

@section('content')
<style>
    .zoom:hover {
            -ms-transform: scale(5); /* IE 9 */
            -webkit-transform: scale(5); /* Safari 3-8 */
            transform: scale(5);
            z-index: 999;
            margin-top: 50px;
            margin-bottom: 50px;
            margin-right: 50px;
            margin-left: 50px;
        }
    .carousel-control-next,
    .carousel-control-prev /*, .carousel-indicators */ {
        filter: invert(100%);
    }
</style>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-5">
                        <h4 class="card-title mb-0">
                            Leaderboard
                        </h4>
                    </div><!--col-->

                    <div class="col-sm-7 ">
                    </div><!--col-->
                </div><!--row-->

                <div class="row mt-4">
                    <div class="col">
                        <div class="table-sm">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Event</th>
                                    <th>Status Publish</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $first  = 0;
                                        $end    = 0;
                                    @endphp
                                    @foreach ($dataevent_ as $key => $data)
                                        <tr>
                                            <td class="align-middle">{{ $key + $dataevent_->firstItem() }}</td>
                                            <td class="align-middle">
                                                <div class="font-weight-bold">
                                                    {{ $data->produk->nama }}
                                                </div>
                                                <div class="text-muted">
                                                    {{ $data->id_event }}
                                                </div>
                                            </td>
                                            <td class="align-middle font-weight-bold">{{ $data->leaderboard == true ? 'Ditampilkan' : 'Tidak Ditampilkan' }}</td>
                                            <td>
                                                @if ($data->leaderboard == false)
                                                    <a class="btn btn-sm btn-primary" href="?id={{ $data->id }}&metode=update&status=1">
                                                        <i class="fas fa-eye"></i>
                                                        Tampilkan
                                                    </a>
                                                @else
                                                    <a class="btn btn-sm btn-danger" href="?id={{ $data->id }}&metode=update&status=0">
                                                        <i class="fas fa-eye-slash"></i>
                                                        Tidak Ditampilkan
                                                    </a>
                                                @endif

                                            </td>
                                        </tr>
                                        @php
                                            $first  = $dataevent_->firstItem();
                                            $end    = $key + $dataevent_->firstItem();
                                        @endphp
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col-7">
                        <div class="float-left">
                            {!! $first !!} - {!! $end !!} From {!! $dataevent_->total() !!} Data
                        </div>
                    </div><!--col-->

                    <div class="col-5">
                        <div class="float-right">
                            {!! $dataevent_->appends(request()->query())->links() !!}
                        </div>
                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->
        </div><!--card-->
    </div>
    <div class="col-md-6">
        <div class="card-2 " style="min-height: 500px">
            <div class="card-body">
                @if ($event)
                    <h3 class="card-title mb-4" style="font-weight:300; margin-bottom: .4rem;">
                        Leaderboard
                        <div class="font-weight-bold">
                            {{ $event->produk->nama }} <text class="text-muted"> {{ $event->id_event }} </text>
                        </div>
                    </h3>
                    <div class="card mb-2 p-1 " style="border-radius: 5px;">
                        <div class="row">
                            <div id="slide" class="col-12 carousel slide" data-ride="carousel" data-interval="false">
                                <div class="carousel-inner">
                                    @foreach ($dataevent as $key => $data)
                                        <div class="{{ $data->id != $event->id ? 'bg-light' : '' }} carousel-item {{ $event->id == $data->id ? 'active' : '' }}">
                                            <div class="row">
                                                <div class="col-5 pr-1 text-right">
                                                    <a href="#" target="_blank">
                                                        <img class="img-fluid" src="https://dashboard.agen-entrepreneurid.com/img/produk/{{ $data->gambar }}" alt="{{ $data->produk->nama }}" style="height: 150px">
                                                    </a>
                                                </div>
                                                <div class="col-7 pl-1 mt-4">
                                                    <h6>{{ data_get($data->produk, 'nama') }}</h6>
                                                    <h4 style="font-weight: 700"><span>Rp {{ strrev(implode('.',str_split(strrev(strval($data->harga)),3))) }}</span></h4>
                                                    <div class="" style="padding-bottom: 15px">
                                                        <small class="text-muted">
                                                            {{ \Carbon\Carbon::parse($data->start)->format('d M Y') }} - {{ \Carbon\Carbon::parse($data->end)->format('d M Y') }}
                                                        </small>
                                                        @if ($data->id != $event->id)
                                                            <div>
                                                                <a class="btn btn-outline-success btn-sm" href="/admin/events/leaderboard?event={{ $data->uuid }}">
                                                                    <i class="fas fa-eye"></i>
                                                                    Lihat Leaderboard
                                                                </a>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#slide" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" style="color:#222222" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#slide" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card mb-2 p-1" style="border-radius: 5px; font-size: 12px; overflow:scroll; height:520px;">
                        @php
                        $first  = 0;
                        $end    = 0;
                        @endphp
                        @foreach ($leaderboard as $key =>$data)
                            <div class="row p-2" style="
                                @if ($key + $leaderboard->firstItem() == 1)
                                    background: rgb(255,190,50);
                                    background: linear-gradient(0deg, rgba(255,190,50,0.1539609593837535) 0%, rgba(255,255,255,1) 80%);
                                @elseif ($key + $leaderboard->firstItem() == 2)
                                    background: rgb(122,122,122);
                                    background: linear-gradient(0deg, rgba(122,122,122,0.1539609593837535) 0%, rgba(255,255,255,1) 80%);
                                @elseif ($key + $leaderboard->firstItem() == 3)
                                    background: rgb(180,81,0);
                                    background: linear-gradient(0deg, rgba(180,81,0,0.1539609593837535) 0%, rgba(255,255,255,1) 80%);
                                @endif
                            ">
                                <div class="col ml-1 font-weight-bold">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td width="20">
                                                    {{ $key + $leaderboard->firstItem() }}
                                                </td>
                                                <td class="pl-2">
                                                    @if ($key + $leaderboard->firstItem() == 1)
                                                        <i class="fas fa-crown" style="color: #ffbe32"></i>
                                                    @elseif ($key + $leaderboard->firstItem() == 2)
                                                        <i class="fas fa-crown" style="color: #7a7a7a"></i>
                                                    @elseif ($key + $leaderboard->firstItem() == 3)
                                                        <i class="fas fa-crown" style="color: #b45100"></i>
                                                    @else
                                                        <i class="fas fa-crown" style="color: #ffffff"></i>
                                                    @endif
                                                </td>
                                                <td class="pl-3">
                                                    @if (!null == $data->agen)
                                                    <img class="zoom card-img-top" src="{{ $data->agen->foto ? 'https://dashboard.agen-entrepreneurid.com/foto/'.$data->agen->foto : 'https://ui-avatars.com/api/?background=random&name='.$data->agen->name }}" alt="Profile Picture" style="
                                                            object-fit: cover;
                                                            height: 30px;
                                                            width: 30px;
                                                            border-radius: 50%;
                                                            ">
                                                    @else
                                                    -
                                                    @endif
                                                </td>
                                                <td class="pl-3">

                                                    @if (!null == $data->agen)
                                                    <a href="{{ route('admin.transaksi.dataagen') }}?idagen={{ $data->id_agen }}&produk={{ $data->id_event }}" class="font-weight-bold justify-content-start" style="color: #ec1b25">
                                                        {{ \Illuminate\Support\Str::title($data->agen->name) }}
                                                    </a>
                                                    @else
                                                    -
                                                    @endif
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-3">
                                    <div class="float-right pt-2 justify-content-center" style="font-weight: 900;color: #222222;">
                                        {{ $data->jumlah }}
                                        <text class="font-weight-light" style="color: #7a7a7a; font-size: 9px">TRANSAKSI</text>
                                    </div>
                                </div>
                            </div>
                            @if ($key + $leaderboard->firstItem() >= 4)
                            <hr class="m-0 p-0">
                            @endif
                            @php
                            $first  = $leaderboard->firstItem();
                            $end    = $key + $leaderboard->firstItem();
                            @endphp
                        @endforeach
                    </div>
                @else
                    <h3 class="card-title mb-4" style="font-weight:300; margin-bottom: .4rem;">
                        Leaderboard
                        <div class="text-muted text-center mt-4" style="font-size: 11px">
                            Tidak ada data yang ditampilkan.
                        </div>
                    </h3>
                @endif
            </div>
        </div>
    </div>
</div>
<script>
    var lazyLoadInstance = new LazyLoad();
</script>
@endsection
