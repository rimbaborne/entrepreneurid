@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_posters.labels.management'))

@section('breadcrumb-links')
    @include('backend.poster.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_posters.labels.management') }} <small class="text-muted">{{ __('backend_posters.labels.deleted') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.poster.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('backend_posters.table.nama')</th>
                            <th>@lang('backend_posters.table.created')</th>
                            <th>@lang('backend_posters.table.deleted')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posters as $poster)
                            <tr>
                                <td class="align-middle"><a href="/admin/posters/{{ $poster->id }}">{{ $poster->nama }}</a></td>
                                <td class="align-middle">{!! $poster->created_at !!}</td>
                                <td class="align-middle">{{ $poster->deleted_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $poster->trashed_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $posters->count() !!} {{ trans_choice('backend_posters.table.total', $posters->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $posters->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
