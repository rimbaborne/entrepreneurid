@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_posters.labels.management'))

@section('breadcrumb-links')
    @include('backend.poster.includes.breadcrumb-links')
@endsection

@section('content')
<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.3.0/dist/lazyload.min.js"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script> --}}

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <form action="/admin/posters/store" method="post" enctype="multipart/form-data">
                            @csrf
                            {{-- <div class="form-group row">
                                <label class="col-md-4 form-control-label">Nama</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="nama" placeholder="Nama Poster" required="">
                                </div><!--col-->
                            </div><!--form-group--> --}}
                            <div class="form-group row">
                                <label class="col-md-4 form-control-label">Link Canva</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="text" name="link" placeholder="Link canva.me" required="">
                                </div><!--col-->
                            </div><!--form-group-->
                            <div class="form-group row">
                                <label class="col-md-4 form-control-label">File</label>
                                <div class="col-md-8">
                                    <input class="form-control" type="file" name="file" required="">
                                </div><!--col-->
                            </div><!--form-group-->
                            <div class="text-right">
                                <button class="btn btn-success pull-right" type="submit">Tambah</button>
                            </div><!--row-->
                        </form>

                    </div><!--col-->
                </div><!--row-->
            </div><!--card-body-->
        </div>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Data Poster - CNL
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                {{-- @include('backend.poster.includes.header-buttons') --}}
            </div><!--col-->
        </div><!--row-->


        <div class="row mt-4" data-masonry='{"percentPosition": true }'>

            @php
            $first  = 0;
            $end    = 0;
            @endphp
            @foreach($posters as  $key => $poster)
            <div class="col-xl-3 col-lg-4 col-md-4 col-6 mb-4">
                <div class="bg-white rounded shadow">
                    <a href="{{ $poster->link }}" target="_blank">
                        <img alt="CNL" class="img img-responsive full-width img-fluid card-img-top lazy" data-src="https://app.copywritingnextlevel.com/poster/{{ $poster->file }}" />
                    </a>
                    <div class="p-0 row">
                        <div class="pt-2 pl-4 col-6">
                            <h5>{{ $key + $posters->firstItem() }}</h5>
                        </div>
                        <div class="col-6 text-right">
                            <div class="btn-group btn-sm" role="group" aria-label="Action">
                                {{-- <a href="#" class="btn btn-success btn-sm" type="button"><i class="fas fa-edit"></i></a> --}}
                                <a href="/admin/posters/?status=hapus&id={{ $poster->id }}" class="btn btn-danger btn-sm" type="button"><i class="fas fa-trash"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @php
              $first  = $posters->firstItem();
              $end    = $key + $posters->firstItem();
            @endphp
            @endforeach
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $first !!} - {!! $end !!} From {!! $posters->total() !!} Data
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $posters->appends(request()->query())->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
<script>
    var lazyLoadInstance = new LazyLoad();
</script>
@endsection
