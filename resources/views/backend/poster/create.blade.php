@extends('backend.layouts.app')

@section('title', __('backend_posters.labels.management') . ' | ' . __('backend_posters.labels.create'))

@section('breadcrumb-links')
    @include('backend.poster.includes.breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.posters.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('backend_posters.labels.management')
                        <small class="text-muted">@lang('backend_posters.labels.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                    {{ html()->label(__('backend_posters.validation.attributes.nama'))->class('col-md-2 form-control-label')->for('nama') }}

                        <div class="col-md-10">
                            {{ html()->text('nama')
                                ->class('form-control')
                                ->placeholder(__('backend_posters.validation.attributes.nama'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.posters.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
