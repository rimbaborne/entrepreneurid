<!DOCTYPE html>
@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'entrepreneurID')">
    <meta name="author" content="@yield('meta_author', 'rimbaborne')">
    <link rel="shortcut icon" href="{{ asset('img/logo-official-agen.png') }}">
    @yield('meta')

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    {{ style('css/backend.css') }}
    {{ style('css/bootstrap-datepicker.css') }}
    {{-- {{ style('https://fonts.googleapis.com/css?family=Nunito&display=swap') }} --}}
    {{-- <link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet"> --}}
    {{ style('css/jquery.dataTables.min.css') }}
    {{ style('https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css') }}
    {{  style('https://cdn.datatables.net/fixedheader/3.1.7/css/fixedHeader.dataTables.min.css') }}

    {{ style('css/bootstrap-editable.css') }}
    {{-- {{ style('https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/css/select2.min.css') }} --}}

    {{-- {{ style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.css') }} --}}
    {{-- {{ style('https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css') }} --}}


    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@500&display=swap" rel="stylesheet">
    {{-- <link href="/css/bootstrap-datepicker.css" rel="stylesheet"> --}}
    <script src="https://cdn.tiny.cloud/1/5g7yssamd0271mz2hhe998gnkav4u9v2a52inq9hul2j7qa4/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>


    <style>
        @media (min-width: 992px) {
            .sidebar-fixed .sidebar {
                width: 250px;
            }

            html:not([dir=rtl]) .sidebar-lg-show.sidebar-fixed .app-footer, html:not([dir=rtl]) .sidebar-lg-show.sidebar-fixed .main, html:not([dir=rtl]) .sidebar-show.sidebar-fixed .app-footer, html:not([dir=rtl]) .sidebar-show.sidebar-fixed .main {
                margin-left: 250px;
            }
        }
        .sidebar .nav {
            width: 250px;
        }
        .sidebar .sidebar-nav, .sidebar .sidebar-scroll {
            width: 250px;
        }
        html:not([dir=rtl]) .sidebar {
            margin-left: -250px;
        }
        body {
            background-color: #F6F7FB;
        }
        .card {
            box-shadow: 0 5px 24px 0 rgba(50, 49, 58, .10);
            border-radius: 10px;
            padding: 10px;
            background-color: #fff;
        }
        .sidebar .nav-link {
            /* color: #29363d; */
        }
        .sidebar .nav-link.active {
            color: #fff;
            background: #5a6065;
        }

        .sidebar .nav-link:hover {
            color: #fff;
            background: #cf1418;
        }

        .sidebar .nav-link.active .nav-icon {
            color: #ff3433;
        }

        .sidebar .nav-link .nav-icon {
            font-size: 1rem;
        }


        .sidebar .nav-dropdown.open {
            /* background: #eee; */
        }
        tfoot input {
            width: 100%;
            padding: 3px;
            box-sizing: border-box;
        }
        .kotak {
            border: #e4e7e9 solid 1px;
            border-radius: 5px;
            padding: 10px;
            margin-bottom: 5px;
        }
        .kotak:hover {
            /* border: #cf1418 solid 1px; */
            /* background-color: #f7f7f7; */
            border: #cf1418 solid 1px;
        }

        .kotak-atas {
            padding: 10px;
            background-color: #e4e7e9;
            border-radius: 5px;
            margin-bottom: 4px;
        }

        .kotak-input {
            padding: 10px;
            border-radius: 5px;
            margin-bottom: 4px;
        }

        a {
            color: #cf1418;
        }
        a:hover {
            color: #555;
            text-decoration: none;
        }

        /* .legend .row:nth-of-type(odd) div {
            background:#999999;
        }
        .legend .row:nth-of-type(even) div {
            background: #FFFFFF; */
        }

        .tampilloading .swal2-popup {
            background: rgba(0, 0, 0, 0);
        }
        .frontend .custom-toggler .navbar-toggler {
            border-color: #f7f7f7;
        }

        .zoom {
            position: relative;
        }
        .zoom:hover {
            -ms-transform: scale(13); /* IE 9 */
            -webkit-transform: scale(13); /* Safari 3-8 */
            transform: scale(13);
            z-index: 999;
        }

        .borderless td, .borderless th {
            border: none;
        }
        .borderless thead th {
            border: none;
        }

        .table-kecil td, .table-kecil th {
            padding: .1rem;
            padding-top: 0.1rem;
            padding-right: 0.1rem;
            padding-bottom: 0.1rem;
            padding-left: 0.1rem;
        }
        </style>
    @livewireStyles

    @stack('after-styles')

    <!-- Scripts -->
    @stack('before-scripts')
    {!! script(mix('js/manifest.js')) !!}
    {!! script(mix('js/vendor.js')) !!}
    {!! script(mix('js/backend.js')) !!}

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/turbolinks/5.2.0/turbolinks.js" integrity="sha512-G3jAqT2eM4MMkLMyQR5YBhvN5/Da3IG6kqgYqU9zlIH4+2a+GuMdLb5Kpxy6ItMdCfgaKlo2XFhI0dHtMJjoRw==" crossorigin="anonymous"></script> --}}
    {{-- {!! script('https://cdnjs.cloudflare.com/ajax/libs/turbolinks/5.2.0/turbolinks.js') !!} --}}
    {!! script('js/bootstrap-editable.min.js') !!}
    {!! script('js/jquery.dataTables.min.js') !!}
    {!! script('js/bootstrap3-typeahead.min.js') !!}

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js" integrity="sha512-LsnSViqQyaXpD4mBBdRYeP6sRwJiJveh2ZIbW41EBrNmKxgr/LFZIiWT6yr+nycvhvauz8c2nYMhrP80YhG7Cw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}
    {{-- {!! script('https://cdnjs.cloudflare.com/ajax/libs/select2/4.1.0-beta.1/js/i18n/en.min.js') !!} --}}

    {{-- {!! script('https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js') !!} --}}

    {{-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.3.5/dist/alpine.min.js" defer></script> --}}

    {{-- {!! script('https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js') !!} --}}
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.6.2/dist/chart.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.0/jquery.min.js" integrity="sha512-ijLvQMMXgWAO85zfDbKeoqNR7015wrZI42XGYorITKkG0sVlP4t+Rt5Dl9EKDkrPxGrWZmVCUW5oIXkVOrnCiw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="/js/bootstrap-datepicker.js"></script>

    @stack('after-scripts')

    {{-- @notifyCss --}}
</head>

<body class="{{ config('backend.body_classes') }}">
    @include('sweet::alert')

    <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered justify-content-center" role="document">
            <span class="fa fa-spinner fa-spin fa-3x"></span>
        </div>
    </div>
    @include('backend.includes.header')

    <div class="app-body" style="overflow:visible">

        @include('backend.includes.sidebar')

        <main class="main">
            @include('includes.partials.demo')
            @include('includes.partials.logged-in-as')
            {!! Breadcrumbs::render() !!}


            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="content-header">
                        @yield('page-header')
                    </div><!--content-header-->

                    {{-- @include('sweet::alert') --}}
                    @include('includes.partials.messages')
                    @yield('content')
                </div><!--animated-->
            </div><!--container-fluid-->
        </main><!--main-->

        @include('backend.includes.aside')


    </div><!--app-body-->

    @include('backend.includes.footer')

    @livewireScripts
    {{-- @notifyJs --}}

    <!-- Scripts -->
    @stack('before-scripts')

        <script type="text/javascript">
            $(document).ready(function() {
                $('.searchproduk').select2();
                $('.searchuser').select2();
                $('.searchaktif').select2();
            });
        </script>

        <script type="text/javascript">
            window.livewire.on('updateresi', () => {
                $('#updateNoresi').modal('hide');
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.input-daterange .datepicker').each(function() {
                    $(this).datepicker({ format: 'yyyy-mm-dd' });
                });
                // $('.datepicker').datepicker({ format: 'yyyy-mm-dd' });
            });
        </script>

    @stack('after-scripts')
</body>
</html>
