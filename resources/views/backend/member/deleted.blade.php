@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_members.labels.management'))

@section('breadcrumb-links')
    @include('backend.member.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_members.labels.management') }} <small class="text-muted">{{ __('backend_members.labels.deleted') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.member.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('backend_members.table.produk')</th>
                            <th>@lang('backend_members.table.created')</th>
                            <th>@lang('backend_members.table.deleted')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($members as $member)
                            <tr>
                                <td class="align-middle"><a href="/admin/members/{{ $member->id }}">{{ $member->produk }}</a></td>
                                <td class="align-middle">{!! $member->created_at !!}</td>
                                <td class="align-middle">{{ $member->deleted_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $member->trashed_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $members->count() !!} {{ trans_choice('backend_members.table.total', $members->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $members->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
