@extends('backend.layouts.app')

@section('title', app_name() . ' | MIC' )

@section('breadcrumb-links')
@include('backend.agen.includes.breadcrumb-links')
@endsection

{{-- @stack('before-styles')
{!! style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css') !!}
@stack('after-styles') --}}

@section('content')
<div class='notifications top-right'></div>
<div class="card">
    <div class="row pb-2">
        <div class="col-md-12">
            <h5>Peserta Member Area MIC</h5>
            <div class="text-muted text-uppercase font-weight-bold">
                TOTAL : {{ $peserta->total() }}
            </div>
        </div>
    </div>
</div>

{{-- @livewire('agen') --}}
<div class="card">
    <form action="" class="row pb-2">
        <div class="col-md-5"></div>
        <div class="col-md-2">
            <label class="form-label">Status Advisor</label>
            <select style="font-weight: 700;" class="form-control" name="status" onchange='if(this.value != 0) { this.form.submit(); }'>
                @isset(request()->status)
                    <option value="">{{ request()->status }}</option>
                    <option value="">-----</option>
                @endisset
                <option value="Semua">Semua</option>
                <option value="Menunggu Koreksi">Menunggu Koreksi</option>
                <option value="Sudah Dibalas">Sudah Dibalas</option>
            </select>
        </div>
        <div class="col-md-4 pull-right">
            <label class="form-label">Cari Nama / Email</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-search"></i> </span>
                </div>
                <input value="{{ request()->get('cari') ?? '' }}" name="cari" class="form-control" type="text" placeholder="Cari Nama / E-mail "
                autocomplete="password">
            </div>
        </div>
    </form>
    <div class="row">
        <div class="col-12">
            <table class="table table-sm table-responsive-sm table-hover table-outline mb-0" style="width:100%; font-size: 13px;">
                <thead class="thead-light">
                    <tr>
                        <th class="text-center">No</th>
                        <th>Nama</th>
                        <th class="text-center">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $first  = 0;
                    $end    = 0;
                    @endphp
                    @foreach ($peserta as $key => $mic)
                    <tr>
                        <td class="text-center">
                            {{ $key + $peserta->firstItem() }}
                        </td>
                        <td>
                            <a href="{{ route('admin.members.mic.advisor') }}?token={{ $mic->password }}&sesi={{ $mic->id }}">
                                <div style="font-weight: 600">
                                    {{ $mic->nama ?? '' }}
                                </div>
                                <div>
                                    {{ $mic->email ?? '' }}
                                </div>
                            </a>
                        </td>
                        <td class="text-center">
                            @if ($mic->status_advisor == 'Menunggu Koreksi')
                                <label class="btn btn-warning btn-sm">
                                    {{ $mic->status_advisor ?? '' }}
                                </label>
                            @elseif ($mic->status_advisor == 'Sudah Dibalas')
                                <label class="btn btn-info btn-sm">
                                    {{ $mic->status_advisor ?? '' }}
                                </label>
                            @else
                                    Belum Konsultasi
                            @endif
                        </td>
                    </tr>
                    @php
                    $first  = $peserta->firstItem();
                    $end    = $key + $peserta->firstItem();
                    @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div><!--card-->
<div class="card">
    {{-- <div class="card-body"> --}}
        <div class="row">
            <div class="col-7">
                {!! $first !!} - {!! $end !!} From {!! $peserta->total() !!} Data
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $peserta->appends(request()->query())->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    {{-- </div> --}}
</div>
@endsection
