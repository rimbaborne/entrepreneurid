@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_members.labels.management'))

@section('breadcrumb-links')
@include('backend.member.includes.breadcrumb-links')
@endsection

@section('content')
@livewire('member-koc-praktikum')
@endsection
