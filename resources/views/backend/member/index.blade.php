@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_members.labels.management'))

@section('breadcrumb-links')
@include('backend.member.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h4>Member Area Produk</h4>
    </div><!--card-body-->
</div><!--card-->
<div class="row">
    <div id="respon" class="col-4">
        <div class="card order-card" style="padding: 25px;">
            <div class="card-block">
                <div class="">
                    <div class="text-center" style="padding-bottom: 10px">
                        <img class="img-fluid" src="/img/produk/KOC.png" alt="Mentoring Organic Marketing" style="height: 200px">
                    </div>
                    <h4 class="text-center">Kelas Online Copywriting</h4>
                    <a class="btn btn-block btn-info d-flex justify-content-between align-items-center" href="{{ route('admin.members.koc') }}">
                        <span class="small font-weight-bold">Batch 1</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <a class="btn btn-success btn-block d-flex justify-content-between align-items-center" href="{{ route('admin.members.koc2') }}">
                        <span class="small font-weight-bold">Batch 2</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="respon" class="col-4">
        <div class="card order-card" style="padding: 25px;">
            <div class="card-block">
                <div class="">
                    <div class="text-center" style="padding-bottom: 10px">
                        <img class="img-fluid" src="/img/produk/MIC.png" alt="Mentoring Organic Marketing" style="height: 200px">
                    </div>
                    <h4 class="text-center">Mentoring Instant Copywriting</h4>
                    <a class="btn btn-block btn-warning d-flex justify-content-between align-items-center" href="{{ route('admin.members.mic') }}">
                        <span class="small font-weight-bold">Copywriting Advisor</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <a class="btn btn-block btn-info d-flex justify-content-between align-items-center" href="{{ route('admin.posters.index') }}">
                        <span class="small font-weight-bold">Poster</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@stack('before-scripts')
<script>
    function resize(x) {
        var element = document.querySelectorAll("[id='respon']");;
        for(var i = 0; i < element.length; i++)
            if (x.matches) { // If media query matches
                element[i].classList.remove("col-4");
                element[i].classList.add("col-6");
            } else {
                element[i].classList.remove("col-6");
                element[i].classList.add("col-4");
            }
    }

    var x = window.matchMedia("(max-width: 1000px)")
    resize(x) // Call listener function at run time
    x.addListener(resize) // Attach listener function on state changes
</script>
@stack('after-stacks')
@endsection
