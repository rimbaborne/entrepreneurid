<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_members.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.members.index') }}">@lang('backend_members.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.members.create') }}">@lang('backend_members.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.members.deactivated') }}">@lang('backed_members.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.members.deleted') }}">@lang('backend_members.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
