<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/members*')) }}" href="{{ route('admin.members.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_members.sidebar.title')
    </a>
</li>