<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/agens*')) }}" href="{{ route('admin.agens.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_agens.sidebar.title')
    </a>
</li>