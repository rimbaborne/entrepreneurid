<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_agens.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.agens.index') }}">@lang('backend_agens.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.agens.create') }}">@lang('backend_agens.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.agens.deactivated') }}">@lang('backed_agens.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.agens.deleted') }}">@lang('backend_agens.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
