@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_agens.labels.management'))

@section('breadcrumb-links')
    @include('backend.agen.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_agens.labels.management') }} <small class="text-muted">{{ __('backend_agens.labels.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.agen.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('backend_agens.table.id_agen')</th>
                            <th>@lang('backend_agens.table.created')</th>
                            <th>@lang('backend_agens.table.last_updated')</th>
                            <th>@lang('backend_agens.table.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($agens as $agen)
                            <tr>
                                <td class="align-middle"><a href="/admin/agens/{{ $agen->id }}">{{ $agen->id_agen }}</a></td>
                                <td class="align-middle">{!! $agen->created_at !!}</td>
                                <td class="align-middle">{{ $agen->updated_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $agen->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $agens->count() !!} {{ trans_choice('backend_agens.table.total', $agens->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $agens->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
