@extends('backend.layouts.app')

@section('title', app_name() . ' | Data Ecourse' )

@section('breadcrumb-links')
@include('backend.agen.includes.breadcrumb-links')
@endsection

{{-- @stack('before-styles')
{!! style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css') !!}
@stack('after-styles') --}}

@section('content')
<div class='notifications top-right'></div>
<div class="card">
    <div class="row pb-2">
        <div class="col-md-12">
            <div class=" badge badge-success">Aktif</div>
        </div>
        <div class="col-md-12">
            <h5>Agen Resmi entrepreneurID</h5>
            <div class="text-muted text-uppercase font-weight-bold">
                TOTAL : {{ $dataagen->total() }}
            </div>
            <div class="float-right">
                <a href="{{ route('admin.agens.export') }}" target="_blank" class="btn btn-success"><i class="fas fa-file-excel"></i> Download Data</a>
            </div>
        </div>
    </div>
</div>

{{-- @livewire('agen') --}}
<div class="card">
    <div class="row pb-2">
        <div class="col-md-7"></div>
        <div class="col-md-5 pull-right">
            <form action="">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-search"></i> </span>
                    </div>
                    <input value="{{ request()->get('cari') ?? '' }}" name="cari" class="form-control" type="text" placeholder="Cari Nama / E-mail / Rekening / Kontak"
                    autocomplete="password">
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-sm table-responsive-sm table-hover table-outline mb-0" style="width:100%; font-size: 12px;">
                <thead class="thead-light">
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Kontak</th>
                        <th>Rekening</th>
                        <th>Domisili</th>
                        <th>Terdaftar</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $first  = 0;
                    $end    = 0;
                    @endphp
                    @foreach ($dataagen as $key => $agen)
                    <tr>
                        <td>
                            {{ $key + $dataagen->firstItem() }}
                        </td>
                        <td>
                            <div style="font-weight: 600">
                                <a href="/admin/data-ecourse/create?ref={{ $agen->id }}" target="_blank">
                                    {{ $agen->nama ?? '' }}
                                </a>
                            </div>
                            <div>
                                {{ $agen->gender ?? '' }} | {{ $agen->tgllahir ?? '' }}
                            </div>
                        </td>
                        <td>
                            {{ $agen->email ?? '' }}
                            <div>
                                {{ $agen->no_telp ?? '' }}
                            </div>
                        </td>
                        <td>
                            {{ $agen->nama_rek ?? '' }}
                            <div>
                                {{ $agen->bank_rek ?? '' }} - {{ $agen->rek_rek ?? '' }}
                            </div>
                        </td>
                        <td>
                            {{ $agen->kota ?? '' }}
                        </td>
                        <td>
                            {{ \Carbon\Carbon::parse($agen->tgl_daftar)->isoFormat('DD MMMM YYYY') ?? '' }}
                        </td>
                    </tr>
                    @php
                    $first  = $dataagen->firstItem();
                    $end    = $key + $dataagen->firstItem();
                    @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div><!--card-->
<div class="card">
    {{-- <div class="card-body"> --}}
        <div class="row">
            <div class="col-7">
                {!! $first !!} - {!! $end !!} From {!! $dataagen->total() !!} Data
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $dataagen->appends(request()->query())->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    {{-- </div> --}}
</div>

{{-- @stack('before-scripts')
{!! script('https://cdn.jsdelivr.net/npm/chart.js@2.8.0') !!}
{!! script('//cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js') !!}
{!! script('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') !!}
{!! script('https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js') !!}
{!! script('https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js') !!}
<script>
    $(document).ready( function () {

        var $table = $('#dataagen').DataTable({
            "scrollX": true,
            "dom": 'Blfrtip',
            "buttons": [
            { extend: 'excel', text: 'Download excel', messageTop: 'Agen eID' }
            ]
        });
    } );
</script>
@stack('after-scripts') --}}
@endsection
