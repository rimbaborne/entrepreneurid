@extends('backend.layouts.app')

@section('title', app_name() . ' | Data Ecourse' )

@section('breadcrumb-links')
@include('backend.agen.includes.breadcrumb-links')
@endsection

@section('content')

<div class="card col-6">
    <div class="card-body">
        <form action="" class="row mt-4 mb-4">
            <div class="col-12">
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">
                        Nama Agen
                    </label>
                    <div class="col-md-10">
                        <input type="text" name="nama" class="form-control"  value="{{ $agen->name }}" readonly>
                    </div><!--col-->
                </div><!--form-group-->
            </div><!--col-->
            <div class="col-12">
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">
                        Email
                    </label>
                    <div class="col-md-10">
                        <input type="text" name="inisial" class="form-control"value="{{ $agen->email }}" readonly>
                    </div><!--col-->
                </div><!--form-group-->
            </div><!--col-->
            <div class="col-12">
                <div class="form-group row">
                    <label class="col-md-2 col-form-label">
                        Password
                    </label>
                    <div class="col-md-10">
                        <input type="text" name="password" class="form-control" placeholder="Password Baru" required>
                    </div><!--col-->
                </div><!--form-group-->
            </div><!--col-->
            <div class="col-12">
                <div class="form-group row">
                    <div class="col-12">
                        <input type="hidden" name="id" value="{{ $agen->id }}">
                        <input type="hidden" name="metode" value="update">

                        <button class="btn btn-primary btn-sm">Update</button>
                    </div>
                </div><!--form-group-->
            </div><!--col-->
        </form>
    </div>

</div>
@endsection
