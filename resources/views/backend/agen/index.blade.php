@extends('backend.layouts.app')

@section('title', app_name() . ' | Data Ecourse' )

@section('breadcrumb-links')
@include('backend.agen.includes.breadcrumb-links')
@endsection

{{-- @stack('before-styles')
{!! style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css') !!}
@stack('after-styles') --}}

@section('content')
<div class='notifications top-right'></div>
<div class="card">
    <div class="row pb-2">
        <div class="col-md-12">
            <div class=" badge badge-success">Aktif</div>
        </div>
        <div class="col-md-12">
            <h5>Agen Resmi entrepreneurID</h5>
            <div class="text-muted text-uppercase font-weight-bold">
                TOTAL : {{ $dataagen->total() }}
            </div>
            <div class="float-right">
                <a href="{{ route('admin.agens.agenbaru') }}" target="_blank" class="btn btn-success"><i class="fas fa-file-excel"></i> Download Data</a>
            </div>
        </div>
    </div>
</div>

{{-- @livewire('agen') --}}
<div class="card">
    <div class="row pb-2">
        <div class="col-md-7"></div>
        <div class="col-md-5 pull-right">
            <form action="">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"> <i class="fa fa-search"></i> </span>
                    </div>
                    <input value="{{ request()->get('cari') ?? '' }}" name="cari" class="form-control" type="text" placeholder="Cari Nama / E-mail / Rekening / Kontak"
                    autocomplete="password">
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card mb-2 bg-gray-500 text-white" style="border-radius: 5px; font-size: 12px;">
                <div class="row p-2 font-weight-bold">
                    <div class="col">Nama</div>
                    <div class="col">Kontak</div>
                    <div class="col">Rekening</div>
                    <div class="col">Domisili</div>
                    <div class="col">Status</div>
                </div>
            </div>

            @php
            $first  = 0;
            $end    = 0;
            @endphp

            @foreach($dataagen as $key => $agen)
                <div>
                    <div style="text-decoration: none;">
                        <div class="card mb-2" style="border-radius: 5px; font-size: 12px; box-shadow: 0 0px">
                            <div class="row p-2 align-items-center">
                                <div class="col">
                                    <div class="font-weight-bold" style="color: #222222">
                                        {{ $agen->name ?? '' }}
                                        <div class="text-muted" style="color: #222222">
                                            {{ $agen->gender ?? '' }} | {{ $agen->tgl_lahir ?? '' }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="font-weight-bold" style="color: #ec1b25 ">
                                        {{ $agen->email ?? '' }}
                                        <div class="text-muted" style="color: #222222">
                                            +{{ $agen->kode_notelp ?? '62' }}{{ $agen->notelp ?? '' }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="font-weight-bold" style="color: #ec1b25 ">
                                        {{ $agen->nama_rek ?? '' }}
                                        <div class="text-muted" style="color: #222222">
                                            {{ $agen->bank_rek ?? '' }} - {{ $agen->no_rek ?? '' }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="font-weight-bold" style="color: #ec1b25 ">
                                        {{ $agen->kota ?? '' }}
                                        <div class="text-muted" style="color: #222222">
                                            <div>
                                                @if ($agen->status_aktif == true)
                                                    <span class="badge badge-success" style="float: right;">AKTIF</span>
                                                @else
                                                    <span class="badge badge-danger" style="float: right;">NON AKTIF</span>
                                                @endif
                                            </div>
                                            {{ $agen->created_at }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="text-right">
                                        <div class="btn-group dropleft">
                                            <a data-toggle="collapse" href="#detail{{ $agen->id }}" aria-expanded="false" class="btn btn-sm btn-warning">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <button class="btn btn-sm btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-expanded="false">
                                                Menu
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a href="#" title="Hapus" data-method="delete" data-trans-button-cancel="Batal" data-trans-button-confirm="Hapus"
                                                data-trans-title="{{ $agen->email }} dihapus?" class=" dropdown-item" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">
                                                    <form action=""  onsubmit="return confirm('Apakah Anda yakin data {{ $agen->email }} dihapus ?');" style="display:none">
                                                        <input type="hidden" name="metode" value="hapus">
                                                        <input type="hidden" name="id" value="{{ $agen->id }}">
                                                    </form>
                                                    <strong style="color: #ec1b25 ">Hapus</strong>
                                                </a>
                                                <a class="dropdown-item" href="{{ route('admin.agens.password') }}?id={{ $agen->id }}" target="_blank" rel="noopener noreferrer">Password</a>
                                                <form action="">
                                                    <input type="hidden" name="id" value="{{ $agen->id }}">
                                                    <input type="hidden" name="aktif" value="mode">
                                                    @if ($agen->status_aktif == false)
                                                        <input type="hidden" name="status" value="1">
                                                        <button class="dropdown-item btn btn-primary btn-sm">Aktifkan</button>
                                                    @else
                                                        <input type="hidden" name="status" value="0">
                                                        <button class="dropdown-item btn btn-warning btn-sm">Non Aktifkan</button>
                                                    @endif
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="color: #4e4e4e">
                            <div class="col-12">
                                <div class="col-12">
                                    <div class="collapse" id="detail{{ $agen->id }}">
                                        <hr>
                                        <form class="row" action="">
                                            <input type="hidden" name="id" value="{{ $agen->id }}">
                                            <input type="hidden" name="metode" value="edit">
                                            <div class="col">
                                                <div class="mb-3">
                                                    <label class="form-label">Nama</label>
                                                    <input type="text" class="form-control" name="nama" value="{{ $agen->name }}">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Gender</label>
                                                    <input type="text" class="form-control" name="gender" value="{{ $agen->gender ?? '' }}">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Tanggal Lahir</label>
                                                    <input type="text" class="form-control" name="tgllahir" value="{{ $agen->tgl_lahir ?? '' }}">
                                                </div>
                                                <div class="mb-3">
                                                    <button type="submit" class="btn btn-sm btn-primary">Update</button>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="mb-3">
                                                    <label class="form-label">Email</label>
                                                    <input type="text" class="form-control" name="email" value="{{ $agen->email }}">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Kode No. HP</label>
                                                    <input type="text" class="form-control" name="kodenotelp" value="{{ $agen->kode_notelp ?? '62'  }}">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">No. HP</label>
                                                    <input type="text" class="form-control" name="notelp" value="{{ $agen->notelp }}">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="mb-3">
                                                    <label class="form-label">Nama Rek</label>
                                                    <input type="text" class="form-control" name="namarek" value="{{ $agen->nama_rek }}">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">Bank Rek</label>
                                                    <input type="text" class="form-control" name="bankrek" value="{{ $agen->bank_rek }}">
                                                </div>
                                                <div class="mb-3">
                                                    <label class="form-label">No Rek</label>
                                                    <input type="text" class="form-control" name="norek" value="{{ $agen->no_rek }}">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="mb-3">
                                                    <label class="form-label">Kota</label>
                                                    <input type="text" class="form-control" name="kota" value="{{ $agen->kota }}">
                                                </div>
                                                <div class="float-right mb-3">
                                                    <button type="button" class="btn btn-outline-dark btn-sm" data-toggle="modal" data-target="#password{{ $agen->id }}">
                                                        Ganti Password
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="password{{ $agen->id }}" tabindex="-1" role="dialog" aria-labelledby="password{{ $agen->id }}Label" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <form action="" class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="password{{ $agen->id }}Label">Ganti Password</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-12">
                                                @csrf
                                                <input type="hidden" name="id" value="{{ $agen->id }}">
                                                <input type="hidden" name="metode" value="password">
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Nama</label>
                                                    <div class="col-sm-9">
                                                        <input value="{{ $agen->name }}" class="form-control" type="text" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Password</label>
                                                    <div class="col-sm-9">
                                                        <input name="password" class="form-control" type="text" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @php
                $first  = $dataagen->firstItem();
                $end    = $key + $dataagen->firstItem();
                @endphp
            @endforeach
        </div>
    </div>
</div><!--card-->
<div class="card">
    {{-- <div class="card-body"> --}}
        <div class="row">
            <div class="col-7">
                {!! $first !!} - {!! $end !!} From {!! $dataagen->total() !!} Data
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $dataagen->appends(request()->query())->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    {{-- </div> --}}
</div>

{{-- @stack('before-scripts')
{!! script('https://cdn.jsdelivr.net/npm/chart.js@2.8.0') !!}
{!! script('//cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js') !!}
{!! script('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') !!}
{!! script('https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js') !!}
{!! script('https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js') !!}
<script>
    $(document).ready( function () {

        var $table = $('#dataagen').DataTable({
            "scrollX": true,
            "dom": 'Blfrtip',
            "buttons": [
            { extend: 'excel', text: 'Download excel', messageTop: 'Agen eID' }
            ]
        });
    } );
</script>
@stack('after-scripts') --}}
@endsection
