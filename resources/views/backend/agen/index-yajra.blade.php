@extends('backend.layouts.app')

@section('title', app_name() . ' | Data Ecourse' )

@section('breadcrumb-links')
@include('backend.agen.includes.breadcrumb-links')
@endsection

{{-- @stack('before-styles')
{!! style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css') !!}
@stack('after-styles') --}}

@section('content')
<div class='notifications top-right'></div>
<div class="card">
    <div class="row pb-2">
        <div class="col-md-12">
            <div class=" badge badge-success">Event Aktif</div>
        </div>
        <div class="col-md-12">
            <h5>Agen Resmi entrepreneurID</h5>
            <div class="text-muted text-uppercase font-weight-bold">
            </div>
        </div>
    </div>
</div>

<div class="card" style="min-width: 800px;">
    <div class="row">
        <div class="col-12">
            <table id="datay" class="table table-sm table-responsive-sm table-hover table-outline mb-0 text-center" style="width:100%; font-size: 11px;">
                <thead class="thead-light">
                    <tr>
                        <th>Nama Agen</th>
                        {{-- <th>Email Agen</th>
                        <th>Nama Customer</th>
                        <th>Email Customer</th>
                        <th>Telpon Customer</th>
                        <th>Gender</th>
                        <th>Kode Unik</th>
                        <th>Jenis</th>
                        <th>Status</th>
                        <th>Waktu Upload</th>
                        <th>Waktu Konfirmasi</th> --}}
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div><!--card-->
<div class="card">
    {{-- <div class="card-body"> --}}
        <div class="row">
            <div class="col-7">
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                </div>
            </div><!--col-->
        </div><!--row-->
    {{-- </div> --}}
</div>

@stack('before-scripts')
{!! script('https://cdn.jsdelivr.net/npm/chart.js@2.8.0') !!}
{!! script('//cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js') !!}
{!! script('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js') !!}
{!! script('https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js') !!}
{!! script('https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js') !!}
{{-- {!! script('https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js') !!} --}}
<script>
    $(document).ready( function () {

        var $table = $('#datay').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.agens.yajra.data') }}",
            columns: [
                        {data: 'nama', name: 'biodata.nama'},
                     ],
            "dom": 'Blfrtip',
            search: true,
            "buttons": [
                        { extend: 'excel', text: 'Download excel', messageTop: 'Agen eID' }
                    ],
        });
    } );
</script>
{{-- {!! script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js') !!} --}}
@stack('after-scripts')
@endsection
