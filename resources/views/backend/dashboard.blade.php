@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="card">
    <div class="card-body">
        Selamat Datang di Dashboard Admin
    </div>
</div>

@include('backend.transaksi.includes.statistik-2')

{{-- <div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <canvas class="chart" id="myChart"></canvas>
            </div>
            <div class="col-md-4">
                <div class="row pb-2">
                    <div class="col-md-12 text-center">
                        <h5>{{ $dataevent->produk->nama }}</h5>
                        <div class="text-muted text-uppercase font-weight-bold">
                            TOTAL : {{ $data['belum_aktif'] + $data['menunggu_konfirmasi'] + $data['aktif'] }}
                        </div>
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-6 col-sm-4" style="padding: 3px">
                        <div class="card border-info" style="box-shadow: none; margin-bottom: 2px">
                            <div class="text-center">
                                <div class="text-info text-uppercase font-weight-bold">AFILIASI</div>
                                <div class="text-value-xl py-2">{{ $data['afiliasi'] }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-4" style="padding: 3px">
                        <div class="card border-danger" style="box-shadow: none; margin-bottom: 2px">
                            <div class="text-center">
                                <div class="text-danger text-uppercase font-weight-bold">MANUAL</div>
                                <div class="text-value-xl py-2">{{ $data['manual'] }}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4" style="padding: 3px">
                        <div class="card" style="box-shadow: none; margin-bottom: 2px">
                            <div class="text-center">
                                <div class="text-muted small text-uppercase font-weight-bold">BLM UPLOAD</div>
                                <div class="text-value-xl py-2">{{ $data['belum_aktif'] }}</div>
                            </div>
                            <div class="row text-center">
                                <div class="col">
                                    <div class="text-value-xl text-info font-weight-bold">{{ $data['belum_aktif_afiliasi'] }}</div>
                                </div>
                                <div class="c-vr"></div>
                                <div class="col">
                                    <div class="text-value-xl text-danger font-weight-bold">{{ $data['belum_aktif_manual'] }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4" style="padding: 3px" >
                        <div class="card" style="box-shadow: none; margin-bottom: 2px">
                            <div class="text-center">
                                <div class="text-muted small text-uppercase font-weight-bold">Confirm</div>
                                <div class="text-value-xl py-2">{{ $data['menunggu_konfirmasi'] }}</div>
                            </div>
                            <div class="row text-center">
                                <div class="col">
                                    <div class="text-value-xl text-info font-weight-bold">{{ $data['menunggu_konfirmasi_afiliasi'] }}</div>
                                </div>
                                <div class="c-vr"></div>
                                <div class="col">
                                    <div class="text-value-xl text-danger font-weight-bold">{{ $data['menunggu_konfirmasi_manual'] }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4" style="padding: 3px" >
                        <div class="card" style="box-shadow: none; margin-bottom: 2px">
                            <div class="text-center">
                                <div class="text-muted small text-uppercase font-weight-bold">Aktif</div>
                                <div class="text-value-xl py-2">{{ $data['aktif'] }}</div>
                            </div>
                            <div class="row text-center">
                                <div class="col">
                                    <div class="text-value-xl text-info font-weight-bold">{{ $data['aktif_afiliasi'] }}</div>
                                </div>
                                <div class="c-vr"></div>
                                <div class="col">
                                    <div class="text-value-xl text-danger font-weight-bold">{{ $data['aktif_manual'] }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-12 col-sm-12" style="padding: 3px">
                        <div class="card border-warning" style="box-shadow: none; margin-bottom: 2px">
                            <div class="text-center">
                                <div class="text-muted text-uppercase font-weight-bold">TOTAL AGEN - {{ $data['agen'] }}</div>
                                <div class="row pt-2">
                                    <div class="col-md-4 col-sm-4" style="padding: 0px" >
                                        <div class="text-muted text-uppercase" style="font-size: 8px;">
                                            Agen Closing Blm Upload
                                        </div>
                                        {{ $data['agen_closing_belum_aktif'] }}
                                        <div class="row text-center">
                                            <div class="col">
                                                <div class="text-value-xl text-info font-weight-bold">{{ $data['agen_belum_aktif_afiliasi'] }}</div>
                                            </div>
                                            <div class="c-vr"></div>
                                            <div class="col">
                                                <div class="text-value-xl text-danger font-weight-bold">{{ $data['agen_belum_aktif_manual'] }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4" style="padding: 0px" >
                                        <div class="text-muted text-uppercase" style="font-size: 8px;">
                                            Agen Closing Confirm
                                        </div>
                                        {{ $data['agen_closing_menunggu_konfirmasi'] }}
                                        <div class="row text-center">
                                            <div class="col">
                                                <div class="text-value-xl text-info font-weight-bold">{{ $data['agen_menunggu_konfirmasi_afiliasi'] }}</div>
                                            </div>
                                            <div class="c-vr"></div>
                                            <div class="col">
                                                <div class="text-value-xl text-danger font-weight-bold">{{ $data['agen_menunggu_konfirmasi_manual'] }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4" style="padding: 0px" >
                                        <div class="text-muted text-uppercase" style="font-size: 8px;">
                                            Agen Closing Aktif
                                        </div>
                                        {{ $data['agen_closing_aktif'] }}
                                        <div class="row text-center">
                                            <div class="col">
                                                <div class="text-value-xl text-info font-weight-bold">{{ $data['agen_aktif_afiliasi'] }}</div>
                                            </div>
                                            <div class="c-vr"></div>
                                            <div class="col">
                                                <div class="text-value-xl text-danger font-weight-bold">{{ $data['agen_aktif_manual'] }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    const ctx = document.getElementById('myChart').getContext('2d');
    const myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
            datasets: [{
                data: [86,114,106,106,107,111,133,221,783,2478],
                label: "Africa",
                borderColor: "#3e95cd",
                fill: false,
                tension: 0.2
            }, {
                data: [282,350,411,502,635,809,947,1402,3700,3267],
                label: "Asia",
                borderColor: "#8e5ea2",
                fill: false,
                tension: 0.2
            }, {
                data: [168,170,178,190,203,276,408,547,675,734],
                label: "Europe",
                borderColor: "#3cba9f",
                fill: false,
                tension: 0.2
            }, {
                data: [40,20,10,16,24,38,74,167,508,784],
                label: "Latin America",
                borderColor: "#e8c3b9",
                fill: false,
                tension: 0.2
            }, {
                data: [6,3,2,2,7,26,82,172,312,433],
                label: "North America",
                borderColor: "#c45850",
                fill: false,
                tension: 0.2
            }
            ]
        },
        options: {
            title: {
            display: true,
            text: 'World population per region (in millions)'
            }
        }
    });
</script> --}}
@endsection
