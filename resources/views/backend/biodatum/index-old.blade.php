@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_biodata.labels.management'))

@section('breadcrumb-links')
    @include('backend.biodatum.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_biodata.labels.management') }} <small class="text-muted">{{ __('backend_biodata.labels.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.biodatum.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('backend_biodata.table.nama')</th>
                            <th>@lang('backend_biodata.table.created')</th>
                            <th>@lang('backend_biodata.table.last_updated')</th>
                            <th>@lang('backend_biodata.table.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($biodata as $biodatum)
                            <tr>
                                <td class="align-middle"><a href="/admin/biodata/{{ $biodatum->id }}">{{ $biodatum->nama }}</a></td>
                                <td class="align-middle">{!! $biodatum->created_at !!}</td>
                                <td class="align-middle">{{ $biodatum->updated_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $biodatum->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $biodata->count() !!} {{ trans_choice('backend_biodata.table.total', $biodata->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $biodata->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
