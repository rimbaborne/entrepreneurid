@extends('backend.layouts.app')

@section('title', app_name() . ' | CNL - Resi' )

@section('breadcrumb-links')
    @include('backend.biodatum.includes.breadcrumb-links')
@endsection

@section('content')
@if(session()->has('notif-berhasil'))
<div class="alert alert-success">
    {{ session('notif-berhasil') }}
    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
@elseif(session()->has('notif-gagal'))
<div class="alert alert-danger">
    {{ session('notif-gagal') }}
    <button class="close" type="button" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
@endif
<div class="card">
    <div class="card-body">
        <div class="row">
            <h5 class="col">Copywriting Next Level - Resi</h5>
        </div>
        <div class="row mb-4">
            <div class="col-md-1">
                <select class="form-control" name="perPage">
                    <option>5</option>
                    <option>10</option>
                    <option>25</option>
                    <option>50</option>
                    <option>100</option>
                </select>
            </div>

            <div class="col text-center">
            </div>

            <div class="col-md-2 mr-2">
                <form class="row" action="">
                    @if(!empty(Request::get('nama')))
                        <input name="nama" value="{{ Request::get('nama') }}" hidden>
                    @endif
                    @if(!empty(Request::get('page')))
                        <input name="page" value="{{ Request::get('page') }}" hidden>
                    @endif
                    <select id="kurir" class="form-control" name="kurir" onchange='if(this.value != 0) { this.form.submit(); }'>
                        <option value="">-- Pilih Kurir --</option>
                        <option value="all">Semua</option>
                        <option value="pos">POS</option>
                        <option value="jne">JNE</option>
                        <option value="tiki">TIKI</option>
                        <option value="j&t">J&T</option>
                    </select>
                </form>
            </div>

            <div class="col-md-4">
                <form class="row" action="">
                    @if(!empty(Request::get('kurir')))
                    <input name="kurir" value="{{ Request::get('kurir') }}" hidden>
                    @endif
                    @if(!empty(Request::get('page')))
                        <input name="page" value="{{ Request::get('page') }}" hidden>
                    @endif
                    <input name="nama" class="col-md-8 form-control" type="text" placeholder="Nama" value="{{ request()->get('nama') }}">
                    <button type="submit" class="col-md-4 btn btn-primary">
                        Cari <i class="fa fa-search"></i>
                    </button>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <table class="table table-sm table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Pemesan</th>
                            <th></th>
                            <th>Banyak</th>
                            <th>No. Resi</th>
                            <th>Input</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $first  = 0;
                            $end    = 0;
                        @endphp
                        @foreach ($data as $key => $cnl)
                            <tr>
                                <td>{{ $key+ $data->firstItem() }}</td>
                                <td>
                                    {{ $cnl->nama }}
                                    <div class="text-muted">
                                        {{ $cnl->no_telp }}
                                    </div>
                                </td>
                                <td>
                                    {{ $cnl->email }}
                                    <div class="text-muted">
                                        {{ $cnl->provinsi }}, {{ $cnl->kota }}
                                    </div>
                                </td>
                                <td>
                                    {{ $cnl->jumlah }} Buku
                                    <div class="text-muted">
                                        Ongkir Rp. {{ $cnl->ongkir }}
                                    </div>
                                </td>
                                <td class="text-uppercase">
                                    {{ $cnl->noresi ?? '-' }}
                                    <div class="text-muted">
                                        {{ $cnl->kurir }}
                                    </div>
                                </td>
                                <td>
                                    <form action="">
                                        @if(!empty(Request::get('kurir')))
                                        <input name="kurir" value="{{ Request::get('kurir') }}" hidden>
                                        @endif
                                        @if(!empty(Request::get('page')))
                                            <input name="page" value="{{ Request::get('page') }}" hidden>
                                        @endif
                                        <input type="hidden" name="id" value="{{ $cnl->id }}">
                                        <input type="hidden" name="proses" value="resi">
                                        <input type="text" name="noresi" placeholder="No. Resi" class="form-control">
                                        <button type="submit" class="btn btn-sm btn-block btn-primary">Submit</button>
                                    </form>
                                </td>
                            </tr>
                            @php
                                $first  = $data->firstItem();
                                $end    = $key + $data->firstItem();
                            @endphp
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <div class="row" style="padding-top:10px">
            <div class="col-7">
                <div class="float-left" style="padding-left:10px">
                    {!! $first !!} - {!! $end !!} From {!! $data->total() !!} Data
                </div>
            </div>
            <div class="col-5">
                <div class="float-right" style="padding-right:10px">
                    {!! $data->appends(request()->query())->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@stack('before-scripts')
<script type="text/javascript">
    $(document).ready(function(){
          $("#kurir").val("{!! request()->get('kurir') !!}");
    });
  </script>
@stack('after-scripts')
@endsection
