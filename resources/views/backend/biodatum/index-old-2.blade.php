@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_biodata.labels.management'))

@section('breadcrumb-links')
    @include('backend.biodatum.includes.breadcrumb-links')
@endsection

@section('content')
<div class='notifications top-right'></div>
@include('notify::messages')

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Biodata <small class="text-muted">Semua Produk</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.biodatum.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4" style="padding-left: 10px;">
            <div class="col">
                <form method="POST" id="filter-form" class="form-inline" role="form">
                    <div class="form-group col">
                        <label class="col-md-3 col-form-label" >Status User</label>
                        <div class="col-md-9">
                            <select class="form-control" name="statususer">
                                <option>Pilih...</option>
                                <option value="0">Affiliasi</option>
                                <option value="2">Manual</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col">
                        <label class="col-md-3 col-form-label" >Status</label>
                        <div class="col-md-9">
                            <select class="form-control" name="produk">
                                <option>Pilih...</option>
                                <option value="1">Copywriting Next Level</option>
                                <option value="6">Copywriting Next Level</option>
                                <option value="4">Pendaftaran Agen</option>
                                <option value="9">Peta Bisnis Online</option>
                                <option value="10">Instant Copywriting</option>
                                <option value="21">Copywriting Next Level</option>
                                <option value="11">Mentoring Sales Funnel</option>
                                <option value="12">Kelas Online Copywriting</option>
                                <option value="15">Kelas Online Copywriting</option>
                                <option value="17">WhatsApp Master Closing</option>
                                <option value="19">Pendaftaran Agen Resmi</option>
                                <option value="5">Kelas Facebook Hacking</option>
                                <option value="3">Reseller Formula</option>
                                <option value="18">WhatsApp Master Closing</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col">
                        <label class="col-md-3 col-form-label" >Status Aktif</label>
                        <div class="col-md-9">
                            <select class="form-control" name="konfirmasi">
                                <option>Pilih...</option>
                                <option value="0">Belum Aktif</option>
                                <option value="1">Menunggu Konfirmasi</option>
                                <option value="2">Aktif</option>
                            </select>
                        </div>
                    </div>
                    {{-- <div class="form-group col">
                        <label class="col-md-2 col-form-label">statususer</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="statususer">
                        </div>
                    </div>
                    <div class="form-group col">
                        <label class="col-md-2 col-form-label">produk</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="produk">
                        </div>
                    </div>
                    <div class="form-group col">
                        <label class="col-md-2 col-form-label">Konfirmasi</label>
                        <div class="col-md-10">
                            <input type="text" class="form-control" name="konfirmasi">
                        </div>
                    </div> --}}
                </form>
            </div>
        </div>


        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive" style="overflow-x:auto;">
                    <table id="tabel_biodata" class="table table-responsive-sm table-hover mb-0 table-sm table-bordered display nowrap"  style="width:100%">
                        <thead>
                            <tr>
                                {{-- <th width="5"><button type="button" name="bulk_delete" id="bulk_delete" class="btn btn-danger btn-sm" title="Hapus Masal"><i class="fas fa-trash"></i></button></th> --}}
                                <th class="text-center">Id </th>
                                <th class="text-center">Nama </th>
                                <th class="text-center">Email </th>
                                <th class="text-center">Gender </th>
                                <th class="text-center">No Telp </th>
                                <th class="text-center">Pendaftaran </th>
                                <th class="text-center">Ref </th>
                                <th class="text-center">Status Aktif </th>
                                {{-- <th class="text-center">Produk </th>
                                <th class="text-center">Status User </th> --}}
                                {{-- <th width="60">Aksi</th> --}}
                            </tr>
                        </thead>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->

<div class="card" style="min-width: 800px;">
    @livewire('biodata')
</div><!--card-->

@include('backend.biodatum.js.index-js')
@endsection
