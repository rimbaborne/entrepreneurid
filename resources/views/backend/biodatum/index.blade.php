@extends('backend.layouts.app')

@section('title', app_name() . ' | Data Ecourse' )

@section('breadcrumb-links')
    @include('backend.biodatum.includes.breadcrumb-links')
@endsection

{{-- @stack('before-styles')
{!! style('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css') !!}
@stack('after-styles') --}}

@section('content')
<div class='notifications top-right'></div>

{{-- <form action="/admin/data-ecourse/teswa" method="post">
    @csrf
<button type="submit">teswa</button>
</form> --}}

<div class="card" style="min-width: 800px;">
    @livewire('biodata')
</div><!--card-->

@stack('before-scripts')
{!! script('https://cdn.jsdelivr.net/npm/chart.js@2.8.0') !!}
{{-- {!! script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js') !!} --}}
@stack('after-scripts')
@endsection
