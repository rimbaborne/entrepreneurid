@extends('backend.layouts.app')

@section('title', __('backend_biodata.labels.management') . ' | ' . __('backend_biodata.labels.create'))

@section('breadcrumb-links')
    @include('backend.biodatum.includes.breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.biodata.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('backend_biodata.labels.management')
                        <small class="text-muted">@lang('backend_biodata.labels.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 form-control-label" >Produk</label>
                        <div class="col-md-10">
                            <select name="produk" class="searchproduk form-control" name="produk">
                                <option value="39">Pendaftaran Agen Baru</option>
                                <option value="35">Mentoring Super Reseller</option>
                                <option value="34">Kelas Facebook Hacking</option>
                                <option value="18">WhatsApp Master Closing</option>
                                <option value="11">Mentoring Sales Funnel</option>
                                <option value="33">Mentoring Instant Copywriting</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 form-control-label" >Agen</label>
                        <label class="col-md-10 form-control-label" >{{ $agen->nama }} - {{ $agen->email }}</label>
                        <input type="hidden" name="ref" value="{{ $agen->id }}">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 form-control-label" >Nama</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="nama"  placeholder="nama">
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 form-control-label" >Panggilan</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="panggilan"  placeholder="panggilan">
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 form-control-label" >Email</label>
                        <div class="col-md-10">
                            <input class="form-control" type="email" name="email"  placeholder="email">
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 form-control-label" >No Telepon</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="no_telp"  placeholder="notelp">
                        </div>
                    </div>
                </div>
                {{-- <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 form-control-label" >Alamat</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="alamat"  placeholder="alamat">
                        </div>
                    </div>
                </div> --}}
                <div class="col-12">
                    <div class="form-group row">
                        <label class="col-md-2 form-control-label" >Domisili</label>
                        <div class="col-md-10">
                            <input class="form-control" type="text" name="kota"  placeholder="kota">
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.biodata.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
