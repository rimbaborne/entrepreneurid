
@stack('before-scripts')
{!! script('js/jszip.min.js') !!}
{!! script('js/dataTables.buttons.min.js') !!}
{!! script('js/buttons.html5.min.js') !!}
{!! script('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.js') !!}

<script type="text/javascript">
    //transaksi data laravel menggunakan token
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //Inisialisasi datatable & pengambilan data dari api menggunakan ajax
    let biodata_ = $('#tabel_biodata').DataTable({
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: "{{ route('admin.biodata.getdata') }}?produk={{ request('produk') }}",
            data: function (d) {
                d.statususer= $('[name=statususer]').val();
                d.produk = $('[name=produk]').val();
                d.konfirmasi = $('[name=konfirmasi]').val();
            }
        },
        'columnDefs': [
            {
            'targets': 7,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('data-pk', rowData['id']).attr('data-name', 'aktif').attr('data-type', 'text').attr('class', 'updatedata');
                }
            },
        ],
        "columns":[
            { "data": "id"},
            { "data": "nama" },
            { "data": "email" },
            { "data": "gender" },
            { "data": "no_telp" },
            { "data": "tgl_daftar" },
            { "data": "ref" },
            { "data": "aktif" },
            { "data": "idprodukreg" , visible : false},
            { "data": "status_user" , visible : false},
        ],
        "drawCallback": function( settings ) {
            var api = this.api();

            $('.updatedata', api.table().body()).editable({
                url: "{{route('admin.biodata.updatedata')}}",
                mode : 'inline',
            });


        },
        dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'excel',
                    text: 'Export Data Excel',
                    messageTop: 'Data entrepreneurID 2020.'
                }
            ],
            exportOptions : {
                modifier : {
                    // DataTables core
                    order : 'index', // 'current', 'applied',
                    //'index', 'original'
                    page : 'all', // 'all', 'current'
                    search : 'none' // 'none', 'applied', 'removed'
                },
                columns: [ 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ]
            }
    });

    $('#filter-form').on('change', function(e) {
        biodata_.draw();
        e.preventDefault();
    });

    //Hapus data satuan
    $(document).on('click', '.delete', function(){
        var id = $(this).attr('id');
        if(confirm("Apakah anda yakin data "+ id +" akan dihapus ?"))
        {
            $.ajax({
                url:"{{route('admin.biodata.hapusdata')}}",
                mehtod:"get",
                data:{id:id},
                success:function(data)
                {
                    alert(data);
                    $('#tabel_biodata').DataTable().ajax.reload();
                }
            })
        }
        else
        {
            return false;
        }
    });

    //Hapus Banyak Data secara bersamaan
    $(document).on('click', '#bulk_delete', function(){
        var id = [];
        if(confirm("Apakah anda yakin pilihan data akan dihapus ?"))
        {
            $('.biodata_checkbox:checked').each(function(){
                id.push($(this).val());
            });
            if(id.length > 0)
            {
                $.ajax({
                    url:"{{route('admin.biodata.hapusdatamasal')}}",
                    method:"get",
                    data:{id:id},
                    success:function(data)
                    {
                        alert(data);
                        $('#tabel_biodata').DataTable().ajax.reload();
                    }
                });
            }
            else
            {
                alert("Silahkan pilih lebih dari 1 data !");
            }
        }
    });


</script>

<script type="text/javascript">

    @if(Session::has('success'))
       $('.top-right').notify({
          message: { text: "{{ Session::get('success') }}" }
        }).show();
       @php
         Session::forget('success');
       @endphp
    @endif


    @if(Session::has('info'))
        $('.top-right').notify({
          message: { text: "{{ Session::get('info') }}" },
          type:'info'
        }).show();
        @php
          Session::forget('info');
        @endphp
    @endif


    @if(Session::has('warning'))
            $('.top-right').notify({
          message: { text: "{{ Session::get('warning') }}" },
          type:'warning'
        }).show();
        @php
          Session::forget('warning');
        @endphp
    @endif


    @if(Session::has('error'))
            $('.top-right').notify({
          message: { text: "{{ Session::get('error') }}" },
          type:'danger'
        }).show();
        @php
          Session::forget('error');
        @endphp
    @endif

  </script>

@stack('after-scripts')
