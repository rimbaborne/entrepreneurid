@stack('before-scripts')
{!! script('js/jszip.min.js') !!}
{!! script('js/dataTables.buttons.min.js') !!}
{!! script('js/buttons.html5.min.js') !!}

<script type="text/javascript">
    //transaksi data laravel menggunakan token
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //Inisialisasi datatable & pengambilan data dari api menggunakan ajax
    let biodata_ = $('#tabel_biodata').DataTable({
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            type: 'GET',
            url: "{{ route('admin.biodata.getdata') }}",
            data: function (d) {
                    d.name = $('input[name=nama]').val();
                }
        },
        'columnDefs': [
            {
            'targets': 0,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('class', 'text-center');
                }
            },
            {
            'targets': 1,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('data-pk', rowData['id']).attr('data-name', 'nama').attr('data-type', 'text').attr('class', 'updatedata');
                }
            },
            {
            'targets': 2,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('data-pk', rowData['id']).attr('data-name', 'email').attr('data-type', 'text').attr('class', 'updatedata');
                }
            },
            {
            'targets': 3,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('data-pk', rowData['id']).attr('data-name', 'no_telp').attr('data-type', 'text').attr('class', 'updatedata');
                }
            },
            {
            'targets': 4,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('data-pk', rowData['id']).attr('data-name', 'alamat').attr('data-type', 'text').attr('class', 'updatedata');
                }
            },
            {
            'targets': 5,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('data-pk', rowData['id']).attr('data-name', 'kota').attr('data-type', 'text').attr('class', 'updatedata');
                }
            },
            {
            'targets': 6,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('data-pk', rowData['id']).attr('data-name', 'status_user').attr('data-type', 'text').attr('class', 'updatedata');
                }
            },
            {
            'targets': 7,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('data-pk', rowData['id']).attr('data-name', 'ref').attr('data-type', 'text').attr('class', 'updatedata');
                }
            },
            {
            'targets': 8,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('data-pk', rowData['id']).attr('data-name', 'aktif').attr('data-type', 'text').attr('class', 'updatedata');
                }
            },
            {
            'targets': 9,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('data-pk', rowData['id']).attr('data-name', 'panggilan').attr('data-type', 'text').attr('class', 'updatedata');
                }
            },
            {
            'targets': 10,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('data-pk', rowData['id']).attr('data-name', 'username').attr('data-type', 'text').attr('class', 'updatedata');
                }
            },
            {
            'targets': 11,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('data-pk', rowData['id']).attr('data-name', 'tgllahir').attr('data-type', 'text').attr('class', 'updatedata');
                }
            },
            {
            'targets': 12,
            'createdCell':  function (td, cellData, rowData, row, col) {
                $(td).attr('data-pk', rowData['id']).attr('data-name', 'gender').attr('data-type', 'text').attr('class', 'updatedata');
                }
            },
        ],
        "columns":[
            { "data": "checkbox", orderable:false, searchable:false},
            { "data": "nama" },
            { "data": "email" },
            { "data": "no_telp" },
            { "data": "alamat" },
            { "data": "kota" },
            { "data": "status_user" },
            { "data": "ref" },
            { "data": "aktif" },
            { "data": "panggilan" },
            { "data": "username" },
            { "data": "tgllahir" },
            { "data": "gender" },
            { "data": "action", orderable:false, searchable: false}
        ],
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var input = document.createElement("input");
                $(input).appendTo($(column.footer()).empty())
                .on('change', function () {
                    var val = $.fn.dataTable.util.escapeRegex($(this).val());

                    column.search(val ? val : '', true, false).draw();
                });
            });
        },
        "drawCallback": function( settings ) {
            var api = this.api();

            $('.updatedata', api.table().body()).editable({
                url: "{{route('admin.biodata.updatedata')}}",
                mode : 'inline',
            });
        },
        dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'excel',
                    text: 'Export Data Excel',
                    messageTop: 'Data entrepreneurID 2020.'
                }
            ],
            exportOptions : {
                modifier : {
                    // DataTables core
                    order : 'index', // 'current', 'applied',
                    //'index', 'original'
                    page : 'all', // 'all', 'current'
                    search : 'applied' // 'none', 'applied', 'removed'
                },
                columns: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ]
            }

    });

    $('#search-form').on('submit', function(e) {
        biodata_.draw();
        e.preventDefault();
    });

    $(document).ready(function() {
        // Setup - add a text input to each footer cell
        $('#tabel_biodata tfoot th').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        } );

        // DataTable
        var table = $('#tabel_biodata').DataTable();

        // Apply the search
        table.columns().every( function () {
            var that = this;

            $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );
        } );
    } );

    //Hapus data satuan
    $(document).on('click', '.delete', function(){
        var id = $(this).attr('id');
        if(confirm("Apakah anda yakin data "+ id +" akan dihapus ?"))
        {
            $.ajax({
                url:"{{route('admin.biodata.hapusdata')}}",
                mehtod:"get",
                data:{id:id},
                success:function(data)
                {
                    alert(data);
                    $('#tabel_biodata').DataTable().ajax.reload();
                }
            })
        }
        else
        {
            return false;
        }
    });

    //Hapus Banyak Data secara bersamaan
    $(document).on('click', '#bulk_delete', function(){
        var id = [];
        if(confirm("Apakah anda yakin pilihan data akan dihapus ?"))
        {
            $('.hsatu_checkbox:checked').each(function(){
                id.push($(this).val());
            });
            if(id.length > 0)
            {
                $.ajax({
                    url:"{{route('admin.biodata.hapusdatamasal')}}",
                    method:"get",
                    data:{id:id},
                    success:function(data)
                    {
                        alert(data);
                        $('#tabel_biodata').DataTable().ajax.reload();
                    }
                });
            }
            else
            {
                alert("Silahkan pilih lebih dari 1 data !");
            }
        }
    });


</script>

@stack('after-scripts')
