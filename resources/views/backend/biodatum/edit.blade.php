@extends('backend.layouts.app')

@section('title', __('backend_biodata.labels.management') . ' | ' . __('backend_biodata.labels.edit'))

@section('breadcrumb-links')
    @include('backend.biodatum.includes.breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($biodatum, 'PATCH', route('admin.biodata.update', $biodatum->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Edit Data Customer
                        <small class="text-muted">{{ $biodatum->nama }}</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="">
                            @csrf
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Nama Lengkap</label>
                                        <div class="col-sm-8">
                                            <input value="{{ $biodatum->nama }}" onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="nama" class="form-control" maxlength="100" placeholder="Nama Lengkap" required="">
                                        </div>
                                    </div><!--col-->
                                </div><!--row-->
                            </div><!--row-->
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Alamat Email</label>
                                        <div class="col-sm-8">
                                            <input value="{{ $biodatum->email }}" onkeyup="this.value = this.value.toLowerCase();" autocomplete="new-password" type="email" name="email" id="email" placeholder="Alamat Email" maxlength="191" required="required" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Nama Panggilan</label>
                                        <div class="col-sm-8">
                                            <input value="{{ $biodatum->panggilan }}" type="text" name="panggilan" class="form-control" maxlength="100" placeholder="Nama Panggilan" required="">
                                        </div>
                                    </div><!--col-->
                                </div><!--row-->
                            </div><!--row-->
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Jenis Kelamin</label>
                                        <div class="col-sm-8">
                                            <input value="{{ $biodatum->gender }}" type="text" name="gender" class="form-control" maxlength="100" placeholder="Jenis Kelamin" required="">
                                        </div>
                                    </div><!--col-->
                                </div><!--row-->
                            </div><!--row-->
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Nomo HP Whatsapp </label>
                                        <div class="col-sm-8">
                                            <input value="{{ $biodatum->no_telp }}" type="text" name="no_telp" class="form-control" maxlength="100" placeholder="Nomor Whatsapp" required="">
                                        </div>
                                    </div><!--form-group-->
                                </div><!--col-->
                            </div><!--row-->
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Tanggal Lahir</label>
                                        <div class="col-sm-8">
                                            <input value="{{ $biodatum->tgllahir }}" type="text" name="tgllahir" class="form-control" maxlength="100" placeholder="Tanggal Lahir" required="">
                                        </div>
                                    </div><!--form-group-->
                                </div><!--col-->
                            </div><!--row-->
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Asal Kota</label>
                                        <div class="col-sm-8">
                                            <input value="{{ $biodatum->kota }}" onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="kota" class="form-control" placeholder="Kota Domisili" required="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{-- <div class="row">
                                <div class="col pull-right">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    <button type="button" class="btn btn-primary">Simpan</button>
                                </div><!--col-->
                            </div> --}}
                    {{-- {{ html()->label(__('backend_biodata.validation.attributes.nama'))->class('col-md-2 form-control-label')->for('nama') }}

                        <div class="col-md-10">
                            {{ html()->text('nama')
                                ->class('form-control')
                                ->placeholder(__('backend_biodata.validation.attributes.nama'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col--> --}}
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.biodata.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
