<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/biodata*')) }}" href="{{ route('admin.biodata.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_biodata.sidebar.title')
    </a>
</li>