<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Data Ecourse</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.biodata.index') }}">@lang('backend_biodata.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.biodata.create') }}">@lang('backend_biodata.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.biodata.deactivated') }}">@lang('backed_biodata.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.biodata.deleted') }}">@lang('backend_biodata.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
