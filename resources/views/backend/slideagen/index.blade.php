@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="card">
    <div class="card-body">
        Seting Slider Dashboard Agen
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <form action="{{ route('admin.events.slideagentambah') }}" enctype="multipart/form-data" method="post" class="col-12">
                        @csrf
                        <input type="hidden" name="metode" value="tambah">
                        <div class="col-12">
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">
                                    Nama
                                </label>
                                <div class="col-md-10">
                                    <input type="text" name="nama" class="form-control" placeholder="Nama" required>
                                </div><!--col-->
                            </div><!--form-group-->
                        </div><!--col-->
                        <div class="col-12">
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">
                                    Alamat link
                                </label>
                                <div class="col-md-10">
                                    <input type="text" name="url" class="form-control" placeholder="Alamat link" required>
                                </div><!--col-->
                            </div><!--form-group-->
                        </div><!--col-->
                        <div class="col-12">
                            <div class="form-group row">
                                <label class="col-md-2 col-form-label">
                                    Gambar
                                </label>
                                <div class="col-md-10">
                                    <input type="file" name="gambar" class="form-control" placeholder="Gambar" required>
                                </div><!--col-->
                            </div><!--form-group-->
                        </div><!--col-->
                        <div class="col-12">
                            <div class="form-group row float-right">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary">Tambah</button>
                                </div>
                            </div><!--form-group-->
                        </div><!--col-->
                    </form>
                    <div class="col-12 mt-4">
                        <h4>
                            List Slider
                        </h4>
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Link</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $first  = 0;
                                $end    = 0;
                                @endphp
                                @foreach ($slidedata as $key => $data)
                                    <tr>
                                        <td>{{ $key + $slidedata->firstItem() }}</td>
                                        <td>{{ $data->nama }}</td>
                                        <td>
                                            <form action="{{ route('admin.events.slideagen') }}?metode=update&id={{ $data->id }}">
                                                <input type="text" name="metode" value="update" hidden>
                                                <input type="text" name="id" value="{{ $data->id }}" hidden>
                                                <input type="text" name="url" class="form-control" value="{{ $data->url }}">
                                                <div>
                                                    <button class="btn btn-sm btn-info">Update</button>
                                                </div>
                                            </form>
                                        </td>
                                        <td>
                                            <img class="img-fluid" width="150" src="/img/slide/{{ $data->gambar }}" alt="">
                                        </td>
                                        <td>
                                            @if ($data->aktif != 1)
                                                <div class="pb-1">
                                                    <a href="{{ route('admin.events.slideagen') }}?metode=aktif&id={{ $data->id }}" class="btn btn-sm btn-success">Aktifkan</a>
                                                </div>
                                            @endif
                                            <a onclick="if (confirm('Apakah yakin akan dihapus ?')) commentDelete(1); return false" href="{{ route('admin.events.slideagen') }}?metode=delete&id={{ $data->id }}" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i> Hapus</a>
                                        </td>
                                    </tr>
                                    @php
                                    $first  = $slidedata->firstItem();
                                    $end    = $key + $slidedata->firstItem();
                                    @endphp
                                @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-7">
                                <div class="float-left">
                                    {!! $first !!} - {!! $end !!} From {!! $slidedata->total() !!} Data
                                </div>
                            </div><!--col-->

                            <div class="col-5">
                                <div class="float-right">
                                    {!! $slidedata->appends(request()->query())->links() !!}
                                </div>
                            </div><!--col-->
                        </div><!--row-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('admin.events.slideagen') }}?metode=urutan&id={{ $data->id ?? '' }}">
                    <input type="text" name="metode" value="urutan" hidden>
                    <div class="row">
                        <div class="col-12">
                            <h4>
                                Slider Aktif
                            </h4>
                            <table class="table table-sm">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Link</th>
                                        <th>Image</th>
                                        <th width="80">Urutan</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $first  = 0;
                                    $end    = 0;
                                    @endphp
                                    @foreach ($slideaktif as $key => $data)
                                        <tr>
                                            <td>{{ $data->nama }}</td>
                                            <td>{{ $data->url }}</td>
                                            <td>
                                                <img class="img-fluid" width="150" src="/img/slide/{{ $data->gambar }}" alt="">
                                            </td>
                                            <td>
                                                <input type="text" name="id[]" value="{{ $data->id }}" hidden>
                                                <input class="form-control" type="number" name="urutan[]" value="{{ $data->sort }}">
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.events.slideagen') }}?metode=nonaktif&id={{ $data->id }}" class="btn btn-sm btn-warning">Nonaktifkan</a>
                                            </td>
                                        </tr>
                                        @php
                                        $first  = $slideaktif->firstItem();
                                        $end    = $key + $slideaktif->firstItem();
                                        @endphp
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-7">
                                    <div class="float-left">
                                        {!! $first !!} - {!! $end !!} From {!! $slideaktif->total() !!} Data
                                    </div>
                                </div><!--col-->

                                <div class="col-5">
                                    <div class="float-right">
                                        {!! $slideaktif->appends(request()->query())->links() !!}
                                    </div>
                                </div><!--col-->
                            </div><!--row-->
                        </div>
                    </div>
                    <button type="submit" class="float-right btn btn-primary">Update Urutan</button>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="row justify-content-center ">
                <div class="col-md-6 ">
                    <div id="slide" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            @foreach ($slideaktif as $key => $data)
                                <div class="carousel-item {{ $key + $slideaktif->firstItem() == 1 ? 'active' : '' }}">
                                    <a href="{{ $data->url ?? '#'}}" target="_blank">
                                        <img src="/img/slide/{{ $data->gambar }}" class="img-fluid rounded" alt="...">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#slide" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#slide" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                </div>
        </div>

    </div>
</div>

@endsection
