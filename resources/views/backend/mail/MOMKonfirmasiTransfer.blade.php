<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>entrepreneurID</title>
  <style type="text/css" media="screen">

    /* Force Hotmail to display emails at full width */
    .ExternalClass {
      display: block !important;
      width: 100%;
    }

    /* Force Hotmail to display normal line spacing */
    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }

    body,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 0;
      padding: 0;
    }

    body,
    p,
    td {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 15px;
      color: #333333;
      line-height: 1.5em;
    }

    h1 {
      font-size: 24px;
      font-weight: normal;
      line-height: 24px;
    }

    body,
    p {
      margin-bottom: 0;
      -webkit-text-size-adjust: none;
      -ms-text-size-adjust: none;
    }

    img {
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    a img {
      border: none;
    }

    .background {
      background-color: #eef1f4;
    }

    table.background {
      margin: 0;
      padding: 0;
      width: 100% !important;
    }

    .block-img {
      display: block;
      line-height: 0;
    }

    a {
      color: white;
      text-decoration: none;
    }

    a,
    a:link {
      color: #2A5DB0;
      text-decoration: underline;
    }

    table td {
      border-collapse: collapse;
    }

    td {
      vertical-align: top;
      text-align: left;
    }

    .wrap {
      width: 600px;
    }

    .wrap-cell {
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .header-cell,
    .body-cell,
    .footer-cell {
      padding-left: 20px;
      padding-right: 20px;
    }

    .header-cell {
      background-color: #cf1418;
      font-size: 24px;
      color: #ffffff;
    }

    .body-cell {
      background-color: #ffffff;
      padding-top: 30px;
      padding-bottom: 34px;
    }

    .footer-cell {
      background-color: #eeeeee;
      text-align: center;
      font-size: 13px;
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .card {
      width: 400px;
      margin: 0 auto;
    }

    .data-heading {
      text-align: right;
      padding: 10px;
      background-color: #ffffff;
      font-weight: bold;
    }

    .data-value {
      text-align: left;
      padding: 10px;
      background-color: #ffffff;
    }

    .force-full-width {
      width: 100% !important;
    }
.logo {
text-align: center;
}

  </style>
  <style type="text/css" media="only screen and (max-width: 600px)">
    @media only screen and (max-width: 600px) {
      body[class*="background"],
      table[class*="background"],
      td[class*="background"] {
        background: #eeeeee !important;
      }

      table[class="card"] {
        width: auto !important;
      }

      td[class="data-heading"],
      td[class="data-value"] {
        display: block !important;
      }

      td[class="data-heading"] {
        text-align: left !important;
        padding: 10px 10px 0;
      }

      table[class="wrap"] {
        width: 100% !important;
      }

      td[class="wrap-cell"] {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
      }
    }
  </style>
</head>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" bgcolor="" class="background">
<table class="background" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td class="background" align="center" valign="top" width="100%"><center>
<table class="wrap" width="600" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="wrap-cell" style="padding-top: 30px; padding-bottom: 30px;" valign="top">
<table class="force-full-width" style="height: 333px;" cellspacing="0" cellpadding="0">
<tbody>
<tr style="height: 80px;">
<td class="header-cell" style="height: 80px; width: 560px;" valign="top" height="60">
<p><font face="sans-serif">&nbsp;</font></p>
<p style="padding-left: 200px;"><font face="sans-serif"><img class="logo" src="http://admin.entrepreneurid.org/img/emblem-sm.png" alt="logo" width="150" height="30" align="center" /></font></p>
<p><font face="sans-serif">&nbsp;</font></p>
</td>
</tr>
<tr style="height: 234px;">
<td class="body-cell" style="height: 234px; width: 560px;" valign="top"><font face="sans-serif"><br /></font>
<table style="height: 594px; width: 100%;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tbody>
<tr style="height: 528px;">
<td style="padding-bottom: 20px; background-color: #ffffff; width: 560px; height: 528px;" valign="top">
<p><font face="sans-serif">Dear, {{ $nama }}</font></p>
<p><font face="sans-serif">&nbsp;</font></p>
<p><font face="sans-serif">Selamat karena telah menyelesaikan proses pendaftaran <strong>Mentoring Organic Marketing (MOM)</strong>.</font></p>
<p><font face="sans-serif">&nbsp;</font></p>
<p><font face="sans-serif">Sebagai salah satu peserta MOM, silahkan simak informasi berikut</font></p>
<p><font face="sans-serif">&nbsp;</font></p>
<ol>
<li><font face="sans-serif"><strong>Jadwal Mentoring</strong><br />Mulai Hari Senin, Anda akan mendapat bimbingan via email selama 7 Hari. <br />Waktu bimbingan adalah pukul <strong>20.00 WIB</strong>. <br />Harap siapkan catatan saat proses belajar nanti, agar hasil yang Anda dapatkan maksimal.<br /><br /></font></li>
<li><font face="sans-serif"><strong>Akses Materi &amp; Grup Diskusi </strong><br />Selain bimbingan, Anda juga bisa mengakses fasilitas <strong>MOM</strong> lainnya, seperti <br />- Member Area Peserta <br />- Akses Grup diskusi, dan<br />- Konsultasi Langsung<br /><br />Untuk mengakses fasilitas pendukung, silahkan aktifkan member area Anda melalui link <a href="mentoringorganicmarketing.com/member-area/registrasi">mentoringorganicmarketing.com/member-area/registrasi</a><br />(Saat mengaktifkan Member Area, mohon gunakan email yang Anda gunakan saat pendaftaran, bukan email lainnya)<br /><br /></font></li>
<li><font face="sans-serif"><strong>Support Contact</strong><br /></font>
<p>Jika Anda menemui kendala, butuh bantuan atau tidak mendapatkan email apapun saat waktu bimbingan dimulai, maka silahkan hubungi Customer Service kami melalui chat WhatsApp di nomor 0857-8757-2580 atau langsung klik <a href="bit.ly/CS-eID">bit.ly/CS-eID</a></p>
<p>&nbsp;</p>
</li>
</ol>
<p><font face="sans-serif"><br />Sekali lagi selamat karena telah menjadi peserta <strong>Mentoring Organic Marketing</strong>. <br /><br />Semoga ini jadi wasilah untuk pertumbuhan bisnis Anda, aamiin.</font></p>
<p><font face="sans-serif">&nbsp;</font></p>
<p><font face="sans-serif">&nbsp;</font></p>
<p><font face="sans-serif">Salam,</font></p>
<p><font face="sans-serif"><br /><strong>Tim entrepreneurID</strong></font></p>
</td>
</tr>
<tr style="height: 22px;">
<td style="width: 560px; height: 22px;"><font face="sans-serif">&nbsp;</font></td>
</tr>
<tr style="height: 22px;">
<td style="padding-top: 20px; background-color: #ffffff; width: 560px; height: 22px;"><font face="sans-serif">&nbsp;</font></td>
</tr>
<tr style="height: 22px;">
<td style="padding-top: 20px; background-color: #ffffff; width: 560px; height: 22px;"><font face="sans-serif">&nbsp;</font></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style="height: 19px;">
<td class="footer-cell" style="height: 19px; width: 560px;" valign="top"><span style="font-size: 15px;"><font face="sans-serif">entrepreneurID &copy; 2021</font></span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</center></td>
</tr>
</tbody>
</table>
</body>
</html>
