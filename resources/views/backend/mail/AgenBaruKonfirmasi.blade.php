<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="entrepreneurID" />
<meta name="description" content="Agen Baru entrepreneurID" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>entrepreneurID</title>
<style type="text/css" media="screen">

    /* Force Hotmail to display emails at full width */
    .ExternalClass {
      display: block !important;
      width: 100%;
    }

    /* Force Hotmail to display normal line spacing */
    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }

    body,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 0;
      padding: 0;
    }

    body,
    p,
    td {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 15px;
      color: #333333;
      line-height: 1.5em;
    }

    h1 {
      font-size: 24px;
      font-weight: normal;
      line-height: 24px;
    }

    body,
    p {
      margin-bottom: 0;
      -webkit-text-size-adjust: none;
      -ms-text-size-adjust: none;
    }

    img {
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    a img {
      border: none;
    }

    .background {
      background-color: #eef1f4;
    }

    table.background {
      margin: 0;
      padding: 0;
      width: 100% !important;
    }

    .block-img {
      display: block;
      line-height: 0;
    }

    a {
      color: white;
      text-decoration: none;
    }

    a,
    a:link {
      color: #2A5DB0;
      text-decoration: underline;
    }

    table td {
      border-collapse: collapse;
    }

    td {
      vertical-align: top;
      text-align: left;
    }

    .wrap {
      width: 600px;
    }

    .wrap-cell {
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .header-cell,
    .body-cell,
    .footer-cell {
      padding-left: 20px;
      padding-right: 20px;
    }

    .header-cell {
      background-color: #cf1418;
      font-size: 24px;
      color: #ffffff;
    }

    .body-cell {
      background-color: #ffffff;
      padding-top: 30px;
      padding-bottom: 34px;
    }

    .footer-cell {
      background-color: #eeeeee;
      text-align: center;
      font-size: 13px;
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .card {
      width: 400px;
      margin: 0 auto;
    }

    .data-heading {
      text-align: right;
      padding: 10px;
      background-color: #ffffff;
      font-weight: bold;
    }

    .data-value {
      text-align: left;
      padding: 10px;
      background-color: #ffffff;
    }

    .force-full-width {
      width: 100% !important;
    }
.logo {
text-align: center;
}

  </style>
<style type="text/css" media="only screen and (max-width: 600px)">
    @media only screen and (max-width: 600px) {
      body[class*="background"],
      table[class*="background"],
      td[class*="background"] {
        background: #eeeeee !important;
      }

      table[class="card"] {
        width: auto !important;
      }

      td[class="data-heading"],
      td[class="data-value"] {
        display: block !important;
      }

      td[class="data-heading"] {
        text-align: left !important;
        padding: 10px 10px 0;
      }

      table[class="wrap"] {
        width: 100% !important;
      }

      td[class="wrap-cell"] {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
      }
    }
  </style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" bgcolor="" class="background">
<table class="background" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td class="background" align="center" valign="top" width="100%"><center>
<table class="wrap" width="600" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="wrap-cell" style="padding-top: 30px; padding-bottom: 30px;" valign="top">
<table class="force-full-width" style="height: 333px;" cellspacing="0" cellpadding="0">
<tbody>
<tr style="height: 80px;">
<td class="header-cell" style="height: 80px; width: 560px;" valign="top" height="60">
<p><font face="sans-serif">&nbsp;</font></p>
<p style="padding-left: 200px;"><font face="sans-serif"><img class="logo" src="http://admin.entrepreneurid.org/img/emblem-sm.png" alt="logo" width="150" height="30" align="center" /></font></p>
<p><font face="sans-serif">&nbsp;</font></p>
</td>
</tr>
<tr style="height: 234px;">
<td class="body-cell" style="height: 234px; width: 560px;" valign="top"><font face="sans-serif"><br /></font>
<table style="height: 582px; width: 100.357%;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tbody>
<tr style="height: 528px;">
<td style="padding-bottom: 20px; background-color: #ffffff; width: 560px; height: 528px;" valign="top">
<p><strong><font face="sans-serif">Dear, {{ $nama }}</font></strong></p>
<p><font face="sans-serif"><br />Selamat datang kedalam keluarga besar entrepreneurID.<br />Kami senang sekali menyambut Anda bergabung dalam bagian keluarga ini. ^_^<br /><br /></font></p>
<p><font face="sans-serif">Pesan yang sedang Anda baca ini adalah bukti resmi bahwa Anda telah terkonfirmasi sebagai Agen entrepreneurID.</font></p>
<p><font face="sans-serif">&nbsp;</font></p>
<p><font face="sans-serif">Sebagai Agen entrepreneurID, sekarang Anda otomatis memiliki hak untuk memasarkan seluruh produk yang ada di entrepreneurID.<br />Selain itu Anda juga akan mendapatkan support Bimbingan, Mentoring Esklusif, Akses Grup Khusus, Amunisi pemasaran dan fasilitas lainnya termasuk Komisi yang akan diberikan setiap minggu berdasarkan sales yang Anda dapatkan nantinya.</font></p>
<p><font face="sans-serif">&nbsp;</font></p>
<p><font face="sans-serif">Nah, agar Anda mendapatkan manfaat maksimal dari program ini, maka silahkan ikuti arahan dibawah ini</font></p>
<p><font face="sans-serif">&nbsp;</font></p>
<h3><font style="background-color: #ffdc00;" face="sans-serif">Pertama, Hubungi Fasilitator Agen</font></h3>
<p>&nbsp;</p>
<p><font face="sans-serif">Simpan nomor <strong>0821-5089-6665</strong> di Hp Anda dengan nama <strong>Mba Wulan.</strong></font></p>
<p>&nbsp;</p>
<p><font face="sans-serif">Mba Wulan adalah fasilitator agen entrepreneurID yang akan mengurus seluruh kebutuhan Anda selama aktif sebagai Agen entrepreneurID. Mba Wulan juga yang nantinya akan memasukan Anda ke grup belajar untuk Agen entrepreneurID dan channel info terupdate untuk Agen.</font></p>
<p>&nbsp;</p>
<p><font face="sans-serif">Karena itu, silahkan langsung hubungi Mba Wulan via WA dengan format berikut<br /><b></b></font></p>
<p>&nbsp;</p>
<p style="padding-left: 40px;"><font color="#E74C3C" face="sans-serif"><b>Aktivasi Agen baru<br /></b></font><font face="sans-serif"><font color="#E74C3C"><em>Nama Panggilan :</em></font><br /><font color="#E74C3C"><em>Alamat Email yang didaftarakan :</em></font><br /><font color="#E74C3C"><em>Nomor Telegram :</em></font></font></p>
<p><font face="sans-serif">&nbsp;</font></p>
<p><font face="sans-serif">Jika sebelumnya Anda sudah melakukan hal ini, maka silahkan abaikan. Tapi jika Anda belum melakukannya, silahkan langsung hubungi mba Wulan sekarang.<br /></font></p>
<p>&nbsp;</p>
<h3><font style="background-color: #ffdc00;"><strong><font face="sans-serif">Kedua, Pasang Alarm di Jadwal Penting </font></strong></font></h3>
<p><font face="sans-serif">&nbsp;</font></p>
<p><font face="sans-serif">Ada 3 proses penting yang akan Anda lalui setelah ini. Karena itu, silahkan pasang alarm di jadwal-jadwal dibawah ini.</font></p>
<p>&nbsp;</p>
<ol>
<li><strong>Terima Email Regulasi</strong><br /><strong>Jadwal : Senin, Tanggal 13 September, Mulai Pukul 10.00 WIB.</strong><br /><br />Saat ini proses pendaftaran Agen entrepreneurID sedang berlangsung sampai tanggal <strong>11 September</strong>, karena itu akses masuk grup untuk agen baru, akan dibuka setelah pendaftaran selesai.<br /><br />Jika Anda tidak mendapat email diwaktu tersebut, silahkan hubungi Mba Wulan untuk membantu Anda.<br /><br /></li>
<li><strong>Masuk Grup Agen</strong><br /><strong>Jadwal : Senin, Tanggal 13 September, Mulai Pukul 10.00 WIB.</strong><br /><br />Saat ini proses pendaftaran Agen entrepreneurID sedang berlangsung sampai tanggal <strong>11 September</strong>, karena itu akses masuk grup untuk agen baru, akan dibuka setelah pendaftaran selesai.<br /><br />Ditanggal yang disebutkan diatas, silahkan hubungi mba Wulan lagi agar bisa masuk ke grupnya.<br /><br /></li>
<li><strong>Mulai Bimbingan</strong><br /><strong>Jadwal : Mulai Hari Selasa, Tanggal 14 September, Mulai Pukul 19.00 WIB<br /></strong><br />Seperti yang disebutkan dipendaftaran, Anda akan dapat bimbingan dari nol tentang apa yang harus dilakukan agar bisa memiliki tambahan penghasilan dari program agen ini. Bimbingan akan dimulai sesuai jadwal yang disebutkan.&nbsp;<br /><br />Kami menyarankan Anda untuk mengikuti sesi bimbingan Agen baru, agar hasil yang Anda dapatkan lebih maksimal. ^_^<br /><br /></li>
</ol>
<p style="padding-left: 40px;">Dan sambil menunggu mentoringnya, Anda bisa akses bonus pendaftaran yang ada dibawah ini.</p>
<p style="padding-left: 40px;">&nbsp;</p>
<p style="padding-left: 40px;">1. Videobook - Reseller Basicpedia <strong><a href="bit.ly/eID-vbrbp">bit.ly/eID-vbrbp</a></strong><br />2. Videobook - Copywriting Basicpedia <strong><a href="bit.ly/eID-vbcbp">bit.ly/eID-vbcbp</a></strong><br />3. Ebook 5 Aturan Dasar Penjualan <strong><a href="bit.ly/5adp1019">bit.ly/5adp1019</a></strong><br />4. Ebook Cara Mudah Nulis Iklan <strong><a href="bit.ly/cmni0919">bit.ly/cmni0919</a></strong><br />5. Ebook Fb WA Fusion Marketing <strong><a href="bit.ly/fbwa0719">bit.ly/fbwa0719</a></strong><br />6. Ebook Lebih Dari Tahun Sebelumnya <strong><a href="bit.ly/lts0118">bit.ly/lts0118</a></strong><br />7. Video 2 Hal Penting Sebelum Menjual Sesuatu <strong><a href="bit.ly/vd-2cm">bit.ly/vd-2cm</a></strong><br />8. Video Cara Positif Merespon Prospek yang Batal Order <strong><a href="bit.ly/vd-cpmbo">bit.ly/vd-cpmbo</a></strong><br />9. Video Tips Atur Waktu yang Efektif untuk Pengusaha <strong><a href="bit.ly/vid-tmwp">bit.ly/vid-tmwp</a></strong><br />10. Video Cara Mengembalikan Semangat dalam Waktu singkat <strong><a href="bit.ly/vid-3mf">bit.ly/vid-3mf</a></strong><br />11. Video Mencapai Target dengan rumus OKSD <strong><a href="bit.ly/vd-oksd">bit.ly/vd-oksd</a></strong><br />12. Video 2 Sifat Wajib Pengusaha Milenial <strong><a href="bit.ly/vid-2spm">bit.ly/vid-2spm</a></strong><br />13. Video Cara Positif Atur Uang Pribadi <strong><a href="bit.ly/vid-cwau">bit.ly/vid-cwau</a></strong><br />14. Video Entrepreneur Value <strong><a href="bit.ly/vid-evpb">bit.ly/vid-evpb</a></strong></p>
<p>&nbsp;</p>
<p>Inilah pesan konfirmasi untuk Agen entrepreneurID baru.</p>
<p>&nbsp;</p>
<p>Sekarang jika Anda belum sama sekali hubungi Mba Wulan, maka silahkan hubungi karena ada informasi penting untuk Anda.<br />Tapi jika sebelum terima email ini Anda sudah hubungi Mba Wulan, maka tunggu email selanjutnya di tanggal <strong>13 September.</strong></p>
<p>&nbsp;</p>
<p>Jika ada yang ingin ditanyakan tentang isi email ini, silahkan hubungi mba Wulan juga ya.</p>
<p>&nbsp;</p>
<p>Sekali lagi kami ucapkan selamat datang ke dalam keluarga besar entrepreneurID. Semoga ini jadi wasilah kebaikan untuk Anda dan orang yang Anda sayangi. ^_^</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Regards,</p>
<p>&nbsp;</p>
<p><strong>Tim entrepreneurID Pusat</strong></p>
<p><font face="sans-serif">&nbsp;</font></p>
<p><font face="sans-serif">&nbsp;</font></p>
</td>
</tr>
<tr style="height: 22px;">
<td style="width: 560px; height: 10px;"><font face="sans-serif">&nbsp;</font></td>
</tr>
<tr style="height: 22px;">
<td style="padding-top: 20px; background-color: #ffffff; width: 560px; height: 22px;">&nbsp;</td>
</tr>
<tr style="height: 22px;">
<td style="padding-top: 20px; background-color: #ffffff; width: 560px; height: 22px;"><font face="sans-serif">&nbsp;</font></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style="height: 19px;">
<td class="footer-cell" style="height: 19px; width: 560px;" valign="top"><span style="font-size: 15px;"><font face="sans-serif">entrepreneurID &copy; 2021</font></span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</center></td>
</tr>
</tbody>
</table>
</body>
</html>