<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.backend.sidebar.general')
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('/admin/dashboard'))
                }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('/admin/produks*'))
                }}" href="/admin/produks">
                    <i class="nav-icon fas fa-database"></i>
                    Produk
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('/admin/events*'))
                }}" href="/admin/events">
                    <i class="nav-icon fas fa-chart-line"></i>
                    Event
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('/admin/events*'))
                }}" href="/admin/events/leaderboard">
                    <i class="nav-icon fas fa-crown"></i>
                    Leaderboard
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('/admin/events*'))
                }}" href="/admin/events/slide-agen">
                    <i class="nav-icon fas fa-clone"></i>
                    Slide Dashboard Agen
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('/admin/transaksi*'))
                }}" href="/admin/transaksi">
                    <i class="nav-icon fas fa-tasks"></i>
                    Transaksi
                </a>
            </li>
            {{-- <li class="nav-title">
                Data Ecourse
            </li> --}}
            {{-- <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/produk/'))
                }}" href="admin/produk/">
                    <i class="nav-icon fas fa-book"></i>
                    Mentoring Organic Marketing
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/produk/'))
                }}" href="admin/produk/">
                    <i class="nav-icon fas fa-book"></i>
                    Menuju 1 Miliar
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/produk/'))
                }}" href="admin/produk/">
                    <i class="nav-icon fas fa-book"></i>
                    Peta Bisnis Online
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/produk/'))
                }}" href="admin/produk/">
                    <i class="nav-icon fas fa-book"></i>
                    Reseller Formula
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/produk/'))
                }}" href="admin/produk/">
                    <i class="nav-icon fas fa-book"></i>
                    Instant Copywriting
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/produk/'))
                }}" href="admin/produk/">
                    <i class="nav-icon fas fa-book"></i>
                    Mentoring Sales Funnel
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/produk/'))
                }}" href="admin/produk/">
                    <i class="nav-icon fas fa-book"></i>
                    Kelas Online Copywriting
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/produk/'))
                }}" href="admin/produk/">
                    <i class="nav-icon fas fa-book"></i>
                    WhatsApp Master Closing
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/produk/'))
                }}" href="admin/produk/">
                    <i class="nav-icon fas fa-book"></i>
                    WhatsApp Master Closing
                </a>
            </li> --}}
            <li class="nav-item ">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('/admin/agen'))
                }}" href="/admin/agen">
                    <i class="nav-icon fas fa-user-tie"></i>
                    Agen
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('/admin/agen/sub-agen'))
                }}" href="/admin/agen/sub-agen">
                    <i class="nav-icon fas fa-user"></i>
                    Sub Agen
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('/admin/agen/agen-lama'))
                }}" href="/admin/agen/agen-lama">
                    <i class="nav-icon fas fa-users"></i>
                    Agen Lama
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('/admin/data-ecourse'))
                }}" href="/admin/data-ecourse">
                    <i class="nav-icon fas fa-book"></i>
                    Transaksi Lama
                </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('/admin/member*'))
                }}" href="/admin/member">
                    <i class="nav-icon fas fa-address-card"></i>
                    Member Area
                </a>
            </li>

            {{-- <li class="nav-item nav-dropdown {{
                active_class(Active::checkUriPattern('/admin/biodata?produk=18*'), 'open')
            }}">
                <a class="nav-link nav-dropdown-toggle {{
                        active_class(Active::checkUriPattern('/admin/biodata?produk=18*'))
                    }}" href="#">
                    <i class="nav-icon fas fa-book"></i> WhatsApp Master Closing
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{
                        active_class(Active::checkUriPattern('/admin/biodata?produk=18&user=0'))
                    }}" href="/admin/biodata?produk=18&user=0">
                            User Affiliasi
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{
                        active_class(Active::checkUriPattern('/admin/biodata?produk=18&user=2'))
                    }}" href="/admin/biodata?produk=18&user=2">
                            User Manual
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{
                        active_class(Active::checkUriPattern('/admin/biodata?produk=18'))
                    }}" href="/admin/biodata?produk=18">
                            Semua Data User
                        </a>
                    </li>
                </ul>
            </li> --}}

            {{-- <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/produk/'))
                }}" href="admin/produk/">
                    <i class="nav-icon fas fa-book"></i>
                    Pendaftaran Agen
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/produk/'))
                }}" href="admin/produk/">
                    <i class="nav-icon fas fa-book"></i>
                    Kelas Facebook Hacking
                </a>
            </li> --}}

            <li class="nav-title">
                @lang('menus.backend.sidebar.system')
            </li>

            @if ($logged_in_user->isAdmin())
                <li class="nav-item nav-dropdown {{
                    active_class(Active::checkUriPattern('admin/auth*'), 'open')
                }}">
                    <a class="nav-link nav-dropdown-toggle {{
                        active_class(Active::checkUriPattern('admin/auth*'))
                    }}" href="#">
                        <i class="nav-icon far fa-user"></i>
                        @lang('menus.backend.access.title')

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Active::checkUriPattern('admin/auth/user*'))
                            }}" href="{{ route('admin.auth.user.index') }}">
                                @lang('labels.backend.access.users.management')

                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Active::checkUriPattern('admin/auth/role*'))
                            }}" href="{{ route('admin.auth.role.index') }}">
                                @lang('labels.backend.access.roles.management')
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="divider"></li>

                <li class="nav-item nav-dropdown {{
                    active_class(Active::checkUriPattern('admin/log-viewer*'), 'open')
                }}">
                        <a class="nav-link nav-dropdown-toggle {{
                            active_class(Active::checkUriPattern('admin/log-viewer*'))
                        }}" href="#">
                        <i class="nav-icon fas fa-list"></i> @lang('menus.backend.log-viewer.main')
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Active::checkUriPattern('admin/log-viewer'))
                        }}" href="{{ route('log-viewer::dashboard') }}">
                                @lang('menus.backend.log-viewer.dashboard')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Active::checkUriPattern('admin/log-viewer/logs*'))
                        }}" href="{{ route('log-viewer::logs.list') }}">
                                @lang('menus.backend.log-viewer.logs')
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
