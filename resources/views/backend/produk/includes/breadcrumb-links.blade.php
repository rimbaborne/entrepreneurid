<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_posters.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.posters.index') }}">@lang('backend_posters.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.posters.create') }}">@lang('backend_posters.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.posters.deactivated') }}">@lang('backed_posters.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.posters.deleted') }}">@lang('backend_posters.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
