@extends('backend.layouts.app')

@section('title', app_name() . ' | Produk')

@section('breadcrumb-links')
    @include('backend.produk.includes.breadcrumb-links')
@endsection

@section('content')
@if (request()->metode == 'edit')
<div class="card">
    <div class="card-body">
        <h4>Edit Produk</h4>
        <form action="">
            <div class="row">
                <div class="col-12">
                    @csrf
                    <input type="hidden" name="metode" value="update">
                    <input type="hidden" name="id" value="{{ $editproduk->id }}">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Nama Produk</label>
                        <div class="col-sm-8">
                            <input name="nama" value="{{ $editproduk->nama }}" class="form-control" type="text" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Inisial Produk</label>
                        <div class="col-sm-8">
                            <input name="inisial" value="{{ $editproduk->inisial }}" class="form-control" type="text" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Domain Produk</label>
                        <div class="col-sm-8">
                            <input name="domain" value="{{ $editproduk->domain }}" class="form-control" type="text" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Jenis Produk</label>
                        <div class="col-sm-8">
                            <select name="jenis" class="form-control">
                                <option value="{{ $editproduk->jenis }}">{{ $editproduk->jenis }}</option>
                                <option value="">-----</option>
                                <option value="ECOURSE">ECOURSE</option>
                                <option value="BUKU">BUKU</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-warning">Update <i class="fas fa-check"></i></button>
                </div>
            </div>
        </form>
    </div>
</div>
@endif
<div class="modal fade" id="buatproduk" tabindex="-1" role="dialog" aria-labelledby="buatprodukLabel" aria-hidden="true">
    <form action="">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="buatprodukLabel">Buat Produk Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            @csrf
                            <input type="hidden" name="metode" value="buat">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nama Produk</label>
                                <div class="col-sm-8">
                                    <input name="nama" value="" class="form-control" type="text" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Inisial Produk</label>
                                <div class="col-sm-8">
                                    <input name="inisial" value="" class="form-control" type="text" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Domain Produk</label>
                                <div class="col-sm-8">
                                    <input name="domain" value="domain.com" class="form-control" type="text" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Jenis Produk</label>
                                <div class="col-sm-8">
                                    <select name="jenis" class="form-control">
                                        <option value="ECOURSE">ECOURSE</option>
                                        <option value="BUKU">BUKU</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Status</label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control">
                                        <option value="AKTIF">AKTIF</option>
                                        <option value="NONAKTIF">NONAKTIF</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Buat <i class="fas fa-plus-circle"></i></button>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Data produk
                </h4>
            </div><!--col-->

            <div class="col-sm-7 ">
                <div class="float-right">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#buatproduk">
                        Tambah Produk Baru
                    </button>
                </div>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-sm">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Inisial</th>
                            <th>Domain</th>
                            <th>Jenis</th>
                            <th>Status</th>
                            <th width="100"></th>
                        </tr>
                        </thead>
                        <tbody>
                            @php
                                $first  = 0;
                                $end    = 0;
                            @endphp
                            @foreach ($dataproduk as $key => $data)
                                <tr>
                                    <td class="align-middle">{{ $key + $dataproduk->firstItem() }}</td>
                                    <td class="align-middle">{{ $data->nama }}</td>
                                    <td class="align-middle">{{ $data->inisial }}</td>
                                    <td class="align-middle">{{ $data->domain }}</td>
                                    <td class="align-middle">{{ $data->jenis }}</td>
                                    <td class="align-middle">{{ $data->status }}</td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="labels.backend.access.users.user_actions">
                                            <a href="?metode=edit&id={{ $data->id }}" data-toggle="tooltip" data-placement="top" title="" class="btn btn-primary" data-original-title="Edit"><i class="fas fa-edit"></i></a>

                                            <div class="btn-group btn-group-sm" role="group">
                                                <button id="userActions" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Lainnya
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="userActions" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 35px, 0px);">
                                                    {{-- <a href="https://eid.test/admin/auth/user/818/mark/0" class="dropdown-item">Non Aktif</a> --}}

                                                    <a href="#" data-method="delete" data-trans-button-cancel="Batal" data-trans-button-confirm="Hapus" data-trans-title="Yakin Dihapus ?" class="dropdown-item" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();">Hapus
                                                        <form action="" name="delete_item" style="display:none">
                                                            <input type="hidden" name="metode" value="hapus">
                                                            <input type="hidden" name="id" value="{{ $data->id }}">
                                                            @csrf
                                                        </form>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @php
                                    $first  = $dataproduk->firstItem();
                                    $end    = $key + $dataproduk->firstItem();
                                @endphp
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->

        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $first !!} - {!! $end !!} From {!! $dataproduk->total() !!} Data
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $dataproduk->appends(request()->query())->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
<script>
    var lazyLoadInstance = new LazyLoad();
</script>
@endsection
