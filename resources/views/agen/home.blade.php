@extends('agen.layouts.app')

@section('content')
{{-- <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.4.0/dist/lazyload.min.js"></script> --}}
<div class="col-sm-6 pb-4">
    <div class="card-2 " style="min-height: 500px">
        <div class="card-body">
            <h3 class="card-title" style="font-weight:300; margin-bottom: .4rem;">
                Selamat Datang, <strong style="font-weight:600">{{ Auth::guard('agen')->user()->name }}</strong>
            </h3>
            <p class="card-title" style="font-weight:300; font-size: 16px; padding-top: -10px">
                di Dashboard Agen entrepreneurID
            </p>
            <div id="slide" class="carousel slide pt-4" data-ride="carousel" data-interval="false">
                <div class="carousel-inner">
                    <div class="carousel-inner">
                        @foreach ($slideaktif as $key => $data)
                            <div class="carousel-item {{ $key + $slideaktif->firstItem() == 1 ? 'active' : '' }}">
                                <a href="{{ $data->url ?? '#'}}" target="_blank">
                                    <img src="/img/slide/{{ $data->gambar }}" class="img-fluid rounded" alt="...">
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <div class="carousel-item">
                        <img src="https://dashboard.agen-entrepreneurid.com/aset/slide/7CA5D4F1-379D-48B0-92C7-ADB65E76A4F7.jpeg" class="img-fluid rounded" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="https://dashboard.agen-entrepreneurid.com/aset/slide/3423FF41-2DE4-46BD-8826-D3AC46476F56.jpeg" class="img-fluid rounded" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="https://dashboard.agen-entrepreneurid.com/aset/slide/83C8E35C-155B-4D0E-B26F-590BF5561482.jpeg" class="img-fluid rounded" alt="...">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#slide" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#slide" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-3 order-sm-1">
    @include('agen.includes.sidebar-kanan')
</div>
@endsection
