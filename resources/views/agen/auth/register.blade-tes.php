@extends('agen.layouts.guest')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="text-center pb-4 pt-4">
                <img src="/img/icon/eID Black.png" alt="CNL" width="200">
            </div>
            <div class="card">
                <div class="card-title">
                    <h4 class="text-center pt-4">
                        Daftar Akun
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('agen.register') }}">
                        @csrf

                        <div class="row">
                            <div class="col-md-12" style="padding: 0rem 1rem 0rem 1rem !important;">
                                <div class="form-floating mb-3" style="padding: 0rem !important;">
                                    <input placeholder="nama lengkap" autocomplete="new-password" id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                    <label for="name" >{{ __('Nama Lengkap') }}</label>
                                </div>

                                <div class="form-floating mb-3">
                                    <input placeholder="name@mail.com" autocomplete="new-password" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                    <label for="email">{{ __('Alamat E-Mail') }}</label>
                                </div>

                                <div class="form-floating mb-3">
                                    <input placeholder="******" autocomplete="new-password" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    <label for="password">{{ __('Password') }}</label>
                                </div>

                                <div class="form-floating mb-3">
                                    <input placeholder="******" autocomplete="new-password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    <label for="password-confirm">{{ __('Konfirmasi Password') }}</label>
                                </div>

                                <hr>

                                <div class="form-floating mb-3" style="padding: 0rem !important;">
                                    <input placeholder="nama panggilan" autocomplete="new-password" id="nama_rek" type="text" class="form-control{{ $errors->has('nama_rek') ? ' is-invalid' : '' }}" name="nama_rek" value="{{ old('nama_rek') }}" required autofocus>

                                    @if ($errors->has('nama_rek'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('nama_rek') }}</strong>
                                        </span>
                                    @endif
                                    <label for="nama_rek" >{{ __('Nama Panggilan') }}</label>
                                </div>

                            </div>
                        </div>
                        <div class="form-floating mb-3 d-flex justify-content-center">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-danger btn-pill btn-block btn-eid">
                                    {{ __('Daftar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
