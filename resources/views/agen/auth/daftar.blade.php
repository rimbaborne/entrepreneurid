@extends('agen.layouts.guest')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="text-center pb-4 pt-4">
                <img src="/img/icon/eID Black.png" alt="" width="200">
            </div>
            <div class="card">
                <div class="card-title">
                    <h4 class="text-center pt-4">
                        Pendaftaran Agen Baru
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('agen.register') }}">
                        @csrf
                        <input type="text" name="agenbaru" value="1" hidden>
                        <div class="row mb-2">
                            <div class="col">
                                <label class="form-label font-weight-bold" style="font-size: 13px; margin-bottom: -5px;">Nama Lengkap</label>
                                <input placeholder="Nama Lengkap" autocomplete="new-password" id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col">
                                <div class="col-form-label">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" value="Laki-laki" name="gender" required="">
                                        <label class="form-check-label">Laki-laki</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" value="Perempuan" name="gender">
                                        <label class="form-check-label">Perempuan</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col">
                                <label style="padding-right: 10px">Tanggal Lahir</label>
                                <select name="tgl">
                                    @for ($a = 1; $a <= 31; $a++)
                                        <option value="{{ $a }}">{{ $a }}</option>
                                    @endfor
                                </select>
                                <select name="bln">
                                    @for ($a = 1; $a <= 12; $a++)
                                        <option value="{{ $a }}">{{ $a }}</option>
                                    @endfor
                                </select>
                                <select name="thn">
                                    @for ($a = 1950; $a <= 2015; $a++)
                                        <option value="{{ $a }}">{{ $a }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col">
                                <label class="form-label font-weight-bold" style="font-size: 13px; margin-bottom: -5px;">No. Handphone</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            +62
                                        </span>
                                    </div>
                                    <input id="notelp" type="number" name="notelp" value="{{ old('notelp') }}" class="form-control{{ $errors->has('notelp') ? ' is-invalid' : '' }}" maxlength="13" placeholder="No. Handphone WhatsApp" required="">
                                </div>
                                @if ($errors->has('notelp'))
                                        <span class="text-danger">Nomor Telepon Sudah Terdaftar</span><br>
                                @endif
                                <span class="help-block text-muted" style="font-size: 10px; font-weight: 700">Tidak Pakai Angka 0 . Contoh : 81234563789</span>
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row mb-2">
                            <div class="col">
                                <label class="form-label font-weight-bold" style="font-size: 13px; margin-bottom: -5px;">Kota Domisili</label>
                                <input autocomplete="new-password" type="text" class="form-control" name="kota" value="{{ strtoupper(old('kota')) }}" required autofocus>
                            </div>
                        </div>

                        <hr>

                        <h4 class="text-center">Akun</h4>
                        <div class="row mb-2">
                            <div class="col">
                                <label class="form-label font-weight-bold" style="font-size: 13px; margin-bottom: -5px;">Username</label>
                                <input placeholder="Username" autocomplete="new-password" id="username" type="username" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required>

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col">
                                <label class="form-label font-weight-bold" style="font-size: 13px; margin-bottom: -5px;">Email</label>
                                <input placeholder="Alamat Email" autocomplete="new-password" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col">
                                <label class="form-label font-weight-bold" style="font-size: 13px; margin-bottom: -5px;">Password</label>
                                <input placeholder="Password" autocomplete="new-password" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col">
                                <label class="form-label font-weight-bold" style="font-size: 13px; margin-bottom: -5px;">Ulangi Password</label>
                                <input placeholder="Konfirmasi Password" autocomplete="new-password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                <span id='message'></span>
                            </div>
                        </div>

                        <hr>
                        <div class="alert alert-primary text-justify" role="alert">
                            <i class="fas fa-check-circle"></i> Silahkan isi form rekening dibawah ini, karena diperlukan untuk mentransfer komisi anda nantinya.
                        </div>
                        <div class="row mb-3">
                            <div class="col">
                                <label class="form-label font-weight-bold" style="font-size: 13px; margin-bottom: -5px;">Nama Pemilik Rekening</label>
                                <input autocomplete="new-password" type="text"
                                    name="nama_rek" value="{{ strtoupper(old('nama_rek')) }}" class="form-control" placeholder="Nama Pemilik Rekening" required="">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col">
                                <label class="form-label font-weight-bold" style="font-size: 13px; margin-bottom: -5px;">Nomor Rekening</label>
                                <input autocomplete="new-password" type="text"
                                    name="no_rek" value="{{ strtoupper(old('no_rek')) }}" class="form-control" placeholder="Nomor Rekening" required="">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col">
                                <label class="form-label font-weight-bold" style="font-size: 13px; margin-bottom: -5px;">Nama Bank</label>
                                <input autocomplete="new-password" type="text"
                                    name="bank_rek" value="{{ strtoupper(old('bank_rek')) }}" class="form-control" placeholder="Nama Bank" required="">
                            </div>
                        </div>

                        <div class="row mb-2 justify-content-center">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-danger btn-pill btn-block btn-eid">
                                    Konfirmasi Akun <i class="fas fa-check"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row" style="padding-top: 10px">
                            <div class="col">
                                <div class="text-left">
                                    <a href="/login" style="color: rgb(65, 65, 75);">
                                        <i class="fas fa-angle-double-left"></i> Login
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("#nohp").on("input", function() {
        if (/^0/.nohp(this.value)) {
            this.value = this.value.replace(/^0/, "")
        }
    })
    $("input#username").on({
        keydown: function(e) {
            if (e.which === 32)
            return false;
        },
        change: function() {
            this.value = this.value.replace(/\s/g, "");
        }
    });

    $('#username').on('keypress', function(e) {
        if (e.which == 32){
            console.log('Space Detected');
            return false;
        }
    });

    $('#email').on('keypress', function(e) {
        if (e.which == 32){
            console.log('Space Detected');
            return false;
        }
    });

    $('#password, #password-confirm').on('keyup', function () {
    if ($('#password').val() == $('#password-confirm').val()) {
        $('#message').html('Konfirmasi Password Sesuai').css('color', 'green');
    } else
        $('#message').html('Konfirmasi Password Belum Sesuai').css('color', 'red');
    });
</script>
@endsection
