@extends('agen.layouts.guest')

@section('content')
<div class="container">
    <div class="text-center pb-4 pt-4">
        <img src="/img/icon/eID Black.png" alt="" width="200">
    </div>
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('agen.password.email') }}" aria-label="{{ __('Reset Password') }}">
                        @csrf
                        <div class="form-group row mb-3">
                            <div class="col-md-4">
                                <label for="email" class="col-form-label float-md-right">{{ __('E-Mail Address') }}</label>
                            </div>

                            <div class="col-md-6">
                                <input autocomplete="new-password" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
