@extends('agen.layouts.guest')

@section('content')
<script src="https://unpkg.com/bootstrap-show-password@1.2.1/dist/bootstrap-show-password.min.js"></script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="text-center pb-4 pt-4">
                <img src="/img/icon/eID Black.png" alt="CNL" width="200">
            </div>
            <div class="card" >
                <div class="card-body">
                    <form method="POST" action="{{ route('agen.login') }}" aria-label="{{ __('Login') }}">
                        @csrf

                        <div class="row">
                            <div class="col-md-12" style="padding: 1rem 2rem 0rem 2rem !important;">
                                <div class="form-floating mb-3">
                                    <input placeholder="nama@email.com" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                    <label>{{ __('E-mail') }}</label>
                                </div>

                                <div class="form-floating mb-3">
                                    <input autocomplete="new-password"
                                    {{-- data-toggle="password" --}}
                                      placeholder="******" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                    <label>{{ __('Password') }}</label>
                                </div>

                                <div class="form-check mb-3">
                                    <input autocomplete="new-password" class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Pengingat Login') }}
                                    </label>
                                </div>

                                <div class="form-floating mb-3 row mt-4 justify-content-center">
                                    <div class="col-md-8">
                                        <button type="submit" class="btn btn-danger btn-eid btn-pill btn-block">
                                            {{ __('Login') }}
                                        </button>
                                        @if (Route::has('agen.password.request'))
                                            <div class="text-center">
                                                <a class="btn warna-eid" href="{{ route('agen.password.request') }}">
                                                    {{ __('Lupa Password ? ') }}
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$("#password").password('toggle');
</script>
@endsection
