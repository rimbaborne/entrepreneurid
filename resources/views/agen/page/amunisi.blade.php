@extends('agen.layouts.app')

@section('content')
<div class="col-sm-6 pb-4">
    <div class="card-2 " style="min-height: 500px">
        <div class="card-body">
            <h3 class="card-title mb-4" style="font-weight:300; margin-bottom: .4rem;">
                Amunisi
            </h3>
            <div class="row">
                <div class="col-6 col-sm-4">
                    <a href="https://agen-entrepreneurid.com/leadmagnet/" style="text-decoration: none" target="_blank">
                        <div class="card p-2">
                            <img src="/aset/icon/lead-magnet.png" class="img-fluid pt-2 pl-4 pr-4" alt="">
                            <div class="text-center pb-2" style="color: #23282c;">
                                Lead Magnet
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-4">
                    <a href="#" style="text-decoration: none" target="_blank">
                        <div class="card p-2">
                            <img src="/aset/icon/event.png" class="img-fluid pt-2 pl-4 pr-4" alt="">
                            <div class="text-center pb-2" style="color: #23282c;">
                                Event
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-6 col-sm-4">
                    <a href="https://drive.google.com/drive/folders/14GUDQe7Bna0SUpJZ384KYZktKWvtxe1S?usp=sharing" style="text-decoration: none" target="_blank">
                        <div class="card p-2">
                            <img src="/aset/icon/evergreen.png" class="img-fluid pt-2 pl-4 pr-4" alt="">
                            <div class="text-center pb-2" style="color: #23282c;">
                                Produk Evergreen
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-3 order-sm-1">
    @include('agen.includes.sidebar-kanan')
</div>
@endsection
