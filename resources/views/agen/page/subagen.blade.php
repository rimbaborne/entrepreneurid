@extends('agen.layouts.verifikasi')

@section('verifikasi')
<div class="col-md-9 pb-4">
    <div class="card-2 " style="min-height: 500px">
        <div class="card-body">
            <h3 class="card-title" style="font-weight:300; margin-bottom: .4rem;">
                Data Sub Agen
            </h3>

                <form class="row" action="{{ url()->current() }}">
                    <div class="col-12">
                        <div class="row mt-4" style=" margin-right: 0px;padding-left: 15px;">
                            <div class="pb-2 col-6" style="border: 1px solid #eee;" >
                                <div style="padding-top: 5px; padding-bottom: 5px">
                                    <label class="form-check-label">Nama</label>
                                </div>
                                <input class="form-control" type="text" value="{{ request()->nama ?? '' }}" name="nama" style="font-size: small" />
                            </div>
                            <div class="pb-2 col-6" style="border: 1px solid #eee;" >
                                <div style="padding-top: 5px; padding-bottom: 5px">
                                    <label class="form-check-label">Email</label>
                                </div>
                                <input class="form-control" type="text" value="{{ request()->email ?? '' }}" name="email" style="font-size: small" />
                            </div>

                        </div>
                    </div>
                    <div class="col-12 mt-2">
                        <div class="row">
                            <div class="col-md-8">
                            </div>
                            <div class="col-md-4 text-right" style="padding-bottom:20px;">
                                <div class="align-middle">
                                    <button type="submit" class="btn btn-primary btn-sm btn-block" > <i class="fas fa-search"></i> Cari</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

            <div class="card mb-2 bg-gray-500 text-white" style="border-radius: 5px; font-size: 12px;">
                <div class="row p-2 font-weight-bold">
                    <div class="col-8">Nama</div>
                    <div class="col-4 text-center">Transaksi Closing</div>
                </div>
            </div>
            @php
                $first  = 0;
                $end    = 0;
            @endphp
            @foreach ($dataagen as $key => $data)
                <div class="card mb-2" style="border-radius: 5px; font-size: 12px">
                    <div class="row p-2 align-items-center">
                        <div class="col-12 d-sm-none">
                            <hr class="mt-2 mb-2 ml-0 mr-0">
                        </div>
                        <div class="col-8">
                            <div class="font-weight-bold" style="color: #ec1b25">
                                {{ $data->name }} | {{ $data->email }}
                            </div>
                            <div class="text-muted">
                                +{{ $data->kode_notelp }}{{ $data->notelp }}
                            </div>
                        </div>
                        <div class="col-4 text-center">
                            <div class="font-weight-bold" style="color: #222222; font-size: 13px">
                                {{ $data->totaltransaksisubagen($data->id) }}
                            </div>
                        </div>
                    </div>
                </div>
                @php
                    $first  = $dataagen->firstItem();
                    $end    = $key + $dataagen->firstItem();
                @endphp
            @endforeach

            <div class="row mt-4">
                <div class="col-7" style="font-size: 12px">
                    {!! $first !!} - {!! $end !!} Dari {!! $dataagen->total() !!} Data Sub Agen
                </div><!--col-->

                <div class="col-5">
                    <div class="float-right">
                        {!! $dataagen->appends(request()->query())->links() !!}
                    </div>
                </div><!--col-->
            </div><!--row-->

        </div>
    </div>
</div>
{{-- <div class="col-sm-3 order-sm-1">
    @include('agen.includes.sidebar-kanan')
</div> --}}
@endsection
