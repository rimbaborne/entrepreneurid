@extends('agen.layouts.verifikasi')

@section('verifikasi')
<div class="col-md-9 pb-4">
    <div class="card-2 " style="min-height: 500px">
        <div class="card-body">
            <h3 class="card-title" style="font-weight:300; margin-bottom: .4rem;">
                Transaksi
            </h3>
            <div class="row">
                <div class="col-12 text-right">
                    <a href="/lead-magnet-export" target="_blank" class="btn btn-sm btn-outline-warning">
                        Download Data Produk Lead Magnet
                        <i class="fas fa-download"></i>
                    </a>
                </div>
                <div class="col-8"></div>
                <div class="col-4">
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-right pb-4">
                    <a href="/transaksi-export" target="_blank" class="btn btn-sm btn-success">
                        Download Data Transaksi
                        <i class="fas fa-download"></i>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-6 col-sm-3">
                    <div class="card overflow-hidden " style="border-radius: 5px;">
                        <div class="card-body p-2 align-items-center">
                            <div>
                                <div class="text-value text-dark">{{ $belumaktif }}</div>
                                <div class="text-muted float-right small">Belum Aktif</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="card overflow-hidden " style="border-radius: 5px;">
                        <div class="card-body p-2 align-items-center">
                            <div>
                                <div class="text-value text-warning">{{ $konfirmasi }}</div>
                                <div class="text-muted float-right small">Konfirmasi</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="card overflow-hidden " style="border-radius: 5px;">
                        <div class="card-body p-2 align-items-center">
                            <div>
                                <div class="text-value text-primary">{{ $aktif }}</div>
                                <div class="text-muted float-right small">Aktif</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="card overflow-hidden " style="border-radius: 5px;">
                        <div class="card-body p-2 align-items-center">
                            <div>
                                <div class="text-value text-success">{{ $total }}</div>
                                <div class="text-muted float-right small">Total</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <form action="{{ url()->current() }}">
                    <div class="col-12">
                        <div class="d-print-none row mt-4" style=" margin-right: 0px;padding-left: 15px;">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="pb-2 col-6" style="border: 1px solid #eee;" >
                                        <div style="padding-top: 5px; padding-bottom: 5px">
                                            <label class="form-check-label">Nama</label>
                                        </div>
                                        <input class="form-control" type="text" value="{{ request()->nama ?? '' }}" name="nama" style="font-size: small" />
                                    </div>
                                    <div class="pb-2 col-6" style="border: 1px solid #eee;" >
                                        <div style="padding-top: 5px; padding-bottom: 5px">
                                            <label class="form-check-label">Email</label>
                                        </div>
                                        <input class="form-control" type="text" value="{{ request()->email ?? '' }}" name="email" style="font-size: small" />
                                    </div>
                                </div>
                            </div>

                            <div class="pb-2 col" style="border: 1px solid #eee;">
                                <div  style="padding-top: 5px; padding-bottom: 5px">
                                    <label class="form-check-label">Produk</label>
                                </div>
                                <select id="produk" name="produk" class="form-control">
                                    <option value="">Semua</option>
                                    @foreach ($produklist as $data)
                                        <option value="{{ $data->id_event }}">{{ data_get($data->event->produk, 'nama') }} ({{ data_get($data->event, 'id_event') }})</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="pb-2 col" style="border: 1px solid #eee;">
                                <div  style="padding-top: 5px; padding-bottom: 5px">
                                    <label class="form-check-label">Status</label>
                                </div>
                                <select id="status" name="status" class="form-control">
                                    <option value="">Semua</option>
                                    <option value="1">Belum Aktif</option>
                                    <option value="2">Konfirmasi</option>
                                    <option value="3">Aktif</option>
                                </select>
                            </div>
                            <div class="pb-2 col" style="border: 1px solid #eee;">
                                <div  style="padding-top: 5px; padding-bottom: 5px">
                                    <label class="form-check-label">Jenis</label>
                                </div>
                                <select id="jenis" name="jenis" class="form-control">
                                    <option value="">Semua</option>
                                    <option value="1">Transaksi Manual</option>
                                    <option value="2">Transaksi Afiliasi</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-2">
                        <div class="row">
                            <div class="col-md-8">
                            </div>
                            <div class="col-md-4 text-right" style="padding-bottom:20px;">
                                <div class="align-middle">
                                    <button type="submit" class="btn btn-primary btn-sm btn-block" > <i class="fas fa-search"></i> Cari</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>

            <div class="card mb-2 bg-gray-500 text-white" style="border-radius: 5px; font-size: 12px;">
                <div class="row p-2 font-weight-bold">
                    <div class="col-3 d-none d-sm-block">Status</div>
                    <div class="col-6 col-sm-5">Nama</div>
                    <div class="col-6 col-sm-4">Produk</div>
                </div>
            </div>
            @php
                $first  = 0;
                $end    = 0;
            @endphp
            @foreach ($transaksi as $key => $data)
                <a href="{{ route('agen.page.produk.pembayaran') }}?id={{ $data->uuid }}" style="text-decoration: none;">
                <div class="card mb-2" style="border-radius: 5px; font-size: 12px">
                    <div class="row p-2">
                        <div class="col-12 col-sm-3">
                            {{-- Jika Tampilan desktop/laptop --}}
                            <div class="row d-none d-sm-block">
                                @if ($data->status == 1)
                                    <div class="col-2 pt-1 float-left d-none d-sm-block" style="padding-right: 40px">
                                        <form action="" onsubmit="return confirm('Apakah Anda yakin, data {{ $data->email }} dihapus ?');">
                                            @csrf
                                            <input type="hidden" name="metode" value="hapus">
                                            <input type="hidden" name="id" value="{{ $data->id }}">
                                            <button class="btn btn-sm btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                                    </div>
                                @endif
                                <div class="pl-3" style="">
                                    @if ($data->status == 1)
                                        <div class="badge badge-danger">Belum Aktif</div>
                                    @elseif ($data->status == 2)
                                        <div class="badge badge-warning">Menuggu Konfirmasi</div>
                                    @elseif ($data->status == 3)
                                        <div class="badge badge-success">Aktif</div>
                                    @endif
                                    <div class="text-muted font-weight-bold">
                                        @if ($data->jenis == 1)
                                            Transaksi Manual
                                        @elseif ($data->jenis == 2)
                                            Transaksi Afiliasi
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{-- Jika Tampilan mobile/hp --}}
                            <div class="row d-sm-none">
                                @if ($data->status == 1)
                                    <div class="pt-1 pull-right position-absolute " style="right: 20px">
                                        <form action="" onsubmit="return confirm('Apakah Anda yakin, data {{ $data->email }} dihapus ?');">
                                            @csrf
                                            <input type="hidden" name="metode" value="hapus">
                                            <input type="hidden" name="id" value="{{ $data->id }}">
                                            <button class="btn btn-sm btn-danger">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>
                                    </div>
                                @elseif ($data->status == 2)
                                    <div class="pt-1 pull-right position-absolute " style="right: 20px">
                                        <button class="btn btn-sm btn-warning btn-pill">
                                            <i class="fas fa-angle-double-right"></i>
                                        </button>
                                    </div>
                                @elseif ($data->status == 3)
                                    <div class="pt-1 pull-right position-absolute " style="right: 20px">
                                        <button class="btn btn-sm btn-success btn-pill">
                                            <i class="fas fa-check"></i>
                                        </button>
                                    </div>
                                @endif
                                <div class="pl-3" style="">
                                    @if ($data->status == 1)
                                        <div class="badge badge-danger">Belum Aktif</div>
                                    @elseif ($data->status == 2)
                                        <div class="badge badge-warning">Menuggu Konfirmasi</div>
                                    @elseif ($data->status == 3)
                                        <div class="badge badge-success">Aktif</div>
                                    @endif
                                    <div class="text-muted font-weight-bold">
                                        @if ($data->jenis == 1)
                                            Transaksi Manual
                                        @elseif ($data->jenis == 2)
                                            Transaksi Afiliasi
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 d-sm-none">
                            <hr class="mt-2 mb-2 ml-0 mr-0">
                        </div>
                        <div class="col-6 col-sm-5">
                            <div class="font-weight-bold" style="color: #ec1b25">
                                {{ $data->nama }} | {{ $data->panggilan }}
                            </div>
                            <div class="text-muted">
                                +{{ $data->kode_nohp }}{{ $data->nohp }} | {{ $data->email }}
                            </div>
                        </div>
                        <div class="col-6 col-sm-4">
                            <div class="font-weight-bold" style="color: #222222">
                                {{ data_get($data->event->produk, 'nama') }}
                            </div>
                            <div class="text-muted">
                                Daftar : {{ $data->created_at }}
                            </div>
                        </div>
                    </div>
                </div>
                </a>
                @php
                    $first  = $transaksi->firstItem();
                    $end    = $key + $transaksi->firstItem();
                @endphp
            @endforeach

            <div class="row mt-4">
                <div class="col-7" style="font-size: 12px">
                    {!! $first !!} - {!! $end !!} Dari {!! $transaksi->total() !!} Data Transaksi
                </div><!--col-->

                <div class="col-5">
                    <div class="float-right">
                        {!! $transaksi->appends(request()->query())->links() !!}
                    </div>
                </div><!--col-->
            </div><!--row-->

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
          $("#produk").val("{!! request()->produk !!}");
          $("#status").val("{!! request()->status !!}");
          $("#jenis").val("{!! request()->jenis !!}");
    });
</script>
{{-- <div class="col-sm-3 order-sm-1">
    @include('agen.includes.sidebar-kanan')
</div> --}}
@endsection
