@extends('agen.layouts.verifikasi')

@section('verifikasi')
<link rel="stylesheet" type="text/css" href="/filepond/app.css">
<div class="col-md-9 pb-4">
    <div class="card-2 " style="min-height: 500px">
        <div class="card-body">
            <h3 class="card-title" style="font-weight:300; margin-bottom: 1rem;">
               <a href="{{ route('agen.page.produk.transaksi') }}"  style="color: #ec1b25"> Transaksi </a> <i class="fa fa-sm fa-chevron-right"></i> Pembayaran
            </h3>
            <div class="row">
                <div class="col-md-8">

                    <div class="card overflow-hidden">
                        <div class="card-body">
                            <table class="mb-4">
                                <tbody>
                                    <tr>
                                        <td>Atas Nama</td>
                                        <td>:</td>
                                        <td style="font-weight: 700">{{ $data->nama }}</td>
                                    </tr>
                                    <tr>
                                        <td>No. Hp</td>
                                        <td>:</td>
                                        <td style="font-weight: 700">+{{ $data->kode_nohp }}{{ $data->nohp }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td style="font-weight: 700">{{ $data->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>Waktu Pemesanan</td>
                                        <td>:</td>
                                        <td style="font-weight: 700">{{ $data->created_at }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="row pb-2">
                                <div class="col-12 text-center pb-2">
                                    <img src="/img/produk/{{ $data->event->gambar }}" alt="" width="100">
                                    <h4 class="text-center" style="font-weight: 600">{{ $data->event->produk->nama }}</h4>
                                </div>
                                @if ($data->jenis == 2)
                                    <div class="col-12 pt-2">
                                        <div class="float-left"><span>Transaksi</span></div>
                                        <div class="text-right" style="font-weight: 600; font-size:15px"><span>AFILIASI</span></div>
                                    </div>
                                @endif
                                {{-- <div class="col-12 pt-2">
                                    <div class="float-left"><span>Kode Unik Transfer</span></div>
                                    <div class="text-right" style="font-weight: 600; font-size:15px"><span>{{ $data->kodeunik }}</span></div>
                                </div> --}}
                                @if ($data->event->produk->jenis == 'ECOURSE')
                                    <div class="col-12 pt-2">
                                        <div class="float-left"><span>Total Harga</span></div>
                                        <h5 class="text-right" style="font-weight: 700"><span>Rp {{ strrev(implode('.',str_split(strrev(strval($data->total)),3))) }}</span></h5>
                                    </div>
                                @elseif ($data->event->produk->jenis == 'BUKU')
                                    @if ($data->book->no_resi)
                                        <div class="col-12 pt-2">
                                            <div class="float-left"><span>No. Resi</span></div>
                                            <h5 class="text-right" style="font-weight: 400"><span>{{ $data->book->no_resi }}</span></h5>
                                        </div>
                                    @endif
                                    <div class="col-12 bg-light pt-2 pb-2 rounded">
                                        <div class="text-left"><span>Alamat</span></div>
                                        <div class="text-right" style="font-weight: 400; font-size: 11px">
                                            <span>
                                                {{ $data->book->alamat }}. Kec. {{ $data->book->kecamatan }}, {{ $data->book->kota }}, Prov. {{ $data->book->provinsi }}
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-12 pt-2">
                                        <div class="float-left"><span>Jumlah Buku</span></div>
                                        <h5 class="text-right" style="font-weight: 400"><span>{{ $data->book->jumlah_buku }} Buku</span></h5>
                                    </div>
                                    <div class="col-12 bg-light pt-2 rounded">
                                        <div class="float-left"><span>Kurir</span></div>
                                        <h5 class="text-right" style="font-weight: 400"><span>{{ $data->book->kurir }}</span></h5>
                                    </div>
                                    <div class="col-12 pt-2">
                                        <div class="float-left"><span>Harga</span></div>
                                        <h5 class="text-right" style="font-weight: 400"><span>Rp {{ strrev(implode('.',str_split(strrev(strval($data->book->harga)),3))) }}</span></h5>
                                    </div>
                                    <div class="col-12 bg-light pt-2 rounded">
                                        <div class="float-left"><span>Ongkir</span></div>
                                        <h5 class="text-right" style="font-weight: 400"><span>Rp {{ strrev(implode('.',str_split(strrev(strval($data->book->ongkir)),3))) }}</span></h5>
                                    </div>
                                    <div class="col-12 pt-2">
                                        <div class="float-left"><span>Total</span></div>
                                        <h5 class="text-right" style="font-weight: 700"><span>Rp {{ strrev(implode('.',str_split(strrev(strval($data->book->total)),3))) }}</span></h5>
                                    </div>
                                @endif

                            </div>

                            @if ($data->status == 1)
                                @if ($data->event->open == 1)
                                    <form action="{{ route('agen.page.produk.pembayaransimpan') }}" method="POST">
                                        @csrf
                                        <input value="{{ request()->id }}" name="id" hidden>
                                        @if ($data->jenis == 1)
                                            <div class="form-group row">
                                                <label class="form-label col-12">Rekening Tujuan Yang Ditransfer</label>
                                                <div class="col-12">
                                                    <select class="form-control" name="rekening" required>
                                                        <option value="Mandiri - 1360007600528">Mandiri - 1360007600528 a/n Wulandari Tri</option>
                                                        <option value="BCA - 1911488766">BCA - 1911488766 a/n Wulandari Tri</option>
                                                        <option value="BRI - 012101009502532">BRI - 012101009502532 a/n Wulandari Tri</option>
                                                        <option value="BNI - 1238568920">BNI - 1238568920 a/n Wulandari Tri</option>
                                                    </select>
                                                </div><!--col-->
                                            </div>
                                            <div class="form-group row">
                                                <div class="col">
                                                    <input type="file" class="upload-buktitransfer" required/>
                                                </div><!--col-->
                                            </div>

                                            <div class="row mt-4">
                                                <div class="col">
                                                    <div class="form-group mb-0 clearfix d-none" id="btn-submit">
                                                        {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(244,67,54"> --}}
                                                        <button type="submit" class="btn btn-pill btn-block btn-primary btn-block">
                                                            Upload Pembayaran <i class="fas fa-paper-plane"></i>
                                                        </button>
                                                    </div><!--form-group-->
                                                </div><!--col-->
                                            </div><!--row-->
                                        @elseif ($data->jenis == 2)
                                            <div class="form-group row mt-4">
                                                <label class="form-label col-12">Upload Bukti Transfer Afiliasi mohon gunakan link yang kami kirim ke Email. Terima Kasih.</label>
                                                <label class="form-label col-12">
                                                    <a href="https://konfirmasi.{{ $data->event->produk->domain }}/invoice" target="_blank">
                                                    konfirmasi.{{ $data->event->produk->domain }}/invoice
                                                    </a>
                                                </label>
                                            </div>
                                            {{-- <div class="form-group row">
                                                <div class="col">
                                                    <input type="file" class="upload-buktitransfer-afiliasi" required/>
                                                </div><!--col-->
                                            </div>

                                            <div class="row mt-4">
                                                <div class="col">
                                                    <div class="form-group mb-0 clearfix">
                                                        <button type="submit" class="btn btn-pill btn-block btn-primary btn-block">
                                                            Upload Pembayaran <i class="fas fa-paper-plane"></i>
                                                        </button>
                                                    </div><!--form-group-->
                                                </div><!--col-->
                                            </div><!--row--> --}}
                                        @endif
                                    </form>
                                @else
                                    <div class="form-group row mt-4">
                                        <div class="form-label col-12">
                                            <div class="alert alert-dark">
                                                Mohon Maaf, Kami sudah menutup pemesanan transaksi / Closed Order. Terima Kasih.
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @else
                                <div class="row">
                                    <div class="col-12">
                                        <div class="float-left"><span>Status</span></div>
                                        @if ($data->status == 2)
                                            <h4 class="text-right" style="font-weight: 700">
                                                <span class="btn btn-sm btn-warning btn-pill">
                                                    Menunggu Konfirmasi
                                                </span>
                                            </h4>
                                        @elseif ($data->status == 3)
                                            <h4 class="text-right" style="font-weight: 700">
                                                <span class="btn btn-sm btn-success btn-pill">
                                                    Aktif
                                                </span>
                                            </h4>
                                        @endif
                                        <h4 class="text-right" style="font-weight: 700">
                                            <span></span>
                                        </h4>
                                    </div>
                                    <div class="col-12 pt-2">
                                        <div class="float-left"><span>Rek. Transfer  eID</span></div>
                                        <div class="text-right" style="font-weight: 500; font-size:15px">
                                            <div style="font-weight: 500; font-size:10px">a/n WULANDARI TRI MAHARANI</div>
                                            <div>{{ $data->keterangan }}</div>
                                        </div>
                                    </div>
                                    @if ($data->jenis == 1)
                                    <div class="col-12 pt-2">
                                        <div class="float-left"><span>Bukti Transfer</span></div>
                                        <div class="text-right">
                                            <img class="img img-fluid rounded" width="250" src="/app/public/bukti-transfer/{{ $data->bukti_tf }}" alt="">
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            @endif
                        </div><!-- card-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
<script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.js"></script>
<script src="https://unpkg.com/filepond-plugin-image-resize/dist/filepond-plugin-image-resize.js"></script>
<script src="/filepond/app.js"></script>
<script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>
<script>
    $(function(){
        $.fn.filepond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginFileValidateType,
            FilePondPluginFileValidateSize,
            FilePondPluginImageResize
        );
    });

    $(function(){
        $('.upload-buktitransfer').filepond({
            labelIdle: '<span class="filepond--label-action"> Upload File/Foto Bukti Transfer.</span>',
            allowMultiple: false,
            acceptedFileTypes: "image/png, image/jpeg",
            allowFileSizeValidation: true,
            maxFileSize: '10MB',
            server: {
                url: '/transaksi/pembayaran/uploadbuktitransfer',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: (response) => response.key,
                    onerror: (response) => response.data,
                    ondata: (formData) => {
                        return console.log('sukses');
                    }
                }
            }
        });

        $('.upload-buktitransfer').on('FilePond:processfile', function(e) {
            document.getElementById("btn-submit").classList.remove('d-none');
        });

    });
    $(function(){
        $('.upload-buktitransfer-afiliasi').filepond({
            labelIdle: '<span class="filepond--label-action"> Upload File/Foto Bukti Transfer.</span>',
            allowMultiple: false,
            acceptedFileTypes: "image/png, image/jpeg",
            allowFileSizeValidation: true,
            maxFileSize: '10MB',
            server: {
                url: '/transaksi/pembayaran/uploadbuktitransfer-afiliasi',
                process: {
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    onload: (response) => response.key,
                    onerror: (response) => response.data,
                    ondata: (formData) => {
                        return console.log('sukses');
                    }
                }
            }
        });

        $('.upload-buktitransfer').on('FilePond:processfile', function(e) {
            document.getElementById("btn-submit").classList.remove('d-none');
        });

    });
    $(document).ready(function () {
        $('#fform').on('submit',function(e) {
            if (pond.status != 4) {
                return false;
            }
            $(this).find(':input[type=submit]').hide();
            return true;
        });
    });

</script>
@endsection
