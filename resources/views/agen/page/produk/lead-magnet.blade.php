@extends('agen.layouts.verifikasi')

@section('verifikasi')
<div id="card" class="col-md-9 pb-4">
    <div class="card-2 " style="min-height: 500px">
        <div class="card-body">
            <h3 class="card-title mb-4" style="font-weight:300; margin-bottom: .4rem;">
                Produk
            </h3>
            <div class="row">
                @php
                    $first  = 0;
                    $end    = 0;
                @endphp

                @foreach ($produkevent as  $key => $data)
                    <div id="respon" class="col-4">
                        @if ($data->open == 1 && $data->start < Carbon\Carbon::now() && $data->end > Carbon\Carbon::now() && $data->start > Carbon\Carbon::now()->subDays(5))
                            <div class="card produk border-success order-card" style="padding-top: 20px; height: 400px; border: 1px solid">
                        @else
                            <div class="card produk order-card" style="padding-top: 20px; height: 400px">
                        @endif
                            <div class="card-block">
                                @if ($data->open == 1 && $data->start < Carbon\Carbon::now() && $data->end > Carbon\Carbon::now() && $data->start > Carbon\Carbon::now()->subDays(5))
                                    <span class="badge badge-success bg-success" style="font-size: 13px; border-radius: 0 5px 5px 0; position: absolute; z-index: 99">
                                        NEW
                                    </span>
                                @endif
                                <div class="col-12">
                                    <div class="text-center" style="padding-bottom: 10px">
                                        <img class="img-fluid" src="/img/produk/{{ $data->gambar }}" alt="{{ data_get($data->produk, 'nama') }}" style="height: 150px">
                                    </div>
                                    <h6 class="text-center">{{ data_get($data->produk, 'nama') }}</h6>
                                    <h4 class="text-right" style="font-weight: 700"><span>Rp {{ strrev(implode('.',str_split(strrev(strval($data->harga)),3))) }}</span></h4>
                                    <div class="" style="padding-bottom: 15px">
                                        <small class="text-muted">Komisi</small>
                                        <br>
                                        @if (!null == Auth::guard('agen')->user()->status_agen)
                                            <strong class="h5" style="font-weight: 600; color:crimson">Rp {{ strrev(implode('.',str_split(strrev(strval($data->komisi_jv)),3))) }}</strong>
                                        @else
                                            <strong class="h5" style="font-weight: 600; color:crimson">Rp {{ strrev(implode('.',str_split(strrev(strval($data->komisi)),3))) }}</strong>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @if ($data->open == 1 && $data->start < Carbon\Carbon::now() && $data->end > Carbon\Carbon::now())
                                <div class="position-absolute" style="right: 0; top: 290px; padding:20px; width: 100%">
                                    @if (null == $data->link_sales)
                                        <p style="padding-top: 20px"></p>
                                        <a href="/produk/pesan?id={{  $data->uuid  }}" class="btn btn-block btn-pill btn-primary" style="font-weight: 600;">Pesan</a>
                                    @else
                                        <label id="link_sales{{ $key+ $produkevent->firstItem() }}" hidden>{{ $data->link_sales }}?ref={{ Auth::guard('agen')->user()->id }}</label>
                                        <button class="btn btn-block btn-pill btn-warning btn-sm" style="font-weight: 600;"  onclick="copyToClipboard('#link_sales{{ $key+ $produkevent->firstItem() }}'); kopi()">Copy Link <i class="fas fa-copy"></i></button>
                                        @if ($data->link_form == '-')
                                            <p style="padding-top: 20px"></p>
                                        @else
                                            @if (Auth::guard('agen')->user()->status_agen == null)
                                                <a href="/produk/pesan?id={{  $data->uuid  }}" class="btn btn-block btn-pill btn-primary" style="font-weight: 600;">Pesan</a>
                                            @endif
                                        @endif
                                    @endif
                                </div>
                            @else
                                <div class="position-absolute" style="right: 0; top: 325px; padding:20px; width: 100%">
                                    <button class="btn btn-block btn-pill btn-light" style="font-weight: 600; " disabled>Closed Order</button>
                                </div>
                            @endif
                        </div>
                    </div>
                    @php
                        $first  = $produkevent->firstItem();
                        $end    = $key + $produkevent->firstItem();
                    @endphp
                @endforeach
                {{-- <div id="respon" class="col-4">
                    <div class="card order-card" style="padding: 20px; height: 400px">
                        <div class="card-block">
                            <div class="">
                                <div class="text-center" style="padding-bottom: 10px">
                                    <img class="img-fluid" src="/img/produk/MOM.png" alt="Mentoring Organic Marketing" style="height: 150px;">
                                </div>
                                <h6 class="text-right">Mentoring Organic Marketing</h6>
                                <h4 class="text-right" style="font-weight: 700"><span>Rp. 149.000</span></h4>
                                <div class="" style="padding-bottom: 15px">
                                    <small class="text-muted">Komisi</small>
                                    <br>
                                    <strong class="h5" style="font-weight: 600; color:crimson">Rp. 60.000</strong>
                                </div>
                            </div>
                        </div>

                        <div class="position-absolute" style="right: 0; top: 325px; padding:20px; width: 100%">
                            <button class="btn btn-block btn-pill btn-light" style="font-weight: 600; " disabled>Closed Order</button>
                        </div>
                    </div>
                </div> --}}

            </div>
            <div class="row mt-4">
                <div class="col">
                    <div class="float-right">
                        {!! $produkevent->appends(request()->query())->links() !!}
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div>
    </div>
</div>
@stack('before-scripts')
<script>
    function kopi() {
      alert("Link Sales Page Berhasil Di Copy !");
    }
    </script>
<script>
    function resize(x) {
        var element     = document.querySelectorAll("[id='respon']");;
        var elementcard = document.querySelectorAll("[id='card']");;

        for(var i = 0; i < element.length; i++)
            if (x.matches) { // If media query matches
                element[i].classList.remove("col-4");
                element[i].classList.add("col-6");
                element[i].classList.add("p-1");
            } else {
                element[i].classList.remove("p-1");
                element[i].classList.remove("col-6");
                element[i].classList.add("col-4");
            }

        for(var i = 0; i < elementcard.length; i++)
            if (x.matches) { // If media query matches
                elementcard[i].classList.add("pr-0");
                elementcard[i].classList.add("pl-0");
            } else {
                elementcard[i].classList.remove("pr-0");
                elementcard[i].classList.remove("pl-0");
            }
    }

    var x = window.matchMedia("(max-width: 700px)")
    resize(x) // Call listener function at run time
    x.addListener(resize) // Attach listener function on state changes
</script>
@stack('after-scripts')
@endsection
