@extends('agen.layouts.app')

@section('content')
<style>
    .menu-agen > a {
        text-decoration: none;
        decoration: none;
        color: #23292c;
    }
    .menu-agen > a:link, .menu-agen > a:visited {
        text-decoration: none;
        decoration: none;
    }
    .menu-agen > a:hover {
        text-decoration: none;
        decoration: none;
    }
</style>
<div class="col-sm-6 pb-4 order-2 order-md-1">
    <div class="card-2 " style="min-height: 500px">
        <div class="card-body">
            <h3 class="card-title" style="font-weight:300; margin-bottom: .4rem;">
                Menu Agen
            </h3>
        </div>
        <div class="row p-3">
            <div class="col-4 menu-agen">
                <a href="#" class="card ml-1 p-2 text-center">
                    <i class="pt-4 pb-4 pl-2" style="max-height:75px; font-weight:lighter;color: #ec1b25;">
                        <svg focusable="false" data-prefix="fal" data-icon="edit" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-edit fa-w-18 fa-fw fa-2x"><path fill="currentColor" d="M417.8 315.5l20-20c3.8-3.8 10.2-1.1 10.2 4.2V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h292.3c5.3 0 8 6.5 4.2 10.2l-20 20c-1.1 1.1-2.7 1.8-4.2 1.8H48c-8.8 0-16 7.2-16 16v352c0 8.8 7.2 16 16 16h352c8.8 0 16-7.2 16-16V319.7c0-1.6.6-3.1 1.8-4.2zm145.9-191.2L251.2 436.8l-99.9 11.1c-13.4 1.5-24.7-9.8-23.2-23.2l11.1-99.9L451.7 12.3c16.4-16.4 43-16.4 59.4 0l52.6 52.6c16.4 16.4 16.4 43 0 59.4zm-93.6 48.4L403.4 106 169.8 339.5l-8.3 75.1 75.1-8.3 233.5-233.6zm71-85.2l-52.6-52.6c-3.8-3.8-10.2-4-14.1 0L426 83.3l66.7 66.7 48.4-48.4c3.9-3.8 3.9-10.2 0-14.1z" class=""></path></svg>
                    </i>
                    <span class="pb-4">Input Data Manual</span>
                </a>
            </div>
            <div class="col-4 menu-agen">
                <a href="#" class="card ml-1 mr-1 p-2 text-center">
                    <i class="pt-4 pb-3 pl-2" style="max-height:75px; font-weight:lighter;color: #ec1b25;">
                        <svg focusable="false" data-prefix="fal" data-icon="link" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-link fa-w-16 fa-fw fa-2x"><path fill="currentColor" d="M301.148 394.702l-79.2 79.19c-50.778 50.799-133.037 50.824-183.84 0-50.799-50.778-50.824-133.037 0-183.84l79.19-79.2a132.833 132.833 0 0 1 3.532-3.403c7.55-7.005 19.795-2.004 20.208 8.286.193 4.807.598 9.607 1.216 14.384.481 3.717-.746 7.447-3.397 10.096-16.48 16.469-75.142 75.128-75.3 75.286-36.738 36.759-36.731 96.188 0 132.94 36.759 36.738 96.188 36.731 132.94 0l79.2-79.2.36-.36c36.301-36.672 36.14-96.07-.37-132.58-8.214-8.214-17.577-14.58-27.585-19.109-4.566-2.066-7.426-6.667-7.134-11.67a62.197 62.197 0 0 1 2.826-15.259c2.103-6.601 9.531-9.961 15.919-7.28 15.073 6.324 29.187 15.62 41.435 27.868 50.688 50.689 50.679 133.17 0 183.851zm-90.296-93.554c12.248 12.248 26.362 21.544 41.435 27.868 6.388 2.68 13.816-.68 15.919-7.28a62.197 62.197 0 0 0 2.826-15.259c.292-5.003-2.569-9.604-7.134-11.67-10.008-4.528-19.371-10.894-27.585-19.109-36.51-36.51-36.671-95.908-.37-132.58l.36-.36 79.2-79.2c36.752-36.731 96.181-36.738 132.94 0 36.731 36.752 36.738 96.181 0 132.94-.157.157-58.819 58.817-75.3 75.286-2.651 2.65-3.878 6.379-3.397 10.096a163.156 163.156 0 0 1 1.216 14.384c.413 10.291 12.659 15.291 20.208 8.286a131.324 131.324 0 0 0 3.532-3.403l79.19-79.2c50.824-50.803 50.799-133.062 0-183.84-50.802-50.824-133.062-50.799-183.84 0l-79.2 79.19c-50.679 50.682-50.688 133.163 0 183.851z" class=""></path></svg>
                    </i>
                    <span class="pb-4">Ambil Link Affiliasi</span>
                </a>
            </div>
            <div class="col-4 menu-agen">
                <a href="#" class="card mr-1 p-2 text-center">
                    <i class="pt-4 pb-4 pl-2" style="max-height:75px; font-weight:lighter;color: #ec1b25;">
                        <svg focusable="false" data-prefix="fal" data-icon="paper-plane" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-paper-plane fa-w-16 fa-fw fa-2x"><path fill="currentColor" d="M464 4.3L16 262.7C-7 276-4.7 309.9 19.8 320L160 378v102c0 30.2 37.8 43.3 56.7 20.3l60.7-73.8 126.4 52.2c19.1 7.9 40.7-4.2 43.8-24.7l64-417.1C515.7 10.2 487-9 464 4.3zM192 480v-88.8l54.5 22.5L192 480zm224-30.9l-206.2-85.2 199.5-235.8c4.8-5.6-2.9-13.2-8.5-8.4L145.5 337.3 32 290.5 480 32l-64 417.1z" class=""></path></svg>
                    </i>
                    <span class="pb-4">Kupon Naik Level</span>
                </a>
            </div>
        </div>
    </div>
</div>
<div class="col-sm-3 order-1 order-md-2">
    @include('agen.includes.sidebar-kanan')
</div>
@endsection
