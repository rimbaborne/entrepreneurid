@extends('agen.layouts.app')

@section('content')
<div class="col-md-9 pb-4">
    <div class="card " style="min-height: 500px">
        <div class="card-body">
            <form class="row" method="POST" action="/profile/update">
                @csrf
                <div class="col-md-12 mb-2">
                    <h3 class="text-muted display-4" style="margin-bottom: .4rem;">
                        Profile
                    </h3>
                </div>
                <div class="col-md-4 order-2 order-md-1 pb-4">
                    <h5 class="font-weight-bold">Informasi Akun</h5>
                    <div class="form-group">
                        <label class="mb-0">Nama Lengkap</label>
                        <input value="{{ Auth::guard('agen')->user()->name }}" class="form-control" type="text" name="nama" placeholder="Nama Lengkap" autocomplete="new-password">
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            <label class="mb-0">Nomor Hp (WA)</label>
                            <div class="input-group">
                                <div class="input-group-prepend" style="width: 40%; ">
                                    <span class="input-group-text" style="background: #fff">
                                        <select name="kode_notelp" id="kode_notelp" class="form-control" style="border: none; height: unset; padding: 0px;border-top-right-radius: 0; border-bottom-right-radius: 0;font-size: 12px">
                                            @include('agen.includes.phone-code')
                                        </select>
                                    </span>
                                </div>
                                <input value="{{ Auth::guard('agen')->user()->notelp }}" class="form-control" type="number" name="notelp" placeholder="Nomor Hp (WA)" autocomplete="new-password">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="mb-0">Username</label>
                        <input value="{{ Auth::guard('agen')->user()->username }}" class="form-control" type="text" placeholder="Username" autocomplete="new-password" disabled>
                    </div>
                    <div class="form-group">
                        <label class="mb-0">E-mail</label>
                        <input value="{{ Auth::guard('agen')->user()->email }}" class="form-control" type="text" placeholder="Email" autocomplete="new-password" disabled>
                    </div>
                </div>

                <div class="col-md-4 order-3 order-md-2">
                    <h5 class="font-weight-bold">Informasi Bank</h5>
                    <div class="form-group">
                        <label class="mb-0">Nama Bank</label>
                        <input value="{{ Auth::guard('agen')->user()->bank_rek }}" class="form-control" type="text" name="bank_rek" placeholder="Nama Bank" autocomplete="new-password">
                    </div>
                    <div class="form-group">
                        <label class="mb-0">Nomor Rekening</label>
                        <input value="{{ Auth::guard('agen')->user()->no_rek }}" class="form-control" type="text" name="no_rek" placeholder="Nomor Rekening" autocomplete="new-password">
                    </div>
                    <div class="form-group">
                        <label class="mb-0">Nama Pemilik Rekening</label>
                        <input value="{{ Auth::guard('agen')->user()->nama_rek }}" class="form-control" type="text" name="nama_rek" placeholder="Nama Pemilik Rekening" autocomplete="new-password">
                    </div>
                    <br>
                    <h5 class="font-weight-bold">Keamanan</h5>
                    <div class="form-group">
                        <a href="/profile/password" class="list-group-item list-group-item-action set-menu-2" style="margin-bottom: -16px !important;">Ganti Password <i class="fa fa-chevron-right float-right" style="line-height: unset"></i></a>
                        <hr>
                    </div>
                </div>
                <div class="col-md-4 order-1 order-md-3 text-center">
                    <img class="card-img-top rounded img-fluid img-thumbnail" src="{{ Auth::guard('agen')->user()->foto ? '/foto/'.Auth::guard('agen')->user()->foto : 'https://ui-avatars.com/api/?background=random&name='.Auth::guard('agen')->user()->name }}" alt="Profile Picture"
                    style="
                    object-fit: cover;
                    width: 200px;
                    /* height: 200px; */
                    border-radius: 50%;
                    ">
                    <a href="profile/edit-foto" class="btn btn-sm btn-light mt-2"><i class="fas fa-edit"></i> edit foto</a>
                    <div class="form-group text-left pt-4">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Gender</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">{{ Auth::guard('agen')->user()->gender }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Lahir</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">{{ Auth::guard('agen')->user()->tgl_lahir }}</td>
                                </tr>
                                <tr>
                                    <td>Kota</td>
                                    <td>:</td>
                                    <td class="font-weight-bold">{{ Auth::guard('agen')->user()->kota }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12 order-last">
                    <button class="btn btn-primary btn-pill"> <i class="fa fa-edit" style="line-height: unset"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#kode_notelp").val("{!! Auth::guard('agen')->user()->kode_notelp ?? 62 !!}");
    });
</script>
@endsection
