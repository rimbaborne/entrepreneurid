@extends('agen.layouts.app')

@section('content')
<div class="col-md-9 pb-4">
    <div class="card " style="min-height: 500px">
        <div class="card-body">
            <form class="row" method="POST" action="/profile/password/update">
                @csrf
                <div class="col-md-12 mb-2">
                    <h3 class="text-muted display-4" style="margin-bottom: .4rem;">
                        Profile
                    </h3>
                </div>
                <div class="col-md-6 order-2 order-md-1">
                    <h5 class="font-weight-bold">Ganti Password</h5>
                    <div class="form-group">
                        <label class="mb-0">Password Baru</label>
                        <div class="input-group">
                            <input class="form-control" id="password" type="password" name="password" autocomplete="new-password" minlength="4" required>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fas field-icon toggle-password fa-eye"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="mb-0">Konfirmasi Password</label>
                        <div class="input-group">
                            <input class="form-control" id="confirm_password" type="password" name="password" autocomplete="new-password" required>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fas field-icon toggle-password-confirm fa-eye"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 order-1 order-md-3 text-center">
                </div>
                <div class="col-md-12 order-last pt-4">
                    <button class="btn btn-primary btn-pill"> <i class="fa fa-edit" style="line-height: unset"></i> Update Password</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var password = document.getElementById("password")
        , confirm_password = document.getElementById("confirm_password");

        function validatePassword(){
            if(password.value != confirm_password.value) {
                confirm_password.setCustomValidity("Maaf, Password Tidak Sama. Periksa Kembali !");
            } else {
                confirm_password.setCustomValidity('');
            }
        }

        password.onchange = validatePassword;
        confirm_password.onkeyup = validatePassword;

    });

    $(document).ready(function() {
        $(".toggle-password").on('click',function() {
            $(this).toggleClass("fa-eye fa-eye-slash");

            if ( $("#password").attr("type") == "password") {
                $("#password").attr("type", "text");
            } else {
                $("#password").attr("type", "password");
            }
        });

        $(".toggle-password-confirm").on('click',function() {
            $(this).toggleClass("fa-eye fa-eye-slash");

            if ( $("#confirm_password").attr("type") == "password") {
                $("#confirm_password").attr("type", "text");
            } else {
                $("#confirm_password").attr("type", "password");
            }
        });
    });


</script>
@endsection
