@extends('agen.layouts.app')

@section('content')
@php
    if(request()->id == 'a32e4e77-29d0-4d39-a692-cb9e2bc2cd5d') {\
        return redirect('https://kelasentrepreneurid.com/pemesanan/kelas-profit-10-juta?ref='.request()->ref);
    }
@endphp
<div class="col-sm-9 pb-4">
    <div class="card-2 " style="min-height: 500px">
        <div class="card-body">
            <h3 class="card-title mb-4" style="font-weight:300; margin-bottom: .4rem;">
                Produk - Pesan <strong>Mentoring Sales Funnel</strong>
            </h3>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="POST">
                                @csrf
                                <input value="" name="ref" hidden>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <input onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="nama" value="{{ old('nama') }}" class="form-control" maxlength="100" placeholder="Nama Lengkap" required="">
                                        </div><!--col-->
                                    </div><!--row-->
                                </div><!--row-->

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <input type="text" name="panggilan" value="{{ old('panggilan') }}" class="form-control" maxlength="100" placeholder="Nama Panggilan" required="">
                                        </div><!--col-->
                                    </div><!--row-->
                                </div><!--row-->

                                <div class="form-group row">
                                    <div class="col-md-9 col-form-label">
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" type="radio" value="Laki-laki" name="jeniskelamin" required>
                                            <label class="form-check-label">Laki-laki</label>
                                        </div>
                                        <div class="form-check form-check-inline mr-1">
                                            <input class="form-check-input" type="radio" value="Perempuan" name="jeniskelamin">
                                            <label class="form-check-label">Perempuan</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row pb-4">
                                    <div class="col">
                                        <div class="input-group ">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    +62
                                                </span>
                                            </div>
                                            <input id="nohp" type="number" name="nohp" value="{{ old('nohp') }}" class="form-control" maxlength="12" placeholder="No. Handphone WhatsApp" required="">
                                        </div><!--form-group-->
                                        <span class="help-block text-muted" style="font-size: 10px; font-weight: 700">Tidak Pakai Angka 0 . Contoh : 81234563789</span>
                                    </div><!--col-->
                                </div><!--row-->

                                <div class="form-group">
                                    <label style="padding-right: 10px">Tanggal Lahir</label>
                                    <select name="tgl">
                                        @for ($a = 1; $a <= 31; $a++)
                                            <option value="{{ $a }}">{{ $a }}</option>
                                        @endfor
                                    </select>
                                    <select name="bln">
                                        @for ($a = 1; $a <= 12; $a++)
                                            <option value="{{ $a }}">{{ $a }}</option>
                                        @endfor
                                    </select>
                                    <select name="thn">
                                        @for ($a = 1950; $a <= 2015; $a++)
                                            <option value="{{ $a }}">{{ $a }}</option>
                                        @endfor
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <input onkeyup="this.value = this.value.toLowerCase();" autocomplete="new-password" type="email" name="email" value="{{ old('email') }}" id="email" placeholder="Alamat Email" maxlength="191" required="required" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <input onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="kota" value="{{ old('kota') }}" class="form-control" placeholder="Kota Domisili" required="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group mb-0 clearfix">
                                            {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(244,67,54"> --}}
                                            <button type="submit" class="btn btn-pill btn-block btn-primary btn-block">
                                                Pesan
                                            </button>
                                        </div><!--form-group-->
                                    </div><!--col-->
                                 </div><!--row-->
                            </form>

                        </div><!-- card-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stack('before-scripts')
    <script>
            $("#nohp").on("input", function() {
                if (/^0/.nohp(this.value)) {
                    this.value = this.value.replace(/^0/, "")
                }
            })
    </script>
@stack('after-scripts')
@endsection
