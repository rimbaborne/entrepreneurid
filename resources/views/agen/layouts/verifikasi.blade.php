@extends('agen.layouts.app')
@if (Auth::guard('agen')->user()->status_aktif == 0)
    @section('content')
    <div class="col-md-9 pb-4">
        <div class="card-2 " style="min-height: 500px">
            <div class="card-body">
                <div class="alert alert-warning" role="alert">
                    <h4 class="alert-heading">Menunggu Aktifasi</h4>
                    <hr>
                    <p class="mb-0">Kami Sedang memverifikasi akun anda untuk menggunakan dashboard baru ini. Terima Kasih</p>
                </div>
            </div>
        </div>
    </div>
    @endsection
@else
    @section('content')
        @yield('verifikasi')
    @endsection

@endif

