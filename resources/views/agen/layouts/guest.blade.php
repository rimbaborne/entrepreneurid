<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('img/logo-official-agen.png') }}">

    <title>{{ config('app.name', 'entrepreneurID') }}</title>

    <!-- Scripts -->
	{{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    {{ style(mix('css/backend.css')) }}
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous"> --}}
    {!! script(mix('js/manifest.js')) !!}
    {!! script(mix('js/vendor.js')) !!}
    {!! script(mix('js/frontend.js')) !!}
    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script> --}}

    <style>
        .card{
            border-radius: 20px;
            -webkit-box-shadow: 0 0.125rem 0.8rem rgba(0, 0, 0, 0.1) !important;
            box-shadow: 0 0.125rem 0.8rem rgba(0, 0, 0, 0.1) !important;
            border: 0px solid;
        }
        .card-2{
            -webkit-box-shadow: 0rem !important;
            box-shadow: 0rem !important;
            background: #f2f5f8;
            border-radius: 20px;
        }
        .set-menu {
            border: none;
            border-radius: 20px 0px 0px 20px !important;
        }
        .j-icon{
            padding-right: 25px;
        }
        .list-group-item.active {
            background-color: #ec1b25;
            border-color: #ec1b25;
        }
        .form-group>label {
        bottom: 26px;
        left: 10px;
        position: relative;
        background-color: white;
        padding: 0px 1px 0px 1px;
        font-size: 0.9em;
        transition: 0.2s;
        pointer-events: none;
        color: #536176;
        font-weight: 500;
        /* letter-spacing: 0.07em; */
        }

        .form-group {
            margin-bottom: -0.5rem;
        }

        .form-control:focus~label {
        bottom: 47px;
        font-size: 1.03em;
        }

        .form-control:valid~label {
        bottom: 47px;
        font-size: 1.03em;
        }

        /* .form-control {
            outline-color: 1px solid #ec1b25;
            border-radius: .5rem;
        } */

        .btn-eid {
            background-color: #ec1b25;
            border-color: #ec1b25;
        }

        .warna-eid {
            color: #ec1b25;
        }
    </style>

</head>
<body class=" bg-white">
<div id="app">

    <main class="py-4">

        @yield('content')
    </main>
    <footer>
        <div class="text-center text-muted pb-4">
            entrepreneurID © {{ \Carbon\Carbon::now()->year }}
        </div>
    </footer>
</div>


<script>
    window.addEventListener( "pageshow", function ( event ) {
    var historyTraversal = event.persisted ||
                            ( typeof window.performance != "undefined" &&
                                window.performance.navigation.type === 2 );
    if ( historyTraversal ) {
        // Handle page restore.
        window.location.reload();
    }
    });
</script>
</body>
</html>
