<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('img/logo-official-agen.png') }}">
    {{--  auto refresh, g perlu di git commit / push --}}
    {{-- <meta http-equiv="refresh" content="7"> --}}

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
	{{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <!-- Styles -->
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}

    {{ style(mix('css/backend.css')) }}
    {!! script(mix('js/manifest.js')) !!}
    {!! script(mix('js/vendor.js')) !!}
    {!! script(mix('js/frontend.js')) !!}
    @notifyCss

    <style>
        i {
            line-height: unset;
        }
        .card{
            border-radius: 20px;
            -webkit-box-shadow: 0 0.125rem 0.8rem rgba(0, 0, 0, 0.1) !important;
            box-shadow: 0 0.125rem 0.8rem rgba(0, 0, 0, 0.1) !important;
            border: 0px solid;
        }
        .card-2{
            -webkit-box-shadow: 0rem !important;
            box-shadow: 0rem !important;
            background: #f2f5f8;
            border-radius: 20px;
        }
        .produk{
            border-radius: 10px;
        }
        .set-menu {
            border: none;
            border-radius: 20px 0px 0px 20px !important;
        }
        .set-menu-2 {
            border: none;
        }
        .j-icon{
            padding-right: 25px;
        }
        .list-group-item.active {
            background-color: #ec1b25;
            border-color: #ec1b25;
        }
        .list-group-item {
            padding: .35rem 1.25rem;
        }
        /* .form-group>label {
        bottom: 26px;
        left: 10px;
        position: relative;
        background-color: white;
        padding: 0px 1px 0px 1px;
        font-size: 0.9em;
        transition: 0.2s;
        pointer-events: none;
        color: #536176;
        font-weight: 500;
        letter-spacing: 0.07em;
        }

        .form-group {
            margin-bottom: -0.5rem;
        }

        .form-control:focus~label {
        bottom: 47px;
        font-size: 1.03em;
        }

        .form-control:valid~label {
        bottom: 47px;
        font-size: 1.03em;
        }

        .form-control {
            outline-color: 1px solid #ec1b25;
            border-radius: .5rem;
        } */

        .zoom {
            position: relative;
        }
        .zoom:hover {
            -ms-transform: scale(5); /* IE 9 */
            -webkit-transform: scale(5); /* Safari 3-8 */
            transform: scale(5);
            z-index: 999;
            margin-top: 50px;
            margin-bottom: 50px;
            margin-right: 50px;
            margin-left: 50px;
        }

        .owl-carousel .item img {
            display: block;
            width: 100%;
            height: auto
        }

        .btn-eid {
            background-color: #ec1b25;
            border-color: #ec1b25;
        }

        .warna-eid {
            color: #ec1b25;
        }
    </style>
    <script>
        function copyToClipboard(element) {
          var $temp = $("<input>");
          $("body").append($temp);
          $temp.val($(element).text()).select();
          document.execCommand("copy");
          $temp.remove();
        }
    </script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
</head>
<body class=" bg-white">
<div id="app">

    <div class="d-block d-sm-none">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="{{ route('frontend.index') }}">
                <img class="navbar-brand-full" src="{{ asset('img/logo-eid.png') }}"height="30" alt="Ar-Rahmah Balikpapan">
            </a>
            <button class="navbar-toggler navbar-toggler-right bg-light" style="color: #fff" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="@lang('labels.general.toggle_navigation')">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                @if (!null == Auth::guard('agen')->user()->status_agen)
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="/profile" class="nav-link {{ request()->is('profile*') ? 'active' : '' }}">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-user"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Profile</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/produk" class="nav-link {{ request()->is('produk*') ? 'active' : '' }}">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-layer-group"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Produk</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/transaksi" class="nav-link {{ request()->is('transaksi*') ? 'active' : '' }}">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-chart-line"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Transaksi</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('agen.logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-unlock"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Logout</div>
                                </div>
                            </a>
                        </li>
                    </ul>
                @else
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="/" class="nav-link {{ request()->is('/') ? 'active' : ''  }}">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-tachometer-alt"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Dashboard</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/profile" class="nav-link {{ request()->is('profile*') ? 'active' : '' }}">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-user"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Profile</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/amunisi" class="nav-link {{ request()->is('amunisi*') ? 'active' : '' }}">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-tools"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Amunisi</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/produk" class="nav-link {{ request()->is('produk') ? 'active' : '' }}">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-layer-group"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Produk</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/produk/lead-magnet" class="nav-link {{ request()->is('*lead-magnet') ? 'active' : '' }}">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-layer-group"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Lead Magnet</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/leaderboard" class="nav-link {{ request()->is('leaderboard*') ? 'active' : '' }}">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-sitemap"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Leaderboard</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/transaksi" class="nav-link {{ request()->is('transaksi*') ? 'active' : '' }}">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-chart-line"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Transaksi</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/sub-agen" class="nav-link {{ request()->is('sub-agen*') ? 'active' : '' }}">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-chart-line"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Sub Agen</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/informasi" class="nav-link {{ request()->is('informasi*') ? 'active' : '' }}">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-bullhorn"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Informasi</div>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('agen.logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <div class="row">
                                    <div class="col-2 text-center">
                                        <i class="fas fa-unlock"></i>
                                    </div>
                                    <div class="col-10" style="margin-left: -20px">Logout</div>
                                </div>
                            </a>
                        </li>
                    </ul>
                @endif

            </div>
        </nav>
    </div>
    <nav class="d-none d-sm-block navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <div class="justify-content-end">
                <button class="navbar-toggler justify-content-end navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    {{-- <div class="">
                        <img src="/img/logo-eid.png" alt="CNL" width="200">
                    </div> --}}
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->

                    @if (Auth::guard('agen')->guest())
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('agen.login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('agen.register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('agen.register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::guard('agen')->user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('agen.logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('agen.logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    @include('notify::messages')
    <main class="py-4">
        @if (session('status'))
            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            </div>
        @endif

        <div class="container">
            <div class="row justify-content-center">
                @include('agen.includes.messages')
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-3 d-none d-sm-block" style="margin-right: -30px !important">
                            @include('agen.includes.sidebar')
                        </div>
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer>
        <div class="text-center text-muted pb-4">
            entrepreneurID © {{ \Carbon\Carbon::now()->year }}
        </div>
    </footer>
    @notifyJs
</div>
</body>
</html>
