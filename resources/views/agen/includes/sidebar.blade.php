
<div class="text-center pb-4 pt-4">
    <img src="/img/logo-eid.png" width="150">
</div>
<div class="mb-4">
    <div class="user-profile">
        @if (!null == Auth::guard('agen')->user()->status_agen)
        <div class="list-group" style="font-weight: 200; font-size: 18px">
            <a href="/profile" class="list-group-item list-group-item-action set-menu
            {{ request()->is('profile*') ? 'active' : '' }}
            ">
                <div class="row">
                    <i class="col-3 col-form-label far m fa-user"></i>
                    <div class="col-9 col-form-label">
                        Profile
                    </div>
                </div>
            </a>
            <a href="/produk" class="list-group-item list-group-item-action set-menu
            {{ request()->is('produk*') ? 'active' : '' }}
            ">
                <div class="row">
                    <i class="col-3 col-form-label fas m fa-layer-group"></i>
                    <div class="col-9 col-form-label">
                        Produk
                    </div>
                </div>
            </a>
            <a href="/transaksi" class="list-group-item list-group-item-action set-menu
            {{ request()->is('transaksi*') ? 'active' : '' }}
            ">
                <div class="row">
                    <i class="col-3 col-form-label fas m fa-chart-line"></i>
                    <div class="col-9 col-form-label">
                        Transaksi
                    </div>
                </div>
            </a>
        </div>
        @else
            <div class="list-group" style="font-weight: 200; font-size: 18px">
                <a href="/" class="list-group-item list-group-item-action set-menu
                {{ request()->is('/') ? 'active' : ''  }}
                ">
                    <div class="row">
                        <i class="col-3 col-form-label fas m fa-tachometer-alt"></i>
                        <div class="col-9 col-form-label">
                            Dashboard
                        </div>
                    </div>
                </a>
                <a href="/profile" class="list-group-item list-group-item-action set-menu
                {{ request()->is('profile*') ? 'active' : '' }}
                ">
                    <div class="row">
                        <i class="col-3 col-form-label far m fa-user"></i>
                        <div class="col-9 col-form-label">
                            Profile
                        </div>
                    </div>
                </a>
                <a href="/amunisi" class="list-group-item list-group-item-action set-menu
                {{ request()->is('amunisi*') ? 'active' : '' }}
                ">
                    <div class="row">
                        <i class="col-3 col-form-label fas m fa-tools"></i>
                        <div class="col-9 col-form-label">
                            Amunisi
                        </div>
                    </div>
                </a>
                <a href="/produk" class="list-group-item list-group-item-action set-menu
                {{ request()->is('produk') ? 'active' : '' }}
                ">
                    <div class="row">
                        <i class="col-3 col-form-label fas m fa-layer-group"></i>
                        <div class="col-9 col-form-label">
                            Produk
                        </div>
                    </div>
                </a>
                <a href="/produk/lead-magnet" class="list-group-item list-group-item-action set-menu
                {{ request()->is('*lead-magnet') ? 'active' : '' }}
                ">
                    <div class="row">
                        <i class="col-3 col-form-label fas m fa-layer-group"></i>
                        <div class="col-9 col-form-label">
                            Lead Magnet
                        </div>
                    </div>
                </a>
                <a href="/leaderboard" class="list-group-item list-group-item-action set-menu
                {{ request()->is('leaderboard*') ? 'active' : '' }}
                ">
                    <div class="row">
                        <i class="col-3 col-form-label fas m fa-sitemap"></i>
                        <div class="col-9 col-form-label">
                            Leaderboard
                        </div>
                    </div>
                </a>
                <a href="/transaksi" class="list-group-item list-group-item-action set-menu
                {{ request()->is('transaksi*') ? 'active' : '' }}
                ">
                    <div class="row">
                        <i class="col-3 col-form-label fas m fa-chart-line"></i>
                        <div class="col-9 col-form-label">
                            Transaksi
                        </div>
                    </div>
                </a>
                <a href="/sub-agen" class="list-group-item list-group-item-action set-menu
                {{ request()->is('sub-agen*') ? 'active' : '' }}
                ">
                    <div class="row">
                        <i class="col-3 col-form-label fas m fa-users"></i>
                        <div class="col-9 col-form-label">
                            Sub Agen
                        </div>
                    </div>
                </a>
                <a href="/informasi" class="list-group-item list-group-item-action set-menu
                {{ request()->is('informasi*') ? 'active' : '' }}
                ">
                    <div class="row">
                        <i class="col-3 col-form-label fas m fa-bullhorn"></i>
                        <div class="col-9 col-form-label">
                            Informasi
                        </div>
                    </div>
                </a>
            </div>
        @endif

    </div>
</div>

@stack('before-scripts')

    <script type="text/javascript">
        $(document).ready(function() {
            menusid = document.getElementById("menusid");
            var myScrollFunc = function() {
            var y = window.scrollY;
                if (y >= 800) {
                    menusid.style.display= "show"
                } else {
                    menusid.style.display = "none"
                }
            };
            window.addEventListener("scroll", myScrollFunc);
        });
    </script>

@stack('after-scripts')
