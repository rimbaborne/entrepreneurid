<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div style="text-align: center;">
                    <img class="card-img-top" src="{{ Auth::guard('agen')->user()->foto ? '/foto/'.Auth::guard('agen')->user()->foto : 'https://ui-avatars.com/api/?background=random&name='.Auth::guard('agen')->user()->name }}" alt="Profile Picture"
                    style="
                    object-fit: cover;
                    height: 60px;
                    width: 60px;
                    border-radius: 50%;
                    ">
                </div>
            </div>
            <div class="col-8">
                <p class="card-title" style="">
                    {{ Auth::guard('agen')->user()->name }}<br/>
                </p>
                @if (Auth::guard('agen')->user()->status_aktif == true)
                    <div class="badge badge-success">
                        Agen Aktif <i class="fas fa-check"></i>
                    </div>
                @else
                    <div class="badge badge-danger">
                        Non Aktif
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>
{{-- <div class="card">
    <div class="card-body">
        <h4 class="card-title" style="text-align: center; min-height: 300px">
            Menu Favorit
        </h4>
    </div>
</div> --}}
