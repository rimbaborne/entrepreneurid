<div>
<div class="row" >
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/dashboard">Kelas Online Copywriting</a></li>
            <li class="breadcrumb-item active">Praktikum Pertama</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form wire:submit.prevent="bagiansatu">
            @csrf
            <div class="card" style="padding: 25px; ">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="text-center" style="padding-top:10px;">
                        Masukan Data tentang Bisnis Anda
                        @if (isset($djawaban1bagian1->updated_at))
                        <div class="text-muted text-center" style="font-size:11px">
                            Tersimpan : {{ $djawaban1bagian1->updated_at->diffForHumans() }}
                        </div>
                        @endif
                    </h4>
                </div>
                <div class="col-md-12">
                    <p>
                        <i class="fa fa-angle-right fa-lg mt-4"></i>
                        Cerita singkat tentang latar belakang Target Pasar
                    </p>
                    <textarea wire:model="jawaban1bagian1" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>

                <div class="col-md-12">
                    <p>
                        <i class="fa fa-angle-right fa-lg mt-4"></i>
                        Tantangan yang mereka hadapi
                    </p>
                    <textarea wire:model="jawaban2bagian1" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>

                <div class="col-md-12">
                    <p>
                        <i class="fa fa-angle-right fa-lg mt-4"></i>
                        Keinginan mereka
                    </p>
                    <textarea wire:model="jawaban3bagian1" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>

                <div class="col-md-12">
                    <p>
                        <i class="fa fa-angle-right fa-lg mt-4"></i>
                        Value produknya
                    </p>
                    <textarea wire:model="jawaban4bagian1" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>

                <div class="col-md-12">
                    <p>
                        <i class="fa fa-angle-right fa-lg mt-4"></i>
                        Penawarannya apa
                    </p>
                    <textarea wire:model="jawaban5bagian1" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>

                <div class="col-md-12">
                    <p>
                        <i class="fa fa-angle-right fa-lg mt-4"></i>
                        Bagaimana cara membuat mereka langsung membeli
                    </p>
                    <textarea wire:model="jawaban6bagian1" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>
                @if(session()->get('satu'))
                <div class="alert alert-success text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    Jawaban Berhasil Tersimpan !
                </div>
                @endif
                <button  class="btn btn-info btn-pill">Simpan</button>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <form wire:submit.prevent="bagiandua">
            @csrf
            <div class="card" style="padding: 25px; ">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="text-center" style="padding-top:10px;">
                        Masukan Kerangka Copywriting
                        @if (isset($djawaban1bagian2->updated_at))
                        <div class="text-muted text-center" style="font-size:11px">
                            Tersimpan : {{ $djawaban1bagian2->updated_at->diffForHumans() }}
                        </div>
                        @endif
                    </h4>
                </div>
                <div class="col-md-12">
                    <p>
                        <i class="fa fa-angle-right fa-lg mt-4"></i>
                        Headline
                    </p>
                    <textarea wire:model="jawaban1bagian2" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>

                <div class="col-md-12">
                    <p>
                        <i class="fa fa-angle-right fa-lg mt-4"></i>
                        Pembuka Copywriting
                    </p>
                    <textarea wire:model="jawaban2bagian2" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>

                <div class="col-md-12">
                    <p>
                        <i class="fa fa-angle-right fa-lg mt-4"></i>
                        Menjelaskan value produk
                    </p>
                    <textarea wire:model="jawaban3bagian2" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>

                <div class="col-md-12">
                    <p>
                        <i class="fa fa-angle-right fa-lg mt-4"></i>
                        Mengarahkan untuk beli
                    </p>
                    <textarea wire:model="jawaban4bagian2" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>
                @if(session()->get('dua'))
                <div class="alert alert-success text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    Jawaban Berhasil Tersimpan !
                </div>
                @endif
                <button  class="btn btn-info btn-pill">Simpan</button>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <form wire:submit.prevent="bagiantiga">
            @csrf
            <div class="card" style="padding: 25px; ">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="text-center" style="padding-top:10px;">
                        Kumpulkan Copywriting dengan Kerangka diatas
                        @if (isset($djawaban1bagian3->updated_at))
                        <div class="text-muted text-center" style="font-size:11px">
                            Tersimpan : {{ $djawaban1bagian3->updated_at->diffForHumans() }}
                        </div>
                        @endif
                    </h4>
                </div>
                <div class="col-md-12" style="padding-top: 20px">
                    <textarea rows="20" wire:model="jawaban1bagian3" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>
                @if(session()->get('tiga'))
                <div class="alert alert-success text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    Jawaban Berhasil Tersimpan !
                </div>
                @endif
                <button class="btn btn-info btn-pill">Simpan</button>
            </div>
        </form>
    </div>
</div>
</div>
