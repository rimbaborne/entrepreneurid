<div>
<div class="row" >
    <div class="col-md-12">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/dashboard">Kelas Online Copywriting</a></li>
            <li class="breadcrumb-item active">Praktikum Kedua</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <form wire:submit.prevent="bagiansatu">
            @csrf
            <div class="card" style="padding: 25px; ">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="text-center" style="padding-top:10px;">
                        Kumpulkan Copywriting dengan Formula AIDCA
                        @if (isset($djawaban1bagian1->updated_at))
                        <div class="text-muted text-center" style="font-size:11px">
                            Tersimpan : {{ $djawaban1bagian1->updated_at->diffForHumans() }}
                        </div>
                        @php
                            if ($djawaban1bagian1->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian1->deskripsi_praktikum_jawaban === '') {
                                $a = 'border border-danger';
                            } else {
                                $a = 'border border-success';
                            }
                        @endphp
                        @endif
                    </h4>
                </div>
                <div class="col-md-12" style="padding-top: 20px">
                    <textarea id="froala-editor" rows="12" wire:model="jawaban1bagian1" class="form-control {{ $a ?? '' }}" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>
                @if(session()->get('satu'))
                <div class="alert alert-success text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    Jawaban Berhasil Tersimpan !
                </div>
                @endif
                <button class="btn btn-info btn-pill">Simpan</button>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form wire:submit.prevent="bagiandua">
            @csrf
            <div class="card" style="padding: 25px; ">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="text-center" style="padding-top:10px;">
                        Kumpulkan Copywriting dengan Formula BAB
                        @if (isset($djawaban1bagian2->updated_at))
                        <div class="text-muted text-center" style="font-size:11px">
                            Tersimpan : {{ $djawaban1bagian2->updated_at->diffForHumans() }}
                        </div>
                        @php
                            if ($djawaban1bagian2->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian2->deskripsi_praktikum_jawaban === '') {
                                $b = 'border border-danger';
                            } else {
                                $b = 'border border-success';
                            }
                        @endphp
                        @endif
                    </h4>
                </div>
                <div class="col-md-12" style="padding-top: 20px">
                    <textarea rows="12" wire:model="jawaban1bagian2" class="form-control {{ $b ?? '' }}" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>
                @if(session()->get('dua'))
                <div class="alert alert-success text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    Jawaban Berhasil Tersimpan !
                </div>
                @endif
                <button class="btn btn-info btn-pill">Simpan</button>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form wire:submit.prevent="bagiantiga">
            @csrf
            <div class="card" style="padding: 25px; ">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="text-center" style="padding-top:10px;">
                        Kumpulkan Copywriting dengan Formula FAB
                        @if (isset($djawaban1bagian3->updated_at))
                        <div class="text-muted text-center" style="font-size:11px">
                            Tersimpan : {{ $djawaban1bagian3->updated_at->diffForHumans() }}
                        </div>
                        @php
                            if ($djawaban1bagian3->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian3->deskripsi_praktikum_jawaban === '') {
                                $c = 'border border-danger';
                            } else {
                                $c = 'border border-success';
                            }
                        @endphp
                        @endif
                    </h4>
                </div>
                <div class="col-md-12" style="padding-top: 20px">
                    <textarea rows="12" wire:model="jawaban1bagian3" class="form-control {{ $c ?? '' }}" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>
                @if(session()->get('tiga'))
                <div class="alert alert-success text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    Jawaban Berhasil Tersimpan !
                </div>
                @endif
                <button class="btn btn-info btn-pill">Simpan</button>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form wire:submit.prevent="bagianempat">
            @csrf
            <div class="card" style="padding: 25px; ">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="text-center" style="padding-top:10px;">
                        Kumpulkan Copywriting dengan Formula PAS
                        @if (isset($djawaban1bagian4->updated_at))
                        <div class="text-muted text-center" style="font-size:11px">
                            Tersimpan : {{ $djawaban1bagian4->updated_at->diffForHumans() }}
                        </div>
                        @php
                            if ($djawaban1bagian4->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian4->deskripsi_praktikum_jawaban === '') {
                                $d = 'border border-danger';
                            } else {
                                $d = 'border border-success';
                            }
                        @endphp
                        @endif
                    </h4>
                </div>
                <div class="col-md-12" style="padding-top: 20px">
                    <textarea rows="12" wire:model="jawaban1bagian4" class="form-control {{ $d ?? '' }}" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>
                @if(session()->get('empat'))
                <div class="alert alert-success text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    Jawaban Berhasil Tersimpan !
                </div>
                @endif
                <button class="btn btn-info btn-pill">Simpan</button>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form wire:submit.prevent="bagianlima">
            @csrf
            <div class="card" style="padding: 25px; ">
                <div class="card-header" style="border-radius: 15px;">
                    <h4 class="text-center" style="padding-top:10px;">
                        Kumpulkan Copywriting dengan Formula PPPP
                        @if (isset($djawaban1bagian5->updated_at))
                        <div class="text-muted text-center" style="font-size:11px">
                            Tersimpan : {{ $djawaban1bagian5->updated_at->diffForHumans() }}
                        </div>
                        @php
                            if ($djawaban1bagian5->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian5->deskripsi_praktikum_jawaban === '') {
                                $e = 'border border-danger';
                            } else {
                                $e = 'border border-success';
                            }
                        @endphp
                        @endif
                    </h4>
                </div>
                <div class="col-md-12" style="padding-top: 20px">
                    <textarea rows="12" wire:model="jawaban1bagian5" class="form-control {{ $e ?? '' }}" placeholder="Isi Jawaban Anda Disini"></textarea>
                </div>
                <hr>
                @if(session()->get('lima'))
                <div class="alert alert-success text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    Jawaban Berhasil Tersimpan !
                </div>
                @endif
                <button class="btn btn-info btn-pill">Simpan</button>
            </div>
        </form>
    </div>
</div>
</div>
