<div>
    {{-- {{ last(request()->segments()) }} --}}
    <div class="card">
        <div class="card-body">
            <h4>Kelas Online Copywriting</h4>
            <div class="text-muted">Member User</div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-4 pull-right">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-search"></i> </span>
                        </div>
                        <input wire:model.debounce.1000ms="search" class="form-control" type="text" placeholder="Cari Nama atau Email"
                            autocomplete="password" width="100">
                    </div>
                </div>
                <div class="col-md-2">
                    <select wire:model.debounce.500ms="praktikum" style="font-weight: 600" class="form-control" name="" id="">
                        <option value="1">PRAKTIKUM 1</option>
                        <option value="2">PRAKTIKUM 2</option>
                        <option value="3">PRAKTIKUM 3</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <select wire:model.debounce.500ms="status" class="form-control" name="" id="">
                        <option value="">Semua Status Praktikum</option>
                        <option value="1">Belum Dikerjakan</option>
                        <option value="Menunggu Koreksi">Menunggu Koreksi</option>
                        <option value="Perlu Diulang">Perlu diulang</option>
                        <option value="Lulus Praktikum">Lulus Praktikum</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <select wire:model.debounce.500ms="urutkanstatus" class="form-control" name="" id="">
                        <option value="">Status Tugas</option>
                        {{-- <option value="1">Belum Dikerjakan</option> --}}
                        <option value="asc">Lebih Dulu</option>
                        <option value="desc">Lebih Lama</option>
                    </select>
                </div>
            </div>
            <div class="text-right text-muted" style="padding-right: 10px">
                Total : {!! $memberkoc->total() ?? 0 !!}
            </div>
            <table class="table table-sm table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                    <tr>
                        <th width="70" class="text-center">
                            <i class="fas fa-user"></i>
                        </th>
                        <th>
                            <a wire:click.prevent="sortBy('created_at')" role="button" href="#">
                                Daftar
                                @include('backend.includes._sort-icon', ['field' => 'created_at'])
                            </a>
                            <span class="float-right">
                                <a wire:click.prevent="sortBy('first_name')" role="button" href="#">
                                    Nama
                                    @include('backend.includes._sort-icon', ['field' => 'first_name'])
                                </a>
                            </span>
                        </th>
                        <th>
                            <a wire:click.prevent="sortBy('email')" role="button" href="#">
                                Email
                                @include('backend.includes._sort-icon', ['field' => 'email'])
                            </a>
                            <span class="float-right">
                                <a wire:click.prevent="sortBy('last_login_at')" role="button" href="#">
                                    Login
                                    @include('backend.includes._sort-icon', ['field' => 'last_login_at'])
                                </a>
                            </span>
                        </th>
                        <th class="text-center">Praktikum</th>
                        <th>Status</th>
                        <th>Update Tugas</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($memberkoc as $key=> $member)
                    <tr>
                        <td class="text-center">
                            <a href="{{ route('admin.members.koc.praktikum') }}?id={{ $member->id }}">
                                <div class="avatar">
                                    <img class="img-avatar" src="{{ $member->picture ?? '' }}" alt="">
                                    <span class="avatar-status badge-success"></span>
                                </div>
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('admin.members.koc.praktikum') }}?ke={{ $praktikum }}&id={{ $member->id }}">
                                <div>{{ $member->first_name }}</div>
                                <div class="small text-muted">
                                    <span>Register : {{ $member->created_at->diffForHumans() }}</span>
                                </div>
                            </a>
                        </td>
                        <td>
                            {{ $member->email }}
                            <div class="small text-muted">
                                Akses Login : {{ isset($member->last_login_at) ? $member->last_login_at->diffForHumans() : 'Logout' }}
                            </div>
                        </td>
                            @php
                                $datanilai   = DB::table('praktikums_nilaibabs')
                                                ->where('user_praktikum_nilai', $member->id)
                                                ->where('id_praktikum_bab', $praktikum)
                                                ->first();
                                $datajawaban = DB::table('praktikums_jawabans')
                                                ->where('user_praktikum_jawaban', $member->id)
                                                ->first();
                                if (isset($datanilai->status_praktikum_nilai)) {
                                    if ($datanilai->status_praktikum_nilai == 'Perlu Diulang') {
                                        $d = $datanilai->status_praktikum_nilai;
                                        $c = 'btn-danger';
                                    } elseif ($datanilai->status_praktikum_nilai == 'Lulus Praktikum') {
                                        $d = $datanilai->status_praktikum_nilai;
                                        $c = 'btn-success';
                                    } elseif ($datanilai->status_praktikum_nilai == 'Menunggu Koreksi') {
                                        $d = 'Menunggu Koreksi';
                                        $c = 'btn-warning';
                                    }
                                } else {
                                    $d = 'Belum Dikerjakan';
                                    $c = 'btn-info';
                                }
                            @endphp
                        <td>
                            <div class="text-center" style="font-weight: 700">
                                {{ $praktikum }}
                            </div>
                        </td>
                        <td>
                            <button class="btn btn-pill btn-sm {{ $c }}">
                                {{ $d }}
                            </button>
                        </td>
                        <td>
                            <strong style="font-size: 11px">
                                {{ isset($datanilai->updated_at) ? \Carbon\Carbon::parse($datanilai->updated_at)->diffForHumans() : '-' }}
                            </strong>
                        </td>
                    </tr>
                    @php
                        $first = $memberkoc->firstItem() ?? 0;
                        $end = $key + $memberkoc->firstItem() ?? 0;
                    @endphp
                    @endforeach
                </tbody>
            </table>
            <div class="row" style="padding-top:10px">
                <div class="col-7">
                    <div class="float-left" style="padding-left:10px">
                        {!! $first ?? 0 !!} - {!! $end ?? 0 !!} Dari {!! $memberkoc->total() ?? 0 !!} Data
                    </div>
                </div>
                <!--col-->

                <div class="col-5">
                    <div class="float-right" style="padding-right:10px">
                        {!! $memberkoc->appends(request()->query())->links() !!}
                    </div>
                </div>
                <!--col-->
            </div>
        </div>
        <!--card-body-->
    </div>
    <!--card-->
</div>
