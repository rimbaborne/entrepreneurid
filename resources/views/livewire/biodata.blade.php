<div>
    <div class="card-body">
        <div class="row mt-4">
            <div class="col-md-4">
                <div class="text-center" style="font-weight: 600;font-size: 12px;">
                    KONFIRMASI TERBARU
                </div>
                <table class="table table-sm " style="margin-bottom: 0rem; font-size: 11px">
                    <thead style="font-weight: 400;">
                        <th>Nama</th>
                        <th class="text-center">Waktu</th>
                    </thead>
                    <tbody style="font-weight: 300;">
                        @foreach($konfirmasi as $key=> $konfirm)
                        <tr>
                            <td>
                                <div style="text-transform: uppercase;font-size: 10px">
                                    <strong>{{ $konfirm->nama }}</strong></div>
                                    <div class="small text-muted">{{ $konfirm->email }}</div>
                                </td>
                                <td>
                                    <div class="text-center" style="text-transform: uppercase;font-size: 10px">
                                        <strong>{{ $konfirm->waktukonfirmasi }}</strong></div>
                                        <div class="small text-muted text-center">
                                            @if($konfirm->idprodukreg == 1)
                                            Mentoring Organic Marketing
                                            @elseif($konfirm->idprodukreg == 6)
                                            Copywriting Next Level
                                            @elseif($konfirm->idprodukreg == 4)
                                            Pendaftaran Agen
                                            @elseif($konfirm->idprodukreg == 9)
                                            Peta Bisnis Online
                                            @elseif($konfirm->idprodukreg == 10)
                                            Instant Copywriting
                                            @elseif($konfirm->idprodukreg == 21)
                                            Copywriting Next Level
                                            @elseif($konfirm->idprodukreg == 11)
                                            Mentoring Sales Funnel
                                            @elseif($konfirm->idprodukreg == 12)
                                            Kelas Online Copywriting
                                            @elseif($konfirm->idprodukreg == 15)
                                            Kelas Online Copywriting
                                            @elseif($konfirm->idprodukreg == 17)
                                            WhatsApp Master Closing
                                            @elseif($konfirm->idprodukreg == 19)
                                            Pendaftaran Agen Resmi
                                            @elseif($konfirm->idprodukreg == 5)
                                            Kelas Facebook Hacking
                                            @elseif($konfirm->idprodukreg == 3)
                                            Reseller Formula
                                            @elseif($konfirm->idprodukreg == 18)
                                            WhatsApp Master Closing
                                            @elseif($konfirm->idprodukreg == 22)
                                            Kelas Online Copywriting
                                            @elseif($konfirm->idprodukreg == 23)
                                            Mentoring Instant Copywriting
                                            @elseif($konfirm->idprodukreg == 24)
                                            Agen Resmi entrepreneurID
                                            @elseif($konfirm->idprodukreg == 31 || $konfirm->idprodukreg == 25)
                                            Kelas 100 Orderan
                                            @elseif($konfirm->idprodukreg == 26)
                                            Mentoring Organic Marketing
                                            @elseif($konfirm->idprodukreg == 27)
                                            Kelas Online Copywriting
                                            @elseif($konfirm->idprodukreg == 29)
                                            Copywriting Next Level
                                            @elseif($konfirm->idprodukreg == 30)
                                            Pendaftaran Agen Baru
                                            @elseif($konfirm->idprodukreg == 33)
                                            Mentoring Instant Copywriting
                                            @elseif($konfirm->idprodukreg == 34)
                                            Kelas Facebook Hacking
                                            @elseif($konfirm->idprodukreg == 35)
                                            Mentoring Super Reseller
                                            @elseif($konfirm->idprodukreg == 39)
                                            Agen Resmi entrepreneurID
                                            @else
                                            {{ $konfirm->idprodukreg }}
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4 p-4">
                        {{-- <div class="row pb-4">
                            <div class="col-md-12">
                                <div class=" badge badge-success">Event Aktif</div>
                            </div>
                            <div class="col-md-12 text-center">
                                <h5>Copywriting Next Level</h5>
                                <div class="text-muted text-uppercase font-weight-bold">
                                    TOTAL : {{ $buku_belumaktif+$buku_belumkonfirmasi+$buku_aktif }} Buku
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-md-6 col-sm-4" style="padding: 3px">
                                <div class="card border-info" style="box-shadow: none; margin-bottom: 2px">
                                    <div class="text-center">
                                        <div class="text-info text-uppercase font-weight-bold">AFILIASI</div>
                                        <div class="text-value-xl py-2">{{ $buku_afiliasi }} Buku</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-4" style="padding: 3px">
                                <div class="card border-danger" style="box-shadow: none; margin-bottom: 2px">
                                    <div class="text-center">
                                        <div class="text-danger text-uppercase font-weight-bold">MANUAL</div>
                                        <div class="text-value-xl py-2">{{ $buku_manual }} Buku</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4" style="padding: 3px">
                                <div class="card" style="box-shadow: none; margin-bottom: 2px">
                                    <div class="text-center">
                                        <div class="text-muted small text-uppercase font-weight-bold">TRANSAKSI BUKU <br>BLM UPLOAD</div>
                                        <div class="text-value-xl py-2">{{ $buku_belumaktif }}</div>
                                    </div>
                                    <div class="row text-center">
                                        <div class="col">
                                            <div class="text-value-xl text-info font-weight-bold">{{ $buku_bkaf }}</div>
                                        </div>
                                        <div class="c-vr"></div>
                                        <div class="col">
                                            <div class="text-value-xl text-danger font-weight-bold">{{ $buku_bkma }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4" style="padding: 3px" >
                                <div class="card" style="box-shadow: none; margin-bottom: 2px">
                                    <div class="text-center">
                                        <div class="text-muted small text-uppercase font-weight-bold">TRANSAKSI BUKU <br>Konfirmasi</div>
                                        <div class="text-value-xl py-2">{{ $buku_belumkonfirmasi }}</div>
                                    </div>
                                    <div class="row text-center">
                                        <div class="col">
                                            <div class="text-value-xl text-info font-weight-bold">{{ $buku_beaf }}</div>
                                        </div>
                                        <div class="c-vr"></div>
                                        <div class="col">
                                            <div class="text-value-xl text-danger font-weight-bold">{{ $buku_bema }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4" style="padding: 3px" >
                                <div class="card" style="box-shadow: none; margin-bottom: 2px">
                                    <div class="text-center">
                                        <div class="text-muted small text-uppercase font-weight-bold">TRANSAKSI BUKU <br>Selesai</div>
                                        <div class="text-value-xl py-2">{{ $buku_aktif }}</div>
                                    </div>
                                    <div class="row text-center">
                                        <div class="col">
                                            <div class="text-value-xl text-info font-weight-bold">{{ $buku_akaf }}</div>
                                        </div>
                                        <div class="c-vr"></div>
                                        <div class="col">
                                            <div class="text-value-xl text-danger font-weight-bold">{{ $buku_akma }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    {{-- <div class="col-md-4">
                        <div class="text-center"  >
                            <select wire:model="chartproduk" style="font-size: 12px" name="chartproduk" autocomplete>
                            <option value="29">Copywriting Next Level</option>
                            </select>
                        </div>
                        <div class="chart-wrapper">
                            <div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;">
                                <div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                </div>
                            </div>
                            <canvas id="statusaktif" width="500" height="400" class="chartjs-render-monitor" style="display: block; width: 300px; height: 265px;"></canvas>
                        </div>
                    </div> --}}

                    <div class="col-md-4">
                        <div class="row pb-2">
                            <div class="col-md-12">
                                <div class=" badge badge-success">Event Aktif</div>
                            </div>
                            <div class="col-md-12 text-center">
                                <h5>Agen Resmi entrepreneurID</h5>
                                <div class="text-muted text-uppercase font-weight-bold">
                                    TOTAL : {{ $belumaktif->total()+$belumkonfirmasi->total()+$aktif->total() }}
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-md-6 col-sm-4" style="padding: 3px">
                                <div class="card border-info" style="box-shadow: none; margin-bottom: 2px">
                                    <div class="text-center">
                                        <div class="text-info text-uppercase font-weight-bold">AFILIASI</div>
                                        <div class="text-value-xl py-2">{{ $afiliasi }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-4" style="padding: 3px">
                                <div class="card border-danger" style="box-shadow: none; margin-bottom: 2px">
                                    <div class="text-center">
                                        <div class="text-danger text-uppercase font-weight-bold">MANUAL</div>
                                        <div class="text-value-xl py-2">{{ $manual }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-4" style="padding: 3px">
                                <div class="card" style="box-shadow: none; margin-bottom: 2px">
                                    <div class="text-center">
                                        <div class="text-muted small text-uppercase font-weight-bold">BLM UPLOAD</div>
                                        <div class="text-value-xl py-2">{{ $belumaktif->total() }}</div>
                                    </div>
                                    <div class="row text-center">
                                        <div class="col">
                                            <div class="text-value-xl text-info font-weight-bold">{{ $bkaf }}</div>
                                        </div>
                                        <div class="c-vr"></div>
                                        <div class="col">
                                            <div class="text-value-xl text-danger font-weight-bold">{{ $bkma }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4" style="padding: 3px" >
                                <div class="card" style="box-shadow: none; margin-bottom: 2px">
                                    <div class="text-center">
                                        <div class="text-muted small text-uppercase font-weight-bold">Confirm</div>
                                        <div class="text-value-xl py-2">{{ $belumkonfirmasi->total() }}</div>
                                    </div>
                                    <div class="row text-center">
                                        <div class="col">
                                            <div class="text-value-xl text-info font-weight-bold">{{ $beaf }}</div>
                                        </div>
                                        <div class="c-vr"></div>
                                        <div class="col">
                                            <div class="text-value-xl text-danger font-weight-bold">{{ $bema }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4" style="padding: 3px" >
                                <div class="card" style="box-shadow: none; margin-bottom: 2px">
                                    <div class="text-center">
                                        <div class="text-muted small text-uppercase font-weight-bold">Aktif</div>
                                        <div class="text-value-xl py-2">{{ $aktif->total() }}</div>
                                    </div>
                                    <div class="row text-center">
                                        <div class="col">
                                            <div class="text-value-xl text-info font-weight-bold">{{ $akaf }}</div>
                                        </div>
                                        <div class="c-vr"></div>
                                        <div class="col">
                                            <div class="text-value-xl text-danger font-weight-bold">{{ $akma }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-md-12 col-sm-12" style="padding: 3px">
                                <div class="card border-warning" style="box-shadow: none; margin-bottom: 2px">
                                    <div class="text-center">
                                        <div class="text-muted text-uppercase font-weight-bold">TOTAL AGEN - {{ $agen }}</div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4" style="padding: 0px" >
                                                {{ $agena }}
                                            </div>
                                            <div class="col-md-4 col-sm-4" style="padding: 0px" >
                                                {{ $agenb }}
                                            </div>
                                            <div class="col-md-4 col-sm-4" style="padding: 0px" >
                                                {{ $agenc }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            {{-- {!! $konfirmasi->total() !!} --}}
            {{-- {!! $belumaktif->total() !!} --}}

            <div wire:loading class="row" style=" text-align: end;">
                <div
                style=" position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 9999; background: rgba(0,0,0,.4); margin: 0 1; ">
                <div
                style=" position: relative; border-radius: .3125em; font-family: inherit; top: 50%; left: 50%; transform: translate(-100%, 100%); color: #fff; ">
                <span class="spinner-border" role="status" aria-hidden="true"></span>
            </div>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-md-1">
            <select class="form-control" wire:model="perPage">
                <option>5</option>
                <option>10</option>
                <option>25</option>
                <option>50</option>
                <option>100</option>
            </select>
        </div>
        <div class="col-md-3">
            <a href="/admin/data-ecourse/exportexcel?produk={{ $searchproduk }}" target="_blank" class="btn btn-primary btn-success">
                <i class="fas fa-download"></i> Export Excel
            </a>
            <a href="/admin/data-ecourse/cnl/resi" class="btn btn-primary ">
                <i class="fas fa-book"></i> Resi CNL
            </a>
        </div>

        <div class="col text-center">
        </div>

        {{-- <div class="col-md-2">
            <select class="form-control" wire:model="statusTanda">
                <option value="0">PILIH STATUS</option>
                <option value="1">PILIH STATUS</option>
                <option value="2">LUNAS</option>
                <option value="3">BELUM LUNAS</option>
            </select>
        </div> --}}

        <div class="col-md-4 pull-right">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"> <i class="fa fa-search"></i> </span>
                </div>
                <input wire:model.debounce.1000ms="search" class="form-control" type="text" placeholder="Cari Nama"
                autocomplete="password" width="100">
            </div>
        </div>
    </div>

    <div class="row mt-4" style="margin-top: 0.35rem!important">
        <div class="col">
            <div class="table-responsive" style="min-width: 400px; padding: 0px 15px 15px 15px">
                <section>
                    <div class="col-12">
                        <div class="pull-right" style="text-align-last: end; padding-bottom: 10px; margin: 0px">
                            Total {!!  $biodatas->total() !!} Data
                        </div>
                    </div>
                    <div class="row kotak-atas">
                        {{-- <div class="col-md-1">No</div> --}}
                        <div class="col-2">
                            <a wire:click.prevent="sortBy('nama')" role="button" href="#">
                                Nama
                                @include('backend.includes._sort-icon', ['field' => 'nama'])
                            </a>
                        </div>
                        <div class="col-3">
                            <a wire:click.prevent="sortBy('email')" role="button" href="#">
                                Email
                                @include('backend.includes._sort-icon', ['field' => 'email'])
                            </a>
                        </div>
                        <div class="col-2">Agen</div>
                        <div class="col-2">Produk</div>
                        <div class="col">
                            <a wire:click.prevent="sortBy('created_at')" role="button" href="#">
                                Daftar
                                @include('backend.includes._sort-icon', ['field' => 'created_at'])
                            </a>
                            <div class="float-right">
                                <a wire:click.prevent="sortBy('waktukonfirmasi')" role="button" href="#">
                                    Konfirmasi
                                    @include('backend.includes._sort-icon', ['field' => 'waktukonfirmasi'])
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row kotak-input">
                        {{-- <div class="col-md-1">No</div> --}}
                        <div class="col-2">
                            <select wire:model="searchuser" class="searchuser form-control" name="statususer">
                                <option value="">Semua Customer</option>
                                <option value="99">Affiliasi</option>
                                <option value="2">Manual</option>
                                {{-- <option value="4">Manual - WMC</option> --}}
                            </select>
                        </div>
                        <div class="col-3">
                            <input wire:model.debounce.1000ms="searchemail" class="form-control" type="text">
                        </div>
                        <div class="col-2">
                            <input wire:model.debounce.1000ms="searchagen" class="form-control" type="text"
                            autocomplete>
                        </div>
                        <div class="col-2">
                            <select wire:model="searchproduk" class="searchproduk form-control" name="produk"
                            autocomplete>

                            <option value="">Semua Produk</option>
                            <option value="39">Agen Resmi entrepreneurID</option>
                            <option value="35">Mentoring Super Reseller</option>
                            <option value="34">Kelas Facebook Hacking</option>
                            <option value="33">Mentoring Instant Copywriting</option>
                            <option value="30">Agen Resmi entrepreneurID</option>
                            <option value="29">Copywriting Next Level</option>
                            <option value="27">Kelas Online Copywriting</option>
                            <option value="26">Mentoring Organic Marketing</option>
                            <option value="31">Kelas 100 Orderan 2 & 3</option>
                            <option value="25">Kelas 100 Orderan 1</option>
                            <option value="24">Agen Resmi entrepreneurID</option>
                            <option value="18">WhatsApp Master Closing</option>
                            <option value="11">Mentoring Sales Funnel</option>
                            <option value="9">Peta Bisnis Online</option>
                            <option value="3">Reseller Formula</option>
                            <option value="">----------------------</option>
                            <option value="23">Mentoring Instant Copywriting</option>
                            <option value="1">Copywriting Next Level</option>
                            <option value="6">Copywriting Next Level</option>
                            <option value="4">Pendaftaran Agen</option>
                            <option value="10">Instant Copywriting</option>
                            <option value="21">Copywriting Next Level</option>
                            <option value="12">Kelas Online Copywriting</option>
                            <option value="15">Kelas Online Copywriting</option>
                            <option value="17">WhatsApp Master Closing</option>
                            <option value="19">Pendaftaran Agen Resmi</option>
                            <option value="5">Kelas Facebook Hacking</option>
                            <option value="22">Kelas Online Copywriting</option>
                        </select>
                    </div>
                    <div class="col text-center">
                        <select wire:model="searchaktif" class="form-control" name="konfirmasi">
                            <option value="">Semua Status</option>
                            <option value="99">Belum Aktif</option>
                            <option value="1">Menunggu Konfirmasi</option>
                            <option value="2">Aktif</option>
                        </select>
                    </div>
                </div>
                @php
                $first = 0;
                $end = 0;
                $number = 1;
                @endphp

                {{-- @if (Session::has('notif'))
                <div class="alert alert-success" style="visibility: hidden;
                opacity: 0;
                transition: visibility 0s 2s, opacity 2s linear;">
                tes
            </div>
            @stack('before-scripts')
            <script type="text/javascript">
                window.livewire.on('konfirmasi', postId => {
                    alert('A post was added with the id of: ' + postId);
                })
            </script>
            @stack('after-scripts')
            @endif--}}
            @if(session()->has('notif-berhasil'))
            <div class="alert alert-success">
                {{ session('notif-berhasil') }}
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            @elseif(session()->has('notif-gagal'))
            <div class="alert alert-danger">
                {{ session('notif-gagal') }}
                <button class="close" type="button" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            @endif

            @foreach($biodatas as $key=> $biodata)
            <div class="legend">
                <div class="row kotak">
                    {{-- <div class="col-md-1">{{ $key + $biodatas->firstItem() }}
                    </div> --}}
                    <div class="col-2">
                        <a data-toggle="collapse" href="#detail{{ $key + $biodatas->firstItem() }}"
                            aria-expanded="false" style="color: rgb(56, 56, 56);">
                            <div><strong>{{ $biodata->nama }}</strong></div>
                            <div class="small text-muted">
                                {{-- @php
                                $birth = !empty($biodata->tgllahir) ? $biodata->tgllahir : \Carbon\Carbon::now()->format('Y-m-d');
                                $umur = !empty(\Carbon\Carbon::parse($birth)->age) ? \Carbon\Carbon::parse($birth)->age : '0';
                                @endphp --}}
                                {{-- {{ $biodata->gender }} | {{ $umur ?? 0 }} Tahun --}}
                                {{ $biodata->gender }}
                            </div>
                        </a>

                    </div>
                    <div class="col-3">
                        {{ $biodata->email }}
                        <div class="small text-muted">
                            Daftar : {{ $biodata->tgl_daftar }}
                        </div>
                    </div>
                    <div class="col-2" style="margin-left: 0px">
                        <div>
                            @if ($biodata->ref >= 100000)
                                @php
                                    $agenn = DB::table('agens')->where('id', $biodata->ref)->first();
                                @endphp
                                <a href="/admin/data-ecourse/create?ref={{ $agenn->id }}" target="_blank">
                                    <strong>{{ $agenn->name }}</strong>
                                </a>
                                <div class="small text-muted" style="text-transform: lowercase;">
                                    {{ $agenn->email }}
                                </div>
                            @else
                                <a href="/admin/data-ecourse/create?ref={{ data_get($biodata->parent, 'id') }}" target="_blank">
                                    <strong>{{ data_get($biodata->parent, 'nama') }}</strong>
                                </a>
                                <div class="small text-muted" style="text-transform: lowercase;">
                                    {{ data_get($biodata->parent, 'email') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-2">
                        <div
                        style="font-size: 11px; text-transform: uppercase; padding-top: 10px; text-align: center">
                        @if($biodata->idprodukreg == 1)
                        Mentoring Organic Marketing
                        @elseif($biodata->idprodukreg == 6)
                        Copywriting Next Level
                        @elseif($biodata->idprodukreg == 4)
                        Pendaftaran Agen
                        @elseif($biodata->idprodukreg == 9)
                        Peta Bisnis Online
                        @elseif($biodata->idprodukreg == 10)
                        Instant Copywriting
                        @elseif($biodata->idprodukreg == 21)
                        Copywriting Next Level
                        @elseif($biodata->idprodukreg == 11)
                        Mentoring Sales Funnel
                        @elseif($biodata->idprodukreg == 12)
                        Kelas Online Copywriting
                        @elseif($biodata->idprodukreg == 15)
                        Kelas Online Copywriting
                        @elseif($biodata->idprodukreg == 17)
                        WhatsApp Master Closing
                        @elseif($biodata->idprodukreg == 19)
                        Pendaftaran Agen Resmi
                        @elseif($biodata->idprodukreg == 5)
                        Kelas Facebook Hacking
                        @elseif($biodata->idprodukreg == 3)
                        Reseller Formula
                        @elseif($biodata->idprodukreg == 18)
                        WhatsApp Master Closing
                        @elseif($biodata->idprodukreg == 22)
                        Kelas Online Copywriting
                        @elseif($biodata->idprodukreg == 23)
                        Mentoring Instant Copywriting
                        @elseif($biodata->idprodukreg == 24)
                        Agen Resmi entrepreneurID
                        @elseif($biodata->idprodukreg == 31 || $biodata->idprodukreg == 25)
                        Kelas 100 Orderan
                        @elseif($biodata->idprodukreg == 26)
                        Mentoring Organic Marketing
                        @elseif($biodata->idprodukreg == 27)
                        Kelas Online Copywriting
                        @elseif($biodata->idprodukreg == 29)
                        Copywriting Next Level
                        @elseif($biodata->idprodukreg == 30)
                        Pendaftaran Agen Baru
                        @elseif($biodata->idprodukreg == 33)
                        Mentoring Instant Copywriting
                        @elseif($biodata->idprodukreg == 34)
                        Kelas Facebook Hacking
                        @elseif($biodata->idprodukreg == 35)
                        Mentoring Super Reseller
                        @elseif($biodata->idprodukreg == 39)
                        Agen Resmi entrepreneurID
                        @else
                        {{ $biodata->idprodukreg }}
                        @endif
                        {{-- {{ $biodata->idprodukreg }} --}}
                    </div>
                </div>
                <div class="col text-center">
                    <div class="row">
                        <div class="col-md-10">
                            @if($biodata->aktif == 0)
                            Belum Aktif
                            @elseif($biodata->aktif == 1)
                            <div style="position: relative; " class="btn-group mb-3" role="group" aria-label="Default button group">
                                {{-- <button wire:click="transferulang({{ $biodata->id }})" wire:key="{{ $biodata->id }}" class="btn btn-danger btn-sm" type="button">
                                    {{ $biodata->tolaktf }}<br>
                                    <i class="fas fa-info-circle"></i> Tolak TF
                                </button> --}}
                                <button wire:click="konfirmasi({{ $biodata->id }})" wire:key="{{ $biodata->id }}" class="btn btn-warning btn-sm" type="button">
                                    <i class="fas fa-edit"></i> Konfirmasi
                                </button>
                            </div>
                            @elseif($biodata->aktif == 2)
                            Aktif
                            <button class="btn btn-info btn-block btn-pill btn-sm"
                            wire:click="konfirmasi({{ $biodata->id }})" wire:key="{{ $biodata->id }}"> <i
                            class="fas fa-check"></i></button>
                            @endif
                        </div>
                        <div class="col-md-2" style="padding: 0px">
                            <a href="/admin/data-ecourse/{{ $biodata->id }}/edit" class="btn-success btn btn-sm btn-block">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="#" title="Hapus" data-method="delete" data-trans-button-cancel="Batal" data-trans-button-confirm="Hapus" data-trans-title="{{ $biodata->email }} dihapus?" class="btn-danger btn btn-sm btn-block" style="cursor:pointer;" onclick="$(this).find(&quot;form&quot;).submit();"><i class="fas fa-trash"></i>
                                <form action="/admin/data-ecourse/{{ $biodata->id }}/destroy" method="POST" name="delete_item" style="display:none">
                                <input type="hidden" name="_method" value="delete">
                                @csrf
                                </form>
                            </a>
                        </div>
                    </div>
                </div>
                {{-- @include('livewire.noresi')
                @if ($biodata->idprodukreg == 29 && $biodata->aktif != 0)
                <div class="col-12">
                    <hr>
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-1">No. Resi :</div>
                        <div class="col-2 text-left"><strong>{{ $biodata->noresi }}</strong></div>
                        <div class="col-2">
                            <button data-toggle="modal" data-target="#updateNoresi" wire:click="editNoresi({{ $biodata->id }})" class="btn btn-primary btn-sm">Update Nomor Resi</button>
                        </div>
                        <div class="col"></div>
                        </div>
                </div>
                @endif --}}
                <div class="col-12" style="color: #4e4e4e">
                    <div class="col">
                        <div class="collapse" id="detail{{ $key + $biodatas->firstItem() }}">
                            <hr>
                            <table class="table table-sm table-borderless" style="margin-bottom: 0rem">
                                <thead style="font-weight: 400;">
                                    <th width="100">Tgl. Lahir</th>
                                    <th>No. Telp</th>
                                    <th>Alamat</th>
                                    <th>Kota</th>
                                    <th>Kode Unik</th>
                                    <th>Bukti Transfer</th>
                                </thead>
                                <tbody style="font-weight: 300;">
                                    <tr>
                                        <td>
                                            {{ $biodata->tgllahir }}
                                        </td>
                                        <td>
                                            {{ $biodata->no_telp }}
                                        </td>
                                        <td>
                                            {{ $biodata->alamat }}
                                        </td>
                                        <td>
                                            {{ $biodata->kota }}
                                        </td>
                                        <td>
                                            <strong>{{ $biodata->kodeunik }}</strong>
                                        </td>
                                        <td>
                                            @php
                                            if ($biodata->idprodukreg == 5 ) {
                                                $buktitf = DB::table('pemesanan')->where('idcustomer', '=', $biodata->id )->orderBy('id', 'DESC')->latest('tglpesan')->first();
                                            } else {
                                                $buktitf = DB::table('pemesanan')->where('idcustomer', '=', $biodata->id )->latest('tglpesan')->first();
                                            }
                                            @endphp
                                            <div>
                                                {{ $buktitf->tglpesan ?? '-' }}
                                            </div>
                                            <div style="position; fixed">
                                                <img class="zoom"
                                                @if (isset($buktitf->buktitf))
                                                    @if ($biodata->idprodukreg == 23 && $biodata->status_user == 0 )
                                                        src="https://mentoringinstantcopywriting.com/member-area/buktitf/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                    @elseif ($biodata->idprodukreg == 5 && $biodata->status_user == 0 )
                                                        src="https://kelasfbhacking.com//member-area/buktitf/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                    @elseif ($biodata->idprodukreg == 24 && $biodata->status_user == 0)
                                                        src="https://konfirmasi.agen-entrepreneurid.com/app/public/bukti-transfer-24-AGENBARU/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                    @elseif ($biodata->idprodukreg == 31 && $biodata->status_user == 0)
                                                        src="https://konfirmasi.kelas100orderan.com/public/bukti-transfer-31-KSO/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                    @elseif ($biodata->idprodukreg == 26 && $biodata->status_user == 0)
                                                        src="https://upload.mentoringorganicmarketing.com/public/bukti-transfer-26-MOM/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                    @elseif ($biodata->idprodukreg == 27 && $biodata->status_user == 0)
                                                        src="https://member.kelasonlinecopywriting.com/public/bukti-transfer-27-KOC/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                    @elseif ($biodata->idprodukreg == 29 )
                                                        src="https://app.copywritingnextlevel.com/public/bukti-transfer-29-CNL/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                    @elseif ($biodata->idprodukreg == 30 && $biodata->status_user == 0)
                                                        src="https://konfirmasi.agen-entrepreneurid.com/public/bukti-transfer-30-AGEN-BARU/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                    @elseif ($biodata->idprodukreg == 33 && $biodata->status_user == 0)
                                                        src="https://konfirmasi.mentoringinstantcopywriting.com/public/bukti-transfer-33-MIC/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                    @elseif ($biodata->idprodukreg == 34 && $biodata->status_user == 0)
                                                        src="https://konfirmasi.kelasfbhacking.com/public/bukti-transfer-34-KFH/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                    @elseif ($biodata->idprodukreg == 35 && $biodata->status_user == 0)
                                                        src="https://konfirmasi.mentoringsuperreseller.com/public/bukti-transfer-35-MSR/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                    @elseif ($biodata->idprodukreg == 39 && $biodata->status_user == 0)
                                                        src="https://konfirmasi.agen-entrepreneurid.com/public/bukti-transfer-39-AGEN-BARU/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                    @elseif ($biodata->idprodukreg == 18 && $biodata->status_user == 0)
                                                        src="https://wamasterclosing.com/member-area/buktitf/{{ $buktitf->buktitf }}"
{{--
                                                        @if(file_exists( 'https://wamasterclosing.com/member-area/buktitf/{{ $buktitf->buktitf }}' ))
                                                             src="https://wamasterclosing.com/member-area/buktitf/{{ $buktitf->buktitf }}"
                                                        @else
                                                             src="https://agen-entrepreneurid.com/affiliasi/buktitf/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                        @endif
                                                        src="https://wamasterclosing.com/member-area/buktitf/{{ $buktitf->buktitf }}" --}}
                                                    @else
                                                        src="https://agen-entrepreneurid.com/affiliasi/buktitf/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                    @endif
                                                @else
                                                    src="https://agen-entrepreneurid.com/affiliasi/buktitf/{{ $buktitf->buktitf ?? '404.jpg' }}"
                                                @endif
                                                alt="" height="50">
                                                @if ($biodata->idprodukreg == 29)
                                                    <img class="zoom" src="https://agen-entrepreneurid.com/affiliasi/buktitf/{{ $buktitf->buktitf ?? '404.jpg' }}" alt="" height="50">
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            @if ($biodata->idprodukreg == 29)
                                <table class="table table-sm table-borderless" style="margin-bottom: 0rem">
                                    <thead style="font-weight: 400;">
                                        <th>Lokasi</th>
                                        <th>Kodepos</th>
                                        <th>No. Resi</th>
                                        <th>Jumlah</th>
                                        <th>Kurir</th>
                                        <th>Ongkir</th>
                                        <th>Total</th>
                                        <th>Rek. Transfer</th>
                                    </thead>
                                    <tbody style="font-weight: 300;">
                                        <tr>
                                            <td>
                                                Prov. {{ $biodata->provinsi }}, {{ $biodata->kota }}, Kec. {{ $biodata->kecamatan }}
                                            </td>
                                            <td>
                                                {{ $biodata->kodepos }}
                                            </td>
                                            <td>
                                                {{ $biodata->noresi }}
                                            </td>
                                            <td>
                                                {{ $biodata->jumlah }} Buku
                                            </td>
                                            <td>
                                                <label style="text-transform:uppercase">{{ $biodata->kurir }}</label>
                                            </td>
                                            <td>
                                                Rp. {{ $biodata->ongkir }}
                                            </td>
                                            <td>
                                                Rp. {{ $biodata->total }}
                                            </td>
                                            <td>
                                                {{ $biodata->rek_bayar }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @php
        $first = $biodatas->firstItem();
        $end = $key + $biodatas->firstItem();
        @endphp
        @endforeach
    </section>
</div>
</div>
</div>
{{--
<div class="modal fade" id="modaledit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" >Update Data Peserta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-left">
                 <form>
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nama Lengkap</label>
                                <div class="col-sm-8">
                                    <input wire:model="unama" onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="nama" class="form-control" maxlength="100" placeholder="Nama Lengkap" required="">
                                </div>
                            </div><!--col-->
                        </div><!--row-->
                    </div><!--row-->
                    <div class="row">
                        <div class="col">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Alamat Email</label>
                                <div class="col-sm-8">
                                    <input wire:model="uemail" onkeyup="this.value = this.value.toLowerCase();" autocomplete="new-password" type="email" name="email" id="email" placeholder="Alamat Email" maxlength="191" required="required" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nama Panggilan</label>
                                <div class="col-sm-8">
                                    <input wire:model="upanggilan" type="text" name="panggilan" class="form-control" maxlength="100" placeholder="Nama Panggilan" required="">
                                </div>
                            </div><!--col-->
                        </div><!--row-->
                    </div><!--row-->
                    <div class="row">
                        <div class="col">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Jenis Kelamin</label>
                                <div class="col-sm-8">
                                    <input wire:model="ugender" type="text" name="gender" class="form-control" maxlength="100" placeholder="Jenis Kelamin" required="">
                                </div>
                            </div><!--col-->
                        </div><!--row-->
                    </div><!--row-->
                    <div class="row">
                        <div class="col">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nomo HP Whatsapp </label>
                                <div class="col-sm-8">
                                    <input wire:model="unohp" type="text" name="nohp" class="form-control" maxlength="100" placeholder="Nomor Whatsapp" required="">
                                </div>
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->
                    <div class="row">
                        <div class="col">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Tanggal Lahir</label>
                                <div class="col-sm-8">
                                    <input wire:model="utgllahir" type="text" name="tgllahir" class="form-control" maxlength="100" placeholder="Tanggal Lahir" required="">
                                </div>
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->
                    <div class="row">
                        <div class="col">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Asal Kota</label>
                                <div class="col-sm-8">
                                    <input wire:model="ukota" onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="kota" class="form-control" placeholder="Kota Domisili" required="">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col pull-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="button" class="btn btn-primary">Simpan</button>
                        </div><!--col-->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
--}}
<div class="row" style="padding-top:10px">
    <div class="col-7">
        <div class="float-left" style="padding-left:10px">
            {!! $first !!} - {!! $end !!} Dari {!! $biodatas->total() !!} Data
        </div>
    </div>
    <!--col-->

    <div class="col-5">
        <div class="float-right" style="padding-right:10px">
            {!! $biodatas->appends(request()->query())->links() !!}
        </div>
    </div>
    <!--col-->
</div>
<!--row-->
</div>
<!--card-body-->

{{-- <script type="text/javascript">
    window.livewire.on('alert', param => {
        toastr[param['type']](param['message'],param['type']);
    });
</script> --}}
<script>
    document.addEventListener("livewire:load", function(event) {
        window.livewire.hook('beforeDomUpdate', () => {

        });

        window.livewire.hook('afterDomUpdate', () => {

            var pieChart = new Chart($('#statusaktif'), {
                type: 'pie',
                data: {
                    labels: ['Belum Aktif - {!! $belumaktif->total() !!}','Belum Konfirmasi - {!! $belumkonfirmasi->total() !!}', 'Aktif - {!! $aktif->total() !!}', ],
                    datasets: [{
                        data: [ {!! $belumaktif->total() !!}, {!! $belumkonfirmasi->total() !!}, {!! $aktif->total() !!} ],
                        backgroundColor: ['#FF6384', '#FFCE56', '#4DBD74'],
                        // hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56', '#52c0c0']
                    }]
                },
                options: {
                    responsive: true
                }
            });
        });

        var pieChart = new Chart($('#statusaktif'), {
            type: 'pie',
            data: {
                labels: ['Belum Aktif - {!! $belumaktif->total() !!}','Belum Konfirmasi - {!! $belumkonfirmasi->total() !!}', 'Aktif - {!! $aktif->total() !!}', ],
                datasets: [{
                    data: [ {!! $belumaktif->total() !!}, {!! $belumkonfirmasi->total() !!}, {!! $aktif->total() !!} ],
                    backgroundColor: ['#FF6384', '#FFCE56', '#4DBD74'],
                    // hoverBackgroundColor: ['#FF6384', '#36A2EB', '#FFCE56', '#52c0c0']
                }]
            },
            options: {
                responsive: true
            }

        });



    });

</script>


</div>
