<div>
    <div class="row">
        <div class="col col-sm-4 order-1 order-sm-1  mb-4 d-none d-md-block" style="z-index: 100">
            <div class="sticky-top" style="top: 80px;">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div style="text-align: center;">
                                    <img class="card-img-top" src="{{ $memberkocpraktikum->picture ?? '' }}" alt="Profile Picture"
                                    style="
                                    object-fit: cover;
                                    height: 60px;
                                    width: 60px;
                                    border-radius: 50%;
                                    ">
                                </div>
                            </div>
                            <div class="col-md-9">
                                <h4 class="card-title">
                                    {{ $memberkocpraktikum->first_name ?? '' }}<br />
                                </h4>
                                <p class="card-text">
                                    <small>
                                        <i class="fas fa-envelope"></i> {{ $memberkocpraktikum->email ?? '' }}<br />
                                    </small>
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-top: 20px">
                                @stack('before-styles')
                                <style>
                                    .list-group-item.active {
                                        padding: 5px 10px;
                                        background-color: #d03c29;
                                        border-color: #d03c29;
                                    }
                                </style>
                                @stack('after-styles')
                                <div class="list-group">
                                    <a href="praktikum?ke=1&id={{ request()->input('id') }}" style="padding: 5px 10px;" class="list-group-item list-group-item-action {{ request()->input('ke') == '1' ? 'active' : '' }}">Praktikum 1</a>
                                    <a href="praktikum?ke=2&id={{ request()->input('id') }}" style="padding: 5px 10px;" class="list-group-item list-group-item-action {{ request()->input('ke') == '2' ? 'active' : '' }}">Praktikum 2</a>
                                    <a href="praktikum?ke=3&id={{ request()->input('id') }}" style="padding: 5px 10px;" class="list-group-item list-group-item-action {{ request()->input('ke') == '3' ? 'active' : '' }}">Praktikum 3</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-body">
                        @if (isset($waktustatusnilai->updated_at))
                        <div class="text-muted text-center" style="font-size:11px">
                            Penilaian Tersimpan : {{ $waktustatusnilai->updated_at->diffForHumans() }}
                        </div>
                        @endif
                        <p class="d-flex justify-content-between align-items-center" style="background-color: #f1f7ff; padding: 10px; border-radius: 10px">
                            <span class="">Penilaian :</span>
                            <strong>{{ $statusnilaihasil }}</strong>
                        </p>
                        <form wire:submit.prevent="penilaian">
                            <div class="row">
                                <div class="col-md-12" style="padding-bottom: 10px">
                                    <span>Form Deskripsi Penilaian :</span>
                                    <textarea wire:model="deskripsinilai" rows="5" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <select wire:model="statusnilai" required class="form-control" style="font-size: 12px" name="chartproduk">
                                        <option value=""></option>
                                        {{-- <option value="Kumpul Tugas">Kumpul Tugas</option> --}}
                                        {{-- <option value="Menunggu Koreksi">Menunggu Koreksi</option> --}}
                                        <option value="Perlu Diulang">Perlu diulang</option>
                                        <option value="Lulus Praktikum">Lulus Praktikum</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn btn-info btn-block btn-sm">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @if (request()->input('ke') === '1')
        <div class="col-md-8 order-2 order-sm-2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card" style="padding: 25px; ">
                        <div class="card-header" style="border-radius: 15px;">
                            <h4 class="text-center" style="padding-top:10px;">
                                Masukan Data tentang Bisnis Anda
                                @if (isset($djawaban1bagian1->updated_at))
                                <div class="text-muted text-center" style="font-size:11px">
                                    Tersimpan : {{ $djawaban1bagian1->updated_at->diffForHumans() }}
                                </div>
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-12">
                            <p>
                                <i class="fa fa-angle-right fa-lg mt-4"></i>
                                Cerita singkat tentang latar belakang Target Pasar
                            </p>
                            <textarea disabled wire:model="jawaban1bagian1" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>

                        <div class="col-md-12">
                            <p>
                                <i class="fa fa-angle-right fa-lg mt-4"></i>
                                Tantangan yang mereka hadapi
                            </p>
                            <textarea disabled wire:model="jawaban2bagian1" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>

                        <div class="col-md-12">
                            <p>
                                <i class="fa fa-angle-right fa-lg mt-4"></i>
                                Keinginan mereka
                            </p>
                            <textarea disabled wire:model="jawaban3bagian1" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>

                        <div class="col-md-12">
                            <p>
                                <i class="fa fa-angle-right fa-lg mt-4"></i>
                                Value produknya
                            </p>
                            <textarea disabled wire:model="jawaban4bagian1" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>

                        <div class="col-md-12">
                            <p>
                                <i class="fa fa-angle-right fa-lg mt-4"></i>
                                Penawarannya apa
                            </p>
                            <textarea disabled wire:model="jawaban5bagian1" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>

                        <div class="col-md-12">
                            <p>
                                <i class="fa fa-angle-right fa-lg mt-4"></i>
                                Bagaimana cara membuat mereka langsung membeli
                            </p>
                            <textarea disabled wire:model="jawaban6bagian1" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card" style="padding: 25px; ">
                        <div class="card-header" style="border-radius: 15px;">
                            <h4 class="text-center" style="padding-top:10px;">
                                Masukan Kerangka Copywriting
                                @if (isset($djawaban1bagian2->updated_at))
                                <div class="text-muted text-center" style="font-size:11px">
                                    Tersimpan : {{ $djawaban1bagian2->updated_at->diffForHumans() }}
                                </div>
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-12">
                            <p>
                                <i class="fa fa-angle-right fa-lg mt-4"></i>
                                Headline
                            </p>
                            <textarea disabled wire:model="jawaban1bagian2" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>

                        <div class="col-md-12">
                            <p>
                                <i class="fa fa-angle-right fa-lg mt-4"></i>
                                Pembuka Copywriting
                            </p>
                            <textarea disabled wire:model="jawaban2bagian2" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>

                        <div class="col-md-12">
                            <p>
                                <i class="fa fa-angle-right fa-lg mt-4"></i>
                                Menjelaskan value produk
                            </p>
                            <textarea disabled wire:model="jawaban3bagian2" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>

                        <div class="col-md-12">
                            <p>
                                <i class="fa fa-angle-right fa-lg mt-4"></i>
                                Mengarahkan untuk beli
                            </p>
                            <textarea disabled wire:model="jawaban4bagian2" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card" style="padding: 25px; ">
                        <div class="card-header" style="border-radius: 15px;">
                            <h4 class="text-center" style="padding-top:10px;">
                                Kumpulkan Copywriting dengan Kerangka diatas
                                @if (isset($djawaban1bagian3->updated_at))
                                <div class="text-muted text-center" style="font-size:11px">
                                    Tersimpan : {{ $djawaban1bagian3->updated_at->diffForHumans() }}
                                </div>
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px">
                            <textarea disabled rows="20" wire:model="jawaban1bagian3" class="form-control" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        @elseif (request()->input('ke') === '2')
        <div class="col-md-8 order-2 order-sm-2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card" style="padding: 25px; ">
                        <div class="card-header" style="border-radius: 15px;">
                            <h4 class="text-center" style="padding-top:10px;">
                                Kumpulkan Copywriting dengan Formula AIDCA
                                @if (isset($djawaban1bagian1->updated_at))
                                <div class="text-muted text-center" style="font-size:11px">
                                    Tersimpan : {{ $djawaban1bagian1->updated_at->diffForHumans() }}
                                </div>
                                @php
                                if ($djawaban1bagian1->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian1->deskripsi_praktikum_jawaban === '') {
                                    $a = 'border border-danger';
                                } else {
                                    $a = 'border border-success';
                                }
                                @endphp
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px">
                            <textarea disabled style="background-color: #fff" id="froala-editor" rows="12" wire:model="jawaban1bagian1" class="form-control {{ $a ?? '' }}" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card" style="padding: 25px; ">
                        <div class="card-header" style="border-radius: 15px;">
                            <h4 class="text-center" style="padding-top:10px;">
                                Kumpulkan Copywriting dengan Formula BAB
                                @if (isset($djawaban1bagian2->updated_at))
                                <div class="text-muted text-center" style="font-size:11px">
                                    Tersimpan : {{ $djawaban1bagian2->updated_at->diffForHumans() }}
                                </div>
                                @php
                                if ($djawaban1bagian2->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian2->deskripsi_praktikum_jawaban === '') {
                                    $b = 'border border-danger';
                                } else {
                                    $b = 'border border-success';
                                }
                                @endphp
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px">
                            <textarea disabled style="background-color: #fff" rows="12" wire:model="jawaban1bagian2" class="form-control {{ $b ?? '' }}" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card" style="padding: 25px; ">
                        <div class="card-header" style="border-radius: 15px;">
                            <h4 class="text-center" style="padding-top:10px;">
                                Kumpulkan Copywriting dengan Formula FAB
                                @if (isset($djawaban1bagian3->updated_at))
                                <div class="text-muted text-center" style="font-size:11px">
                                    Tersimpan : {{ $djawaban1bagian3->updated_at->diffForHumans() }}
                                </div>
                                @php
                                if ($djawaban1bagian3->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian3->deskripsi_praktikum_jawaban === '') {
                                    $c = 'border border-danger';
                                } else {
                                    $c = 'border border-success';
                                }
                                @endphp
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px">
                            <textarea disabled style="background-color: #fff" rows="12" wire:model="jawaban1bagian3" class="form-control {{ $c ?? '' }}" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card" style="padding: 25px; ">
                        <div class="card-header" style="border-radius: 15px;">
                            <h4 class="text-center" style="padding-top:10px;">
                                Kumpulkan Copywriting dengan Formula PAS
                                @if (isset($djawaban1bagian4->updated_at))
                                <div class="text-muted text-center" style="font-size:11px">
                                    Tersimpan : {{ $djawaban1bagian4->updated_at->diffForHumans() }}
                                </div>
                                @php
                                if ($djawaban1bagian4->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian4->deskripsi_praktikum_jawaban === '') {
                                    $d = 'border border-danger';
                                } else {
                                    $d = 'border border-success';
                                }
                                @endphp
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px">
                            <textarea disabled style="background-color: #fff" rows="12" wire:model="jawaban1bagian4" class="form-control {{ $d ?? '' }}" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card" style="padding: 25px; ">
                        <div class="card-header" style="border-radius: 15px;">
                            <h4 class="text-center" style="padding-top:10px;">
                                Kumpulkan Copywriting dengan Formula PPPP
                                @if (isset($djawaban1bagian5->updated_at))
                                <div class="text-muted text-center" style="font-size:11px">
                                    Tersimpan : {{ $djawaban1bagian5->updated_at->diffForHumans() }}
                                </div>
                                @php
                                if ($djawaban1bagian5->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian5->deskripsi_praktikum_jawaban === '') {
                                    $e = 'border border-danger';
                                } else {
                                    $e = 'border border-success';
                                }
                                @endphp
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px">
                            <textarea disabled style="background-color: #fff" rows="12" wire:model="jawaban1bagian5" class="form-control {{ $e ?? '' }}" placeholder="Isi Jawaban Anda Disini"></textarea>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
        @elseif (request()->input('ke') === '3')
        <div class="col-md-8 order-2 order-sm-2">
            <div class="row">
                <div class="col-md-12">
                    <div class="card" style="padding: 25px; ">
                        <div class="card-header" style="border-radius: 15px;">
                            <h4 class="text-center" style="padding-top:10px;">
                                Nama Produk
                                @if (isset($djawaban1bagian1->updated_at))
                                <div class="text-muted text-center" style="font-size:11px">
                                    Tersimpan : {{ $djawaban1bagian1->updated_at->diffForHumans() }}
                                </div>
                                @php
                                if ($djawaban1bagian1->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian1->deskripsi_praktikum_jawaban === '') {
                                    $a = 'border border-danger';
                                } else {
                                    $a = 'border border-success';
                                }
                                @endphp
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px">
                            <input type="text" rows="10" wire:model="jawaban1bagian1" class="form-control {{ $a ?? '' }}" placeholder="Isi Jawaban Anda Disini">
                        </div>
                        <hr>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card" style="padding: 25px; ">
                        <div class="card-header" style="border-radius: 15px;">
                            <h4 class="text-center" style="padding-top:10px;">
                                Story Telling
                                @if (isset($djawaban1bagian2->updated_at))
                                <div class="text-muted text-center" style="font-size:11px">
                                    Tersimpan : {{ $djawaban1bagian2->updated_at->diffForHumans() }}
                                </div>
                                @php
                                if ($djawaban1bagian2->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian2->deskripsi_praktikum_jawaban === '') {
                                    $b = 'border border-danger';
                                } else {
                                    $b = 'border border-success';
                                }
                                @endphp
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px">
                            <div class="row">
                                <label class="col-md-5 col-form-label" for="select1">Teknik Story Telling yang Digunakan :</label>
                                <div class="col-md-7">
                                    <select class="form-control" wire:model="jawaban1bagian2" disabled>
                                        <option value="Teknik Monomyth">Teknik Monomyth</option>
                                        <option value="Teknik Sparkline">Teknik Sparkline</option>
                                        <option value="Teknik The Mountain">Teknik The Mountain</option>
                                        <option value="Teknik False Start">Teknik False Start</option>
                                        <option value="Teknik Nasted Loops">Teknik Nasted Loops</option>
                                        <option value="Teknik Medias Res">Teknik Medias Res</option>
                                        <option value="Teknik Converging Ideas">Teknik Converging Ideas</option>
                                        <option value="Teknik Petak Structure">Teknik Petak Structure</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px">
                            <textarea rows="10" wire:model="jawaban2bagian2" class="form-control {{ $b ?? '' }}" placeholder="Isi Story Telling Disini"></textarea>
                        </div>
                        <hr>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card" style="padding: 25px; ">
                        <div class="card-header" style="border-radius: 15px;">
                            <h4 class="text-center" style="padding-top:10px;">
                                Pola Bahasa Hipnotik
                                @if (isset($djawaban1bagian3->updated_at))
                                <div class="text-muted text-center" style="font-size:11px">
                                    Tersimpan : {{ $djawaban1bagian3->updated_at->diffForHumans() }}
                                </div>
                                @php
                                if ($djawaban1bagian3->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian3->deskripsi_praktikum_jawaban === '') {
                                    $c = 'border border-danger';
                                } else {
                                    $c = 'border border-success';
                                }
                                @endphp
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px">
                            <div class="row">
                                <label class="col-md-6 col-form-label">
                                    Pola bahasa hipnotik yang digunakan :
                                    <div class="text-muted" style="font-size: 12px">Pola bahasa hipnotik bisa digunakan lebih dari satu pola.</div>
                                </label>
                                <div class="col-md-6 col-form-label">
                                    <div class="form-check checkbox">
                                        <input class="form-check-input" wire:model="jawaban1bagian3.Delection" type="checkbox" value="Delection" disabled>
                                        <label class="form-check-label">Delection </label>
                                    </div>
                                    <div class="form-check checkbox">
                                        <input class="form-check-input" wire:model="jawaban1bagian3.Generalitation" type="checkbox" value="Generalitation" disabled>
                                        <label class="form-check-label">Generalitation</label>
                                    </div>
                                    <div class="form-check checkbox">
                                        <input class="form-check-input" wire:model="jawaban1bagian3.Distortion" type="checkbox" value="Distortion" disabled>
                                        <label class="form-check-label">Distortion</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px">
                            <textarea rows="10" wire:model="jawaban2bagian3" class="form-control {{ $c ?? '' }}" placeholder="Isi Pola Bahasa Hipnotik"></textarea>
                        </div>
                        <hr>

                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card" style="padding: 25px; ">
                        <div class="card-header" style="border-radius: 15px;">
                            <h4 class="text-center" style="padding-top:10px;">
                                Handle Objection
                                @if (isset($djawaban1bagian4->updated_at))
                                <div class="text-muted text-center" style="font-size:11px">
                                    Tersimpan : {{ $djawaban1bagian4->updated_at->diffForHumans() }}
                                </div>
                                @php
                                if ($djawaban1bagian4->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian4->deskripsi_praktikum_jawaban === '') {
                                    $d = 'border border-danger';
                                } else {
                                    $d = 'border border-success';
                                }
                                @endphp
                                @endif
                            </h4>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px">
                            <div class="row">
                                <label class="col-md-6 col-form-label">
                                    Handle Objection yang Digunakan :
                                    <div class="text-muted" style="font-size: 12px">Pilihan dapat lebih dari satu.</div>
                                </label>
                                <div class="col-md-6 col-form-label">
                                    <div class="form-check checkbox">
                                        <input class="form-check-input" wire:model="jawaban1bagian4.Kebutuhan" type="checkbox" value="Kebutuhan" disabled>
                                        <label class="form-check-label">Kebutuhan</label>
                                    </div>
                                    <div class="form-check checkbox">
                                        <input class="form-check-input" wire:model="jawaban1bagian4.Waktu" type="checkbox" value="Waktu" disabled>
                                        <label class="form-check-label">Waktu</label>
                                    </div>
                                    <div class="form-check checkbox">
                                        <input class="form-check-input" wire:model="jawaban1bagian4.Harga" type="checkbox" value="Harga" disabled>
                                        <label class="form-check-label">Harga</label>
                                    </div>
                                    <div class="form-check checkbox">
                                        <input class="form-check-input" wire:model="jawaban1bagian4.Kualitas" type="checkbox" value="Kualitas" disabled>
                                        <label class="form-check-label">Kualitas</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top: 20px">
                            <textarea rows="10" wire:model="jawaban2bagian4" class="form-control {{ $d ?? '' }}" placeholder="Isi Handle Objection"></textarea>
                        </div>
                        <hr>

                    </div>
                </form>
            </div>
            <div class="col-md-12">
                <div class="card" style="padding: 25px; ">
                    <div class="card-header" style="border-radius: 15px;">
                        <h4 class="text-center" style="padding-top:10px;">
                            Indirrect Elicitation
                            @if (isset($djawaban1bagian5->updated_at))
                            <div class="text-muted text-center" style="font-size:11px">
                                Tersimpan : {{ $djawaban1bagian5->updated_at->diffForHumans() }}
                            </div>
                            @php
                            if ($djawaban1bagian5->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian5->deskripsi_praktikum_jawaban === '') {
                                $e = 'border border-danger';
                            } else {
                                $e = 'border border-success';
                            }
                            @endphp
                            @endif
                        </h4>
                    </div>
                    <div class="col-md-12" style="padding-top: 20px">
                        <div class="row">
                            <label class="col-md-6 col-form-label">
                                Indirrect Elicitation yang Digunakan :
                                <div class="text-muted" style="font-size: 12px">Pilihan dapat lebih dari satu.</div>
                            </label>
                            <div class="col-md-6 col-form-label">
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian5.Embedded-Command" type="checkbox" value="Embedded Command" disabled>
                                    <label class="form-check-label">Embedded Command</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian5.Embedded-Question" type="checkbox" value="Embedded Question" disabled>
                                    <label class="form-check-label">Embedded Question</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian5.Question-Tag" type="checkbox" value="Question Tag" disabled>
                                    <label class="form-check-label">Question Tag</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian5.Negative-Command" type="checkbox" value="Negative Command" disabled>
                                    <label class="form-check-label">Negative Command</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian5.Converstation-Postulates" type="checkbox" value="Converstation Postulates" disabled>
                                    <label class="form-check-label">Converstation Postulates</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian5.Ambiguity" type="checkbox" value="Ambiguity" disabled>
                                    <label class="form-check-label">Ambiguity</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="padding-top: 20px">
                        <textarea rows="10" wire:model="jawaban2bagian5" class="form-control {{ $e ?? '' }}" placeholder="Isi Indirrect Elicitation"></textarea>
                    </div>
                    <hr>

                </div>
            </div>
            <div class="col-md-12">
                <div class="card" style="padding: 25px; ">
                    <div class="card-header" style="border-radius: 15px;">
                        <h4 class="text-center" style="padding-top:10px;">
                            Kumpulkan Copywriting Lengkap dengan Semua Elemen diatas
                            @if (isset($djawaban1bagian6->updated_at))
                            <div class="text-muted text-center" style="font-size:11px">
                                Tersimpan : {{ $djawaban1bagian6->updated_at->diffForHumans() }}
                            </div>
                            @php
                                if ($djawaban1bagian6->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian6->deskripsi_praktikum_jawaban === '') {
                                    $e = 'border border-danger';
                                } else {
                                    $e = 'border border-success';
                                }
                            @endphp
                            @endif
                        </h4>
                    </div>
                    <div class="col-md-12" style="padding-top: 20px">
                        <textarea rows="14" wire:model="jawaban1bagian6" class="form-control {{ $e ?? '' }}" placeholder="Isi Indirrect Elicitation"></textarea>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
</div><!-- row -->
