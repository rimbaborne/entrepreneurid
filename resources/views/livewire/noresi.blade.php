<div wire:ignore.self class="modal fade" id="updateNoresi" tabindex="-1" role="dialog" aria-labelledby="updateNoresiLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="updateNoresiLabel">Update Nomor Resi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <input type="hidden" wire:model="user_id">
                        <label for="noresiInput">No. Resi <strong>{{ $nama }} - {{ $email }}</strong></label>
                        <input type="text" class="form-control" wire:model.lazy="noresi" id="noresiInput" placeholder="Masukkan Nomor Resi">
                        @error('noresi') <span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancelFormNoresi()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="updateNoresi()" class="btn btn-primary" data-dismiss="modal">Simpan</button>
            </div>
       </div>
    </div>
</div>
