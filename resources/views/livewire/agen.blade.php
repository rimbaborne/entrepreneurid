<div>
    {{-- {{ last(request()->segments()) }} --}}
    <div class="card">
        <div class="card-body">
            <h4>Agen entrepreneurID</h4>
            <div class="text-muted">Dialy Contest</div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-4 pull-right">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> <i class="fa fa-search"></i> </span>
                        </div>
                        <input wire:model.debounce.1000ms="search" class="form-control" type="text" placeholder="Cari Nama atau Email"
                            autocomplete="password" width="100">
                    </div>
                </div>
                <div class="col-md-8">

                </div>
            </div>
            <div class="text-right text-muted" style="padding-right: 10px">
                Total : {!! $agens->total() ?? 0 !!}
            </div>
            <table class="table table-sm table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                    <tr>
                        <th width="70" class="text-center">
                            <i class="fas fa-user"></i>
                        </th>
                        <th>
                            <a wire:click.prevent="sortBy('created_at')" role="button" href="#">
                                Daftar
                                @include('backend.includes._sort-icon', ['field' => 'created_at'])
                            </a>
                            <span class="float-right">
                                <a wire:click.prevent="sortBy('nama')" role="button" href="#">
                                    Nama
                                    @include('backend.includes._sort-icon', ['field' => 'nama'])
                                </a>
                            </span>
                        </th>
                        <th>
                            <a wire:click.prevent="sortBy('email')" role="button" href="#">
                                Email
                                @include('backend.includes._sort-icon', ['field' => 'email'])
                            </a>
                        </th>
                        <th class="text-center">Praktikum</th>
                        <th>Status</th>
                        <th>Update Tugas</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($agens as $key=> $agen)
                    <tr>
                        <td class="text-center">
                            <a href="#">
                                <div class="avatar">
                                    <img class="img-avatar" src="" alt="">
                                    <span class="avatar-status badge-success"></span>
                                </div>
                            </a>
                        </td>
                        <td>
                            <a href="#">
                                <div>{{ $agen->nama }}</div>
                                <div class="small text-muted">
                                    <span>Register : {{ $agen->created_at->diffForHumans() }}</span>
                                </div>
                            </a>
                        </td>
                        <td>
                            {{ $agen->email }}
                            <div class="small text-muted">
                                Akses
                            </div>
                        </td>
                        <td>
                            <div class="text-center" style="font-weight: 700">
                                oke
                            </div>
                        </td>
                        <td>
                            <button class="btn btn-pill btn-sm ">

                            </button>
                        </td>
                        <td>
                            <strong style="font-size: 11px">
                                {{-- {{ isset($datanilai->updated_at) ? \Carbon\Carbon::parse($datanilai->updated_at)->diffForHumans() : '-' }} --}}
                            </strong>
                        </td>
                    </tr>
                    @php
                        $first = $agens->firstItem() ?? 0;
                        $end = $key + $agens->firstItem() ?? 0;
                    @endphp
                    @endforeach
                </tbody>
            </table>
            <div class="row" style="padding-top:10px">
                <div class="col-7">
                    <div class="float-left" style="padding-left:10px">
                        {!! $first ?? 0 !!} - {!! $end ?? 0 !!} Dari {!! $agens->total() ?? 0 !!} Data
                    </div>
                </div>
                <!--col-->

                <div class="col-5">
                    <div class="float-right" style="padding-right:10px">
                        {!! $agens->appends(request()->query())->links() !!}
                    </div>
                </div>
                <!--col-->
            </div>
        </div>
        <!--card-body-->
    </div>
    <!--card-->
</div>
