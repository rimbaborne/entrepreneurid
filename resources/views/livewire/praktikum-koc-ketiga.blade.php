<div>
    <div class="row" >
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/dashboard">Kelas Online Copywriting</a></li>
                <li class="breadcrumb-item active">Praktikum Ketiga</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <form wire:submit.prevent="bagiansatu">
                @csrf
                <div class="card" style="padding: 25px; ">
                    <div class="card-header" style="border-radius: 15px;">
                        <h4 class="text-center" style="padding-top:10px;">
                            Nama Produk
                            @if (isset($djawaban1bagian1->updated_at))
                            <div class="text-muted text-center" style="font-size:11px">
                                Tersimpan : {{ $djawaban1bagian1->updated_at->diffForHumans() }}
                            </div>
                            @php
                            if ($djawaban1bagian1->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian1->deskripsi_praktikum_jawaban === '') {
                                $a = 'border border-danger';
                            } else {
                                $a = 'border border-success';
                            }
                            @endphp
                            @endif
                        </h4>
                    </div>
                    <div class="col-md-12" style="padding-top: 20px">
                        <input type="text" rows="10" wire:model="jawaban1bagian1" class="form-control {{ $a ?? '' }}" placeholder="Isi Jawaban Anda Disini">
                    </div>
                    <hr>
                    @if(session()->get('satu'))
                    <div class="alert alert-success text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        Jawaban Berhasil Tersimpan !
                    </div>
                    @endif
                    <button class="btn btn-info btn-pill">Simpan</button>
                </div>
            </form>
        </div>
        <div class="col-md-12">
            <form wire:submit.prevent="bagiandua">
                @csrf
                <div class="card" style="padding: 25px; ">
                    <div class="card-header" style="border-radius: 15px;">
                        <h4 class="text-center" style="padding-top:10px;">
                            Story Telling
                            @if (isset($djawaban1bagian2->updated_at))
                            <div class="text-muted text-center" style="font-size:11px">
                                Tersimpan : {{ $djawaban1bagian2->updated_at->diffForHumans() }}
                            </div>
                            @php
                            if ($djawaban1bagian2->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian2->deskripsi_praktikum_jawaban === '') {
                                $b = 'border border-danger';
                            } else {
                                $b = 'border border-success';
                            }
                            @endphp
                            @endif
                        </h4>
                    </div>
                    <div class="col-md-12" style="padding-top: 20px">
                        <div class="row">
                            <label class="col-md-5 col-form-label" for="select1">Teknik Story Telling yang Digunakan :</label>
                            <div class="col-md-7">
                                <select class="form-control" wire:model="jawaban1bagian2">
                                    <option value="Teknik Monomyth">Teknik Monomyth</option>
                                    <option value="Teknik Sparkline">Teknik Sparkline</option>
                                    <option value="Teknik The Mountain">Teknik The Mountain</option>
                                    <option value="Teknik False Start">Teknik False Start</option>
                                    <option value="Teknik Nasted Loops">Teknik Nasted Loops</option>
                                    <option value="Teknik Medias Res">Teknik Medias Res</option>
                                    <option value="Teknik Converging Ideas">Teknik Converging Ideas</option>
                                    <option value="Teknik Petak Structure">Teknik Petak Structure</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="padding-top: 20px">
                        <textarea rows="10" wire:model="jawaban2bagian2" class="form-control {{ $b ?? '' }}" placeholder="Isi Story Telling Disini"></textarea>
                    </div>
                    <hr>
                    @if(session()->get('dua'))
                    <div class="alert alert-success text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        Jawaban Berhasil Tersimpan !
                    </div>
                    @endif
                    <button class="btn btn-info btn-pill">Simpan</button>
                </div>
            </form>
        </div>
        <div class="col-md-12">
            <form wire:submit.prevent="bagiantiga">
                @csrf
                <div class="card" style="padding: 25px; ">
                    <div class="card-header" style="border-radius: 15px;">
                        <h4 class="text-center" style="padding-top:10px;">
                            Pola Bahasa Hipnotik
                            @if (isset($djawaban1bagian3->updated_at))
                            <div class="text-muted text-center" style="font-size:11px">
                                Tersimpan : {{ $djawaban1bagian3->updated_at->diffForHumans() }}
                            </div>
                            @php
                            if ($djawaban1bagian3->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian3->deskripsi_praktikum_jawaban === '') {
                                $c = 'border border-danger';
                            } else {
                                $c = 'border border-success';
                            }
                            @endphp
                            @endif
                        </h4>
                    </div>
                    <div class="col-md-12" style="padding-top: 20px">
                        <div class="row">
                            <label class="col-md-6 col-form-label">
                                Pola bahasa hipnotik yang digunakan :
                                <div class="text-muted" style="font-size: 12px">Pola bahasa hipnotik bisa digunakan lebih dari satu pola.</div>
                            </label>
                            <div class="col-md-6 col-form-label">
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian3.Delection" type="checkbox" value="Delection">
                                    <label class="form-check-label">Delection </label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian3.Generalitation" type="checkbox" value="Generalitation">
                                    <label class="form-check-label">Generalitation</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian3.Distortion" type="checkbox" value="Distortion">
                                    <label class="form-check-label">Distortion</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="padding-top: 20px">
                        <textarea rows="10" wire:model="jawaban2bagian3" class="form-control {{ $c ?? '' }}" placeholder="Isi Pola Bahasa Hipnotik"></textarea>
                    </div>
                    <hr>
                    @if(session()->get('tiga'))
                    <div class="alert alert-success text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        Jawaban Berhasil Tersimpan !
                    </div>
                    @endif
                    <button class="btn btn-info btn-pill">Simpan</button>
                </div>
            </form>
        </div>
        <div class="col-md-12">
            <form wire:submit.prevent="bagianempat">
                @csrf
                <div class="card" style="padding: 25px; ">
                    <div class="card-header" style="border-radius: 15px;">
                        <h4 class="text-center" style="padding-top:10px;">
                            Handle Objection
                            @if (isset($djawaban1bagian4->updated_at))
                            <div class="text-muted text-center" style="font-size:11px">
                                Tersimpan : {{ $djawaban1bagian4->updated_at->diffForHumans() }}
                            </div>
                            @php
                            if ($djawaban1bagian4->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian4->deskripsi_praktikum_jawaban === '') {
                                $d = 'border border-danger';
                            } else {
                                $d = 'border border-success';
                            }
                            @endphp
                            @endif
                        </h4>
                    </div>
                    <div class="col-md-12" style="padding-top: 20px">
                        <div class="row">
                            <label class="col-md-6 col-form-label">
                                Handle Objection yang Digunakan :
                                <div class="text-muted" style="font-size: 12px">Pilihan dapat lebih dari satu.</div>
                            </label>
                            <div class="col-md-6 col-form-label">
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian4.Kebutuhan" type="checkbox" value="Kebutuhan">
                                    <label class="form-check-label">Kebutuhan</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian4.Waktu" type="checkbox" value="Waktu">
                                    <label class="form-check-label">Waktu</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian4.Harga" type="checkbox" value="Harga">
                                    <label class="form-check-label">Harga</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian4.Kualitas" type="checkbox" value="Kualitas">
                                    <label class="form-check-label">Kualitas</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="padding-top: 20px">
                        <textarea rows="10" wire:model="jawaban2bagian4" class="form-control {{ $d ?? '' }}" placeholder="Isi Handle Objection"></textarea>
                    </div>
                    <hr>
                    @if(session()->get('empat'))
                    <div class="alert alert-success text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        Jawaban Berhasil Tersimpan !
                    </div>
                    @endif
                    <button class="btn btn-info btn-pill">Simpan</button>
                </div>
            </form>
        </div>
        <div class="col-md-12">
            <form wire:submit.prevent="bagianlima">
                @csrf
                <div class="card" style="padding: 25px; ">
                    <div class="card-header" style="border-radius: 15px;">
                        <h4 class="text-center" style="padding-top:10px;">
                            Indirrect Elicitation
                            @if (isset($djawaban1bagian5->updated_at))
                            <div class="text-muted text-center" style="font-size:11px">
                                Tersimpan : {{ $djawaban1bagian5->updated_at->diffForHumans() }}
                            </div>
                            @php
                                if ($djawaban1bagian5->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian5->deskripsi_praktikum_jawaban === '') {
                                    $e = 'border border-danger';
                                } else {
                                    $e = 'border border-success';
                                }
                            @endphp
                            @endif
                        </h4>
                    </div>
                    <div class="col-md-12" style="padding-top: 20px">
                        <div class="row">
                            <label class="col-md-6 col-form-label">
                                Indirrect Elicitation yang Digunakan :
                                <div class="text-muted" style="font-size: 12px">Pilihan dapat lebih dari satu.</div>
                            </label>
                            <div class="col-md-6 col-form-label">
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian5.Embedded-Command" type="checkbox" value="Embedded Command">
                                    <label class="form-check-label">Embedded Command</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian5.Embedded-Question" type="checkbox" value="Embedded Question">
                                    <label class="form-check-label">Embedded Question</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian5.Question-Tag" type="checkbox" value="Question Tag">
                                    <label class="form-check-label">Question Tag</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian5.Negative-Command" type="checkbox" value="Negative Command">
                                    <label class="form-check-label">Negative Command</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian5.Converstation-Postulates" type="checkbox" value="Converstation Postulates">
                                    <label class="form-check-label">Converstation Postulates</label>
                                </div>
                                <div class="form-check checkbox">
                                    <input class="form-check-input" wire:model="jawaban1bagian5.Ambiguity" type="checkbox" value="Ambiguity">
                                    <label class="form-check-label">Ambiguity</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="padding-top: 20px">
                        <textarea rows="10" wire:model="jawaban2bagian5" class="form-control {{ $e ?? '' }}" placeholder="Isi Indirrect Elicitation"></textarea>
                    </div>
                    <hr>
                    @if(session()->get('lima'))
                    <div class="alert alert-success text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        Jawaban Berhasil Tersimpan !
                    </div>
                    @endif
                    <button class="btn btn-info btn-pill">Simpan</button>
                </div>
            </form>
        </div>
        <div class="col-md-12">
            <form wire:submit.prevent="bagianenam">
                @csrf
                <div class="card" style="padding: 25px; ">
                    <div class="card-header" style="border-radius: 15px;">
                        <h4 class="text-center" style="padding-top:10px;">
                            Kumpulkan Copywriting Lengkap dengan Semua Elemen diatas
                            @if (isset($djawaban1bagian6->updated_at))
                            <div class="text-muted text-center" style="font-size:11px">
                                Tersimpan : {{ $djawaban1bagian6->updated_at->diffForHumans() }}
                            </div>
                            @php
                                if ($djawaban1bagian6->deskripsi_praktikum_jawaban === ' ' || $djawaban1bagian6->deskripsi_praktikum_jawaban === '') {
                                    $e = 'border border-danger';
                                } else {
                                    $e = 'border border-success';
                                }
                            @endphp
                            @endif
                        </h4>
                    </div>
                    <div class="col-md-12" style="padding-top: 20px">
                        <textarea rows="14" wire:model="jawaban1bagian6" class="form-control {{ $e ?? '' }}" placeholder="Isi Indirrect Elicitation"></textarea>
                    </div>
                    <hr>
                    @if(session()->get('enam'))
                    <div class="alert alert-success text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        Jawaban Berhasil Tersimpan !
                    </div>
                    @endif
                    <button class="btn btn-info btn-pill">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
