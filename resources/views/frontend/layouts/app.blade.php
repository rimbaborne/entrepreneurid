<!DOCTYPE html>
@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', 'entrepreneurID')">
        <meta name="author" content="@yield('meta_author', 'rimbaborne')">
        <link rel="shortcut icon" href="{{ asset('img/logo-official-agen.png') }}">
        @yield('meta')

        {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
        @stack('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style(mix('css/backend.css')) }}
        {{ style('https://coreui.io/demo/2.0/vendors/simple-line-icons/css/simple-line-icons.css') }}


        <style>
            body {
                background-color: #F6F7FB;
            }

            .card {
                box-shadow: 0 5px 24px 0 rgba(50, 49, 58, .10);
                border-radius: 10px;
                padding: 10px;
                background-color: #fff;
            }
            .m {
                color: #ef4153;
                padding-right: 15px;
                font-size: 16px
            }

            .list-group > a {
                color: #040404;
            }

            .list-group > a:hover {
                background-color: #eee;
                text-decoration: none
            }

            .list-group-item.active {
                padding: 5px 10px;
                background-color: #d03c29;
                border-color: #d03c29;
            }

        </style>

        @stack('after-styles')
        @livewireStyles
        @stack('before-scripts')
        {!! script(mix('js/manifest.js')) !!}
        {!! script(mix('js/vendor.js')) !!}
        {!! script(mix('js/frontend.js')) !!}
        {!! script('https://cdnjs.cloudflare.com/ajax/libs/turbolinks/5.2.0/turbolinks.js') !!}

        @stack('after-scripts')
    </head>
    <body>
        @include('includes.partials.demo')

        <div id="app">
            @include('includes.partials.logged-in-as')
            @if ( $logged_in_user->jenis_user == 'AGEN')

            @include('frontend.includes.nav')
            <div class="container">

            @else
            <div class="container">
                @include('frontend.includes.nav-customer')

            @endif

                @include('includes.partials.messages')
                @yield('content')
            </div><!-- container -->
            <div class="text-center text-muted">
                entrepreneurID © {{ \Carbon\Carbon::now()->year }}
            </div>
        </div><!-- #app -->

        @include('includes.partials.ga')
        @livewireScripts
    </body>
</html>
