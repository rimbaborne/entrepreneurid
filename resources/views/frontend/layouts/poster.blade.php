<!DOCTYPE html>
@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <meta name="description" content="@yield('meta_description', 'entrepreneurID')">
        <meta name="author" content="@yield('meta_author', 'rimbaborne')">
        <link rel="shortcut icon" href="{{ asset('img/logo-official-agen.png') }}">
        @yield('meta')

        {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
        @stack('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style(mix('css/backend.css')) }}


        {{-- <link href="https://coreui.io/demo/3.4.0/vendors/@coreui/icons/css/free.min.css" rel="stylesheet"> --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
        <style>
            body, html {
                height: 100%;
            }
            body {
                /* background: linear-gradient(90deg, #7b2123 15%, #d63f42 70%, #d63f42 94%); */
                /* background: linear-gradient(90deg, #171720 15%, #1f1f2b 70%, #4f4e59 100%); */
                    background: aliceblue;
            }

            .card {
                /* box-shadow: 0 5px 24px 0 rgba(50, 49, 58, .25); */
                border-radius: 10px;
                padding: 15px;
                background-color: #fff;
            }
        </style>

        @stack('after-styles')

        <!-- Scripts -->
        @stack('before-scripts')
        {{-- {{ script('https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js') }} --}}


        {!! script(mix('js/manifest.js')) !!}
        {!! script(mix('js/vendor.js')) !!}
        {!! script(mix('js/backend.js')) !!}
        {!! script('https://cdnjs.cloudflare.com/ajax/libs/turbolinks/5.2.0/turbolinks.js') !!}

        @stack('after-scripts')
    </head>
    <body>
        @include('includes.partials.demo')

        <div id="app">
            <div class="container" style="padding-top: 50px">
                @include('includes.partials.messages')
                <center>
                    <img src="/img/logo-eid.png" width="250" style="padding-bottom: 30px">
                </center>
                @yield('content')
            </div><!-- container -->
            <div class="text-center text-muted pb-4">
                entrepreneurID © {{ \Carbon\Carbon::now()->year }}
            </div>
        </div><!-- #app -->
        @include('includes.partials.ga')
    </body>
</html>
