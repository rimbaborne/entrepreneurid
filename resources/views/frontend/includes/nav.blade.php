<nav class="navbar navbar-expand-lg navbar-light mb-4" style="box-shadow: 0 5px 24px 0 rgba(50, 49, 58, .08); background: #fff; padding: 0px; border-bottom: 1px solid #c8ced3;">
    <a href="{{ route('frontend.index') }}" class="navbar-brand" style="color: #fff; background: #cf1418; width: 200px; justify-content: center; padding: 15px">
        <img src="/img/emblem-sm.png">
    </a>

    <button class=" frontend navbar-toggler navbar-toggler-right" style="color: #fff; margin-right: 20px" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="@lang('labels.general.toggle_navigation')">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
        <ul class="navbar-nav">
            {{-- @if(config('locale.status') && count(config('locale.languages')) > 1)
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownLanguageLink" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">@lang('menus.language-picker.language') ({{ strtoupper(app()->getLocale()) }})</a>

                @include('includes.partials.lang')
            </li>
            @endif --}}

            @auth
            <div class="d-lg-none">
                <li class="nav-item">
                    <a href="{{route('frontend.user.dashboard')}}"
                    class="nav-link {{ active_class(Active::checkRoute('frontend.user.dashboard')) }}"
                    style="border-bottom: solid 0.01rem; font-weight: 500; padding: 15px">
                        @lang('navs.frontend.dashboard')
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#"
                    class="nav-link {{ active_class(Active::checkRoute('frontend.user.dashboard')) }}"
                    style="border-bottom: solid 0.01rem; font-weight: 500; padding: 15px">
                        Produk
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#"
                    class="nav-link {{ active_class(Active::checkRoute('frontend.user.dashboard')) }}"
                    style="border-bottom: solid 0.01rem; font-weight: 500; padding: 15px">
                        Customer
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#"
                    class="nav-link {{ active_class(Active::checkRoute('frontend.user.dashboard')) }}"
                    style="border-bottom: solid 0.01rem; font-weight: 500; padding: 15px">
                        Pemesanan
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#"
                    class="nav-link {{ active_class(Active::checkRoute('frontend.user.dashboard')) }}"
                    style="border-bottom: solid 0.01rem; font-weight: 500; padding: 15px">
                        Komisi
                    </a>
                </li>
            </div>
            @endauth

            @guest
            <li class="nav-item"><a style="border-bottom: solid 0.01rem; font-weight: 500; padding: 15px" href="{{route('frontend.auth.login')}}" class="nav-link {{ active_class(Active::checkRoute('frontend.auth.login')) }}">@lang('navs.frontend.login')</a></li>

            @if(config('access.registration'))
            <li class="nav-item"><a style="border-bottom: solid 0.01rem; font-weight: 500; padding: 15px" href="{{route('frontend.auth.register')}}" class="nav-link {{ active_class(Active::checkRoute('frontend.auth.register')) }}">@lang('navs.frontend.register')</a></li>
            @endif
            @else
            <li class="nav-item dropdown">
                <a href="#" style=" font-weight: 500; padding-left: 15px" class="nav-link dropdown-toggle" id="navbarDropdownMenuUser" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                    <span>{{ $logged_in_user->email }}</span>
                    <img style="width: 35px;" src="{{ $logged_in_user->picture }}" class="img-avatar" alt="{{ $logged_in_user->email }}">
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuUser">
                    {{-- @can('view backend')
                    <a href="{{ route('admin.dashboard') }}" class="dropdown-item">@lang('navs.frontend.user.administration')</a>
                    @endcan --}}
                    <a href="{{ route('frontend.user.account') }}" class="dropdown-item {{ active_class(Active::checkRoute('frontend.user.account')) }}">@lang('navs.frontend.user.account')</a>
                    <a href="{{ route('frontend.auth.logout') }}" class="dropdown-item">@lang('navs.general.logout')</a>
                </div>
            </li>
            @endguest

            {{-- <li class="nav-item"><a href="{{route('frontend.contact')}}" class="nav-link {{ active_class(Active::checkRoute('frontend.contact')) }}">@lang('navs.frontend.contact')</a></li> --}}
        </ul>
    </div>
</nav>
