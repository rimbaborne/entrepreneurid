
<div class="sticky-top" style="top: 25px">
<div class="card mb-4">
    <div style="text-align: center; padding-top: 15px">
        <img class="card-img-top" src="{{ $logged_in_user->picture }}" alt="Profile Picture"
        style="
        object-fit: cover;
        height: 120px;
        width: 120px;
        border-radius: 50%;
        ">
    </div>


    <div class="card-body">
        <h4 class="card-title" style="text-align: center;">
            {{ $logged_in_user->first_name }}<br/>
        </h4>

        <p class="card-text">
            <small>
                <i class="fas fa-envelope"></i> {{ $logged_in_user->email }}<br/>
                <i class="fas fa-user"></i> {{ $logged_in_user->produk }}<br/>
                <i class="fas fa-calendar-check"></i> @lang('strings.frontend.general.joined') {{ timezone()->convertToLocal($logged_in_user->created_at, 'F jS, Y') }}
            </small>
        </p>

        {{-- <p class="card-text">

            <a href="{{ route('frontend.user.account')}}" class="btn btn-danger btn-pill btn-block" style="background: rgb(255, 83, 112); color: #fff">
                <i class="fas fa-user-circle"></i> @lang('navs.frontend.user.account')
            </a>

            @can('view backend')
            &nbsp;<a href="{{ route('admin.dashboard')}}" class="btn btn-info btn-pill btn-block">
                <i class="fas fa-user-secret"></i> @lang('navs.frontend.user.administration')
            </a>
            @endcan
        </p> --}}
    </div>
</div>

{{-- <div id="menusid" class="card mb-4" style="margin-top: 320px;">
    <div class="user-profile">
        <div class="list-group" >
            <a href="#" class="list-group-item"><i class="fas m fa-tachometer-alt" style="color: #73818f;"></i> Dashboard</a>
            <a href="#" class="list-group-item"><i class="far m fa-user" style="color: #73818f;"></i> Akun</a>
        </div>
    </div>
</div> --}}

@if ( $logged_in_user->jenis_user == 'AGEN')
<div class="card mb-4" style="margin-top: 320px">
    <div class="user-profile">
        <div class="list-group" style="">
            <a href="#" class="list-group-item"><i class="fas m fa-tachometer-alt"></i> Dashboard</a>
            <a href="#" class="list-group-item"><i class="far m fa-user"></i> Profil</a>
            <a href="#" class="list-group-item"><i class="fas m fa-shopping-cart"></i> Produk</a>
            <a href="#" class="list-group-item"><i class="fas m fa-layer-group"></i> Customer</a>
            <a href="#" class="list-group-item"><i class="fas m fa-bullhorn"></i> Pemesanan</a>
            <a href="#" class="list-group-item"><i class="far m fa-hdd"></i> Komisi</a>
        </div>
    </div>
</div><!--card-->
@endif
</div>

@stack('before-scripts')

    <script type="text/javascript">
        $(document).ready(function() {
            menusid = document.getElementById("menusid");
            var myScrollFunc = function() {
            var y = window.scrollY;
                if (y >= 800) {
                    menusid.style.display= "show"
                } else {
                    menusid.style.display = "none"
                }
            };
            window.addEventListener("scroll", myScrollFunc);
        });
    </script>

@stack('after-scripts')
