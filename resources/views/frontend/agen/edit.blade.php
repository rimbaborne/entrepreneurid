@extends('frontend.layouts.app')

@section('title', __('frontend_agens.labels.management') . ' | ' . __('frontend_agens.labels.edit'))

@section('content')
{{ html()->modelForm($agen, 'PATCH', route('frontend.agens.update', $agen->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('frontend_agens.labels.management')
                        <small class="text-muted">@lang('frontend_agens.labels.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                    {{ html()->label(__('frontend_agens.validation.attributes.id_agen'))->class('col-md-2 form-control-label')->for('id_agen') }}

                        <div class="col-md-10">
                            {{ html()->text('id_agen')
                                ->class('form-control')
                                ->placeholder(__('frontend_agens.validation.attributes.id_agen'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('frontend.agens.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
