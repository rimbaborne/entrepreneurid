@extends('frontend.layouts.poster')

@section('title', app_name() . ' | CNL Bonus')

@section('content')
<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.3.0/dist/lazyload.min.js"></script>
<div class=""> <!--card-->
    <div class=""> <!--card-body-->
        <div class="row card">
            <div class="col-sm-5">
                <h4 class="card-title mb-0 ">
                    Copywriting Next Level
                    <div class="text-muted pt-2" style="font-size: 12px">Bonus Editor</div>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            @php
            $first  = 0;
            $end    = 0;
            @endphp
            @foreach($posters as  $key => $poster)
            <div class="col-xl-3 col-lg-4 col-md-4 col-6 mb-4">
                <div class="bg-white rounded shadow-sm">
                    <a href="{{ $poster->link }}" target="_blank">
                        <img alt="CNL" class="img img-responsive full-width img-fluid card-img-top lazy" data-src="/poster/{{ $poster->file }}" />
                    </a>
                </div>
            </div>
            @php
              $first  = $posters->firstItem();
              $end    = $key + $posters->firstItem();
            @endphp
            @endforeach
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $first !!} - {!! $end !!} From {!! $posters->total() !!} Data
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $posters->appends(request()->query())->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
{{-- <script>
  const ImageEditor = new FilerobotImageEditor();
  ImageEditor.open('https://cdn.scaleflex.it/demo/stephen-walker-unsplash.jpg');
</script> --}}
<script>
    var lazyLoadInstance = new LazyLoad();
</script>
@endsection
