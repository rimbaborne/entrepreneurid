@extends('frontend.layouts.poster')

@section('title', app_name() . ' | CNL - Desain Poster')

@section('content')
<div class=""> <!--card-->
    <div class=""> <!--card-body-->
        <div class="row card">
            <div class="col-sm-5">
                <h5 class="card-title mb-0 ">
                    <i class="fa fa-chevron-left"></i> Kembali
                </h5>
            </div><!--col-->

            <div class="col-sm-7">
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
    <div class="row justify-content-center align-items-center">
        <div class="card" style="border-radius: 0px;">
            <div class="col-6 pt-4 pb-4">
                <div id="container" class="col-md-12 editor-container ">
                    <canvas id="abc"></canvas>
                </div>
            </div>
            <div class="col-6">
                <div id="text-wrapper" style="margin-top: 10px" ng-show="getText()">
                    <div id="text-controls">
                        <input type="color" value="" id="text-color" size="10">
                        <label for="font-family" style="display:inline-block">Font family:</label>
                        <select id="font-family">
                            <option value="arial">Arial</option>
                            <option value="helvetica" selected>Helvetica</option>
                            <option value="myriad pro">Myriad Pro</option>
                            <option value="delicious">Delicious</option>
                            <option value="verdana">Verdana</option>
                            <option value="georgia">Georgia</option>
                            <option value="courier">Courier</option>
                            <option value="comic sans ms">Comic Sans MS</option>
                            <option value="impact">Impact</option>
                            <option value="monaco">Monaco</option>
                            <option value="optima">Optima</option>
                            <option value="hoefler text">Hoefler Text</option>
                            <option value="plaster">Plaster</option>
                            <option value="engagement">Engagement</option>
                        </select>
                        <br>
                        <label for="text-align" style="display:inline-block">Text align:</label>
                        <select id="text-align">
                            <option value="left">Left</option>
                            <option value="center">Center</option>
                            <option value="right">Right</option>
                            <option value="justify">Justify</option>
                        </select>
                        <div>
                            <label for="text-bg-color">Background color:</label>
                            <input type="color" value="" id="text-bg-color" size="10">
                        </div>
                        <div>
                            <label for="text-lines-bg-color">Background text color:</label>
                            <input type="color" value="" id="text-lines-bg-color" size="10">
                        </div>
                        <div>
                            <label for="text-stroke-color">Stroke color:</label>
                            <input type="color" value="" id="text-stroke-color">
                        </div>
                        <div>
                            <label for="text-stroke-width">Stroke width:</label>
                            <input type="range" value="1" min="1" max="5" id="text-stroke-width">
                        </div>
                        <div>
                            <label for="text-font-size">Font size:</label>
                            <input type="range" value="" min="1" max="120" step="1" id="text-font-size">
                        </div>
                        <div>
                            <label for="text-line-height">Line height:</label>
                            <input type="range" value="" min="0" max="10" step="0.1" id="text-line-height">
                        </div>
                    </div>
                    <div id="text-controls-additional">
                        <input type='checkbox' name='fonttype' id="text-cmd-bold">
                        Bold

                        <input type='checkbox' name='fonttype' id="text-cmd-italic">
                        Italic

                        <input type='checkbox' name='fonttype' id="text-cmd-underline" >
                        Underline

                        <input type='checkbox' name='fonttype'  id="text-cmd-linethrough">
                        Linethrough

                        <input type='checkbox' name='fonttype'  id="text-cmd-overline" >
                        Overline

                    </div>
                </div>
            </div>
        </div>
        <div class="card pb-0 col-12">
            <div class="btn-group btn-group-lg mb-3 btn-block" role="group" aria-label="Large button group">
                <button class="btn btn-ghost-dark" type="button">
                    <i class="fa fa-plus fa-lg"></i> <br> <i class="fa fa-image"></i> Logo<input type="file" id="imgLoader" style="display:none">
                </button>
                <button onclick="addText()" class="btn btn-ghost-dark" type="button">
                    <i class="fa fa-plus fa-lg"></i> <br> <i class="fa fa-font"></i> Teks
                </button>
                <button onclick="handleRemove()" class="btn btn-ghost-dark" type="button">
                    <i class="fa fa-trash fa-lg"></i> <br>Hapus
                </button>
                <button id="save" class="btn btn-ghost-dark" type="button">
                    <i class="fa fa-download fa-lg"></i> <br>Simpan
                </button>
            </div>
        </div>
    </div><!--card-->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/fabric.js/3.2.0/fabric.js' type='text/javascript'></script>
    <script src='https://purl.eligrey.com/github/canvas-toBlob.js/blob/master/canvas-toBlob.js' type='text/javascript'></script>
    <script src='https://cdn.rawgit.com/eligrey/FileSaver.js/5ed507ef8aa53d8ecfea96d96bc7214cd2476fd2/FileSaver.min.js' type='text/javascript'></script>
    <script>
        var canvas = new fabric.Canvas('abc');
        var cwidth = 400;
        canvas.setHeight(cwidth);
        canvas.setWidth(cwidth);

        var scaling = cwidth / 500;

        var bgsize = cwidth * scaling;

        document.getElementById('abc').style.backgroundImage = "url('https://images.unsplash.com/photo-1594117782204-5c398aa0e330?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80')";
        document.getElementById('abc').style.backgroundSize = bgsize + "px " + bgsize + "px";

        let imgUrl = 'https://images.unsplash.com/photo-1594117782204-5c398aa0e330?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80';
        Image.crossOrigin = 'anonymous';
        fabric.Image.fromURL(imgUrl, function(img) {
            // add background image
            canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
                scaleX: canvas.width / img.width,
                scaleY: canvas.height / img.height
            });
        });

        function addbg() {
            fabric.Image.fromURL(imgUrl, function(img) {
                // add background image
                canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
                    scaleX: canvas.width / img.width,
                    scaleY: canvas.height / img.height
                });

                canvas.sendToBack(img);
            });
        }

        canvas.add(new fabric.IText('Klik dan edit', {
            width: 400 * scaling,
            fontFamily: 'arial black',
            left: 50 * scaling,
            top: 200 * scaling,
            fill: 'white',
            textAlign: 'center',
            lineHeight: 1,
            fontSize: 25 * scaling
        }));

        function setbg(l) {
            document.getElementById('abc').style.backgroundImage = "url('" + l + "')";

            fabric.Image.fromURL(l, function(img) {
                // add background image
                canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas), {
                    scaleX: canvas.width / img.width,
                    scaleY: canvas.height / img.height
                });
            });

        }

        function addText() {
            var oText = new fabric.IText('Klik dan edit', {
                fontFamily: 'arial black',
                left: 50 * scaling,
                top: 100 * scaling,
                textAlign: 'center',
                fill: 'white',
                width: 400 * scaling,
                lineHeight: 1,
                fontSize: 25 * scaling
            });

            canvas.add(oText);
            oText.bringToFront();
            canvas.setActiveObject(oText);
        }

        function handleRemove() {
            canvas.clear().renderAll();
            setbg('https://images.unsplash.com/photo-1594117782204-5c398aa0e330?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80')
        }

        document.getElementById('imgLoader').onchange = function handleImage(e) {
            var reader = new FileReader();
            reader.onload = function(event) {
                console.log('success');
                var imgObj = new Image();
                imgObj.src = event.target.result;
                imgObj.onload = function() {
                    // start fabricJS stuff
                    let scale = 100 / imgObj.width;
                    var image = new fabric.Image(imgObj);
                    image.set({
                        left: bgsize * 0.5 - 50,
                        top: bgsize * 0.5 - 50,
                        angle: 0,
                        padding: 2,
                        scaleX: scale,
                        scaleY: scale
                    });
                    //image.scale(getRandomNum(0.1, 0.25)).setCoords();
                    canvas.add(image);
                    // end fabricJS stuff
                }

            }
            reader.readAsDataURL(e.target.files[0]);
        }

        $('#save').click(function() {
            canvas.discardActiveObject().renderAll();

            $("#abc").get(0).toBlob(function(blob) {
                saveAs(blob, "Poster eid.jpg");
            },'image/jpeg', 1);
        });


        $(document).ready(function () {

            document.getElementById('text-color').onchange = function() {
                canvas.getActiveObject().setFill(this.value);
                canvas.renderAll();
            };
            document.getElementById('text-color').onchange = function() {
                canvas.getActiveObject().setFill(this.value);
                canvas.renderAll();
            };

            document.getElementById('text-bg-color').onchange = function() {
                canvas.getActiveObject().setBackgroundColor(this.value);
                canvas.renderAll();
            };

            document.getElementById('text-lines-bg-color').onchange = function() {
                canvas.getActiveObject().setTextBackgroundColor(this.value);
                canvas.renderAll();
            };

            document.getElementById('text-stroke-color').onchange = function() {
                canvas.getActiveObject().setStroke(this.value);
                canvas.renderAll();
            };

            document.getElementById('text-stroke-width').onchange = function() {
                canvas.getActiveObject().setStrokeWidth(this.value);
                canvas.renderAll();
            };

            document.getElementById('font-family').onchange = function() {
                canvas.getActiveObject().setFontFamily(this.value);
                canvas.renderAll();
            };

            document.getElementById('text-font-size').onchange = function() {
                canvas.getActiveObject().setFontSize(this.value);
                canvas.renderAll();
            };

            document.getElementById('text-line-height').onchange = function() {
                canvas.getActiveObject().setLineHeight(this.value);
                canvas.renderAll();
            };

            document.getElementById('text-align').onchange = function() {
                canvas.getActiveObject().setTextAlign(this.value);
                canvas.renderAll();
            };


        radios5 = document.getElementsByName("fonttype");  // wijzig naar button
            for(var i = 0, max = radios5.length; i < max; i++) {
                radios5[i].onclick = function() {

                    if(document.getElementById(this.id).checked == true) {
                        if(this.id == "text-cmd-bold") {
                            canvas.getActiveObject().set("fontWeight", "bold");
                        }
                        if(this.id == "text-cmd-italic") {
                            canvas.getActiveObject().set("fontStyle", "italic");
                        }
                        if(this.id == "text-cmd-underline") {
                            canvas.getActiveObject().set("textDecoration", "underline");
                        }
                        if(this.id == "text-cmd-linethrough") {
                            canvas.getActiveObject().set("textDecoration", "line-through");
                        }
                        if(this.id == "text-cmd-overline") {
                            canvas.getActiveObject().set("textDecoration", "overline");
                        }



                    } else {
                        if(this.id == "text-cmd-bold") {
                            canvas.getActiveObject().set("fontWeight", "");
                        }
                        if(this.id == "text-cmd-italic") {
                            canvas.getActiveObject().set("fontStyle", "");
                        }
                        if(this.id == "text-cmd-underline") {
                            canvas.getActiveObject().set("textDecoration", "");
                        }
                        if(this.id == "text-cmd-linethrough") {
                            canvas.getActiveObject().set("textDecoration", "");
                        }
                        if(this.id == "text-cmd-overline") {
                            canvas.getActiveObject().set("textDecoration", "");
                        }
                    }
                    canvas.renderAll();
                }
            }
        });
    </script>
    @endsection
