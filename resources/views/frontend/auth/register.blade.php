@extends('frontend.layouts.lock')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

@section('content')
<div class="row justify-content-center align-items-center">
    <div class="col col-md-4 align-self-center">
        <div class="card">
            {{-- <span class="text-center" style="font-size: 1.4rem; font-weight: 300; line-height: 1.2;">Daftar Akun</span> --}}
            @include('frontend.auth.title')
            <span class="text-center text-muted">Daftar Akun</span>
            <div class="card-body">
                {{ html()->form('POST', route('frontend.auth.register.post'))->open() }}
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->text('first_name')
                            ->class('form-control')
                            ->placeholder('Nama Lengkap')
                            ->attribute('maxlength', 191)
                            ->required()}}
                        </div><!--col-->
                    </div><!--row-->
                </div><!--row-->

                {{-- <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->text('last_name')
                            ->class('form-control')
                            ->placeholder('Nama Panggilan')
                            ->attribute('maxlength', 191)
                            ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div>
                <div class="form-group row">
                    <div class="col-md-9 col-form-label">
                        <div class="form-check form-check-inline mr-1">
                            <input class="form-check-input" type="radio" value="Laki-laki" name="jeniskelamin">
                            <label class="form-check-label">Laki-laki</label>
                        </div>
                        <div class="form-check form-check-inline mr-1">
                            <input class="form-check-input" type="radio" value="Perempuan" name="jeniskelamin">
                            <label class="form-check-label">Perempuan</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->text('nohp')
                            ->class('form-control')
                            ->placeholder('Nomor Handphone (Whatsapp)')
                            ->attribute('maxlength', 191)
                            ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="form-group">
                    <label style="padding-right: 10px">Tanggal Lahir</label>
                    <select name="tgl">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                    </select>
                    <select name="bln">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                    </select>
                    <select name="thn">
                        <option value="1950">1950</option>
                        <option value="1951">1951</option>
                        <option value="1952">1952</option>
                        <option value="1953">1953</option>
                        <option value="1954">1954</option>
                        <option value="1955">1955</option>
                        <option value="1956">1956</option>
                        <option value="1957">1957</option>
                        <option value="1958">1958</option>
                        <option value="1959">1959</option>
                        <option value="1960">1960</option>
                        <option value="1961">1961</option>
                        <option value="1962">1962</option>
                        <option value="1963">1963</option>
                        <option value="1964">1964</option>
                        <option value="1965">1965</option>
                        <option value="1966">1966</option>
                        <option value="1967">1967</option>
                        <option value="1968">1968</option>
                        <option value="1969">1969</option>
                        <option value="1970">1970</option>
                        <option value="1971">1971</option>
                        <option value="1972">1972</option>
                        <option value="1973">1973</option>
                        <option value="1974">1974</option>
                        <option value="1975">1975</option>
                        <option value="1976">1976</option>
                        <option value="1977">1977</option>
                        <option value="1978">1978</option>
                        <option value="1979">1979</option>
                        <option value="1980">1980</option>
                        <option value="1981">1981</option>
                        <option value="1982">1982</option>
                        <option value="1983">1983</option>
                        <option value="1984">1984</option>
                        <option value="1985">1985</option>
                        <option value="1986">1986</option>
                        <option value="1987">1987</option>
                        <option value="1988">1988</option>
                        <option value="1989">1989</option>
                        <option value="1990">1990</option>
                        <option value="1991">1991</option>
                        <option value="1992">1992</option>
                        <option value="1993">1993</option>
                        <option value="1994">1994</option>
                        <option value="1995">1995</option>
                        <option value="1996">1996</option>
                        <option value="1997">1997</option>
                        <option value="1998">1998</option>
                        <option value="1999">1999</option>
                        <option value="2000">2000</option>
                        <option value="2001">2001</option>
                        <option value="2002">2002</option>
                        <option value="2003">2003</option>
                        <option value="2004">2004</option>
                        <option value="2005">2005</option>
                        <option value="2006">2006</option>
                        <option value="2007">2007</option>
                        <option value="2008">2008</option>
                        <option value="2009">2009</option>
                        <option value="2010">2010</option>
                        <option value="2011">2011</option>
                        <option value="2012">2012</option>
                        <option value="2013">2013</option>
                        <option value="2014">2014</option>
                        <option value="2015">2015</option>
                    </select>
                </div> --}}

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <input autocomplete="new-password" type="email" name="email" id="email" placeholder="Alamat Email" maxlength="191" required="required" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->password('password')
                            ->class('form-control')
                            ->placeholder(__('validation.attributes.frontend.password'))
                            ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->password('password_confirmation')
                            ->class('form-control')
                            ->placeholder('Konfirmasi Password')
                            ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                {{-- <div class="text-center" style="font-size: 1.0rem; font-weight: 500; line-height: 1.2; padding-bottom:5px;">
                    <center>Akun Rekening</center>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                        <input onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="bank" class="form-control" placeholder="Nama Bank" required="">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <input onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="norek" class="form-control" placeholder="Nomor Rekening" required="">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <input onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="namarek" class="form-control" placeholder="Nama Pemilik Rekening" required="">
                        </div>
                    </div>
                </div> --}}

                @if(config('access.captcha.registration'))
                <div class="row">
                    <div class="col">
                        @captcha
                        {{ html()->hidden('captcha_status', 'true') }}
                    </div><!--col-->
                </div><!--row-->
                @endif

                <div class="row">
                    <div class="col">
                        <div class="form-group mb-0 clearfix">
                            {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(244,67,54"> --}}
                            <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="background-color: #41414b; border-color: #41414b; box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(66, 65, 76)">
                                DAFTAR
                            </button>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
                <div class="row" style="padding-top: 10px">
                    <div class="col">
                        <div class="text-left">
                            <a href="/login" style="color: rgb(65, 65, 75);">
                                <i class="fas fa-angle-double-left"></i> Login
                            </a>
                        </div>
                    </div>
                </div>
                {{ html()->form()->close() }}

                <div class="row">
                    <div class="col">
                        <div class="text-center">
                            {!! $socialiteLinks !!}
                        </div>
                    </div><!--/ .col -->
                </div><!-- / .row -->

            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- col-md-8 -->
</div><!-- row -->
@endsection

@push('after-scripts')
@if(config('access.captcha.registration'))
@captchaScripts
@endif
@endpush
