@extends('frontend.layouts.lock')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.login_box_title'))

@section('content')
{{-- {{ $_SERVER['HTTP_HOST'] }} --}}
    <div class="row justify-content-center align-items-center">
        <div class="col col-sm-4 align-self-center">
            <div class="card">
                {{-- <span class="text-center" style="font-size: 1.7rem; font-weight: 300; line-height: 1.2;">Login</span> --}}
                @include('frontend.auth.title')
                <span class="text-center text-muted">Login</span>
                <div class="card-body">
                    {{-- {{ html()->form('POST', route('frontend.auth.login.post'))->open() }} --}}
                    <form action="/login" method="post">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ html()->email('email')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.email'))
                                        ->attribute('maxlength', 191)
                                        ->required() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    {{ html()->password('password')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.password'))
                                        ->required() }}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <div class="checkbox" style="color: #696969;">
                                        {{ html()->label(html()->checkbox('remember', true, 1) . ' ' . __('labels.frontend.auth.remember_me'))->for('remember') }}
                                    </div>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group clearfix">
                                    {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(244,67,54"> --}}
                                    <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="background-color: #41414b; border-color: #41414b; box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(66, 65, 76)">
                                        LOGIN
                                    </button>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->

                        <div class="row">
                            <div class="col">
                                <div class="form-group text-right" >
                                    <a href="{{ route('frontend.auth.password.reset') }}"  style="color: #696969;">Lupa Password</a>
                                    <br>
                                    <a href="{{ route('frontend.auth.register') }}" style="color: rgb(65, 65, 75);">
                                        Register <i class="fas fa-angle-double-right"></i>
                                    </a>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                    </form>
                    {{-- {{ html()->form()->close() }} --}}
                </div><!--card body-->
            </div><!--card-->
        </div><!-- col-md-8 -->
    </div><!-- row -->
@endsection
