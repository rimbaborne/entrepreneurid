@extends('frontend.layouts.app')

@section('content')
<div class="row mb-4">
    <div class="col">
        <div class="row">
            <div class="col col-sm-3 order-1 order-sm-1 mb-4 d-none d-lg-block">
                @include('frontend.includes.sidebar')
            </div><!--col-md-4-->
            <div class="col-md-9 order-2 order-sm-2">
                <div class="card">
                    <div class="card-body">
                        <div role="tabpanel">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a href="#profile" class="nav-link active" aria-controls="profile" role="tab" data-toggle="tab">@lang('navs.frontend.user.profile')</a>
                                </li>

                                <li class="nav-item">
                                    <a href="#edit" class="nav-link" aria-controls="edit" role="tab" data-toggle="tab">Edit</a>
                                </li>

                                @if($logged_in_user->canChangePassword())
                                    <li class="nav-item">
                                        <a href="#password" class="nav-link" aria-controls="password" role="tab" data-toggle="tab">Ganti Password</a>
                                    </li>
                                @endif
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade show active pt-3" id="profile" aria-labelledby="profile-tab">
                                    @include('frontend.user.account.tabs.profile')
                                </div><!--tab panel profile-->

                                <div role="tabpanel" class="tab-pane fade show pt-3" id="edit" aria-labelledby="edit-tab">
                                    @include('frontend.user.account.tabs.edit')
                                </div><!--tab panel profile-->

                                @if($logged_in_user->canChangePassword())
                                    <div role="tabpanel" class="tab-pane fade show pt-3" id="password" aria-labelledby="password-tab">
                                        @include('frontend.user.account.tabs.change-password')
                                    </div><!--tab panel change password-->
                                @endif
                            </div><!--tab content-->
                        </div><!--tab panel-->
                    </div><!--card body-->
                </div><!-- card -->
            </div><!-- col-xs-12 -->
        </div><!-- row -->
    </div>
</div>
@endsection
