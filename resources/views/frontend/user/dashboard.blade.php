@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')
<div class="row mb-4">
    <div class="col">
        <div class="row">
            <div class="col col-sm-3 order-1 order-sm-1  mb-4 d-none d-lg-block">
                @include('frontend.includes.sidebar')
            </div>
            <div class="col-md-9 order-2 order-sm-2">
                @if ( $logged_in_user->jenis_user == 'AGEN')
                    @include('frontend.user.modul.dashboard')
                @elseif ( $logged_in_user->jenis_user == 'CUSTOMER')
                    @include('frontend.user.customer.dashboard')
                @else
                    <div class="alert alert-danger">
                        Jenis User Tidak Terdaftar !
                    </div>
                @endif
            </div>
        </div><!-- row -->
    </div><!-- row -->
</div><!-- row -->
@endsection
