@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')
<div class="row mb-4">
    <div class="col">
        <div class="row">
            <div class="col col-sm-3 order-1 order-sm-1  mb-4 d-none d-lg-block">
                @include('frontend.includes.sidebar')
            </div><!--col-md-4-->
            <div class="col-md-9 order-2 order-sm-2">
                <div class="row">
                    <div id="respon" class="col-4">
                        <div class="card order-card" style="padding: 25px;">
                            <div class="card-block">
                                <div class="">
                                    <div class="text-center" style="padding-bottom: 10px">
                                        <img class="img-fluid" src="/img/produk/MOM.png" alt="Mentoring Organic Marketing" style="height: 150px">
                                    </div>
                                    <h6 class="text-right">Mentoring Organic Marketing</h6>
                                    <h4 class="text-right" style="font-weight: 700"><span>Rp. 149.000</span></h4>
                                    <div class="" style="padding-bottom: 15px">
                                        <small class="text-muted">Komisi</small>
                                        <br>
                                        <strong class="h5" style="font-weight: 600; color:crimson">Rp. 60.000</strong>
                                    </div>
                                    <button class="btn btn-pill btn-block btn-light" style="font-weight: 600" disabled>Close Order</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="respon" class="col-4">
                        <div class="card order-card" style="padding: 25px;">
                            <div class="card-block">
                                <div class="">
                                    <div class="text-center" style="padding-bottom: 10px">
                                        <img class="img-fluid" src="/img/produk/MOM.png" alt="Mentoring Organic Marketing" style="height: 150px">
                                    </div>
                                    <h6 class="text-right">Mentoring Organic Marketing</h6>
                                    <h4 class="text-right" style="font-weight: 700"><span>Rp. 149.000</span></h4>
                                    <div class="" style="padding-bottom: 15px">
                                        <small class="text-muted">Komisi</small>
                                        <br>
                                        <strong class="h5" style="font-weight: 600; color:crimson">Rp. 60.000</strong>
                                    </div>
                                    <button class="btn btn-pill btn-block btn-primary" style="font-weight: 600">Pesan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="respon" class="col-4">
                        <div class="card order-card" style="padding: 25px;">
                            <div class="card-block">
                                <div class="">
                                    <div class="text-center" style="padding-bottom: 10px">
                                        <img class="img-fluid" src="/img/produk/MOM.png" alt="Mentoring Organic Marketing" style="height: 150px">
                                    </div>
                                    <h6 class="text-right">Mentoring Organic Marketing</h6>
                                    <h4 class="text-right" style="font-weight: 700"><span>Rp. 149.000</span></h4>
                                    <div class="" style="padding-bottom: 15px">
                                        <small class="text-muted">Komisi</small>
                                        <br>
                                        <strong class="h5" style="font-weight: 600; color:crimson">Rp. 60.000</strong>
                                    </div>
                                    <button class="btn btn-pill btn-block btn-primary" style="font-weight: 600">Pesan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="respon" class="col-4">
                        <div class="card order-card" style="padding: 25px;">
                            <div class="card-block">
                                <div class="">
                                    <div class="text-center" style="padding-bottom: 10px">
                                        <img class="img-fluid" src="/img/produk/MOM.png" alt="Mentoring Organic Marketing" style="height: 150px">
                                    </div>
                                    <h6 class="text-right">Mentoring Organic Marketing</h6>
                                    <h4 class="text-right" style="font-weight: 700"><span>Rp. 149.000</span></h4>
                                    <div class="" style="padding-bottom: 15px">
                                        <small class="text-muted">Komisi</small>
                                        <br>
                                        <strong class="h5" style="font-weight: 600; color:crimson">Rp. 60.000</strong>
                                    </div>
                                    <button class="btn btn-pill btn-block btn-primary" style="font-weight: 600">Pesan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="respon" class="col-4">
                        <div class="card order-card" style="padding: 25px;">
                            <div class="card-block">
                                <div class="">
                                    <div class="text-center" style="padding-bottom: 10px">
                                        <img class="img-fluid" src="/img/produk/MOM.png" alt="Mentoring Organic Marketing" style="height: 150px">
                                    </div>
                                    <h6 class="text-right">Mentoring Organic Marketing</h6>
                                    <h4 class="text-right" style="font-weight: 700"><span>Rp. 149.000</span></h4>
                                    <div class="" style="padding-bottom: 15px">
                                        <small class="text-muted">Komisi</small>
                                        <br>
                                        <strong class="h5" style="font-weight: 600; color:crimson">Rp. 60.000</strong>
                                    </div>
                                    <button class="btn btn-pill btn-block btn-primary" style="font-weight: 600">Pesan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="respon" class="col-4">
                        <div class="card order-card" style="padding: 25px;">
                            <div class="card-block">
                                <div class="">
                                    <div class="text-center" style="padding-bottom: 10px">
                                        <img class="img-fluid" src="/img/produk/MOM.png" alt="Mentoring Organic Marketing" style="height: 150px">
                                    </div>
                                    <h6 class="text-right">Mentoring Organic Marketing</h6>
                                    <h4 class="text-right" style="font-weight: 700"><span>Rp. 149.000</span></h4>
                                    <div class="" style="padding-bottom: 15px">
                                        <small class="text-muted">Komisi</small>
                                        <br>
                                        <strong class="h5" style="font-weight: 600; color:crimson">Rp. 60.000</strong>
                                    </div>
                                    <button class="btn btn-pill btn-block btn-primary" style="font-weight: 600">Pesan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="respon" class="col-4">
                        <div class="card order-card" style="padding: 25px;">
                            <div class="card-block">
                                <div class="">
                                    <div class="text-center" style="padding-bottom: 10px">
                                        <img class="img-fluid" src="/img/produk/MOM.png" alt="Mentoring Organic Marketing" style="height: 150px">
                                    </div>
                                    <h6 class="text-right">Mentoring Organic Marketing</h6>
                                    <h4 class="text-right" style="font-weight: 700"><span>Rp. 149.000</span></h4>
                                    <div class="" style="padding-bottom: 15px">
                                        <small class="text-muted">Komisi</small>
                                        <br>
                                        <strong class="h5" style="font-weight: 600; color:crimson">Rp. 60.000</strong>
                                    </div>
                                    <button class="btn btn-pill btn-block btn-primary" style="font-weight: 600">Pesan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="respon" class="col-4">
                        <div class="card order-card" style="padding: 25px;">
                            <div class="card-block">
                                <div class="">
                                    <div class="text-center" style="padding-bottom: 10px">
                                        <img class="img-fluid" src="/img/produk/MOM.png" alt="Mentoring Organic Marketing" style="height: 150px">
                                    </div>
                                    <h6 class="text-right">Mentoring Organic Marketing</h6>
                                    <h4 class="text-right" style="font-weight: 700"><span>Rp. 149.000</span></h4>
                                    <div class="" style="padding-bottom: 15px">
                                        <small class="text-muted">Komisi</small>
                                        <br>
                                        <strong class="h5" style="font-weight: 600; color:crimson">Rp. 60.000</strong>
                                    </div>
                                    <button class="btn btn-pill btn-block btn-primary" style="font-weight: 600">Pesan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="respon" class="col-4">
                        <div class="card order-card" style="padding: 25px;">
                            <div class="card-block">
                                <div class="">
                                    <div class="text-center" style="padding-bottom: 10px">
                                        <img class="img-fluid" src="/img/produk/MOM.png" alt="Mentoring Organic Marketing" style="height: 150px">
                                    </div>
                                    <h6 class="text-right">Mentoring Organic Marketing</h6>
                                    <h4 class="text-right" style="font-weight: 700"><span>Rp. 149.000</span></h4>
                                    <div class="" style="padding-bottom: 15px">
                                        <small class="text-muted">Komisi</small>
                                        <br>
                                        <strong class="h5" style="font-weight: 600; color:crimson">Rp. 60.000</strong>
                                    </div>
                                    <button class="btn btn-pill btn-block btn-primary" style="font-weight: 600">Pesan</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                {{-- <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="col-md-4">tes</div>
                            <div class="col-md-4">tes</div>
                            <div class="col-md-4">tes</div>
                        </div>
                    </div>
                </div> --}}
            </div><!--col-md-8-->
        </div><!-- row -->
    </div><!-- row -->
</div><!-- row -->

@stack('before-scripts')
<script>
    function resize(x) {
        var element = document.querySelectorAll("[id='respon']");;
        for(var i = 0; i < element.length; i++)
            if (x.matches) { // If media query matches
                element[i].classList.remove("col-4");
                element[i].classList.add("col-6");
            } else {
                element[i].classList.remove("col-6");
                element[i].classList.add("col-4");
            }
    }

    var x = window.matchMedia("(max-width: 1000px)")
    resize(x) // Call listener function at run time
    x.addListener(resize) // Attach listener function on state changes
</script>
@stack('after-scripts')

@endsection
