@extends('frontend.layouts.lock')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

@section('content')

<div class="row justify-content-center align-items-center">

    <div class="col col-md-4 align-self-center">
        <div class="card bg-success">
            @include('frontend.auth.title')
            <h2 style="margin-top: -5px; font-weight: 800">Rp. 193.000</h2>
        </div>
        <div class="card">
            {{-- <span class="text-center" style="font-size: 1.4rem; font-weight: 300; line-height: 1.2;">Daftar Akun</span> --}}
            <span class="text-center text-muted">Form Pemesanan</span>
            <div class="card-body">
                <form action="/simpan" method="POST">
                    @csrf
                    <input value="{{ $ref }}" name="ref" hidden>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="nama" value="{{ old('nama') }}" class="form-control" maxlength="100" placeholder="Nama Lengkap" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input onkeyup="this.value = this.value.toLowerCase();" autocomplete="new-password" type="email" name="email" value="{{ old('email') }}" id="email" placeholder="Alamat Email" maxlength="191" required="required" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        +62
                                    </span>
                                </div>
                                <input id="nohp" type="number" name="nohp" value="{{ old('nohp') }}" class="form-control" maxlength="12" placeholder="No. Handphone WhatsApp" required="">
                            </div>
                        </div>
                        <span class="col-12 help-block text-muted" style="font-size: 10px; font-weight: 700">Tidak Pakai Angka 0 . Contoh : 81234563789</span>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="form-label"> Asal Kota Pengiriman : <strong>BALIKPAPAN</strong></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="form-label"> Isi Data Alamat Tujuan : </label>
                                <textarea onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="alamat" class="form-control" placeholder="Alamat Lengkap" cols="5" required="">{{ old('alamat') }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <select class="form-control" id="provinsi" name="provinsi" required>
                                    <option value="">-- Pilih Provinsi -- </option>
                                    {{-- {{ $dataprovinsi }} --}}
                                    @foreach ($dataprovinsi->rajaongkir->results as $key => $data)
                                        <option value="{{ $data->province_id }}">{{ $data->province }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row d-none" id="formkota">
                        <div class="col">
                            <div class="form-group">
                                <select class="form-control" id="kota" name="kota" required>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group d-none" id="formkecamatan">
                                <select class="form-control" id="kecamatan" name="kecamatan" required>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="d-none" id="formongkir">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <input onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="number" name="kodepos" value="{{ old('kodepos') }}" class="form-control" placeholder="Masukkan Kodepos .." cols="5" required="">
                                    <span class="help-block text-muted" style="font-size: 10px; font-weight: 700">cari <a href="https://www.nomor.net/_kodepos.php" target="_blank">kodepos disini</a></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <select class="form-control" id="ongkir" name="ongkir" style="text-transform: uppercase;" required>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Jumlah Buku</label>
                            <div class="col-md-6">
                                <select class="form-control" name="jumlah" id="jumlah">
                                    @for ($i = 1; $i <= 50; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <span class="col-md-12 help-block text-muted" style="font-size: 10px; font-weight: 700">Berat 1 Pcs Buku : 500 gram</span>
                        </div>
                        <div class="row">
                            <label class="col-md-6 col-form-label">Kode Unik</label>
                            <div class="col-md-6 col-form-label text-right" style="right: 0px">
                                <input disabled class="form-control" style="border: 0px" id="kodeunik" value="{{ $kodeunik }}">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-md-6 col-form-label">Harga</label>
                            <div class="col-md-6 col-form-label text-right" >
                                <label class="text-left" style="float: left;">Rp.</label>
                                <label id="harga"> </label>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-md-6 col-form-label">Ongkir</label>
                            <div class="col-md-6 col-form-label text-right" >
                                <label class="text-left" style="float: left;">Rp.</label>
                                <label id="hargaongkir"> </label>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-md-6"></label>
                            <div class="col-md-6">
                                <hr style="margin: 0px; padding: 0px">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-md-6 col-form-label">Total</label>
                            <div class="col-md-6 col-form-label text-right">
                                <strong>
                                    <label class="text-left" style="float: left;">Rp.</label>
                                    <label id="total"> </label>
                                </strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <div class="checkbox" style="color: #696969;">
                                        <label class="col-form-label"><input type="checkbox" id="remember" required> Data yang diisi sudah benar.</label>
                                    </div>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div>
                        <input type="hidden" name="harga" id="harga_">
                        <input type="hidden" name="hargaongkir" id="hargaongkir_">
                        <input type="hidden" name="total" id="total_">
                        <input type="hidden" name="kodeunik" value="{{ $kodeunik }}">
                        <div class="row">
                            <div class="col">
                                <div class="form-group mb-0 clearfix">
                                    {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(244,67,54"> --}}
                                    <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="background-color: #41414b; border-color: #41414b; box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(66, 65, 76)">
                                        PESAN
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="loading" style=" text-align: end;">
                        <div style=" position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 9999; background: rgba(0,0,0,.4); margin: 0 1; ">
                            <div style=" position: relative; border-radius: .3125em; font-family: inherit; top: 50%; left: 50%; transform: translate(-100%, 100%); color: #fff; ">
                            <span class="spinner-border" role="status" aria-hidden="true"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stack('before-scripts')
    <script>
            $("#nohp").on("input", function() {
                if (/^0/.nohp(this.value)) {
                    this.value = this.value.replace(/^0/, "")
                }
            })
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#loading').hide();
            function formkotashow() {
                document.getElementById("formkecamatan").classList.add('d-none');

                var select = document.getElementById("kota");
                var length = select.options.length;
                for (i = length-1; i >= 0; i--) {
                    select.options[i] = null;
                }

                var select = document.getElementById("ongkir");
                var length = select.options.length;
                for (i = length-1; i >= 0; i--) {
                    select.options[i] = null;
                }

                document.getElementById("formkota").classList.remove('d-none');
            }

            function formkecamatanshow() {
                var select = document.getElementById("kecamatan");
                var length = select.options.length;
                for (i = length-1; i >= 0; i--) {
                    select.options[i] = null;
                }

                var select = document.getElementById("ongkir");
                var length = select.options.length;
                for (i = length-1; i >= 0; i--) {
                    select.options[i] = null;
                }

                document.getElementById("formkecamatan").classList.remove('d-none');
            }
            function formongkirshow() {
                var select = document.getElementById("ongkir");
                var length = select.options.length;
                for (i = length-1; i >= 0; i--) {
                    select.options[i] = null;
                }

                document.getElementById("formongkir").classList.remove('d-none');
            }
            function uang(x) {
                return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
            }
            function hitungongkir(){
                var ongkir      = $('#ongkir').val();
                var ongkir_     = ongkir.split(",");
                var dataongkir  = ongkir_[2];
                var jumlah      = $('#jumlah').val();
                var harga       = 193000*jumlah;
                var kodeunik    = $('#kodeunik').val();

                if (jumlah > 2) {
                    var penambahan = jumlah % 2 ;
                    if ( penambahan === 1) {
                        var berat = ((jumlah - 1) / 2) + 1;
                    } else {
                        var berat = (jumlah / 2);
                    }
                } else {
                    var berat = 1;
                }

                var total       = parseInt((parseInt(harga)+parseInt(kodeunik))+(parseInt(dataongkir)*parseInt(berat)));
                var hargaongkir = dataongkir * berat;

                console.log(harga);
                console.log(kodeunik);
                console.log(dataongkir);
                console.log(berat);
                console.log((harga + kodeunik) + (dataongkir * berat));

                document.getElementById("hargaongkir").innerHTML = uang(hargaongkir);
                document.getElementById("harga").innerHTML       = uang(harga);
                document.getElementById("total").innerHTML       = uang(total);
                document.getElementById("hargaongkir_").setAttribute('value', hargaongkir);
                document.getElementById("harga_").setAttribute('value', harga);
                document.getElementById("total_").setAttribute('value', total);
            }
            $('#provinsi').change(function () {
                //Mengambil value dari option select provinsi kemudian parameternya dikirim menggunakan ajax
                var prov = $('#provinsi').val();
                $.ajax({
                    type: 'GET',
                    url: '/cek-ongkir',
                    data: 'cari=kota&provinsi=' + prov,
                    beforeSend: function(){
                        $('#loading').show();
                    },
                    complete: function(){
                        $('#loading').hide();
                    },
                    success: function (data) {
                        //jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten
                        formkotashow();
                        data.forEach(e =>$('#kota').append(e.datakota));
                    }
                });
            });

            $('#kota').change(function () {
                //Mengambil value dari option select kota kemudian parameternya dikirim menggunakan ajax
                var city = $('#kota').val();
                $.ajax({
                    type: 'GET',
                    url: '/cek-ongkir',
                    data: 'cari=kecamatan&kota=' + city,
                    beforeSend: function(){
                        $('#loading').show();
                    },
                    complete: function(){
                        $('#loading').hide();
                    },
                    success: function (data) {
                        //jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten
                        formkecamatanshow();
                        data.forEach(e =>$('#kecamatan').append(e.datakecamatan));
                    }
                });
            });


            $('#kecamatan').change(function () {
                var kec   = $('#kecamatan').val();
                $.ajax({
                    type: 'GET',
                    url: '/cek-ongkir',
                    data: 'cari=ongkir&kec_id=' + kec,
                    beforeSend: function(){
                        $('#loading').show();
                    },
                    complete: function(){
                        $('#loading').hide();
                    },
                    success: function (data) {
                        //jika data berhasil didapatkan, tampilkan ke dalam element div ongkir
                        formongkirshow();
                        data.forEach(e =>$('#ongkir').append(e.dataongkir));
                        hitungongkir();
                    }
                });
            });

            $('#ongkir').change(function () {
                hitungongkir();
            });

            $('#jumlah').change(function () {
                hitungongkir();
            });
        });
    </script>
@stack('after-scripts')

@endsection

@push('after-scripts')
@if(config('access.captcha.registration'))
@captchaScripts
@endif
@endpush
