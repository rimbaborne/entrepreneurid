@extends('frontend.layouts.lock')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

@section('content')

<div class="row justify-content-center align-items-center">

    <div class="col col-md-8 align-self-center">
        <div class="card">
            {{-- <span class="text-center" style="font-size: 1.4rem; font-weight: 300; line-height: 1.2;">Daftar Akun</span> --}}
            @include('frontend.auth.title')
            <span class="text-center text-muted">Pencarian Data Pemesanan</span>
            <div class="card-body">
                <form action="">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                {{-- <label for="form-label text-center">Sedang Ada Kendala. Mohon Tunggu, Kami sedang memperbaikinya. Terima Kasih :)</label> --}}
                                <input type="text" name="email" value="{{ old('cekresi') }}" class="form-control" maxlength="100" placeholder="Masukkan Alamat E-mail" required="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group mb-0 clearfix">
                                <button type="submit" class="btn btn-pill btn-block btn-primary btn-block" style=" box-shadow: 0 4px 20px 0 rgba(202, 202, 202, 0.14), 0 7px 10px -5px rgb(155, 153, 170)">
                                    CARI <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @if (null !== request()->get('email'))
            <div class="card">
                <div class="card-body">
                    @if ($data !== null)
                        <table class="pb-4 table table-sm table-striped">
                            <tbody>
                                <tr>
                                    <td>Nama</td>
                                    <td class="font-weight-bold">: {{ $data->nama }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td class="font-weight-bold">: {{ $data->email }}</td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td class="font-weight-bold">: {{ $data->alamat }}</td>
                                </tr>
                                <tr>
                                    <td>Kota</td>
                                    <td class="font-weight-bold">: {{ $data->kota }}</td>
                                </tr>
                                <tr>
                                    <td>Ekspedisi</td>
                                    <td class="font-weight-bold" style="text-transform: uppercase">: {{ $data->kurir }}</td>
                                </tr>
                                <tr>
                                    <td>No. Resi</td>
                                    <td class="font-weight-bold">: {{ $data->noresi }}</td>
                                </tr>
                                <tr>
                                    <td>Ongkir</td>
                                    <td class="font-weight-bold">: Rp. {{ $data->ongkir }}</td>
                                </tr>
                                <tr>
                                    <td>Jumlah</td>
                                    <td class="font-weight-bold">: {{ $data->jumlah }} Buku</td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td class="font-weight-bold">: Rp. {{ $data->total }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="text-center">
                            <a href="https://berdu.id/cek-resi" target="_blank" class="btn btn-xl btn-primary"><i class="fas fa-search"></i> Cek Resi</a>
                        </div>

                    @else
                        <h5 class="text-center">Data Tidak Ditemukan.</h5>
                    @endif
                </div>
            </div>


        @endif
    </div>
</div>

@endsection
