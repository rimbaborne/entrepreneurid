@extends('frontend.layouts.mic')

@section('title', 'Mentoring Instang Copywriting' )

@section('content')
@if (!null == $data)

    <div class="row mb-4">
        <div class="col">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-10">
                    <div class="card">
                        <div class="row">
                            <div class="col-12">
                                <a href="https://mentoringinstantcopywriting.com/member-area/dashboard/">
                                    <i class="fas fa-chevron-left"></i> Kembali ke Member Area
                                </a>
                            </div>
                        </div>
                        <hr class="pb-4">
                        <form class="row ">
                            <input type="hidden" name="sesi" value="{{ request()->sesi }}">
                            <div class="col-12">
                                <h3 class="display text-center">Adviser Copywriting</h3>
                                <div class="form-group">
                                    <label class="form-label">Konsultasikan Copywriting Anda Maximal 5.000 karakter</label>
                                    <textarea class="form-control" name="advisor" cols="100" rows="5" maxlength="5000"></textarea>
                                </div>
                                <div class="form-group text-right">
                                    <button class="btn btn-primary">
                                        <i class="fa fa-paper-plane"></i>
                                        Kirim
                                    </button>
                                </div>
                                <div class="form-group float-left">
                                    <p class="text-muted float-left">
                                        * Setelah anda memasukkan copywriting, silahkan tunggu 1x24 jam untuk mendapatkan responnya.
                                    </p>
                                    {{-- <br>
                                    <p class="text-muted float-left">
                                        * Maximal 5000 Karakter.
                                    </p> --}}
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="card">
                        <div class="container px-4">
                                <div class="py-5 chat-box bg-white">
                                    <div class="scrolling-pagination">
                                        @foreach ( $advisor as $a )
                                            @if ($a->user == 'admin')
                                                <div class="media w-75"><img src="https://ui-avatars.com/api/?background=random&name=eid" alt="user" width="50" class="rounded-circle">
                                                    <div class="media-body ml-3">
                                                        <div class="bg-light rounded py-2 px-3 mb-2">
                                                            <p class="text-small mb-0 text-muted">{!! $a->advisor !!}</p>
                                                        </div>
                                                        <p class="small text-muted">{{ $a->created_at }}</p>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="media w-75 ml-auto">
                                                    <div class="media-body">
                                                        <div class="bg-primary rounded py-2 px-3 mb-2">
                                                            <div class="float-right">
                                                                <a href="{{ request()->url() }}?id={{ $a->id }}&info=delete"  onclick="return confirm('Apakah Yakin Dihapus ?');" style="color:rgb(231, 243, 255)">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            </div>
                                                            <p class="text-small mb-0 text-white">{!! $a->advisor !!}</p>
                                                        </div>
                                                        <p class="small text-muted">{{ $a->created_at }}</p>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                        {{ $advisor->appends(request()->query())->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- row -->
        </div><!-- row -->
    </div><!-- row -->
@else
    <div class="row mb-4">
        <div class="col">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-6">
                    <div class="card text-center p-4">
                        Terjadi Kesalahan. Silahkan Hubungi CS Kami. Terima Kasih.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection
