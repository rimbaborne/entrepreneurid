@extends('frontend.layouts.app')

@section('title', app_name() . ' | Member Area' )

@section('content')
<div class="row mb-4">
    <div class="col">
        <div class="row">
            <div class="col col-sm-3 order-1 order-sm-1  mb-4 d-none d-md-block" style="z-index: 100">
                @include('frontend.includes.sidebar')
            </div>
            <div class="col-md-9 order-2 order-sm-2">
                @yield('content-customer')
            </div>
        </div><!-- row -->
    </div><!-- row -->
</div><!-- row -->
@endsection
