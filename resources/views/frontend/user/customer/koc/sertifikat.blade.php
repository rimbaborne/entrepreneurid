<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0">
    <title>Sertifikat - Kelaas Online Copywriting</title>
    <meta name="robots" content="index,follow"/>
    <meta property="og:type" content="website"/>
    <meta property="og:site_name" content="Eclass KOC"/>
    <meta property="og:url" content="https://kelasonlinecopywriting.com/coba/"/>
    <link rel="alternate" type="application/rss+xml" title="Eclass KOC &raquo; Feed" href="https://kelasonlinecopywriting.com/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Eclass KOC &raquo; Comments Feed" href="https://kelasonlinecopywriting.com/comments/feed/" />
    <link rel='stylesheet' id='font-awesome-css'  href='https://kelasonlinecopywriting.com/wp-content/themes/landingpress-wp/assets/lib/font-awesome/css/font-awesome.min.css?ver=4.7.0' type='text/css' media='all' />
    <link rel='stylesheet' id='wp-block-library-css'  href='https://kelasonlinecopywriting.com/wp-includes/css/dist/block-library/style.min.css?ver=5.3.4' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-css'  href='https://kelasonlinecopywriting.com/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-animations-css'  href='https://kelasonlinecopywriting.com/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=2.8.5.1-LP' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-css'  href='https://kelasonlinecopywriting.com/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=2.8.5.1-LP' type='text/css' media='all' />
    <style id='elementor-frontend-inline-css' type='text/css'>
        .elementor-widget-heading.elementor-widget-heading .elementor-heading-title{color:#6ec1e4;}.elementor-widget-heading .elementor-heading-title{font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-image .widget-image-caption{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-text-editor{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-text-editor.elementor-drop-cap-view-stacked .elementor-drop-cap{background-color:#6ec1e4;}.elementor-widget-text-editor.elementor-drop-cap-view-framed .elementor-drop-cap, .elementor-widget-text-editor.elementor-drop-cap-view-default .elementor-drop-cap{color:#6ec1e4;border-color:#6ec1e4;}.elementor-widget-button a.elementor-button, .elementor-widget-button .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-divider{--divider-border-color:#54595f;}.elementor-widget-divider .elementor-divider__text{color:#54595f;font-family:"Roboto Slab", Sans-serif;font-weight:400;}.elementor-widget-divider.elementor-view-stacked .elementor-icon{background-color:#54595f;}.elementor-widget-divider.elementor-view-framed .elementor-icon, .elementor-widget-divider.elementor-view-default .elementor-icon{color:#54595f;border-color:#54595f;}.elementor-widget-divider.elementor-view-framed .elementor-icon, .elementor-widget-divider.elementor-view-default .elementor-icon svg{fill:#54595f;}.elementor-widget-image-box .elementor-image-box-content .elementor-image-box-title{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-image-box .elementor-image-box-content .elementor-image-box-description{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-icon.elementor-view-stacked .elementor-icon{background-color:#6ec1e4;}.elementor-widget-icon.elementor-view-framed .elementor-icon, .elementor-widget-icon.elementor-view-default .elementor-icon{color:#6ec1e4;border-color:#6ec1e4;}.elementor-widget-icon.elementor-view-framed .elementor-icon, .elementor-widget-icon.elementor-view-default .elementor-icon svg{fill:#6ec1e4;}.elementor-widget-icon-box.elementor-view-stacked .elementor-icon{background-color:#6ec1e4;}.elementor-widget-icon-box.elementor-view-framed .elementor-icon, .elementor-widget-icon-box.elementor-view-default .elementor-icon{fill:#6ec1e4;color:#6ec1e4;border-color:#6ec1e4;}.elementor-widget-icon-box .elementor-icon-box-content .elementor-icon-box-title{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-icon-box .elementor-icon-box-content .elementor-icon-box-description{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-star-rating .elementor-star-rating__title{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-image-carousel .elementor-image-carousel-caption{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-image-gallery .gallery-item .gallery-caption{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-icon-list .elementor-icon-list-item:not(:last-child):after{border-color:#7a7a7a;}.elementor-widget-icon-list .elementor-icon-list-icon i{color:#6ec1e4;}.elementor-widget-icon-list .elementor-icon-list-icon svg{fill:#6ec1e4;}.elementor-widget-icon-list .elementor-icon-list-text{color:#54595f;}.elementor-widget-icon-list .elementor-icon-list-item{font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-counter .elementor-counter-number-wrapper{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-counter .elementor-counter-title{color:#54595f;font-family:"Roboto Slab", Sans-serif;font-weight:400;}.elementor-widget-progress .elementor-progress-wrapper .elementor-progress-bar{background-color:#6ec1e4;}.elementor-widget-progress .elementor-title{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-testimonial .elementor-testimonial-content{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-testimonial .elementor-testimonial-name{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-testimonial .elementor-testimonial-job{color:#54595f;font-family:"Roboto Slab", Sans-serif;font-weight:400;}.elementor-widget-tabs .elementor-tab-title{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-tabs .elementor-tab-title.elementor-active{color:#61ce70;}.elementor-widget-tabs .elementor-tab-content{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-accordion .elementor-accordion .elementor-tab-title{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-accordion .elementor-accordion .elementor-tab-title.elementor-active{color:#61ce70;}.elementor-widget-accordion .elementor-accordion .elementor-tab-content{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-toggle .elementor-toggle .elementor-tab-title, .elementor-widget-toggle .elementor-toggle .elementor-tab-title a{color:#6ec1e4;}.elementor-widget-toggle .elementor-toggle .elementor-tab-title.elementor-active, .elementor-widget-toggle .elementor-toggle .elementor-tab-title.elementor-active a{color:#61ce70;}.elementor-widget-toggle .elementor-toggle .elementor-tab-title{font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-toggle .elementor-toggle .elementor-tab-content{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-alert .elementor-alert-title{font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-alert .elementor-alert-description{font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-lp_navigation_menu .lp-navmenu-items li a, .elementor-widget-lp_navigation_menu .lp-navmenu-items li a:visited{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-lp_posts_grid .lp-posts-grid-wrapper li h4 a{font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-lp_posts_grid .lp-posts-grid-wrapper li p{font-family:"Roboto Slab", Sans-serif;font-weight:400;}.elementor-widget-lp_contact_form .elementor-lp-form-wrapper label{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-lp_contact_form .elementor-lp-form-wrapper input[type="text"], .elementor-widget-lp_contact_form .elementor-lp-form-wrapper input[type="email"], .elementor-widget-lp_contact_form .elementor-lp-form-wrapper textarea{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-lp_contact_form .elementor-lp-form-wrapper input[type="submit"], .elementor-widget-lp_contact_form .elementor-lp-form-wrapper button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-lp_confirmation_form .elementor-lp-form-wrapper label{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-lp_confirmation_form .elementor-lp-form-wrapper input[type="text"], .elementor-widget-lp_confirmation_form .elementor-lp-form-wrapper input[type="email"], .elementor-widget-lp_confirmation_form .elementor-lp-form-wrapper textarea{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-lp_confirmation_form .elementor-lp-form-wrapper input[type="submit"], .elementor-widget-lp_confirmation_form .elementor-lp-form-wrapper button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-lp_slider_image .lp-slider-wrapper .lp-slide-image-caption{font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-lp_slider_content .lp-slider-heading{font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-lp_slider_content .lp-slider-description{font-family:"Roboto Slab", Sans-serif;font-weight:400;}.elementor-widget-button_sms .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_tel .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_bbm .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_line .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_whatsapp .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_wagroup .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_messenger .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_telegram .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_instagram .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_video .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-countdown_simple .elementor-countdown-simple{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-optin .elementor-lp-form-wrapper label{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-optin .elementor-lp-form-wrapper input[type="text"], .elementor-widget-optin .elementor-lp-form-wrapper input[type="email"], .elementor-widget-optin .elementor-lp-form-wrapper textarea{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-optin .elementor-lp-form-wrapper input[type="submit"], .elementor-widget-optin .elementor-lp-form-wrapper button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-popup-block-white .elementor-lp-form-wrapper label{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-popup-block-white .elementor-lp-form-wrapper input[type="text"], .elementor-popup-block-white .elementor-lp-form-wrapper input[type="email"], .elementor-popup-block-white .elementor-lp-form-wrapper textarea{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-popup-block-white .elementor-lp-form-wrapper input[type="submit"], .elementor-popup-block-white .elementor-lp-form-wrapper button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-optin_2steps .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}
        .elementor-339 .elementor-element.elementor-element-9687c93 > .elementor-container{max-width:1000px;}.elementor-339 .elementor-element.elementor-element-9687c93:not(.elementor-motion-effects-element-type-background), .elementor-339 .elementor-element.elementor-element-9687c93 > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-color:#FFFFFF;}.elementor-339 .elementor-element.elementor-element-9687c93{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-339 .elementor-element.elementor-element-9687c93 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-339 .elementor-element.elementor-element-8c09d0a:not(.elementor-motion-effects-element-type-background) > .elementor-element-populated, .elementor-339 .elementor-element.elementor-element-8c09d0a > .elementor-column-wrap > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-color:#FFFFFF;background-image:url("https://kelasonlinecopywriting.com/wp-content/uploads/2020/07/back.png");background-position:center center;background-repeat:no-repeat;background-size:cover;}.elementor-339 .elementor-element.elementor-element-8c09d0a > .elementor-element-populated >  .elementor-background-overlay{opacity:0.8;}.elementor-339 .elementor-element.elementor-element-8c09d0a > .elementor-element-populated{border-style:double;border-width:3px 3px 3px 3px;border-color:#151419;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin:0px 0px 0px 0px;padding:50px 0px 50px 0px;}.elementor-339 .elementor-element.elementor-element-8c09d0a > .elementor-element-populated, .elementor-339 .elementor-element.elementor-element-8c09d0a > .elementor-element-populated > .elementor-background-overlay, .elementor-339 .elementor-element.elementor-element-8c09d0a > .elementor-background-slideshow{border-radius:0px 0px 0px 0px;}.elementor-339 .elementor-element.elementor-element-8c09d0a > .elementor-element-populated > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-339 .elementor-element.elementor-element-91f68dd{text-align:center;}.elementor-339 .elementor-element.elementor-element-91f68dd.elementor-widget-heading .elementor-heading-title{color:#151419;}.elementor-339 .elementor-element.elementor-element-91f68dd .elementor-heading-title{font-family:"Vesper Libre", Sans-serif;font-size:30px;}.elementor-339 .elementor-element.elementor-element-91f68dd > .elementor-widget-container{margin:-10px 0px -10px 0px;}.elementor-339 .elementor-element.elementor-element-f16747d{color:#151419;}.elementor-339 .elementor-element.elementor-element-f16747d > .elementor-widget-container{margin:0px 0px -30px 0px;}.elementor-339 .elementor-element.elementor-element-0a50759{color:#151419;font-family:"Courgette", Sans-serif;font-size:30px;}.elementor-339 .elementor-element.elementor-element-0a50759 > .elementor-widget-container{margin:-10px 0px -65px 0px;}.elementor-339 .elementor-element.elementor-element-697ef1b{--divider-border-style:solid;--divider-border-color:#000;--divider-border-width:1.2px;}.elementor-339 .elementor-element.elementor-element-697ef1b .elementor-divider-separator{width:50%;margin:0 auto;margin-center:0;}.elementor-339 .elementor-element.elementor-element-697ef1b .elementor-divider{text-align:center;padding-top:15px;padding-bottom:15px;}.elementor-339 .elementor-element.elementor-element-6f621d8{color:#151419;}.elementor-339 .elementor-element.elementor-element-6f621d8 > .elementor-widget-container{margin:-20px 0px -20px 0px;}.elementor-339 .elementor-element.elementor-element-33cea24{color:#151419;}.elementor-339 .elementor-element.elementor-element-33cea24 > .elementor-widget-container{margin:0px 0px -30px 0px;}.elementor-339 .elementor-element.elementor-element-4dc7b7e{color:#151419;}.elementor-339 .elementor-element.elementor-element-4dc7b7e > .elementor-widget-container{margin:0px 0px -30px 0px;border-radius:0px 0px 0px 0px;}.elementor-339 .elementor-element.elementor-element-64819f0{color:#151419;}.elementor-339 .elementor-element.elementor-element-64819f0 > .elementor-widget-container{margin:0px 0px -30px 0px;}.elementor-339 .elementor-element.elementor-element-c1f879c{color:#151419;}.elementor-339 .elementor-element.elementor-element-c1f879c > .elementor-widget-container{margin:0px 0px -30px 0px;}.elementor-339 .elementor-element.elementor-element-bcfb5ca{color:#151419;}.elementor-339 .elementor-element.elementor-element-bcfb5ca > .elementor-widget-container{margin:0px 0px -30px 0px;}.elementor-339 .elementor-element.elementor-element-e766e6e{color:#151419;}.elementor-339 .elementor-element.elementor-element-e766e6e > .elementor-widget-container{margin:0px 0px -30px 0px;}.elementor-339 .elementor-element.elementor-element-20858bb{color:#151419;}.elementor-339 .elementor-element.elementor-element-20858bb > .elementor-widget-container{margin:0px 0px -30px 0px;}.elementor-339 .elementor-element.elementor-element-cf11c04{color:#151419;}.elementor-339 .elementor-element.elementor-element-cf11c04 > .elementor-widget-container{margin:0px 0px -20px 0px;}.elementor-339 .elementor-element.elementor-element-246a0d2{text-align:center;}.elementor-339 .elementor-element.elementor-element-246a0d2.elementor-widget-heading .elementor-heading-title{color:#01B4EA;}.elementor-339 .elementor-element.elementor-element-246a0d2 .elementor-heading-title{font-family:"Vesper Libre", Sans-serif;font-size:30px;}.elementor-339 .elementor-element.elementor-element-246a0d2 > .elementor-widget-container{margin:0px 0px -10px 0px;}.elementor-339 .elementor-element.elementor-element-d98a690{color:#151419;}.elementor-339 .elementor-element.elementor-element-d98a690 > .elementor-widget-container{margin:0px 0px -10px 0px;}.elementor-339 .elementor-element.elementor-element-e11b895{color:#151419;}.elementor-339 .elementor-element.elementor-element-e11b895 > .elementor-widget-container{margin:-20px 0px -10px 0px;}.elementor-339 .elementor-element.elementor-element-672a215{margin-top:-25px;margin-bottom:-40px;}.elementor-339 .elementor-element.elementor-element-b014455 .elementor-image img{width:75%;}.elementor-339 .elementor-element.elementor-element-b014455 > .elementor-widget-container{margin:0px 0px 0px 0px;}@media(min-width:768px){.elementor-339 .elementor-element.elementor-element-a004079{width:29.175%;}.elementor-339 .elementor-element.elementor-element-a5a86d1{width:11.821%;}.elementor-339 .elementor-element.elementor-element-98aa152{width:34.004%;}.elementor-339 .elementor-element.elementor-element-ec61190{width:29.175%;}.elementor-339 .elementor-element.elementor-element-ce37bf1{width:11.821%;}.elementor-339 .elementor-element.elementor-element-2a606e4{width:34.004%;}.elementor-339 .elementor-element.elementor-element-71e5595{width:29.175%;}.elementor-339 .elementor-element.elementor-element-e107d84{width:11.821%;}.elementor-339 .elementor-element.elementor-element-1f8b423{width:34.004%;}}
    </style>
    <style type="text/css" media="print">
        @media print {
            body {-webkit-print-color-adjust: exact;}
        }
        @page {
            size:A4 landscape;
            margin-left: 0px;
            margin-right: 0px;
            margin-top: 0px;
            margin-bottom: 0px;
            margin: 0;
            -webkit-print-color-adjust: exact;
        }
    </style>
    <link rel='stylesheet' id='landingpress-css'  href='https://kelasonlinecopywriting.com/wp-content/themes/landingpress-wp/style.css?ver=3.0.0-beta10' type='text/css' media='all' />
    <link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CVesper+Libre%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCourgette%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;ver=5.3.4' type='text/css' media='all' />
    <link rel="canonical" href="https://kelasonlinecopywriting.com/coba/" />
    <link rel='shortlink' href='https://kelasonlinecopywriting.com/?p=339' />
    <link rel="icon" href="https://kelasonlinecopywriting.com/wp-content/uploads/2020/01/cropped-Web-Logo-KOC-32x32.png" sizes="32x32" />
    <link rel="icon" href="https://kelasonlinecopywriting.com/wp-content/uploads/2020/01/cropped-Web-Logo-KOC-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="https://kelasonlinecopywriting.com/wp-content/uploads/2020/01/cropped-Web-Logo-KOC-180x180.png" />
    <meta name="msapplication-TileImage" content="https://kelasonlinecopywriting.com/wp-content/uploads/2020/01/cropped-Web-Logo-KOC-270x270.png" />
</head>
<body onload="window.print();" class="page-template page-template-page_landingpress page-template-page_landingpress-php page page-id-339 page-landingpress page-landingpress-full header-inactive footer-inactive elementor-default elementor-page elementor-page-339">
    <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
    <div class="site-canvas">
        <div id="page" class="site-container">
            <div class="site-inner">
                <div id="content" class="site-content">
                    <div class="container">
                        <div id="primary" class="content-area">
                            <main id="main" class="site-main">
                                <div data-elementor-type="wp-page" data-elementor-id="339" class="elementor elementor-339" data-elementor-settings="[]">
                                    <div class="elementor-inner">
                                        <div class="elementor-section-wrap">
                                            <section class="elementor-element elementor-element-9687c93 elementor-section-height-full elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-id="9687c93" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-element elementor-element-8c09d0a elementor-column elementor-col-100 elementor-top-column" data-id="8c09d0a" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                            <div class="elementor-column-wrap  elementor-element-populated">
                                                                <div class="elementor-background-overlay"></div>
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-91f68dd elementor-widget elementor-widget-heading" data-id="91f68dd" data-element_type="widget" data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2 class="elementor-heading-title elementor-size-default">SERTIFIKAT KELULUSAN</h2>		</div>
                                                                        </div>
                                                                        <div class="elementor-element elementor-element-f16747d elementor-widget elementor-widget-text-editor" data-id="f16747d" data-element_type="widget" data-widget_type="text-editor.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-text-editor elementor-clearfix"><p style="text-align: center;">Menyatakan bahwa&nbsp;</p></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="elementor-element elementor-element-0a50759 elementor-widget elementor-widget-text-editor" data-id="0a50759" data-element_type="widget" data-widget_type="text-editor.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-text-editor elementor-clearfix"><p style="text-align: center;">{{ $nama }}</p></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="elementor-element elementor-element-697ef1b elementor-widget elementor-widget-divider" data-id="697ef1b" data-element_type="widget" data-widget_type="divider.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-divider">
                                                                                    <span class="elementor-divider-separator">
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="elementor-element elementor-element-6f621d8 elementor-widget elementor-widget-text-editor" data-id="6f621d8" data-element_type="widget" data-widget_type="text-editor.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-text-editor elementor-clearfix"><p style="text-align: center;">Pada tanggal : {{ $waktu }}</p></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="elementor-element elementor-element-33cea24 elementor-widget elementor-widget-text-editor" data-id="33cea24" data-element_type="widget" data-widget_type="text-editor.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-text-editor elementor-clearfix"><p style="text-align: center;">Telah menyelesaikan semua Praktikum di Kelas Online Copywriting yang diadakan oleh entrepreneurID</p></div>
                                                                            </div>
                                                                        </div>
                                                                        <section class="elementor-element elementor-element-aaf13e6 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="aaf13e6" data-element_type="section">
                                                                            <div class="elementor-container elementor-column-gap-default">
                                                                                <div class="elementor-row">
                                                                                    <div class="elementor-element elementor-element-a004079 elementor-column elementor-col-25 elementor-inner-column" data-id="a004079" data-element_type="column">
                                                                                        <div class="elementor-column-wrap">
                                                                                            <div class="elementor-widget-wrap">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="elementor-element elementor-element-a5a86d1 elementor-column elementor-col-25 elementor-inner-column" data-id="a5a86d1" data-element_type="column">
                                                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                                                            <div class="elementor-widget-wrap">
                                                                                                <div class="elementor-element elementor-element-4dc7b7e elementor-widget elementor-widget-text-editor" data-id="4dc7b7e" data-element_type="widget" data-widget_type="text-editor.default">
                                                                                                    <div class="elementor-widget-container">
                                                                                                        <div class="elementor-text-editor elementor-clearfix"><p style="text-align: right;">Praktikum 1</p></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="elementor-element elementor-element-98aa152 elementor-column elementor-col-25 elementor-inner-column" data-id="98aa152" data-element_type="column">
                                                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                                                            <div class="elementor-widget-wrap">
                                                                                                <div class="elementor-element elementor-element-64819f0 elementor-widget elementor-widget-text-editor" data-id="64819f0" data-element_type="widget" data-widget_type="text-editor.default">
                                                                                                    <div class="elementor-widget-container">
                                                                                                        <div class="elementor-text-editor elementor-clearfix"><p style="text-align: left;">Nulis Copywriting dengan Formula Standar</p></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="elementor-element elementor-element-bd052c8 elementor-column elementor-col-25 elementor-inner-column" data-id="bd052c8" data-element_type="column">
                                                                                        <div class="elementor-column-wrap">
                                                                                            <div class="elementor-widget-wrap">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </section>
                                                                        <section class="elementor-element elementor-element-aa626bb elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="aa626bb" data-element_type="section">
                                                                            <div class="elementor-container elementor-column-gap-default">
                                                                                <div class="elementor-row">
                                                                                    <div class="elementor-element elementor-element-ec61190 elementor-column elementor-col-25 elementor-inner-column" data-id="ec61190" data-element_type="column">
                                                                                        <div class="elementor-column-wrap">
                                                                                            <div class="elementor-widget-wrap">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="elementor-element elementor-element-ce37bf1 elementor-column elementor-col-25 elementor-inner-column" data-id="ce37bf1" data-element_type="column">
                                                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                                                            <div class="elementor-widget-wrap">
                                                                                                <div class="elementor-element elementor-element-c1f879c elementor-widget elementor-widget-text-editor" data-id="c1f879c" data-element_type="widget" data-widget_type="text-editor.default">
                                                                                                    <div class="elementor-widget-container">
                                                                                                        <div class="elementor-text-editor elementor-clearfix"><p style="text-align: right;">Praktikum 2</p></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="elementor-element elementor-element-2a606e4 elementor-column elementor-col-25 elementor-inner-column" data-id="2a606e4" data-element_type="column">
                                                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                                                            <div class="elementor-widget-wrap">
                                                                                                <div class="elementor-element elementor-element-bcfb5ca elementor-widget elementor-widget-text-editor" data-id="bcfb5ca" data-element_type="widget" data-widget_type="text-editor.default">
                                                                                                    <div class="elementor-widget-container">
                                                                                                        <div class="elementor-text-editor elementor-clearfix"><p style="text-align: left;">Nulis Copywriting dengan 5 Formula Praktis</p></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="elementor-element elementor-element-ab50627 elementor-column elementor-col-25 elementor-inner-column" data-id="ab50627" data-element_type="column">
                                                                                        <div class="elementor-column-wrap">
                                                                                            <div class="elementor-widget-wrap">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </section>
                                                                        <section class="elementor-element elementor-element-b57304e elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="b57304e" data-element_type="section">
                                                                            <div class="elementor-container elementor-column-gap-default">
                                                                                <div class="elementor-row">
                                                                                    <div class="elementor-element elementor-element-71e5595 elementor-column elementor-col-25 elementor-inner-column" data-id="71e5595" data-element_type="column">
                                                                                        <div class="elementor-column-wrap">
                                                                                            <div class="elementor-widget-wrap">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="elementor-element elementor-element-e107d84 elementor-column elementor-col-25 elementor-inner-column" data-id="e107d84" data-element_type="column">
                                                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                                                            <div class="elementor-widget-wrap">
                                                                                                <div class="elementor-element elementor-element-e766e6e elementor-widget elementor-widget-text-editor" data-id="e766e6e" data-element_type="widget" data-widget_type="text-editor.default">
                                                                                                    <div class="elementor-widget-container">
                                                                                                        <div class="elementor-text-editor elementor-clearfix"><p style="text-align: right;">Praktikum 3</p></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="elementor-element elementor-element-1f8b423 elementor-column elementor-col-25 elementor-inner-column" data-id="1f8b423" data-element_type="column">
                                                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                                                            <div class="elementor-widget-wrap">
                                                                                                <div class="elementor-element elementor-element-20858bb elementor-widget elementor-widget-text-editor" data-id="20858bb" data-element_type="widget" data-widget_type="text-editor.default">
                                                                                                    <div class="elementor-widget-container">
                                                                                                        <div class="elementor-text-editor elementor-clearfix"><p style="text-align: left;">Nulis Copywriting dengan 4 Teknik Terapan</p></div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="elementor-element elementor-element-4a3e08e elementor-column elementor-col-25 elementor-inner-column" data-id="4a3e08e" data-element_type="column">
                                                                                        <div class="elementor-column-wrap">
                                                                                            <div class="elementor-widget-wrap">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </section>
                                                                        <div class="elementor-element elementor-element-cf11c04 elementor-widget elementor-widget-text-editor" data-id="cf11c04" data-element_type="widget" data-widget_type="text-editor.default">
                                                                            <div class="elementor-widget-container">
                                                                                <div class="elementor-text-editor elementor-clearfix"><p style="text-align: center;">dengan hasil penilaian</p></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="elementor-element elementor-element-246a0d2 elementor-widget elementor-widget-heading" data-id="246a0d2" data-element_type="widget" data-widget_type="heading.default">
                                                                            <div class="elementor-widget-container">
                                                                                <h2 class="elementor-heading-title elementor-size-default">LULUS</h2>		</div>
                                                                            </div>
                                                                            <div class="elementor-element elementor-element-d98a690 elementor-widget elementor-widget-text-editor" data-id="d98a690" data-element_type="widget" data-widget_type="text-editor.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-text-editor elementor-clearfix"><p style="text-align: center;">Semoga ilmu yang didapatkan di kelas online ini bermanfaat untuk bisnis Anda.</p></div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-element elementor-element-e11b895 elementor-widget elementor-widget-text-editor" data-id="e11b895" data-element_type="widget" data-widget_type="text-editor.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <div class="elementor-text-editor elementor-clearfix"><p style="text-align: center;">www.kelasonlinecopywriting.com</p></div>
                                                                                </div>
                                                                            </div>
                                                                            <section class="elementor-element elementor-element-672a215 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-id="672a215" data-element_type="section">
                                                                                <div class="elementor-container elementor-column-gap-default">
                                                                                    <div class="elementor-row">
                                                                                        <div class="elementor-element elementor-element-ce2f605 elementor-column elementor-col-20 elementor-inner-column" data-id="ce2f605" data-element_type="column">
                                                                                            <div class="elementor-column-wrap">
                                                                                                <div class="elementor-widget-wrap">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="elementor-element elementor-element-cd06bda elementor-column elementor-col-20 elementor-inner-column" data-id="cd06bda" data-element_type="column">
                                                                                            <div class="elementor-column-wrap">
                                                                                                <div class="elementor-widget-wrap">
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="elementor-element elementor-element-a529986 elementor-column elementor-col-20 elementor-inner-column" data-id="a529986" data-element_type="column">
                                                                                            <div class="elementor-column-wrap  elementor-element-populated">
                                                                                                <div class="elementor-widget-wrap">
                                                                                                    <div class="elementor-element elementor-element-b014455 elementor-widget elementor-widget-image" data-id="b014455" data-element_type="widget" data-widget_type="image.default">
                                                                                                        <div class="elementor-widget-container">
                                                                                                            <div class="elementor-image">
                                                                                                                <img width="750" height="185" src="https://kelasonlinecopywriting.com/wp-content/uploads/2020/07/entrepreneurID-Red-Emblem-1024x252.png" class="attachment-large size-large" alt="" />											</div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="elementor-element elementor-element-b046fbf elementor-column elementor-col-20 elementor-inner-column" data-id="b046fbf" data-element_type="column">
                                                                                                <div class="elementor-column-wrap">
                                                                                                    <div class="elementor-widget-wrap">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="elementor-element elementor-element-84511d5 elementor-column elementor-col-20 elementor-inner-column" data-id="84511d5" data-element_type="column">
                                                                                                <div class="elementor-column-wrap">
                                                                                                    <div class="elementor-widget-wrap">
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </section>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </div>
                                        </main>
                                    </div>
                                </div>
                            </div>
                        </div></div></div><div id="back-to-top"><i class="fa fa-angle-up"></i></div><script type='text/javascript' src='https://kelasonlinecopywriting.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
                        <script type='text/javascript' src='https://kelasonlinecopywriting.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1' defer='defer'></script>
                        <script type='text/javascript' src='https://kelasonlinecopywriting.com/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=2.8.5.1-LP' defer='defer'></script>
                        <script type='text/javascript' src='https://kelasonlinecopywriting.com/wp-includes/js/jquery/ui/position.min.js?ver=1.11.4' defer='defer'></script>
                        <script type='text/javascript' src='https://kelasonlinecopywriting.com/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.7.3' defer='defer'></script>
                        <script type='text/javascript' src='https://kelasonlinecopywriting.com/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2' defer='defer'></script>
                        <script type='text/javascript' src='https://kelasonlinecopywriting.com/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=4.4.6' defer='defer'></script>
                        <script type='text/javascript'>
                            var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"2.8.5.1-LP","urls":{"assets":"https:\/\/kelasonlinecopywriting.com\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"general":{"elementor_global_image_lightbox":"yes"},"editorPreferences":[]},"post":{"id":339,"title":"Coba","excerpt":""}};
                        </script>
                        <script type='text/javascript' src='https://kelasonlinecopywriting.com/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=2.8.5.1-LP' defer='defer'></script>
                        <script type='text/javascript' src='https://kelasonlinecopywriting.com/wp-content/themes/landingpress-wp/assets/js/script.min.js?ver=3.0.0-beta10' defer='defer'></script>

                        <!--[if LandingPress]></body></html><![endif]-->
                        <!-- </body></html> -->
                    </body>
                    </html>
