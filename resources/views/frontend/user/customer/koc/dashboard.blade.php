

@stack('before-style')
<style>
    .linkp a {
        color: #313334;
    }
</style>
@stack('after-style')
<div class="row">
    <div class="col-md-12">
        <div class="text-value">
            <p>Selamat Datang, Peserta <span style="text-decoration:underline">Kelas Online Copywriting</span></p>
        </div>
        <div class="text-muted">
            Berikut Praktikum yang harus dikerjaan. Selamat Berjuang !
        </div>
    </div>
    @php
    $valid     = DB::table('praktikums_nilaibabs')
                ->where('user_praktikum_nilai', auth()->user()->id)
                ->where('status_praktikum_nilai', 'Lulus Praktikum')
                ->count();
    @endphp
    @if ( $valid == 3)
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-9">
                                    <h6>
                                        Selamat, Anda telah berhasil menyelesaikan semua praktikum. Sebagai apresiasi dari kami, silahkan download e-sertifikat Kelas Online Copywriting.
                                    </h6>
                                </div>
                                <div class="col-md-3" style="padding: 0px">
                                    <div class="float-right">
                                        <a href="/praktikum-koc/sertifikat?email={{ auth()->user()->email }}" target="_blank">
                                            <button class="btn btn-pill btn-info"><i class="fas fa-print"></i> <strong>Download Sertifikat</strong></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="col-md-12">
        <div class="row">
            <div class="col-2" style="padding-right: 0px;">
                <div class="card" style="border-radius: 0px; background: linear-gradient(180deg,#f44881,#ec454f);">
                    <div class="text-value text-center" style="color: #fff">
                        <i class="icon-speech" style="font-size: 57px"></i>
                    </div>
                </div>
            </div>
            @php
                $datanilai1   = DB::table('praktikums_nilaibabs')
                                ->where('user_praktikum_nilai', auth()->user()->id)
                                ->where('id_praktikum_bab', 1)
                                ->first();
                $datajawaban1 = DB::table('praktikums_jawabans')
                                ->where('user_praktikum_jawaban', auth()->user()->id)
                                ->first();
                if (isset($datanilai1->status_praktikum_nilai)) {
                    if ($datanilai1->status_praktikum_nilai == 'Perlu Diulang') {
                        $d1 = $datanilai1->status_praktikum_nilai;
                        $c1 = 'btn-danger';
                    } elseif ($datanilai1->status_praktikum_nilai == 'Lulus Praktikum') {
                        $d1 = $datanilai1->status_praktikum_nilai;
                        $c1 = 'btn-success';
                    } elseif ($datanilai1->status_praktikum_nilai == 'Menunggu Koreksi') {
                        $d1 = 'Menunggu Koreksi';
                        $c1 = 'btn-warning';
                    } else {
                        $d1 = 'Belum Dikerjakan';
                        $c1 = 'btn-info';
                    }
                } elseif (isset($datajawaban1->id)) {
                    $d1 = 'Menunggu Koreksi';
                    $c1 = 'btn-warning';
                } else {
                    $d1 = 'Belum Dikerjakan';
                    $c1 = 'btn-info';
                }
            @endphp
            <div class="linkp col-10" style="padding-left: 0px;">
                <div class="card" style="border-radius: 0px; padding: 15px; ">
                    <a href="/praktikum-koc/1" style="color: #313334">
                        <div class="row">
                            <div class="col-9">
                                <div class="text-value">Praktikum Pertama</div>
                                <div class="text-muted">Praktik nulis copywriting setelah mempelajari materi KOC minggu pertama.</div>
                            </div>
                            <div class="col-3">
                                <label class="btn-sm btn {{ $c1 }} btn-block btn-pill" style="position: absolute">
                                    <strong>{{ $d1 }}</strong>
                                </label>
                            </div>
                        </div>
                    </a>

                    @isset($datanilai1->deskripsi_praktikum_nilai)
                        <div class="card-footer" style="padding: 10px 10px 0px 10px; margin-top: 10px">
                            <span style="text-decoration:underline">Deskripsi Penilaian :</span>
                            <div class="row" style="padding: 0px 10px;">
                                <textarea rows="5" style="margin-top: 10px; margin-bottom: 10px; background-color: #fff" class="form-control" disabled>{{ $datanilai1->deskripsi_praktikum_nilai ?? '' }}</textarea>
                            </div>
                        </div>
                    @endisset
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-2" style="padding-right: 0px;">
                <div class="card" style="border-radius: 0px; background: linear-gradient(180deg,#21c8f6,#637bff);">
                    <div class="text-value text-center" style="color: #fff">
                        <i class="icon-speedometer" style="font-size: 57px"></i>
                    </div>
                </div>
            </div>
            @php
                $datanilai2   = DB::table('praktikums_nilaibabs')
                                ->where('user_praktikum_nilai', auth()->user()->id)
                                ->where('id_praktikum_bab', 2)
                                ->first();
                $datajawaban2 = DB::table('praktikums_jawabans')
                                ->where('user_praktikum_jawaban', auth()->user()->id)
                                ->first();
                if (isset($datanilai2->status_praktikum_nilai)) {
                    if ($datanilai2->status_praktikum_nilai == 'Perlu Diulang') {
                        $d2 = $datanilai2->status_praktikum_nilai;
                        $c2 = 'btn-danger';
                    } elseif ($datanilai2->status_praktikum_nilai == 'Lulus Praktikum') {
                        $d2 = $datanilai2->status_praktikum_nilai;
                        $c2 = 'btn-success';
                    } elseif ($datanilai2->status_praktikum_nilai == 'Menunggu Koreksi') {
                        $d2 = 'Menunggu Koreksi';
                        $c2 = 'btn-warning';
                    }
                } else {
                    $d2 = 'Belum Dikerjakan';
                    $c2 = 'btn-info';
                }
            @endphp
            <div class="linkp col-10" style="padding-left: 0px;">
                <div class="card" style="border-radius: 0px; padding: 15px; color: #5b5f63;">
                    <a href="/praktikum-koc/2" style="color: #313334">
                        <div class="row">
                            <div class="col-9">
                                <div class="text-value">Praktikum Kedua</div>
                                <div class="text-muted">Praktik nulis copywriting setelah mempelajari materi KOC minggu kedua.</div>
                            </div>
                            <div class="col-3">
                                <label class="btn-sm btn {{ $c2 }} btn-block btn-pill" style="position: absolute">
                                    <strong>{{ $d2 }}</strong>
                                </label>
                            </div>
                        </div>
                    </a>
                    @isset($datanilai2->deskripsi_praktikum_nilai)
                        <div class="card-footer" style="padding: 10px 10px 0px 10px; margin-top: 10px">
                            <span style="text-decoration:underline">Deskripsi Penilaian :</span>
                            <div class="row" style="padding: 0px 10px;">
                                <textarea rows="5" style="margin-top: 10px; margin-bottom: 10px; background-color: #fff" class="form-control" disabled>{{ $datanilai2->deskripsi_praktikum_nilai ?? '' }}</textarea>
                            </div>
                        </div>
                    @endisset
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-2" style="padding-right: 0px;">
                <div class="card" style="border-radius: 0px; background: linear-gradient(180deg,#6edcc4,#1aab8b);">
                    <div class="text-value text-center" style="color: #fff">
                        <i class="icon-pie-chart" style="font-size: 57px"></i>
                    </div>
                </div>
            </div>
            @php
                $datanilai3   = DB::table('praktikums_nilaibabs')
                                ->where('user_praktikum_nilai', auth()->user()->id)
                                ->where('id_praktikum_bab', 3)
                                ->first();
                $datajawaban3 = DB::table('praktikums_jawabans')
                                ->where('user_praktikum_jawaban', auth()->user()->id)
                                ->first();
                if (isset($datanilai3->status_praktikum_nilai)) {
                    if ($datanilai3->status_praktikum_nilai == 'Perlu Diulang') {
                        $d3 = $datanilai3->status_praktikum_nilai;
                        $c3 = 'btn-danger';
                    } elseif ($datanilai3->status_praktikum_nilai == 'Lulus Praktikum') {
                        $d3 = $datanilai3->status_praktikum_nilai;
                        $c3 = 'btn-success';
                    } elseif ($datanilai3->status_praktikum_nilai == 'Menunggu Koreksi') {
                        $d3 = 'Menunggu Koreksi';
                        $c3 = 'btn-warning';
                    }
                } else {
                    $d3 = 'Belum Dikerjakan';
                    $c3 = 'btn-info';
                }
            @endphp
            <div class="linkp col-10" style="padding-left: 0px;">
                <div class="card" style="border-radius: 0px; padding: 15px; color: #5b5f63;">
                    <a href="/praktikum-koc/3" style="color: #313334">
                        <div class="row">
                            <div class="col-9">
                                <div class="text-value">Praktikum Ketiga</div>
                                <div class="text-muted">Praktik nulis copywriting setelah mempelajari materi KOC minggu ketiga.</div>
                            </div>
                            <div class="col-3">
                                <label class="btn-sm btn {{ $c3 }} btn-block btn-pill" style="position: absolute">
                                    <strong>{{ $d3 }}</strong>
                                </label>
                            </div>
                        </div>
                    </a>
                    @isset($datanilai3->deskripsi_praktikum_nilai)
                        <div class="card-footer" style="padding: 10px 10px 0px 10px; margin-top: 10px">
                            <span style="text-decoration:underline">Deskripsi Penilaian :</span>
                            <div class="row" style="padding: 0px 10px;">
                                <textarea rows="5" style="margin-top: 10px; margin-bottom: 10px; background-color: #fff" class="form-control" disabled>{{ $datanilai3->deskripsi_praktikum_nilai ?? '' }}</textarea>
                            </div>
                        </div>
                    @endisset
                </div>
            </div>
        </div>
    </div>
</div>
