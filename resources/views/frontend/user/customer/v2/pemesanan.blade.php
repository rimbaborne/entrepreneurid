@extends('frontend.layouts.lock')

@section('title', app_name() . ' | Pemesanan '. $cekeventaktif->produk->nama)

@section('content')
<div class="row justify-content-center align-items-center">
    <div class="col col-md-5 align-self-center">
        <div class="card-2">
            <div class="row p-3">
                <div class="col-4 text-center">
                    <img class="img-fluid" src="https://dashboard.agen-entrepreneurid.com/img/produk/{{ $cekeventaktif->gambar }}" alt="{{ $cekeventaktif->produk->nama }}" style="width: 100px">
                </div>
                <div class="col-8 pt-3">
                    <div style="font-size: 19px; font-weight: 700; padding-bottom: 5px">DAFTAR</div>
                    <div style="font-size: 19px; font-weight: 700; padding-bottom: 5px">{{ $cekeventaktif->produk->nama }}</div>
                    <h2 style="margin-top: -5px; font-weight: 700">
                        Rp {{ strrev(implode('.',str_split(strrev(strval($cekeventaktif->harga)),3))) }}
                        <span class="position-absolute" style="font-size: 0.5em; color: rgb(0, 179, 68);">
                            <i class="fas fa-check-circle"></i>
                        </span>
                    </h2>
                </div>
            </div>

        </div>
        <div class="card">
            {{-- <span class="text-center" style="font-size: 1.4rem; font-weight: 300; line-height: 1.2;">Daftar Akun</span> --}}

            <span class="text-center text-muted">Form Pemesanan</span>
            <div class="card-body">
                <form action="/simpan-pemesanan" method="POST">
                    @csrf
                    <input value="{{ $ref }}" name="ref" hidden>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input autocomplete="new-password" type="text" name="nama" value="{{ old('nama') }}"  class="form-control" maxlength="100" placeholder="Nama Lengkap" required="" >
                            </div><!--col-->
                        </div><!--row-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input type="text" name="panggilan" value="{{ old('panggilan') }}" class="form-control" maxlength="100" placeholder="Nama Panggilan" required="">
                            </div><!--col-->
                        </div><!--row-->
                    </div><!--row-->

                    <div class="form-group row">
                        <div class="col-md-9 col-form-label">
                            <div class="form-check form-check-inline mr-1">
                                <input class="form-check-input" type="radio" value="Laki-laki" name="gender" required>
                                <label class="form-check-label">Laki-laki</label>
                            </div>
                            <div class="form-check form-check-inline mr-1">
                                <input class="form-check-input" type="radio" value="Perempuan" name="gender">
                                <label class="form-check-label">Perempuan</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <div class="input-group row pr-0 mr-0">
                                <div class="input-group-prepend col-4 pr-0" >
                                    <span class="input-group-text" style="background: #fff">
                                        <select name="kode_nohp" id="kode_nohp" class="form-control" style="border: none; height: unset; padding: 0px;border-top-right-radius: 0; border-bottom-right-radius: 0;font-size: 12px">
                                            @include('agen.includes.phone-code')
                                        </select>
                                    </span>
                                </div>
                                <input id="nohp"
                                        type="number"
                                        name="nohp"
                                        value="{{ old('nohp') }}"
                                        class="form-control col-8"
                                        maxlength="12"
                                        placeholder="8123456789 (No. WhatsApp)"
                                        {{-- oninvalid="this.setCustomValidity('Diisi No. WhatsApp')" --}}
                                        required="">
                            </div><!--form-group-->
                            <div class="row">
                                <div class="col-4"></div>
                                <div class="col-8 pl-0">
                                    <span class="help-block text-muted" style="font-size: 10px; font-weight: 700">Tidak Pakai Angka 0 . Contoh : 81234563789</span>
                                </div>
                            </div>
                        </div><!--col-->
                    </div><!--row-->

                    <div class="form-group">
                        <label style="padding-right: 10px">Tanggal Lahir</label>
                        <select name="tgl">
                            @for ($a = 1; $a <= 31; $a++)
                                <option value="{{ $a }}">{{ $a }}</option>
                            @endfor
                        </select>
                        <select name="bln">
                            @for ($a = 1; $a <= 12; $a++)
                                <option value="{{ $a }}">{{ $a }}</option>
                            @endfor
                        </select>
                        <select name="thn">
                            @for ($a = 1950; $a <= 2015; $a++)
                                <option value="{{ $a }}">{{ $a }}</option>
                            @endfor
                        </select>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input autocomplete="new-password" type="email" name="email" value="{{ old('email') }}" id="email" placeholder="Alamat Email" maxlength="191" required="required" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    {{-- <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="kota" value="{{ old('kota') }}" class="form-control" placeholder="Kota Domisili" required="">
                            </div>
                        </div>
                    </div> --}}

                    <div class="row">
                        <div class="col">
                            <div class="form-group mb-0 clearfix">
                                <button type="submit" class="btn btn-pill btn-block btn-info btn-block" style="box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgb(1, 173, 226)">
                                {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="background-color: #41414b; border-color: #41414b; box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(66, 65, 76)"> --}}
                                    <strong>DAFTAR</strong> <i class="fas fa-arrow-alt-circle-right"></i>
                                </button>
                            </div><!--form-group-->
                        </div><!--col-->
                     </div><!--row-->
                </form>

            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- col-md-8 -->
</div><!-- row -->

@stack('before-scripts')
    <script>
            $("#nohp").on("input", function() {
                if (/^0/.nohp(this.value)) {
                    this.value = this.value.replace(/^0/, "")
                }
            })
    </script>
@stack('after-scripts')

@endsection

@push('after-scripts')
@if(config('access.captcha.registration'))
@captchaScripts
@endif
@endpush
