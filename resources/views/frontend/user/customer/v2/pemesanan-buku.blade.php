@extends('frontend.layouts.lock')

@section('title', app_name() . ' | Pemesanan '. $cekeventaktif->produk->nama)

@section('content')
<div class="row justify-content-center align-items-center">
    <div class="col col-md-5 align-self-center">
        <div class="card-2">
            <div class="row p-3">
                <div class="col-4 text-center">
                    <img class="img-fluid" src="https://dashboard.agen-entrepreneurid.com/img/produk/{{ $cekeventaktif->gambar }}" alt="{{ $cekeventaktif->produk->nama }}" style="width: 100px">
                </div>
                <div class="col-8 pt-3">
                    <div style="font-size: 19px; font-weight: 700; padding-bottom: 5px">DAFTAR</div>
                    <div style="font-size: 19px; font-weight: 700; padding-bottom: 5px">{{ $cekeventaktif->produk->nama }}</div>
                    <h2 style="margin-top: -5px; font-weight: 700">
                        Rp {{ strrev(implode('.',str_split(strrev(strval($cekeventaktif->harga)),3))) }}
                        <span class="position-absolute" style="font-size: 0.5em; color: rgb(0, 179, 68);">
                            <i class="fas fa-check-circle"></i>
                        </span>
                    </h2>
                </div>
            </div>

        </div>
        <div class="card">
            <div id="spinner" class="row d-none" style=" text-align: end;">
                <div style="border-radius: 10px; position: absolute; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 9999; background: rgba(0,0,0,.4); margin: 0 1; ">
                    <div style=" position: relative; border-radius: .3125em; font-family: inherit; top: 50%; left: 50%; transform: translate(-100%, 100%); color: #fff; ">
                        <span class="spinner-border" role="status" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
            <span class="text-center text-muted">Form Pemesanan</span>
            <div class="card-body">
                <form action="/simpan-pemesanan" method="POST" onClick="this.form.submit(); this.disabled=true; this.value='Proses';">
                    @csrf
                    <input value="{{ $ref }}" name="ref" hidden>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input autocomplete="new-password" type="text" name="nama" value="{{ old('nama') }}"  class="form-control" maxlength="100" placeholder="Nama Lengkap" required="" >
                            </div><!--col-->
                        </div><!--row-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input type="text" name="panggilan" value="{{ old('panggilan') }}" class="form-control" maxlength="100" placeholder="Nama Panggilan" required="">
                            </div><!--col-->
                        </div><!--row-->
                    </div><!--row-->

                    <div class="form-group row">
                        <div class="col-md-9 col-form-label">
                            <div class="form-check form-check-inline mr-1">
                                <input class="form-check-input" type="radio" value="Laki-laki" name="gender" required>
                                <label class="form-check-label">Laki-laki</label>
                            </div>
                            <div class="form-check form-check-inline mr-1">
                                <input class="form-check-input" type="radio" value="Perempuan" name="gender">
                                <label class="form-check-label">Perempuan</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <div class="input-group row pr-0 mr-0">
                                <div class="input-group-prepend col-4 pr-0" >
                                    <span class="input-group-text" style="background: #fff">
                                        <select name="kode_nohp" id="kode_nohp" class="form-control" style="border: none; height: unset; padding: 0px;border-top-right-radius: 0; border-bottom-right-radius: 0;font-size: 12px">
                                            @include('agen.includes.phone-code')
                                        </select>
                                    </span>
                                </div>
                                <input id="nohp"
                                        type="number"
                                        name="nohp"
                                        value="{{ old('nohp') }}"
                                        class="form-control col-8"
                                        maxlength="12"
                                        placeholder="8123456789 (No. WhatsApp)"
                                        {{-- oninvalid="this.setCustomValidity('Diisi No. WhatsApp')" --}}
                                        required="">
                            </div><!--form-group-->
                            <div class="row">
                                <div class="col-4"></div>
                                <div class="col-8 pl-0">
                                    <span class="help-block text-muted" style="font-size: 10px; font-weight: 700">Tidak Pakai Angka 0 . Contoh : 81234563789</span>
                                </div>
                            </div>
                        </div><!--col-->
                    </div><!--row-->

                    <div class="form-group">
                        <label style="padding-right: 10px">Tanggal Lahir</label>
                        <select name="tgl">
                            @for ($a = 1; $a <= 31; $a++)
                                <option value="{{ $a }}">{{ $a }}</option>
                            @endfor
                        </select>
                        <select name="bln">
                            @for ($a = 1; $a <= 12; $a++)
                                <option value="{{ $a }}">{{ $a }}</option>
                            @endfor
                        </select>
                        <select name="thn">
                            @for ($a = 1950; $a <= 2015; $a++)
                                <option value="{{ $a }}">{{ $a }}</option>
                            @endfor
                        </select>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input autocomplete="new-password" type="email" name="email" value="{{ old('email') }}" id="email" placeholder="Alamat Email" maxlength="191" required="required" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="form-label"> Asal Kota Pengiriman : <strong>BALIKPAPAN</strong></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label class="form-label"> Isi Data Alamat Tujuan : </label>
                                <textarea autocomplete="new-password" type="text" name="alamat" class="form-control" placeholder="Alamat Lengkap" cols="5" required="">{{ old('alamat') }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <select class="form-control" id="provinsi" name="provinsi" required>
                                    <option value="">-- Pilih Provinsi -- </option>
                                    @foreach ($dataprovinsi->rajaongkir->results as $key => $data)
                                        <option value="{{ $data->province_id }}">{{ $data->province }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row d-none" id="formkota">
                        <div class="col">
                            <div class="form-group">
                                <select class="form-control" id="kota" name="kota" required>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group d-none" id="formkecamatan">
                                <select class="form-control" id="kecamatan" name="kecamatan" required>
                                </select>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="row d-none" id="spinner">
                        <div class="col text-center">
                            <div class="spinner-border" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div> --}}
                    <div class="d-none" id="formongkir">
                        <div class="row mb-3">
                            <div class="col">
                                <div class="input-group ">
                                    <span class="input-group-text" id="basic-addon1">Kodepos</span>
                                    <input autocomplete="new-password" type="number" name="kodepos" id="kodepos" value="{{ old('kodepos') }}" class="form-control" placeholder="Masukkan Kodepos .." cols="5" required="">
                                </div>
                                <span class="help-block text-muted " style="font-size: 10px; font-weight: 700">apabila kodepos diatas salah, klik link & cari <a href="https://www.nomor.net/_kodepos.php" target="_blank">kodepos disini</a></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <select class="form-control" id="ongkir" name="ongkir" style="text-transform: uppercase;" required>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-6 col-form-label">Jumlah Buku</label>
                            <div class="col-md-6">
                                <select class="form-control" name="jumlah" id="jumlah">
                                    @for ($i = 1; $i <= 50; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <span class="col-md-12 help-block text-muted" style="font-size: 10px; font-weight: 700">Berat 1 Pcs Buku : 500 gram</span>
                        </div>
                        {{-- <div class="row">
                            <label class="col-md-6 col-form-label">Kode Unik</label>
                            <div class="col-md-6 col-form-label text-right" style="right: 0px">
                                <input disabled class="form-control" style="border: 0px" id="kodeunik" value=" $kodeunik }}">
                            </div>
                        </div> --}}
                        <div class="row">
                            <label class="col-6 col-form-label">Harga</label>
                            <div class="col-6 col-form-label text-right" >
                                <label class="text-left" style="float: left;">Rp.</label>
                                <label id="harga"> </label>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-6 col-form-label">Ongkir</label>
                            <div class="col-6 col-form-label text-right" >
                                <label class="text-left" style="float: left;">Rp.</label>
                                <label id="hargaongkir"> </label>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-6"></label>
                            <div class="col-6">
                                <hr style="margin: 0px; padding: 0px">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-6 col-form-label">Total</label>
                            <div class="col-6 col-form-label text-right">
                                <strong>
                                    <label class="text-left" style="float: left;">Rp.</label>
                                    <label id="total"> </label>
                                </strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <div class="checkbox" style="color: #696969;">
                                        <label class="col-form-label"><input type="checkbox" id="remember" required> Data yang diisi sudah benar.</label>
                                    </div>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div>
                        <input type="hidden" name="harga" id="harga_">
                        <input type="hidden" name="hargaongkir" id="hargaongkir_">
                        <input type="hidden" name="total" id="total_">
                        {{-- <input type="hidden" name="kodeunik" value=" $kodeunik }}"> --}}

                        <div class="row">
                            <div class="col">
                                <div class="form-group mb-0 clearfix">
                                    {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(244,67,54"> --}}
                                    <button type="submit" class="btn btn-pill btn-block btn-primary btn-block">
                                        Pesan
                                    </button>
                                    {{-- <input type="submit" id="submit_" class="btn btn-pill btn-block btn-primary btn-block" value="Pesan"> --}}
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                    </div>
                </form>

            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- col-md-8 -->
</div><!-- row -->

@stack('before-scripts')
    <script>
            $("#nohp").on("input", function() {
                if (/^0/.nohp(this.value)) {
                    this.value = this.value.replace(/^0/, "")
                }
            })
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#loading').hide();
            function formkotashow() {
                document.getElementById("formkecamatan").classList.add('d-none');

                var select = document.getElementById("kota");
                var length = select.options.length;
                for (i = length-1; i >= 0; i--) {
                    select.options[i] = null;
                }

                var select = document.getElementById("ongkir");
                var length = select.options.length;
                for (i = length-1; i >= 0; i--) {
                    select.options[i] = null;
                }

                document.getElementById("formkota").classList.remove('d-none');
            }

            function formkecamatanshow() {
                var select = document.getElementById("kecamatan");
                var length = select.options.length;
                for (i = length-1; i >= 0; i--) {
                    select.options[i] = null;
                }

                var select = document.getElementById("ongkir");
                var length = select.options.length;
                for (i = length-1; i >= 0; i--) {
                    select.options[i] = null;
                }

                document.getElementById("formkecamatan").classList.remove('d-none');
            }
            function formongkirshow() {
                var select = document.getElementById("ongkir");
                var length = select.options.length;
                for (i = length-1; i >= 0; i--) {
                    select.options[i] = null;
                }

                document.getElementById("formongkir").classList.remove('d-none');
            }
            function uang(x) {
                return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
            }
            function hitungongkir(){
                var ongkir      = $('#ongkir').val();
                var ongkir_     = ongkir.split(",");
                var dataongkir  = ongkir_[2];
                var jumlah      = $('#jumlah').val();
                var harga       = {!! $cekeventaktif->harga !!}*jumlah;

                if (jumlah > 2) {
                    var penambahan = jumlah % 2 ;
                    if ( penambahan === 1) {
                        var berat = ((jumlah - 1) / 2) + 1;
                    } else {
                        var berat = (jumlah / 2);
                    }
                } else {
                    var berat = 1;
                }

                var total       = parseInt((parseInt(harga)+(parseInt(dataongkir)*parseInt(berat))));
                var hargaongkir = dataongkir * berat;

                console.log(harga);
                console.log(dataongkir);
                console.log(berat);
                console.log(harga + (dataongkir * berat));

                document.getElementById("hargaongkir").innerHTML = uang(hargaongkir);
                document.getElementById("harga").innerHTML       = uang(harga);
                document.getElementById("total").innerHTML       = uang(total);
                document.getElementById("hargaongkir_").setAttribute('value', hargaongkir);
                document.getElementById("harga_").setAttribute('value', harga);
                document.getElementById("total_").setAttribute('value', total);
            }
            $('#provinsi').change(function () {
                //Mengambil value dari option select provinsi kemudian parameternya dikirim menggunakan ajax
                var prov = $('#provinsi').val();

                $.ajax({
                    type: 'GET',
                    url: '/cek-ongkir',
                    data: 'cari=kota&provinsi=' + prov,
                    beforeSend: function(){
                        $('#loading').show();
                        document.getElementById("spinner").classList.remove('d-none');
                    },
                    complete: function(){
                        $('#loading').hide();
                    },
                    success: function (data) {
                        //jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten
                        formkotashow();
                        data.forEach(e =>$('#kota').append(e.datakota));
                        document.getElementById("spinner").classList.add('d-none');
                    }
                });

            });

            $('#kota').change(function () {
                //Mengambil value dari option select kota kemudian parameternya dikirim menggunakan ajax
                var city = $('#kota').val();
                var kodepos_=$(this).find('option:selected').attr('data-kodepos');
                $('#kodepos').val(kodepos_);

                $.ajax({
                    type: 'GET',
                    url: '/cek-ongkir',
                    data: 'cari=kecamatan&kota=' + city,
                    beforeSend: function(){
                        $('#loading').show();
                        document.getElementById("spinner").classList.remove('d-none');
                    },
                    complete: function(){
                        $('#loading').hide();
                    },
                    success: function (data) {
                        //jika data berhasil didapatkan, tampilkan ke dalam option select kabupaten
                        formkecamatanshow();
                        data.forEach(e =>$('#kecamatan').append(e.datakecamatan));
                        document.getElementById("spinner").classList.add('d-none');
                    }
                });
            });


            $('#kecamatan').change(function () {
                var kec   = $('#kecamatan').val();
                $.ajax({
                    type: 'GET',
                    url: '/cek-ongkir',
                    data: 'cari=ongkir&kec_id=' + kec,
                    beforeSend: function(){
                        $('#loading').show();
                        document.getElementById("spinner").classList.remove('d-none');
                    },
                    complete: function(){
                        $('#loading').hide();
                    },
                    success: function (data) {
                        //jika data berhasil didapatkan, tampilkan ke dalam element div ongkir
                        formongkirshow();
                        data.forEach(e =>$('#ongkir').append(e.dataongkir));
                        hitungongkir();
                        document.getElementById("spinner").classList.add('d-none');
                    }
                });
            });

            $('#ongkir').change(function () {
                hitungongkir();
            });

            $('#jumlah').change(function () {
                hitungongkir();
            });
        });
    </script>
@stack('after-scripts')

@endsection

@push('after-scripts')
@if(config('access.captcha.registration'))
@captchaScripts
@endif
@endpush
