@extends('frontend.layouts.lock')

@section('title', app_name() . ' | Pemesanan '. $cekeventaktif->produk->nama)

@section('content')
@stack('before-styles')
    <link rel="stylesheet" type="text/css" href="/filepond/app.css">
@stack('after-styles')
<div class="row justify-content-center align-items-center">
    <div class="col col-md-4 align-self-center">
        <div class="card">
            <div class="text-center">
                <h5>{{ $cekeventaktif->produk->nama }}</h5>
            </div>
            <span class="text-center text-muted">Form Pembayaran</span>
            <div class="card-body">
                <form action="">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input onkeyup="this.value = this.value.toLowerCase();" autocomplete="new-password" type="email" name="email" value="{{ request()->email }}" id="email" placeholder="Masukkan Alamat Email" maxlength="191" required="required" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group mb-0 clearfix">
                                {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(244,67,54"> --}}
                                {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="background-color: #41414b; border-color: #41414b; box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(66, 65, 76)"> --}}
                                <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="background-color: #41414b; border-color: #41414b; ">
                                    Submit <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->
                </form>
            </div><!-- card-body -->
        </div><!-- card -->
        @if ($data == '0' && request()->get('email') == !null)
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="text-center">
                                <p>
                                    Mohon maaf data email yang Anda masukkan tidak ada.
                                    <br>
                                    Silahkan Masukkan Email yang benar.
                                </p>
                            </div>
                        </div><!--col-->
                    </div><!--row-->
                </div>
            </div>
        @elseif (request()->email != '')
        <div class="card">
            <span class="text-center text-muted">Bukti Transfer</span>
            <div class="card-body">
                @if ($data->event->produk->jenis == 'ECOURSE')
                    <div class="row">
                        <div class="col">
                            <label class="form-label font-weight-bold">Email</label>
                            <div class="form-group">
                                <input style="font-weight: 700; background-color: #fff;" onkeyup="this.value = this.value.toLowerCase();" autocomplete="new-password" type="email" name="email" value="{{ request()->email }}" id="email" placeholder="Masukkan Alamat Email" maxlength="191" required="required" class="form-control" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <label class="form-label font-weight-bold">Nominal</label>
                            <div class="form-group">
                                    <input style="font-weight: 700; background-color: #fff;" type="text" name="nominal" value="Rp. {{ $data->total }}"  maxlength="191" required="required" class="form-control" disabled>
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->
                @elseif ($data->event->produk->jenis == 'BUKU')
                    @if ($data->book->no_resi)
                    <div class="row">
                        <div class="col-12 pt-2">
                            <div class="float-left"><span>No. Resi</span></div>
                            <h5 class="text-right" style="font-weight: 400"><span>{{ $data->book->no_resi}}</span></h5>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-12 bg-light pb-2 pt-2 rounded">
                            <div class="float-left"><span>Nama</span></div>
                            <h5 class="text-right" style="font-weight: 400; font-size: 12px"><span>{{ $data->nama }}</span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 pt-2 pb-2">
                            <div class="float-left"><span>Email</span></div>
                            <h5 class="text-right" style="font-weight: 400; font-size: 12px"><span>{{ $data->email }}</span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 bg-light pt-2 pb-2 rounded">
                            <div class="text-left"><span>Alamat</span></div>
                            <div class="text-right" style="font-weight: 400; font-size: 11px">
                                <span>
                                    {{ $data->book->alamat }}. Kec. {{ $data->book->kecamatan }}, {{ $data->book->kota }}, Prov. {{ $data->book->provinsi }}
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 pt-2">
                            <div class="float-left"><span>Jumlah Buku</span></div>
                            <h5 class="text-right" style="font-weight: 400"><span>{{ $data->book->jumlah_buku }} Buku</span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 bg-light pt-2 rounded">
                            <div class="float-left"><span>Kurir</span></div>
                            <h5 class="text-right" style="font-weight: 400"><span>{{ $data->book->kurir }}</span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 pt-2">
                            <div class="float-left"><span>Harga</span></div>
                            <h5 class="text-right" style="font-weight: 400"><span>Rp {{ strrev(implode('.',str_split(strrev(strval($data->book->harga)),3))) }}</span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 bg-light pt-2 rounded">
                            <div class="float-left"><span>Ongkir</span></div>
                            <h5 class="text-right" style="font-weight: 400"><span>Rp {{ strrev(implode('.',str_split(strrev(strval($data->book->ongkir)),3))) }}</span></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 pt-2">
                            <div class="float-left"><span>Total</span></div>
                            <h5 class="text-right" style="font-weight: 700"><span>Rp {{ strrev(implode('.',str_split(strrev(strval($data->book->total)),3))) }}</span></h5>
                        </div>
                    </div>
                @endif
                <div class="row pt-4">
                    <div class="col">
                        <label class="form-label font-weight-bold">Status</label>
                        <div class="form-group">
                            <input style="font-weight: 700; background-color: #fff;" type="text" name="status" value="{{ $status }}" class="form-control border-{{ $wstatus }} text-{{ $wstatus }}" disabled>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->


                @if ($data->status == '1')
                    <form id="fform" action="/simpan-invoice" method="post">
                        @csrf
                        <input type="text" name="id" value="{{ $data->id }}" hidden>
                        <div class="form-group row">
                            <label class="form-label col-12">Rekening Tujuan Yang Ditransfer</label>
                            <div class="col-12">
                                <select class="form-control" name="rekening" required>
                                    <option value="Mandiri = 1360007600528">Mandiri = 1360007600528 a/n Wulandari Tri</option>
                                    <option value="BCA = 1911488766">BCA = 1911488766 a/n Wulandari Tri</option>
                                    <option value="BRI = 012101009502532">BRI = 012101009502532 a/n Wulandari Tri</option>
                                    <option value="BNI = 1238568920">BNI = 1238568920 a/n Wulandari Tri</option>
                                </select>
                            </div><!--col-->
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <input type="file" class="upload-buktitransfer" required/>
                                <span class="help-block text-muted" style="font-size: 10px; font-weight: 700; ">Maksimal File 10 MB</span>
                            </div><!--col-->
                        </div>
                        <div class="row">
                            <div class="col" style="">
                                <div class="form-group mb-0 clearfix d-none" id="btn-submit">
                                    <button type="submit" class="btn btn-pill btn-block btn-info btn-block">
                                        Upload Bukti Transfer <i class="fa fa-upload"></i>
                                    </button>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                    </form>
                @else
                    <div class="row">
                        <div class="col">
                            <label class="form-label font-weight-bold">Waktu Upload</label>
                            <div class="form-group">
                                <input style="font-weight: 700; background-color: #fff;" type="text" name="status " value="{{ $data->waktu_upload ?? '' }}" class="form-control" disabled>
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->
                    <img src="/app/public/bukti-transfer/{{ $data->bukti_tf ?? '' }}" class="img-fluid rounded" alt="">
                @endif
            </div><!-- card-body -->
        </div><!-- card -->
        @endif
    </div><!-- col-md-8 -->
</div><!-- row -->
@stack('before-scripts')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script> --}}

<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
<script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.js"></script>
<script src="https://unpkg.com/filepond-plugin-image-resize/dist/filepond-plugin-image-resize.js"></script>
<script src="/filepond/app.js"></script>
<script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>
<script>
    $(function(){
        $.fn.filepond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginFileValidateType,
            FilePondPluginFileValidateSize,
            FilePondPluginImageResize
        );
    });

    $(function(){
            $('.upload-buktitransfer').filepond({
                labelIdle: '<span class="filepond--label-action"> Upload File/Foto Bukti Transfer.</span>',
                allowMultiple: false,
                acceptedFileTypes: "image/png, image/jpeg",
                allowFileSizeValidation: true,
                maxFileSize: '10MB',
                server: {
                    url: '/upload-invoice',
                    process: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        onload: (response) => response.key,
                        onerror: (response) => response.data,
                        ondata: (formData) => {
                            return console.log('sukses');
                        }
                    }
                }
            });

            $('.upload-buktitransfer').on('FilePond:processfile', function(e) {
                document.getElementById("btn-submit").classList.remove('d-none');
            });

        });
    $(document).ready(function () {
        $('#fform').on('submit',function(e) {
            if (pond.status != 4) {
                return false;
            }
            $(this).find(':input[type=submit]').hide();
            return true;
        });
    });

</script>
@endsection

@push('after-scripts')
@if(config('access.captcha.registration'))
@captchaScripts
@endif
@endpush
