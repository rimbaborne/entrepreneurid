<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js">
    </script>
</head>
<body>

    <input type="number" id="test" data-toggle="tooltip" data-placement="bottom" title="Tooltip on bottom">
    <script>
    $("#test").on("input", function() {
        if (/^0/.test(this.value)) {
          this.value = this.value.replace(/^0/, "")
        }
      })
    </script>
</body>
</html>
