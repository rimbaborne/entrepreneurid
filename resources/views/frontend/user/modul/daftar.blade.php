@extends('frontend.layouts.lock')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

@section('content')
<div class="row justify-content-center align-items-center">
    <div class="col col-md-4 align-self-center">
        {{-- <ul class="list-group mb-3">
            <li class="list-group-item lh-condensed">
              <div>
                <h6 class="my-0">@include('frontend.auth.title')</h6>
              </div>
            </li>
            <li class="list-group-item d-flex justify-content-between">
              <span>Harga</span>
              <strong style="font-size: 18px">
                @if ($_SERVER['HTTP_HOST'] == 'app.copywritingnextlevel.com')
                    Rp. {{ $data->total }}
                @elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringinstantcopywriting.com')
                    Rp. 149.000
                @endif
              </strong>
            </li>
          </ul> --}}
        <div class="card bg-success">
            @include('frontend.auth.title')
            @if ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringinstantcopywriting.com')
                <h2 style="margin-top: -5px; font-weight: 700">Rp. 149.000</h2>
            @elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringsuperreseller.com')
                <h2 style="margin-top: -5px; font-weight: 700">Rp. 93.000</h2>
            @elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.agen-entrepreneurid.com' || $_SERVER['HTTP_HOST'] == 'eid.test')
                <h2 style="margin-top: -5px; font-weight: 700">Rp. 96.000</h2>
            @endif
        </div>
        <div class="card">
            {{-- <span class="text-center" style="font-size: 1.4rem; font-weight: 300; line-height: 1.2;">Daftar Akun</span> --}}

            <span class="text-center text-muted">Form Pemesanan</span>
            <div class="card-body">
                <form action="/simpan" method="POST">
                    @csrf
                    <input value="{{ $ref }}" name="ref" hidden>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="nama" value="{{ old('nama') }}" class="form-control" maxlength="100" placeholder="Nama Lengkap" required="">
                            </div><!--col-->
                        </div><!--row-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input type="text" name="panggilan" value="{{ old('panggilan') }}" class="form-control" maxlength="100" placeholder="Nama Panggilan" required="">
                            </div><!--col-->
                        </div><!--row-->
                    </div><!--row-->

                    <div class="form-group row">
                        <div class="col-md-9 col-form-label">
                            <div class="form-check form-check-inline mr-1">
                                <input class="form-check-input" type="radio" value="Laki-laki" name="jeniskelamin" required>
                                <label class="form-check-label">Laki-laki</label>
                            </div>
                            <div class="form-check form-check-inline mr-1">
                                <input class="form-check-input" type="radio" value="Perempuan" name="jeniskelamin">
                                <label class="form-check-label">Perempuan</label>
                            </div>
                        </div>
                    </div>

                    <div class="row pb-4">
                        <div class="col-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        +62
                                    </span>
                                </div>
                                <input id="nohp" type="number" name="nohp" value="{{ old('nohp') }}" class="form-control" maxlength="12" placeholder="No. Handphone WhatsApp" required="">
                            </div><!--form-group-->
                        </div><!--col-->
                        <div class="col-12">
                            <span class="help-block text-muted" style="font-size: 10px; font-weight: 700">Tidak Pakai Angka 0 . Contoh : 81234563789</span>
                        </div>
                    </div><!--row-->

                    <div class="form-group">
                        <label style="padding-right: 10px">Tanggal Lahir</label>
                        <select name="tgl">
                            @for ($a = 1; $a <= 31; $a++)
                                <option value="{{ $a }}">{{ $a }}</option>
                            @endfor
                        </select>
                        <select name="bln">
                            @for ($a = 1; $a <= 12; $a++)
                                <option value="{{ $a }}">{{ $a }}</option>
                            @endfor
                        </select>
                        <select name="thn">
                            @for ($a = 1950; $a <= 2015; $a++)
                                <option value="{{ $a }}">{{ $a }}</option>
                            @endfor
                        </select>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input onkeyup="this.value = this.value.toLowerCase();" autocomplete="new-password" type="email" name="email" value="{{ old('email') }}" id="email" placeholder="Alamat Email" maxlength="191" required="required" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="kota" value="{{ old('kota') }}" class="form-control" placeholder="Kota Domisili" required="">
                            </div>
                        </div>
                    </div>

                    {{-- <div class="text-center" style="font-size: 1.0rem; font-weight: 500; line-height: 1.2; padding-bottom:5px;">
                        <center>Akun Rekening</center>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                            <input onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="bank" class="form-control" placeholder="Nama Bank" required="">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="norek" class="form-control" placeholder="Nomor Rekening" required="">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input onkeyup="this.value = this.value.toUpperCase();" autocomplete="new-password" type="text" name="namarek" class="form-control" placeholder="Nama Pemilik Rekening" required="">
                            </div>
                        </div>
                    </div> --}}

                    {{-- @if(config('access.captcha.registration'))
                    <div class="row">
                        <div class="col">
                            @captcha
                            {{ html()->hidden('captcha_status', 'true') }}
                        </div><!--col-->
                    </div><!--row-->
                    @endif --}}

                    <div class="row">
                        <div class="col">
                            <div class="form-group mb-0 clearfix">
                                {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(244,67,54"> --}}
                                <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="background-color: #41414b; border-color: #41414b; box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(66, 65, 76)">
                                    DAFTAR
                                </button>
                            </div><!--form-group-->
                        </div><!--col-->
                     </div><!--row-->
                    {{-- <div class="row" style="padding-top: 10px">
                        <div class="col">
                            <div class="text-left">
                                <a href="/login" style="color: rgb(65, 65, 75);">
                                    <i class="fas fa-angle-double-left"></i> Login
                                </a>
                            </div>
                        </div>
                    </div>  --}}
                </form>

            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- col-md-8 -->
</div><!-- row -->

@stack('before-scripts')
    <script>
            $("#nohp").on("input", function() {
                if (/^0/.nohp(this.value)) {
                    this.value = this.value.replace(/^0/, "")
                }
            })
    </script>
@stack('after-scripts')

@endsection

@push('after-scripts')
@if(config('access.captcha.registration'))
@captchaScripts
@endif
@endpush
