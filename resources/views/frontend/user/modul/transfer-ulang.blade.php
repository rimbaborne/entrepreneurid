@extends('frontend.layouts.lock')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

@section('content')
@stack('before-styles')
    <link rel="stylesheet" type="text/css" href="/filepond/app.css">
@stack('after-styles')
<div class="row justify-content-center align-items-center">
    <div class="col col-md-4 align-self-center">
        <div class="card">
            @include('frontend.auth.title')
            <span class="text-center text-muted">Transfer Ulang</span>
            <div class="card-body">
                <form action="">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input onkeyup="this.value = this.value.toLowerCase();" autocomplete="new-password" type="email" name="email" value="{{ request()->get('email') ?? '' }}" id="email" placeholder="Masukkan Alamat Email" maxlength="191" required="required" class="form-control">
                            </div>
                            <div class="form-group">
                                <input onkeyup="this.value = this.value.toLowerCase();" autocomplete="new-password" type="text" name="nohp" value="{{ request()->get('nohp') ?? '' }}" id="email" placeholder="Masukkan No. HP" maxlength="191" required="required" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group mb-0 clearfix">
                                {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(244,67,54"> --}}
                                {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="background-color: #41414b; border-color: #41414b; box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(66, 65, 76)"> --}}
                                <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="background-color: #41414b; border-color: #41414b; ">
                                    Submit <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->
                </form>
            </div><!-- card-body -->
        </div><!-- card -->
        @php
            $email = request()->get('email') ?? NULL;
            $nohp  = request()->get('nohp') ?? NULL;
            $data  = DB::table('biodata')->where('email', $email)->where('idprodukreg', '26')->first();
            if (isset($data)) {
                if ($data->aktif == '0') {
                    $status  = 'BELUM UPLOAD';
                    $wstatus = 'danger';
                } elseif ($data->aktif == '1') {
                    $status  = 'MENUNGGU KONFIRMASI';
                    $wstatus = 'warning';
                } elseif ($data->aktif == '2') {
                    $status  = 'AKTIF';
                    $wstatus = 'info';
                }
            }
        @endphp
        @if (!isset($data))
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="text-center">Email tidak ditemukan</h5>
                        </div><!--col-->
                    </div><!--row-->
                </div>
            </div>
        @elseif ($email != '')
        <div class="card">
            <span class="text-center text-muted">Bukti Transfer</span>
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <input style="font-weight: 700; background-color: #fff;" onkeyup="this.value = this.value.toLowerCase();" autocomplete="new-password" type="email" name="email" value="{{ $email }}" id="email" placeholder="Masukkan Alamat Email" maxlength="191" required="required" class="form-control" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <input style="font-weight: 700; background-color: #fff;" type="text" name="nominal" value="Rp. {{ 55000+$data->kodeunik }}"  maxlength="191" required="required" class="form-control" disabled>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <input style="font-weight: 700; background-color: #fff;" type="text" name="status" value="{{ $status }}" class="form-control border-{{ $wstatus }} text-{{ $wstatus }}" disabled>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                @if (request()->get('status') != 'BERHASIL')
                    <form action="{{ route('frontend.pembayaran.simpan') }}" method="post">
                        @csrf
                        <input type="text" name="id" value="{{ $data->id }}" hidden>
                        <div class="form-group row">
                            <div class="col">
                                <input type="file" class="upload-buktitransfer" required/>
                            </div><!--col-->
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group mb-0 clearfix">
                                    <button type="submit" class="btn btn-pill btn-block btn-info btn-block">
                                        Upload Bukti Transfer <i class="fa fa-upload"></i>
                                    </button>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                    </form>
                @else
                    @php
                        $bukti = DB::table('pemesanan')->where('idcustomer', $data->id)->first();
                    @endphp
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input style="font-weight: 700; background-color: #fff;" type="text" name="status" value="{{ $bukti->tglpesan ?? '' }}" class="form-control" disabled>
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->
                    <img src="public/bukti-transfer-26-MOM/{{ $bukti->buktitf ?? '' }}" class="img-fluid rounded" alt="">
                @endif
            </div><!-- card-body -->
        </div><!-- card -->
        @endif
    </div><!-- col-md-8 -->
</div><!-- row -->
@stack('before-scripts')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script> --}}

<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
<script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.js"></script>
<script src="https://unpkg.com/filepond-plugin-image-resize/dist/filepond-plugin-image-resize.js"></script>
<script src="/filepond/app.js"></script>
<script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>
<script>
    $(function(){
        $.fn.filepond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginFileValidateType,
            FilePondPluginFileValidateSize,
            FilePondPluginImageResize
        );
    });

    $(function(){
            $('.upload-buktitransfer').filepond({
                labelIdle: '<span class="filepond--label-action"> Upload File/Foto Bukti Transfer.</span>',
                allowMultiple: false,
                acceptedFileTypes: "image/png, image/jpeg",
                allowFileSizeValidation: true,
                maxFileSize: '10MB',
                server: {
                    url: '/pembayaran/uploadbuktitransfer',
                    process: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }
                }
            });

        });

    $('#fform').on('submit',function(e) {
		if (pond.status != 4) {
			return false;
		}
		$(this).find(':input[type=submit]').hide();
		return true;
	});

</script>
@endsection


@push('after-scripts')
@if(config('access.captcha.registration'))
@captchaScripts
@endif
@endpush
