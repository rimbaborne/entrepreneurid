@extends('frontend.layouts.lock')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

@section('content')
@stack('before-styles')
    <link rel="stylesheet" type="text/css" href="/filepond/app.css">
@stack('after-styles')
<div class="row justify-content-center align-items-center">
    <div class="col col-md-4 align-self-center">
        <div class="card">
            <div class="text-center">
                @include('frontend.auth.title')
            </div>
            <span class="text-center text-muted">Pembayaran</span>
            <div class="card-body">
                <form action="">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input onkeyup="this.value = this.value.toLowerCase();" autocomplete="new-password" type="email" name="email" value="{{ $email }}" id="email" placeholder="Masukkan Alamat Email" maxlength="191" required="required" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group mb-0 clearfix">
                                {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(244,67,54"> --}}
                                {{-- <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="background-color: #41414b; border-color: #41414b; box-shadow: 0 4px 20px 0 rgba(0,0,0,.14), 0 7px 10px -5px rgba(66, 65, 76)"> --}}
                                <button type="submit" class="btn btn-pill btn-block btn-danger btn-block" style="background-color: #41414b; border-color: #41414b; ">
                                    Submit <i class="fa fa-arrow-circle-right"></i>
                                </button>
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->
                </form>
            </div><!-- card-body -->
        </div><!-- card -->
        @php
            $email = request()->get('email') ?? NULL;
            if ($_SERVER['HTTP_HOST'] == 'app.copywritingnextlevel.com') {
                $data  = DB::table('biodata')->where('email', $email)->where('idprodukreg', '29')->first();
            } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.kelasfbhacking.com') {
                $data  = DB::table('biodata')->where('email', $email)->where('idprodukreg', '34')->first();
            } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringinstantcopywriting.com') {
                $data  = DB::table('biodata')->where('email', $email)->where('idprodukreg', '33')->first();
            } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringsuperreseller.com') {
                $data  = DB::table('biodata')->where('email', $email)->where('idprodukreg', '35')->first();
            } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.agen-entrepreneurid.com') {
                $data  = DB::table('biodata')->where('email', $email)->where('idprodukreg', '39')->first();
            }

            if (isset($data)) {
                if ($data->aktif == '0') {
                    $status  = 'BELUM UPLOAD';
                    $wstatus = 'danger';
                } elseif ($data->aktif == '1') {
                    $status  = 'MENUNGGU KONFIRMASI';
                    $wstatus = 'warning';
                } elseif ($data->aktif == '2') {
                    $status  = 'AKTIF';
                    $wstatus = 'info';
                } elseif ($data->aktif == '5') {
                    $status  = 'UPLOAD ULANG';
                    $wstatus = 'danger';
                }
            }
        @endphp
        @if (!isset($data) && request()->get('email') == !null)
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="text-center">Email tidak ditemukan</h5>
                        </div><!--col-->
                    </div><!--row-->
                </div>
            </div>
        @elseif ($email != '')
        <div class="card">
            <span class="text-center text-muted">Bukti Transfer</span>
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <input style="font-weight: 700; background-color: #fff;" onkeyup="this.value = this.value.toLowerCase();" autocomplete="new-password" type="email" name="email" value="{{ $email }}" id="email" placeholder="Masukkan Alamat Email" maxlength="191" required="required" class="form-control" disabled>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            @if ($_SERVER['HTTP_HOST'] == 'app.copywritingnextlevel.com')
                                <input style="font-weight: 700; background-color: #fff;" type="text" name="nominal" value="Rp. {{ $data->total }}"  maxlength="191" required="required" class="form-control" disabled>
                            @elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.kelasfbhacking.com')
                                <input style="font-weight: 700; background-color: #fff;" type="text" name="nominal" value="Rp. 55.000"  maxlength="191" required="required" class="form-control" disabled>
                            @elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringinstantcopywriting.com')
                                <input style="font-weight: 700; background-color: #fff;" type="text" name="nominal" value="Rp. 149.000"  maxlength="191" required="required" class="form-control" disabled>
                            @elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringsuperreseller.com')
                                <input style="font-weight: 700; background-color: #fff;" type="text" name="nominal" value="Rp. 93.000"  maxlength="191" required="required" class="form-control" disabled>
                            @elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.agen-entrepreneurid.com')
                                <input style="font-weight: 700; background-color: #fff;" type="text" name="nominal" value="Rp. 96.000"  maxlength="191" required="required" class="form-control" disabled>
                            @endif
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <input style="font-weight: 700; background-color: #fff;" type="text" name="status" value="{{ $status }}" class="form-control border-{{ $wstatus }} text-{{ $wstatus }}" disabled>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                @if ($data->aktif == '0' || $data->aktif == '5')
                    <form id="fform" action="/pembayaran/simpan" method="post">
                        @csrf
                        <input type="text" name="id" value="{{ $data->id }}" hidden>
                        <div class="form-group row">
                            <label class="form-label col-12">Rekening Tujuan Yang Ditransfer</label>
                            <div class="col-12">
                                <select class="form-control" name="rekening" required>
                                    <option value="Mandiri = 1360007600528">Mandiri = 1360007600528 a/n Wulandari Tri</option>
                                    <option value="BCA = 1911488766">BCA = 1911488766 a/n Wulandari Tri</option>
                                    <option value="BRI = 012101009502532">BRI = 012101009502532 a/n Wulandari Tri</option>
                                    <option value="BNI = 1238568920">BNI = 1238568920 a/n Wulandari Tri</option>
                                </select>
                            </div><!--col-->
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <input type="file" class="upload-buktitransfer" required/>
                            </div><!--col-->
                        </div>
                        <div class="row">
                            <div class="col" style="">
                                <span class="help-block text-muted" style="font-size: 10px; font-weight: 700; ">Maksimal File 10 MB</span>
                                <div class="form-group mb-0 clearfix d-none" id="btn-submit">
                                    <button type="submit" class="btn btn-pill btn-block btn-info btn-block">
                                        Upload Bukti Transfer <i class="fa fa-upload"></i>
                                    </button>
                                </div><!--form-group-->
                            </div><!--col-->
                        </div><!--row-->
                    </form>
                @else
                    @php
                        $bukti = DB::table('pemesanan')->where('idcustomer', $data->id)->latest('tglpesan')->first();
                    @endphp
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <input style="font-weight: 700; background-color: #fff;" type="text" name="status" value="{{ $bukti->tglpesan ?? '' }}" class="form-control" disabled>
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->
                    @if ($_SERVER['HTTP_HOST'] == 'app.copywritingnextlevel.com')
                        <img src="https://app.copywritingnextlevel.com/app/public/bukti-transfer-29-CNL/{{ $bukti->buktitf ?? '' }}" class="img-fluid rounded" alt="">
                    @elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.kelasfbhacking.com')
                        <img src="https://konfirmasi.kelasfbhacking.com/public/bukti-transfer-34-KFH/{{ $bukti->buktitf ?? '' }}" class="img-fluid rounded" alt="">
                    @elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringinstantcopywriting.com')
                        <img src="public/bukti-transfer-33-MIC/{{ $bukti->buktitf ?? '' }}" class="img-fluid rounded" alt="">
                    @elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringsuperreseller.com')
                        <img src="public/bukti-transfer-35-MSR/{{ $bukti->buktitf ?? '' }}" class="img-fluid rounded" alt="">
                    @elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.agen-entrepreneurid.com')
                        <img src="public/bukti-transfer-39-AGEN-BARU/{{ $bukti->buktitf ?? '' }}" class="img-fluid rounded" alt="">
                    @endif
                @endif
            </div><!-- card-body -->
        </div><!-- card -->
        @endif
    </div><!-- col-md-8 -->
</div><!-- row -->
@stack('before-scripts')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script> --}}

<script src="https://unpkg.com/filepond-plugin-file-validate-type/dist/filepond-plugin-file-validate-type.js"></script>
<script src="https://unpkg.com/filepond-plugin-file-validate-size/dist/filepond-plugin-file-validate-size.js"></script>
<script src="https://unpkg.com/filepond-plugin-image-resize/dist/filepond-plugin-image-resize.js"></script>
<script src="/filepond/app.js"></script>
<script src="https://unpkg.com/jquery-filepond/filepond.jquery.js"></script>
<script>
    $(function(){
        $.fn.filepond.registerPlugin(
            FilePondPluginImagePreview,
            FilePondPluginFileValidateType,
            FilePondPluginFileValidateSize,
            FilePondPluginImageResize
        );
    });

    $(function(){
            $('.upload-buktitransfer').filepond({
                labelIdle: '<span class="filepond--label-action"> Upload File/Foto Bukti Transfer.</span>',
                allowMultiple: false,
                acceptedFileTypes: "image/png, image/jpeg",
                allowFileSizeValidation: true,
                maxFileSize: '10MB',
                server: {
                    url: '/pembayaran/uploadbuktitransfer',
                    process: {
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        onload: (response) => response.key,
                        onerror: (response) => response.data,
                        ondata: (formData) => {
                            return console.log('sukses');
                        }
                    }
                }
            });

            $('.upload-buktitransfer').on('FilePond:processfile', function(e) {
                document.getElementById("btn-submit").classList.remove('d-none');
            });

        });
    $(document).ready(function () {
        $('#fform').on('submit',function(e) {
            if (pond.status != 4) {
                return false;
            }
            $(this).find(':input[type=submit]').hide();
            return true;
        });
    });

</script>
@endsection


@push('after-scripts')
@if(config('access.captcha.registration'))
@captchaScripts
@endif
@endpush
