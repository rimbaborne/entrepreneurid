<div style="padding-bottom: 25px">
    <div id="carouselExampleControls"  class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="/img/slide/0.png" alt="Agen Eid">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="/img/slide/1.png" alt="WMC">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="/img/slide/2.png" alt="FH">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<div class="row">
    <div class="col-4">
        <div class="card order-card" style="background: linear-gradient(45deg, #4099ff, #73b4ff); padding: 15px; color: #fff">
            <div class="card-block">
                <div class="d-none d-lg-block">
                    <h6>Total Pemesanan</h6>
                    <h2 class="text-right"><i class="fas fa-chart-pie" style="float:left"></i><span>486</span></h2>
                    <p class="mb-0">Closing<span style="float:right">351</span></p>
                </div>
                <div class="d-lg-none">
                    <h2 style="position: absolute;">
                        <i class="fas fa-chart-pie" style="position: relative; margin-top: 40px; opacity: 0.6;"></i>
                    </h2>
                    <h6>Closing</h6>
                    <h2 class="text-right"><span>351</span></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="card order-card" style="background: linear-gradient(45deg, #2ed8b6, #59e0c5); padding: 15px; color: #fff">
            <div class="d-none d-lg-block">
                <h6>Total Kunjungan Link</h6>
                <h2 class="text-right"><i class="fas fa-layer-group" style="float:left"></i><span>1641</span></h2>
                <p class="mb-0">Bulan Ini<span style="float:right">213</span></p>
            </div>
            <div class="d-lg-none">
                <h2 style="position: absolute;">
                    <i class="fas fa-layer-group" style="position: relative; margin-top: 40px; opacity: 0.6;"></i>
                </h2>
                <h6>Kunjungan Link</>
                    <h2 class="text-right"><span>1.641</span></h2>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card order-card" style="background: linear-gradient(45deg, #FF5370, #ff869a); padding: 15px; color: #fff">
                <div class="d-none d-lg-block">
                    <h6>Komisi Periode Ini</h6>
                    <h2 class="text-right"><i class="far fa-credit-card" style="float:left"></i><span style="font-size: 20px">Rp. 9,562,000</span></h2>
                    <p class="mb-0">Total Komisi<span style="float:right">Rp. 46,542,000</span></p>
                </div>
                <div class="d-lg-none">
                    <h6>Total Komisi</h6>
                    <h2 class="text-right"><span style="font-size: 14px; padding-bottom: 8px">Rp. 46,542,000</span></h2>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-block"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
            </div>
        </div>
    </div>
</div>
