<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>entrepreneurID</title>
  <style type="text/css" media="screen">

    /* Force Hotmail to display emails at full width */
    .ExternalClass {
      display: block !important;
      width: 100%;
    }

    /* Force Hotmail to display normal line spacing */
    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }

    body,
    p,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      margin: 0;
      padding: 0;
    }

    body,
    p,
    td {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 15px;
      color: #333333;
      line-height: 1.5em;
    }

    h1 {
      font-size: 24px;
      font-weight: normal;
      line-height: 24px;
    }

    body,
    p {
      margin-bottom: 0;
      -webkit-text-size-adjust: none;
      -ms-text-size-adjust: none;
    }

    img {
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }

    a img {
      border: none;
    }

    .background {
      background-color: #eef1f4;
    }

    table.background {
      margin: 0;
      padding: 0;
      width: 100% !important;
    }

    .block-img {
      display: block;
      line-height: 0;
    }

    a {
      color: white;
      text-decoration: none;
    }

    a,
    a:link {
      color: #2A5DB0;
      text-decoration: underline;
    }

    table td {
      border-collapse: collapse;
    }

    td {
      vertical-align: top;
      text-align: left;
    }

    .wrap {
      width: 600px;
    }

    .wrap-cell {
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .header-cell,
    .body-cell,
    .footer-cell {
      padding-left: 20px;
      padding-right: 20px;
    }

    .header-cell {
      background-color: #aaaaaa;
      font-size: 24px;
      color: #ffffff;
    }

    .body-cell {
      background-color: #ffffff;
      padding-top: 30px;
      padding-bottom: 34px;
    }

    .footer-cell {
      background-color: #eeeeee;
      text-align: center;
      font-size: 13px;
      padding-top: 30px;
      padding-bottom: 30px;
    }

    .card {
      width: 400px;
      margin: 0 auto;
    }

    .data-heading {
      text-align: right;
      padding: 10px;
      background-color: #ffffff;
      font-weight: bold;
    }

    .data-value {
      text-align: left;
      padding: 10px;
      background-color: #ffffff;
    }

    .force-full-width {
      width: 100% !important;
    }

  </style>
  <style type="text/css" media="only screen and (max-width: 600px)">
    @media only screen and (max-width: 600px) {
      body[class*="background"],
      table[class*="background"],
      td[class*="background"] {
        background: #eeeeee !important;
      }

      table[class="card"] {
        width: auto !important;
      }

      td[class="data-heading"],
      td[class="data-value"] {
        display: block !important;
      }

      td[class="data-heading"] {
        text-align: left !important;
        padding: 10px 10px 0;
      }

      table[class="wrap"] {
        width: 100% !important;
      }

      td[class="wrap-cell"] {
        padding-top: 0 !important;
        padding-bottom: 0 !important;
      }
    }
  </style>
</head>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" bgcolor="" class="background">
<table class="background" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td class="background" align="center" valign="top" width="100%"><center>
<table class="wrap" width="600" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="wrap-cell" style="padding-top: 30px; padding-bottom: 30px;" valign="top">
<table class="force-full-width" style="height: 333px;" cellspacing="0" cellpadding="0">
<tbody>
<tr style="height: 80px;">
<td class="header-cell" style="height: 80px; width: 560px;" valign="top" height="60">
<p>&nbsp;</p>
<p><img src="http://admin.entrepreneurid.org/img/emblem-sm.png" alt="logo" width="150" height="30" /></p>
<p>&nbsp;</p>
</td>
</tr>
<tr style="height: 234px;">
<td class="body-cell" style="height: 234px; width: 560px;" valign="top"><br />
<table style="height: 594px; width: 100%;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
<tbody>
<tr style="height: 528px;">
<td style="padding-bottom: 20px; background-color: #ffffff; width: 560px; height: 528px;" valign="top">
<p>Dear, {{ $nama }}<br /><br />Selamat karena telah menyelesaikan proses pendaftaran Ecourse {{ $produk }}.</p>
<p>Karena Anda mendaftar di minggu ini, Maka Anda akan mengikuti rangkaian program belajar mulai Hari Senin minggu depan, pukul 20.00 WIB.<br /><br />Sambil menunggu program belajar dimulai, Anda bisa registrasi terlebih dahulu member area yang merupakan salah satu fasilitas ecourse {{ $produk }} ini.<br />Silahkan registrasi member area melalui link&nbsp;<br /><br />Mohon registrasi menggunakan email yang Anda gunakan untuk pendaftaran ecourse ini.<br />Member area Anda akan bisa diakses bersamaan dengan mulainya program belajar.<br /><br />Jika pada waktu program belajar dimulai Anda tidak menerima email dari Tim entrepreneurID, maka silahkan hubungi CS kami di WhatsApp 0857-8757-2580 atau langsung klik <a href="bit.ly/CS-eID">bit.ly/CS-eID</a></p>
<p>Sekali lagi selamat karena telah menjadi peserta ICO<br />Sampai jumpa di materi.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Salam,<br />Tim entrepreneurID</p>
</td>
</tr>
<tr style="height: 22px;">
<td style="width: 560px; height: 22px;">&nbsp;</td>
</tr>
<tr style="height: 22px;">
<td style="padding-top: 20px; background-color: #ffffff; width: 560px; height: 22px;">&nbsp;</td>
</tr>
<tr style="height: 22px;">
<td style="padding-top: 20px; background-color: #ffffff; width: 560px; height: 22px;">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style="height: 19px;">
<td class="footer-cell" style="height: 19px; width: 560px;" valign="top"><span style="font-size: 15px;">entrepreneurID &copy; 2021</span></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</center></td>
</tr>
</tbody>
</table>
</body>
</html>
