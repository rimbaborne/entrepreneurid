<?php

return [
    'table' => [
        'nama'    => 'nama',
        'created'       => 'Created',
        'actions'       => 'Actions',
        'last_updated'  => 'Updated',
        'total'         => 'Total|Totals',
        'deleted'       => 'Deleted',
    ],

    'alerts' => [
        'created' => 'New Biodatum created',
        'updated' => 'Biodatum updated',
        'deleted' => 'Biodatum was deleted',
        'deleted_permanently' => 'Biodatum was permanently deleted',
        'restored'  => 'Biodatum was restored',
    ],

    'labels'    => [
        'management'    => 'Data Ecourse',
        'active'        => 'Active',
        'create'        => 'Create',
        'edit'          => 'Edit',
        'view'          => 'View',
        'nama'    => 'nama',
        'created_at'    => 'Created at',
        'last_updated'  => 'Updated at',
        'deleted'       => 'Deleted',
    ],

    'validation' => [
        'attributes' => [
            'nama' => 'nama',
        ]
    ],

    'sidebar' => [
        'title'  => 'Title',
    ],

    'tabs' => [
        'nama'    => 'nama',
        'content'   => [
            'overview' => [
                'nama'    => 'nama',
                'created_at'    => 'Created',
                'last_updated'  => 'Updated'
            ],
        ],
    ],

    'menus' => [
      'main' => 'Biodatum',
      'all' => 'All',
      'create' => 'Create',
      'deleted' => 'Deleted'
    ]
];