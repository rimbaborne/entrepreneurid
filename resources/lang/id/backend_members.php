<?php

return [
    'table' => [
        'produk'    => 'produk',
        'created'       => 'Created',
        'actions'       => 'Actions',
        'last_updated'  => 'Updated',
        'total'         => 'Total|Totals',
        'deleted'       => 'Deleted',
    ],

    'alerts' => [
        'created' => 'New Member created',
        'updated' => 'Member updated',
        'deleted' => 'Member was deleted',
        'deleted_permanently' => 'Member was permanently deleted',
        'restored'  => 'Member was restored',
    ],

    'labels'    => [
        'management'    => 'Member',
        'active'        => 'Active',
        'create'        => 'Create',
        'edit'          => 'Edit',
        'view'          => 'View',
        'produk'    => 'produk',
        'created_at'    => 'Created at',
        'last_updated'  => 'Updated at',
        'deleted'       => 'Deleted',
    ],

    'validation' => [
        'attributes' => [
            'produk' => 'produk',
        ]
    ],

    'sidebar' => [
        'title'  => 'Title',
    ],

    'tabs' => [
        'produk'    => 'produk',
        'content'   => [
            'overview' => [
                'produk'    => 'produk',
                'created_at'    => 'Created',
                'last_updated'  => 'Updated'
            ],
        ],
    ],

    'menus' => [
      'main' => 'Member',
      'all' => 'All',
      'create' => 'Create',
      'deleted' => 'Deleted'
    ]
];