<?php

return [
    'table' => [
        'nama'    => 'nama',
        'created'       => 'Created',
        'actions'       => 'Actions',
        'last_updated'  => 'Updated',
        'total'         => 'Total|Totals',
        'deleted'       => 'Deleted',
    ],

    'alerts' => [
        'created' => 'New Poster created',
        'updated' => 'Poster updated',
        'deleted' => 'Poster was deleted',
        'deleted_permanently' => 'Poster was permanently deleted',
        'restored'  => 'Poster was restored',
    ],

    'labels'    => [
        'management'    => 'Management of Poster',
        'active'        => 'Active',
        'create'        => 'Create',
        'edit'          => 'Edit',
        'view'          => 'View',
        'nama'    => 'nama',
        'created_at'    => 'Created at',
        'last_updated'  => 'Updated at',
        'deleted'       => 'Deleted',
    ],

    'validation' => [
        'attributes' => [
            'nama' => 'nama',
        ]
    ],

    'sidebar' => [
        'title'  => 'Title',
    ],

    'tabs' => [
        'nama'    => 'nama',
        'content'   => [
            'overview' => [
                'nama'    => 'nama',
                'created_at'    => 'Created',
                'last_updated'  => 'Updated'
            ],
        ],
    ],

    'menus' => [
      'main' => 'Poster',
      'all' => 'All',
      'create' => 'Create',
      'deleted' => 'Deleted'
    ]
];
