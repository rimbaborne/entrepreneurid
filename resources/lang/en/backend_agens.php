<?php

return [
    'table' => [
        'id_agen'    => 'id_agen',
        'created'       => 'Created',
        'actions'       => 'Actions',
        'last_updated'  => 'Updated',
        'total'         => 'Total|Totals',
        'deleted'       => 'Deleted',
    ],

    'alerts' => [
        'created' => 'New Agen created',
        'updated' => 'Agen updated',
        'deleted' => 'Agen was deleted',
        'deleted_permanently' => 'Agen was permanently deleted',
        'restored'  => 'Agen was restored',
    ],

    'labels'    => [
        'management'    => 'Management of Agen',
        'active'        => 'Active',
        'create'        => 'Create',
        'edit'          => 'Edit',
        'view'          => 'View',
        'id_agen'    => 'id_agen',
        'created_at'    => 'Created at',
        'last_updated'  => 'Updated at',
        'deleted'       => 'Deleted',
    ],

    'validation' => [
        'attributes' => [
            'id_agen' => 'id_agen',
        ]
    ],

    'sidebar' => [
        'title'  => 'Title',
    ],

    'tabs' => [
        'id_agen'    => 'id_agen',
        'content'   => [
            'overview' => [
                'id_agen'    => 'id_agen',
                'created_at'    => 'Created',
                'last_updated'  => 'Updated'
            ],
        ],
    ],

    'menus' => [
      'main' => 'Agen',
      'all' => 'All',
      'create' => 'Create',
      'deleted' => 'Deleted'
    ]
];
