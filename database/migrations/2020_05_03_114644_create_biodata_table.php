<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiodataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('biodata')) {
            Schema::create('biodata', function (Blueprint $table) {
                $table->increments('id');
                $table->string('idagenbaru', 180)->nullable();
                $table->string('nama', 180)->nullable();
                $table->string('email', 180)->nullable();
                $table->string('passwordd')->nullable();
                $table->string('no_telp', 180)->nullable();
                $table->string('telegram', 180)->nullable();
                $table->string('tgl_daftar', 180)->nullable();
                $table->string('alamat', 180)->nullable();
                $table->string('kota', 180)->nullable();
                $table->string('status_user', 180)->nullable();
                $table->string('ref', 180)->nullable();
                $table->string('aktif', 180)->nullable();
                $table->string('closing', 180)->nullable();
                $table->string('panggilan', 180)->nullable();
                $table->string('username', 180)->nullable();
                $table->string('tgllahir', 180)->nullable();
                $table->string('gender', 180)->nullable();
                $table->string('nama_rek', 180)->nullable();
                $table->string('rek_rek', 180)->nullable();
                $table->string('bank_rek', 180)->nullable();
                $table->string('idprodukreg', 180)->nullable();
                $table->string('provinsi', 180)->nullable();
                $table->string('kecamatan', 180)->nullable();
                $table->string('kodepos', 180)->nullable();
                $table->string('noresi', 180)->nullable();
                $table->string('berat', 180)->nullable();
                $table->string('jumlah', 180)->nullable();
                $table->string('kurir', 180)->nullable();
                $table->string('harga', 180)->nullable();
                $table->string('ongkir', 180)->nullable();
                $table->string('total', 180)->nullable();
                $table->string('rek_bayar', 180)->nullable();
                $table->string('manual', 180)->nullable();
                $table->string('tolaktf', 5)->nullable();
                $table->string('waktutolaktf', 180)->nullable();
                $table->string('waktukonfirmasi', 180)->nullable();
                $table->string('kodeunik', 180)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biodata');
    }
}
