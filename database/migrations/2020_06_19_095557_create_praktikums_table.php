<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePraktikumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('praktikums', function (Blueprint $table) {
            $table->increments('id');
            $table->text('nama_praktikum', 180);
            $table->text('judul_praktikum', 180)->nullable();
            $table->text('deskripsi_praktikum', 180)->nullable();
            $table->text('produk_praktikum', 180);
            $table->text('status_praktikum', 80);
            $table->date('waktu_awal_praktikum');
            $table->date('waktu_akhir_praktikum');
            $table->text('periode_praktikum', 180);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('praktikums');
    }
}