<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('agens')) {
            Schema::create('agens', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_agen', 80);
                // $table->string('idagenbaru', 180);
                $table->string('nama', 180);
                $table->string('email', 180);
                $table->string('passwordd', 180);
                $table->string('kode_notelp', 20);
                $table->string('notelp', 20);
                $table->string('telegram', 180);
                $table->string('tgl_daftar', 180);
                $table->string('alamat', 180);
                $table->string('kota', 180);
                // $table->string('status_user', 20);
                // $table->string('ref', 180);
                $table->string('aktif', 180);
                $table->string('closing', 180);
                $table->string('panggilan', 180);
                $table->string('username', 180);
                $table->string('tgllahir', 180);
                $table->string('gender', 180);
                $table->string('nama_rek', 180);
                $table->string('rek_rek', 180);
                $table->string('bank_rek', 180);
                // $table->string('idprodukreg', 180);
                $table->string('provinsi', 180);
                $table->string('kecamatan', 180);
                $table->string('kodepos', 180);
                // $table->string('noresi', 180);
                // $table->string('manual', 180);
                // $table->string('waktukonfirmasi', 180);
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agens');
    }
}
