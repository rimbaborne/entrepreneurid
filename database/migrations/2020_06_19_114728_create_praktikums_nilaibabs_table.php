<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePraktikumsNilaibabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('praktikums_nilaibabs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('id_praktikum_bab', 10);
            $table->text('user_praktikum_nilai', 20);
            $table->text('deskripsi_praktikum_nilai')->nullable();
            $table->text('nilai_praktikum_nilai', 80)->nullable();
            $table->text('status_praktikum_nilai', 80)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('praktikums_nilaibabs');
    }
}
