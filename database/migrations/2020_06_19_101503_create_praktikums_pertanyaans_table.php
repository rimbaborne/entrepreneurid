<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePraktikumsPertanyaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('praktikums_pertanyaans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('id_praktikum_bab', 10);
            $table->text('deskripsi_praktikum_pertanyaan', 180);
            $table->text('gambar_praktikum_pertanyaan')->nullable();
            $table->text('jenis_praktikum_pertanyaan', 80)->nullable();
            $table->text('poin_praktikum_pertanyaan', 80)->nullable();
            $table->text('status_praktikum_pertanyaan', 80)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('praktikum_pertanyaan');
    }
}