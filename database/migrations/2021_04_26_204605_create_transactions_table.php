<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->integer('id_event');
            $table->integer('id_agen');
            $table->string('email');
            $table->string('nama');
            $table->string('nohp');
            $table->string('panggilan')->nullable();
            $table->string('tgllahir')->nullable();
            $table->string('gender')->nullable();
            $table->string('domisili')->nullable();
            $table->integer('konfirmasi')->default(0); // JUMLAH KIRIM NOTIF KONFIRMASI
            $table->integer('tolaktf')->default(0); // JUMLAH KIRIM NOTIF MENOLAK BUKTI TF
            $table->integer('status')->default(0); // 1 BELUM AKTIF - 2 MENUNGGU KONFIRMASI - 3 AKTIF
            $table->integer('jenis'); // 1 MANUAL - 2 AFILIASI
            $table->string('bukti_tf')->nullable();
            $table->string('waktu_upload')->nullable();
            $table->string('waktu_konfirmasi')->nullable();
            $table->string('waktu_tolaktf')->nullable();
            $table->integer('total')->default(0);
            $table->integer('komisi')->default(0);
            $table->integer('kodeunik')->default(0);
            $table->text('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
