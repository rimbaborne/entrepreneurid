<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('kode_notelp')->nullable();
            $table->string('notelp')->unique();
            $table->text('tgl_lahir', 180)->nullable();
            $table->text('alamat', 180)->nullable();
            $table->text('kota', 180)->nullable();
            $table->text('gender', 180)->nullable();
            $table->text('nama_rek', 180)->nullable();
            $table->text('no_rek', 180)->nullable();
            $table->text('bank_rek', 180)->nullable();
            $table->text('status_agen', 180)->nullable();
            $table->string('sub_agen_id', 180)->nullable(); //Backup id - ALTER TABLE `agens` ADD `sub_agen_id` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL AFTER `status_agen`;
            $table->text('status_aktif', 180)->nullable();
            $table->text('foto')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agens');
    }
}
