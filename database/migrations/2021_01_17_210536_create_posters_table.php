<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('posters')) {
            Schema::create('posters', function (Blueprint $table) {
                $table->increments('id');
                $table->text('nama', 100)->nullable();
                $table->text('link')->nullable();
                $table->text('file');
                $table->text('kategori', 100)->nullable();
                $table->text('jenis', 100)->nullable();
                $table->json('data')->nullable();
                $table->text('deskripsi')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posters');
    }
}
