<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_event');
            $table->string('name_email')->nullable();
            $table->string('from_email')->nullable();
            $table->string('subject_email')->nullable();
            $table->text('email_pendaftaran')->nullable();
            $table->text('email_upload_pembayaran')->nullable();
            $table->text('email_konfirmasi_pembayaran')->nullable();
            $table->text('email_upload_ulang')->nullable();
            $table->text('wa_pendaftaran')->nullable();
            $table->text('wa_konfirmasi_pembayaran')->nullable();
            $table->text('wa_belum_upload')->nullable();
            $table->text('wa_upload_ulang')->nullable();
            $table->string('status')->nullable();
            $table->text('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
