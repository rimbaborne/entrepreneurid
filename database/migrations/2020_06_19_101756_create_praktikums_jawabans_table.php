<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePraktikumsJawabansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('praktikums_jawabans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('id_praktikum_pertanyaan', 10);
            $table->text('user_praktikum_jawaban', 20);
            $table->text('deskripsi_praktikum_jawaban');
            $table->text('jenis_praktikum_jawaban', 80)->nullable();
            $table->text('nilai_praktikum_jawaban', 80)->nullable();
            $table->text('status_praktikum_jawaban', 80)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('praktikums_jawabans');
    }
}