<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->integer('id_transaction');
            $table->string('alamat')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kota')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('negara')->nullable();
            $table->string('kodepos')->nullable();
            $table->string('alamat_pengiriman')->nullable();
            $table->string('kota_pengiriman')->nullable();
            $table->string('kurir')->nullable();
            $table->string('harga')->nullable();
            $table->string('ongkir')->nullable();
            $table->string('jumlah_buku')->nullable();
            $table->string('waktu_pengiriman')->nullable();
            $table->string('total')->nullable();
            $table->string('no_resi')->nullable();
            $table->string('tanggal_pengiriman')->nullable();
            $table->text('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_transactions');
    }
}
