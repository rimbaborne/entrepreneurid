<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid');
            $table->integer('produk_id');
            $table->string('id_event');
            $table->boolean('open');
            $table->boolean('publish');
            $table->string('harga');
            $table->string('komisi');
            $table->boolean('open_jv');
            $table->boolean('publish_jv');
            $table->string('komisi_jv');
            $table->string('gambar')->nullable();
            $table->string('link_sales')->nullable();
            $table->string('link_form')->nullable();
            $table->string('kode_akses', 6)->nullable();
            $table->integer('leaderboard')->default(0);
            $table->timestampTz('start', $precision = 0)->nullable();
            $table->timestampTz('end', $precision = 0)->nullable();
            $table->text('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
