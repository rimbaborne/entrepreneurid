<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePraktikumsBabsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('praktikums_babs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('id_praktikum', 20);
            $table->text('nama_praktikum_bab', 180);
            $table->text('deskripsi_praktikum_bab')->nullable();
            $table->text('gambar_praktikum_bab')->nullable();
            $table->text('status_praktikum_bab', 80)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('praktikums_babs');
    }
}