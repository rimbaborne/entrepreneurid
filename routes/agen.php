<?php
use App\Http\Controllers\Agen\ProdukController;
use App\Http\Controllers\Agen\ProfileController;

// Route::group(['namespace' => 'agen'], function() {

//     Route::get('/', 'HomeController@index')->name('agen.dashboard');

//     // Login
//     Route::get('login', 'Auth\LoginController@showLoginForm')->name('agen.login');
//     Route::post('login', 'Auth\LoginController@login');
//     Route::post('logout', 'Auth\LoginController@logout')->name('agen.logout');

//     // Register
//     Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('agen.register');
//     Route::post('register', 'Auth\RegisterController@register');

//     // Passwords
//     Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('agen.password.email');
//     Route::post('password/reset', 'Auth\ResetPasswordController@reset');
//     Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('agen.password.request');
//     Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('agen.password.reset');

//     // Verify
//     // Route::get('email/resend', 'Auth\VerificationController@resend')->name('agen.verification.resend');
//     // Route::get('email/verify', 'Auth\VerificationController@show')->name('agen.verification.notice');
//     // Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('agen.verification.verify');

// });

 // dashboard agen
//  Route::get('/', 'Agen\HomeController@index')->name('agen.dashboard');

//  Route::get('/profile', 'Agen\HomeController@profile')->name('agen.page.profile');
//  Route::post('/profile/update', 'Agen\HomeController@updateprofile')->name('agen.page.profile.update');
//  Route::get('/profile/password', 'Agen\HomeController@password')->name('agen.page.profile.password');
//  Route::post('/profile/password/update', 'Agen\HomeController@updatepassword')->name('agen.page.profile.password.update');
//  Route::get('/amunisi', 'Agen\HomeController@amunisi')->name('agen.page.amunisi');
//  Route::get('/menu-agen', 'Agen\HomeController@menuagen')->name('agen.page.menuagen');
// //  Route::get('/produk', 'Agen\HomeController@produk')->name('agen.page.produk');
//  Route::get('/informasi', 'Agen\HomeController@informasi')->name('agen.page.informasi');

//  // Login
//  Route::get('login', 'Agen\Auth\LoginController@showLoginForm')->name('agen.login');
//  Route::post('login', 'Agen\Auth\LoginController@login');
//  Route::post('logout', 'Agen\Auth\LoginController@logout')->name('agen.logout');

//  // Register
//  Route::get('register', 'Agen\Auth\RegisterController@showRegistrationForm')->name('agen.register');
//  Route::post('register', 'Agen\Auth\RegisterController@register');

//  // Passwords
//  Route::post('password/email', 'Agen\Auth\ForgotPasswordController@sendResetLinkEmail')->name('agen.password.email');
//  Route::post('password/reset', 'Agen\Auth\ResetPasswordController@reset');
//  Route::get('password/reset', 'Agen\Auth\ForgotPasswordController@showLinkRequestForm')->name('agen.password.request');
//  Route::get('password/reset/{token}', 'Agen\Auth\ResetPasswordController@showResetForm')->name('agen.password.reset');




$agenRoutes = function() {


    // Login
    Route::get('login', 'Agen\Auth\LoginController@showLoginForm')->name('agen.login');
    Route::post('login', 'Agen\Auth\LoginController@login');
    Route::post('logout', 'Agen\Auth\LoginController@logout')->name('agen.logout');

    // Register -> Migrasi Agen dari Dashboard Lama
    Route::get('register', 'Agen\Auth\RegisterController@showRegistrationForm')->name('agen.register');
    Route::post('register', 'Agen\Auth\RegisterController@register');

    // Daftar Akun Agen Baru ID-39
    Route::get('daftar', 'Agen\Auth\RegisterController@daftar')->name('agen.daftar');
    Route::get('daftar-sub', 'Agen\Auth\RegisterController@daftarsub')->name('agen.daftarsub');

    // Passwords
    Route::post('password/email', 'Agen\Auth\ForgotPasswordController@sendResetLinkEmail')->name('agen.password.email');
    Route::post('password/reset', 'Agen\Auth\ResetPasswordController@reset');
    Route::get('password/reset', 'Agen\Auth\ForgotPasswordController@showLinkRequestForm')->name('agen.password.request');
    Route::get('password/reset/{token}', 'Agen\Auth\ResetPasswordController@showResetForm')->name('agen.password.reset');

    // Verify
    Route::get('email/resend', 'Auth\VerificationController@resend')->name('agen.verification.resend');
    Route::get('email/verify', 'Auth\VerificationController@show')->name('agen.verification.notice');
    Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('agen.verification.verify');

    // Prepare
    Route::get('/', 'Agen\HomeController@index')->name('agen.dashboard');
    Route::get('/amunisi', 'Agen\HomeController@amunisi')->name('agen.page.amunisi');
    Route::get('/menu-agen', 'Agen\HomeController@menuagen')->name('agen.page.menuagen');
    Route::get('/informasi', 'Agen\HomeController@informasi')->name('agen.page.informasi');
    Route::get('/leaderboard', 'Agen\HomeController@leaderboard')->name('agen.page.leaderboard');
    Route::get('/sub-agen', 'Agen\HomeController@subagen')->name('agen.page.subagen');

    // Produk
    Route::get(	'/produk', 		                                        [ProdukController::class, 'index']		        )->name('agen.page.produk.index');
    Route::get(	'/produk/lead-magnet', 		                            [ProdukController::class, 'leadmagnet']		    )->name('agen.page.produk.leadmagnet');
    Route::get(	'/produk/pesan', 		                                [ProdukController::class, 'pesan']		        )->name('agen.page.produk.pesan');
    Route::post('/produk/pesan/simpan',                                 [ProdukController::class, 'pesansimpan']		)->name('agen.page.produk.pesansimpan');
    Route::get(	'/transaksi', 		                                    [ProdukController::class, 'transaksi']		    )->name('agen.page.produk.transaksi');
    Route::get(	'/transaksi/pembayaran', 	                            [ProdukController::class, 'pembayaran']		    )->name('agen.page.produk.pembayaran');
    Route::post('/transaksi/pembayaran/uploadbuktitransfer', 	        [ProdukController::class, 'pembayaranupload']   )->name('agen.page.produk.pembayaranupload');
    Route::post('/transaksi/pembayaran/uploadbuktitransfer-afiliasi', 	[ProdukController::class, 'pembayaranuploadafiliasi']   )->name('agen.page.produk.pembayaranuploadafiliasi');
    Route::post('/transaksi/pembayaran/simpan', 	                    [ProdukController::class, 'pembayaransimpan']   )->name('agen.page.produk.pembayaransimpan');
    Route::get(	'/transaksi-lama', 		                                [ProdukController::class, 'transaksilama']		)->name('agen.page.produk.transaksilama');
    Route::get(	'/transaksi-export', 		                            [ProdukController::class, 'transaksiexport']	)->name('agen.page.produk.transaksiexport');
    Route::get(	'/lead-magnet-export', 		                            [ProdukController::class, 'leadmagnetexport']	)->name('agen.page.produk.leadmagnetexport');
    Route::get(	'/cek-ongkir', 		                                    [ProdukController::class, 'cekongkir']	)->name('agen.page.produk.cekongkir');


    //Profile
    Route::get('/profile',                  [ProfileController::class, 'index'])->name('agen.page.profile');
    Route::get('/profile/edit-foto',        [ProfileController::class, 'editfoto'])->name('agen.page.profile.editfoto');
    Route::post('/profile/edit-foto/update',[ProfileController::class, 'updateeditfoto'])->name('agen.page.profile.editfoto.update');
    Route::post('/profile/update',          [ProfileController::class, 'updateprofile'])->name('agen.page.profile.update');
    Route::get('/profile/password',         [ProfileController::class, 'password'])->name('agen.page.profile.password');
    Route::post('/profile/password/update', [ProfileController::class, 'updatepassword'])->name('agen.page.profile.password.update');
};
