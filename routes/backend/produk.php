<?php

use App\Http\Controllers\Backend\ProdukController;

use App\Models\Produk;

Route::bind('produk', function ($value) {
	$produk = new Produk;

	return Produk::withTrashed()->where($produk->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'produks'], function () {
	Route::get(	'', 		[ProdukController::class, 'index']		)->name('produks.index');
    Route::get(	'create', 	[ProdukController::class, 'create']	)->name('produks.create');
	Route::post('store', 	[ProdukController::class, 'store']		)->name('produks.store');
    Route::get(	'deleted', 	[ProdukController::class, 'deleted']	)->name('produks.deleted');
});

Route::group(['prefix' => 'produks/{produk}'], function () {
	// Produk
	Route::get('/', [ProdukController::class, 'show'])->name('produks.show');
	Route::get('edit', [ProdukController::class, 'edit'])->name('produks.edit');
	Route::patch('update', [ProdukController::class, 'update'])->name('produks.update');
	Route::delete('destroy', [ProdukController::class, 'destroy'])->name('produks.destroy');
	// Deleted
	Route::get('restore', [ProdukController::class, 'restore'])->name('produks.restore');
	Route::get('delete', [ProdukController::class, 'delete'])->name('produks.delete-permanently');
});
