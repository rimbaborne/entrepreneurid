<?php

use App\Http\Controllers\Backend\EventController;
use App\Http\Controllers\Backend\DashboardController;

use App\Models\Event;

Route::bind('event', function ($value) {
	$event = new Event;

	// return Event::withTrashed()->where($event->getRouteKeyName(), $value)->first();
	return Event::where($event->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'events'], function () {
	Route::get(	'', 		            [EventController::class, 'index']		)->name('events.index');
    Route::get(	'create', 	            [EventController::class, 'create']	)->name('events.create');
    Route::get(	'leaderboard', 	        [EventController::class, 'leaderboard']	)->name('events.leaderboard');
    Route::get(	'slide-agen', 	        [DashboardController::class, 'slideagen']	)->name('events.slideagen');
    Route::post(	'slide-agen-tambah', 	[DashboardController::class, 'slideagentambah']	)->name('events.slideagentambah');
    Route::get(	'edit', 	            [EventController::class, 'edit']	)->name('events.edit');
	Route::post('store', 	            [EventController::class, 'store']		)->name('events.store');
    Route::get(	'deleted', 	            [EventController::class, 'deleted']	)->name('events.deleted');
    Route::get(	'notifikasi', 	        [EventController::class, 'notifikasi']	)->name('events.notifikasi');
    Route::post('notifikasi/simpan', 	[EventController::class, 'notifikasisimpan']	)->name('events.notifikasi.simpan');
	Route::post('update',               [EventController::class, 'update'])->name('events.update');

});

// Route::group(['prefix' => 'events/{event}'], function () {
// 	// event
// 	Route::get('/', [EventController::class, 'show'])->name('events.show');
// 	Route::get('edit', [EventController::class, 'edit'])->name('events.edit');
// 	Route::patch('update', [EventController::class, 'update'])->name('events.update');
// 	Route::delete('destroy', [EventController::class, 'destroy'])->name('events.destroy');
// 	// Deleted
// 	Route::get('restore', [EventController::class, 'restore'])->name('events.restore');
// 	Route::get('delete', [EventController::class, 'delete'])->name('events.delete-permanently');
// });
