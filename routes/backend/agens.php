<?php

use App\Http\Controllers\Backend\AgenController;

use App\Models\Agen;

Route::bind('agen', function ($value) {
    $agen = new Agen;

    return Agen::withTrashed()->where($agen->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'agen'], function () {
    Route::get('',         [AgenController::class, 'index'])->name('agens.index');
    Route::get('password', [AgenController::class, 'password'])->name('agens.password');
    Route::get('yajra',    [AgenController::class, 'yajra'])->name('agens.yajra');
    Route::get('yajradata',[AgenController::class, 'yajradata'])->name('agens.yajra.data');
    Route::get('create',   [AgenController::class, 'create'])->name('agens.create');
    Route::post('store',   [AgenController::class, 'store'])->name('agens.store');
    Route::get('deleted',  [AgenController::class, 'deleted'])->name('agens.deleted');
    Route::get('export',   [AgenController::class, 'export'])->name('agens.export');
    Route::get('agen-lama',[AgenController::class, 'agenlama'])->name('agens.agenlama');
    Route::get('agen-baru',[AgenController::class, 'agenbaru'])->name('agens.agenbaru');
    Route::get('sub-agen', [AgenController::class, 'subagen'])->name('agens.subagen');
});

Route::group(['prefix' => 'agen/{agen}'], function () {
    // Agen
    Route::get('/', [AgenController::class, 'show'])->name('agens.show');
    Route::get('edit', [AgenController::class, 'edit'])->name('agens.edit');
    Route::patch('update', [AgenController::class, 'update'])->name('agens.update');
    Route::delete('destroy', [AgenController::class, 'destroy'])->name('agens.destroy');
    // Deleted
    Route::get('restore', [AgenController::class, 'restore'])->name('agens.restore');
    Route::get('delete', [AgenController::class, 'delete'])->name('agens.delete-permanently');
});
