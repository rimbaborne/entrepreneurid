<?php

use App\Http\Controllers\Backend\MemberController;

use App\Models\Member;

Route::bind('member', function ($value) {
	$member = new Member;

	return Member::withTrashed()->where($member->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'member'], function () {
	Route::get(	'', 		                            [MemberController::class, 'index']		    )->name('members.index');
    Route::get( 'kelas-online-copywriting',             [MemberController::class, 'koc']            )->name('members.koc');
    Route::get( 'kelas-online-copywriting/praktikum',   [MemberController::class, 'kocpraktikum']   )->name('members.koc.praktikum');
    Route::get( 'kelas-online-copywriting-2',           [MemberController::class, 'koc2']           )->name('members.koc2');
    Route::get( 'create',                               [MemberController::class, 'create']         )->name('members.create');
	Route::post('store', 	                            [MemberController::class, 'store']		    )->name('members.store');
    Route::get(	'deleted', 	                            [MemberController::class, 'deleted']	    )->name('members.deleted');

    // MIC
    Route::get( 'mentoring-instant-copywriting',        [MemberController::class, 'mic']            )->name('members.mic');
    Route::get( 'mentoring-instant-copywriting/advisor',[MemberController::class, 'micadvisor']     )->name('members.mic.advisor');
});

Route::group(['prefix' => 'member/{member}'], function () {
	// Member
	// Route::get('/', [MemberController::class, 'show'])->name('members.show');
	// Route::get('edit', [MemberController::class, 'edit'])->name('members.edit');
	// Route::patch('update', [MemberController::class, 'update'])->name('members.update');
	// Route::delete('destroy', [MemberController::class, 'destroy'])->name('members.destroy');
	// // Deleted
	// Route::get('restore', [MemberController::class, 'restore'])->name('members.restore');
	// Route::get('delete', [MemberController::class, 'delete'])->name('members.delete-permanently');
});
