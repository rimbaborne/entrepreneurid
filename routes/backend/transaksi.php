<?php

use App\Http\Controllers\Backend\TransaksiController;
use App\Models\Transaction;

Route::bind('transaksi', function ($value) {
	$transaksi = new Transaction;

	return Transaction::withTrashed()->where($transaksi->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'transaksi'], function () {
	Route::get(	'', 		    [TransaksiController::class, 'index'])->name('transaksi.index');
    Route::get(	'create', 	    [TransaksiController::class, 'create'])->name('transaksi.create');
	Route::post('store', 	    [TransaksiController::class, 'store'])->name('transaksi.store');
    Route::get(	'deleted', 	    [TransaksiController::class, 'deleted'])->name('transaksi.deleted');
    Route::get(	'exportdata', 	[TransaksiController::class, 'exportdata'])->name('transaksi.exportdata');
    Route::get(	'leadmagnetexport', 	[TransaksiController::class, 'leadmagnetexport'])->name('transaksi.leadmagnetexport');
    Route::post('upload', 	    [TransaksiController::class, 'uploadbuktitf'])->name('transaksi.uploadbuktitf');
    Route::get('agen', 	        [TransaksiController::class, 'cariagen'])->name('transaksi.cariagen');
    Route::get('data-agen', 	 [TransaksiController::class, 'dataagen'])->name('transaksi.dataagen');
    Route::post('update-upload', [TransaksiController::class, 'updateuploadbuktitf'])->name('transaksi.updateuploadbuktitf');
});

Route::group(['prefix' => 'transaksi/{transaksi}'], function () {
	Route::get('/',         [TransaksiController::class, 'show'])->name('transaksi.show');
	Route::get('edit',      [TransaksiController::class, 'edit'])->name('transaksi.edit');
});
