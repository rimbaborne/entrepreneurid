<?php

use App\Http\Controllers\Backend\BiodatumController;

use App\Models\Biodatum;

Route::bind('biodatum', function ($value) {
    $biodatum = new Biodatum;

    return Biodatum::withTrashed()->where($biodatum->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'data-ecourse'], function () {
    Route::get('',                  [BiodatumController::class, 'index'])           ->name('biodata.index');
    Route::get('create',            [BiodatumController::class, 'create'])          ->name('biodata.create');
    Route::post('store',            [BiodatumController::class, 'store'])           ->name('biodata.store');
    Route::get('deleted',           [BiodatumController::class, 'deleted'])         ->name('biodata.deleted');
    Route::get('getdata',           [BiodatumController::class, 'getdata'])         ->name('biodata.getdata');
    Route::get('hapusdata',         [BiodatumController::class, 'hapusdata'])       ->name('biodata.hapusdata');
    Route::get('hapusdatamasal',    [BiodatumController::class, 'hapusdatamasal'])  ->name('biodata.hapusdatamasal');
    Route::post('updatedata',       [BiodatumController::class, 'updatedata'])      ->name('biodata.updatedata');
    Route::post('teswa',            [BiodatumController::class, 'teswa'])           ->name('biodata.teswa');
    Route::get('exportexcel',       [BiodatumController::class, 'exportexcel'])     ->name('biodata.exportexcel');
    Route::get('cnl/resi',          [BiodatumController::class, 'cnlresi'])         ->name('biodata.cnl.resi');
});

Route::group(['prefix' => 'data-ecourse/{biodatum}'], function () {
    // Biodatum
    Route::get('/', [BiodatumController::class, 'show'])->name('biodata.show');
    Route::get('edit', [BiodatumController::class, 'edit'])->name('biodata.edit');
    Route::patch('update', [BiodatumController::class, 'update'])->name('biodata.update');
    Route::delete('destroy', [BiodatumController::class, 'destroy'])->name('biodata.destroy');
    // Deleted
    Route::get('restore', [BiodatumController::class, 'restore'])->name('biodata.restore');
    Route::get('delete', [BiodatumController::class, 'delete'])->name('biodata.delete-permanently');
});
