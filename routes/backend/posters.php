<?php

use App\Http\Controllers\Backend\PosterController;

use App\Models\Poster;

Route::bind('poster', function ($value) {
	$poster = new Poster;

	return Poster::withTrashed()->where($poster->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'posters'], function () {
	Route::get(	'', 		[PosterController::class, 'index']		)->name('posters.index');
    Route::get(	'create', 	[PosterController::class, 'create']	)->name('posters.create');
	Route::post('store', 	[PosterController::class, 'store']		)->name('posters.store');
    Route::get(	'deleted', 	[PosterController::class, 'deleted']	)->name('posters.deleted');
});

Route::group(['prefix' => 'posters/{poster}'], function () {
	// Poster
	Route::get('/', [PosterController::class, 'show'])->name('posters.show');
	Route::get('edit', [PosterController::class, 'edit'])->name('posters.edit');
	Route::patch('update', [PosterController::class, 'update'])->name('posters.update');
	Route::delete('destroy', [PosterController::class, 'destroy'])->name('posters.destroy');
	// Deleted
	Route::get('restore', [PosterController::class, 'restore'])->name('posters.restore');
	Route::get('delete', [PosterController::class, 'delete'])->name('posters.delete-permanently');
});