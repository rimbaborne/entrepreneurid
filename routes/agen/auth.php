<?php
 // Login
 Route::get('login', 'Agen\Auth\LoginController@showLoginForm')->name('login');
 Route::post('login', 'Agen\Auth\LoginController@login');
 Route::post('logout', 'Agen\Auth\LoginController@logout')->name('logout');

 // Register
 Route::get('register', 'Agen\Auth\RegisterController@showRegistrationForm')->name('register');
 Route::post('register', 'Agen\Auth\RegisterController@register');

 // Passwords
 Route::post('password/email', 'Agen\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
 Route::post('password/reset', 'Agen\Auth\ResetPasswordController@reset');
 Route::get('password/reset', 'Agen\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
 Route::get('password/reset/{token}', 'Agen\Auth\ResetPasswordController@showResetForm')->name('password.reset');
//
