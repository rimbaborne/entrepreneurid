<?php
 // dashboard agen
 Route::get('/', 'Agen\HomeController@index')->name('dashboard');
 Route::get('/profile', 'Agen\HomeController@profile')->name('page.profile');
 Route::post('/profile/update', 'Agen\HomeController@updateprofile')->name('page.profile.update');
 Route::get('/profile/password', 'Agen\HomeController@password')->name('page.profile.password');
 Route::post('/profile/password/update', 'Agen\HomeController@updatepassword')->name('page.profile.password.update');
 Route::get('/amunisi', 'Agen\HomeController@amunisi')->name('page.amunisi');
 Route::get('/menu-agen', 'Agen\HomeController@menuagen')->name('page.menuagen');
//  Route::get('/produk', 'Agen\ProdukController@index')->name('page.produk');
 Route::get('/informasi', 'Agen\HomeController@informasi')->name('page.informasi');
