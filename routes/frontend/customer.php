<?php
use App\Http\Controllers\Frontend\CustomerController;

Route::get('pemesanan',             [CustomerController::class, 'pemesanan'])->name('pemesanan');
Route::post('simpan-pemesanan',     [CustomerController::class, 'simpanpemesanan'])->name('simpanpemesanan');
Route::get('pemesanan-selesai',     [CustomerController::class, 'pemesananselesai'])->name('pemesananselesai');
Route::get('invoice',               [CustomerController::class, 'invoice'])->name('invoice');
Route::post('upload-invoice',       [CustomerController::class, 'uploadinvoice'])->name('uploadinvoice');
Route::post('simpan-invoice',       [CustomerController::class, 'simpaninvoice'])->name('simpaninvoice');
Route::get('cek-ongkir', 		    [CustomerController::class, 'cekongkir'])->name('cekongkir');

// Route::group(['middleware' => 'admin'], function () {
    Route::get('pesan-manual',      [CustomerController::class, 'pesanmanual'])->name('pesanmanual');
// });



