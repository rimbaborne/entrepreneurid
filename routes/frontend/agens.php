<?php

use App\Http\Controllers\Frontend\AgenController;

use App\Models\Agen;

Route::bind('agens', function ($value) {
	$agen = new Agen;

	return Agen::withTrashed()->where($agen->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'agens'], function () {
	Route::get(	'', 		[AgenController::class, 'index']		)->name('agens.index');
    Route::get(	'create', 	[AgenController::class, 'create']	)->name('agens.create');
	Route::post('store', 	[AgenController::class, 'store']		)->name('agens.store');
    Route::get(	'deleted', 	[AgenController::class, 'deleted']	)->name('agens.deleted');
});

Route::group(['prefix' => 'agens/{agen}'], function () {
	// Agen
	Route::get('/', [AgenController::class, 'show'])->name('agens.show');
	Route::get('edit', [AgenController::class, 'edit'])->name('agens.edit');
	Route::patch('update', [AgenController::class, 'update'])->name('agens.update');
	Route::delete('destroy', [AgenController::class, 'destroy'])->name('agens.destroy');
	// Deleted
	Route::get('restore', [AgenController::class, 'restore'])->name('agens.restore');
	Route::get('delete', [AgenController::class, 'delete'])->name('agens.delete-permanently');
});
