<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\User\ProdukController;
use App\Http\Controllers\Frontend\User\CustomerController;


/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');
Route::get('cek-ongkir', [HomeController::class, 'cekongkir'])->name('cekongkir');
Route::get('bonus-cnl', [ContactController::class, 'poster'])->name('posters.index');
Route::get('cek-resi', [HomeController::class, 'cekresi'])->name('cekresi');
Route::get('advisor', [HomeController::class, 'micadvisor'])->name('mic.advisor');


//MIC

// if ( $_SERVER['HTTP_HOST'] == 'eid.test' ) {
// // if ( $_SERVER['HTTP_HOST'] == 'konfirmasi.agen-entrepreneurid.com') {

//     // Daftar [agen-entrepreneurid.com]
//     Route::get('daftar', [HomeController::class, 'daftar'])->name('daftar');
//     // Route::get('daftar', function () {
//     //     return redirect()->to('https://agen-entrepreneurid.com/');
//     // });
//     // Route::get('tes', [HomeController::class, 'tes'])->name('tes');
//     Route::post('simpan', [HomeController::class, 'simpan'])->name('simpan');
//     // Route::get('pembayaran', [HomeController::class, 'pembayaran'])->name('pembayaran');
//     Route::get('pembayaran', function () {
//         return redirect()->to('https://agen-entrepreneurid.com/');
//     });
//     Route::get('--pembayaran', [HomeController::class, 'pembayaran'])->name('pembayaran');
//     // Route::get('pembayaran', function () {
//     //     return redirect()->to('https://agen-entrepreneurid.com/');
//     // });
//     Route::post('pembayaran/simpan', [HomeController::class, 'simpanpembayaran'])->name('pembayaran.simpan');
//     Route::get('selesai', [HomeController::class, 'selesai'])->name('selesai');
//     Route::post('pembayaran/uploadbuktitransfer', [HomeController::class, 'uploadbuktitransfer'])->name('pembayaran.uploadbuktitransfer');

//     Route::get('transfer-ulang', [HomeController::class, 'transferulang'])->name('transferulang');
// }

// if ( $_SERVER['HTTP_HOST'] == 'app.copywritingnextlevel.com') {


//     // Daftar [agen-entrepreneurid.com]
//     Route::get('daftar', [HomeController::class, 'daftar'])->name('daftar');
//     Route::get('pesan', [HomeController::class, 'daftar'])->name('daftar');
//     // Route::get('pesan', function () {
//     //     return redirect()->to('https://copywritingnextlevel.com/');
//     // });
//     // Route::get('tes', [HomeController::class, 'tes'])->name('tes');
//     Route::post('simpan', [HomeController::class, 'simpan'])->name('simpan');
//     Route::get('pembayaran', [HomeController::class, 'pembayaran'])->name('pembayaran');
//     // Route::get('--pembayaran', [HomeController::class, 'pembayaran'])->name('pembayaran');
//     // Route::get('pembayaran', function () {
//     //     return redirect()->to('https://copywritingnextlevel.com/');
//     // });
//     Route::post('pembayaran/simpan', [HomeController::class, 'simpanpembayaran'])->name('pembayaran.simpan');
//     Route::get('selesai', [HomeController::class, 'selesai'])->name('selesai');
//     Route::post('pembayaran/uploadbuktitransfer', [HomeController::class, 'uploadbuktitransfer'])->name('pembayaran.uploadbuktitransfer');

//     Route::get('transfer-ulang', [HomeController::class, 'transferulang'])->name('transferulang');

// }

// if ( $_SERVER['HTTP_HOST'] == 'konfirmasi.kelasfbhacking.com') {


//     // Route::get('daftar', [HomeController::class, 'daftar'])->name('daftar');
//     Route::get('daftar', function () {
//         return redirect()->to('https://kelasfbhacking.com/');
//     });
//     // Route::get('tes', [HomeController::class, 'tes'])->name('tes');
//     Route::post('simpan', [HomeController::class, 'simpan'])->name('simpan');
//     // Route::get('pembayaran', [HomeController::class, 'pembayaran'])->name('pembayaran');
//     Route::get('--pembayaran', [HomeController::class, 'pembayaran'])->name('pembayaran');
//     Route::get('pembayaran', function () {
//         return redirect()->to('https://kelasfbhacking.com/');
//     });
//     Route::post('pembayaran/simpan', [HomeController::class, 'simpanpembayaran'])->name('pembayaran.simpan');
//     Route::get('selesai', [HomeController::class, 'selesai'])->name('selesai');
//     Route::post('pembayaran/uploadbuktitransfer', [HomeController::class, 'uploadbuktitransfer'])->name('pembayaran.uploadbuktitransfer');

//     Route::get('transfer-ulang', [HomeController::class, 'transferulang'])->name('transferulang');

// }

// if ( $_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringinstantcopywriting.com') {


//     Route::get('daftar', [HomeController::class, 'daftar'])->name('daftar');
//     // Route::get('daftar', function () {
//     //     return redirect()->to('https://mentoringinstantcopywriting.com/');
//     // });
//     // Route::get('tes', [HomeController::class, 'tes'])->name('tes');
//     Route::post('simpan', [HomeController::class, 'simpan'])->name('simpan');
//     Route::get('pembayaran', [HomeController::class, 'pembayaran'])->name('pembayaran');
//     Route::get('--pembayaran', [HomeController::class, 'pembayaran'])->name('pembayaran');
//     // Route::get('pembayaran', function () {
//     //     return redirect()->to('https://mentoringinstantcopywriting.com/');
//     // });
//     Route::post('pembayaran/simpan', [HomeController::class, 'simpanpembayaran'])->name('pembayaran.simpan');
//     Route::get('selesai', [HomeController::class, 'selesai'])->name('selesai');
//     Route::post('pembayaran/uploadbuktitransfer', [HomeController::class, 'uploadbuktitransfer'])->name('pembayaran.uploadbuktitransfer');

//     Route::get('transfer-ulang', [HomeController::class, 'transferulang'])->name('transferulang');
// }

// if ( $_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringsuperreseller.com') {


//     // Route::get('daftar', [HomeController::class, 'daftar'])->name('daftar');
//     Route::get('daftar', function () {
//         return redirect()->to('https://mentoringsuperreseller.com/');
//     });
//     // Route::get('tes', [HomeController::class, 'tes'])->name('tes');
//     Route::post('simpan', [HomeController::class, 'simpan'])->name('simpan');
//     // Route::get('pembayaran', [HomeController::class, 'pembayaran'])->name('pembayaran');
//     Route::get('--pembayaran', [HomeController::class, 'pembayaran'])->name('pembayaran');
//     Route::get('pembayaran', function () {
//         return redirect()->to('https://mentoringsuperreseller.com/');
//     });
//     Route::post('pembayaran/simpan', [HomeController::class, 'simpanpembayaran'])->name('pembayaran.simpan');
//     Route::get('selesai', [HomeController::class, 'selesai'])->name('selesai');
//     Route::post('pembayaran/uploadbuktitransfer', [HomeController::class, 'uploadbuktitransfer'])->name('pembayaran.uploadbuktitransfer');

//     Route::get('transfer-ulang', [HomeController::class, 'transferulang'])->name('transferulang');
// }

// if ( $_SERVER['HTTP_HOST'] == 'eid.test') {


//     Route::get('daftar', [HomeController::class, 'daftar'])->name('daftar');
//     Route::get('pesan', [HomeController::class, 'daftar'])->name('pesan');
//     // Route::get('daftar', function () {
//     //     return redirect()->to('https://mentoringinstantcopywriting.com/');
//     // });
//     // Route::get('tes', [HomeController::class, 'tes'])->name('tes');
//     Route::post('simpan', [HomeController::class, 'simpan'])->name('simpan');
//     Route::get('pembayaran', [HomeController::class, 'pembayaran'])->name('pembayaran');
//     Route::get('--pembayaran', [HomeController::class, 'pembayaran'])->name('pembayaran');
//     // Route::get('pembayaran', function () {
//     //     return redirect()->to('https://mentoringinstantcopywriting.com/');
//     // });
//     Route::post('pembayaran/simpan', [HomeController::class, 'simpanpembayaran'])->name('pembayaran.simpan');
//     Route::get('selesai', [HomeController::class, 'selesai'])->name('selesai');
//     Route::post('pembayaran/uploadbuktitransfer', [HomeController::class, 'uploadbuktitransfer'])->name('pembayaran.uploadbuktitransfer');

//     Route::get('transfer-ulang', [HomeController::class, 'transferulang'])->name('transferulang');

// }
/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        // User Dashboard Specific
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

        // User Account Specific
        Route::get('account', [AccountController::class, 'index'])->name('account');

        // User Profile Specific
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');

        // Tambahan
        Route::get('produk', [ProdukController::class, 'index'])->name('produk');

        // Kelas Online Marketing
        Route::get('praktikum-koc/1', [CustomerController::class, 'kocpraktikumpertama'])->name('koc.praktikum.pertama');
        Route::get('praktikum-koc/2', [CustomerController::class, 'kocpraktikumkedua'])->name('koc.praktikum.kedua');
        Route::get('praktikum-koc/3', [CustomerController::class, 'kocpraktikumketiga'])->name('koc.praktikum.ketiga');
        Route::get('praktikum-koc/sertifikat', [CustomerController::class, 'kocsertifikat'])->name('koc.praktikum.sertifikat');
    });
});
