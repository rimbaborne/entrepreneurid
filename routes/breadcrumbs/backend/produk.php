<?php

Breadcrumbs::for('admin.produks.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Data Produk', route('admin.produks.index'));
});

Breadcrumbs::for('admin.produks.create', function ($trail) {
    $trail->parent('admin.produks.index');
    $trail->push('Buat Produk', route('admin.produks.create'));
});

Breadcrumbs::for('admin.produks.show', function ($trail, $id) {
    $trail->parent('admin.produks.index');
    $trail->push('Produk', route('admin.produks.show', $id));
});

Breadcrumbs::for('admin.produks.edit', function ($trail, $id) {
    $trail->parent('admin.produks.index');
    $trail->push('Edit Produk', route('admin.produks.edit', $id));
});

Breadcrumbs::for('admin.produks.deleted', function ($trail) {
    $trail->parent('admin.produks.index');
    $trail->push('Produk Dihapus', route('admin.produks.deleted'));
});
