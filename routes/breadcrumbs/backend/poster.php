<?php

Breadcrumbs::for('admin.posters.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_posters.labels.management'), route('admin.posters.index'));
});

Breadcrumbs::for('admin.posters.create', function ($trail) {
    $trail->parent('admin.posters.index');
    $trail->push(__('backend_posters.labels.create'), route('admin.posters.create'));
});

Breadcrumbs::for('admin.posters.show', function ($trail, $id) {
    $trail->parent('admin.posters.index');
    $trail->push(__('backend_posters.labels.view'), route('admin.posters.show', $id));
});

Breadcrumbs::for('admin.posters.edit', function ($trail, $id) {
    $trail->parent('admin.posters.index');
    $trail->push(__('backend.posters.labels.edit'), route('admin.posters.edit', $id));
});

Breadcrumbs::for('admin.posters.deleted', function ($trail) {
    $trail->parent('admin.posters.index');
    $trail->push(__('backend_posters.labels.deleted'), route('admin.posters.deleted'));
});
