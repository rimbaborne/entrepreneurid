<?php

Breadcrumbs::for('admin.transaksi.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Transaksi', route('admin.transaksi.index'));
});

Breadcrumbs::for('admin.transaksi.create', function ($trail) {
    $trail->parent('admin.transaksi.index');
    $trail->push(__('backend_transaksi.labels.create'), route('admin.transaksi.create'));
});

Breadcrumbs::for('admin.transaksi.show', function ($trail, $id) {
    $trail->parent('admin.transaksi.index');
    $trail->push(__('backend_transaksi.labels.view'), route('admin.transaksi.show', $id));
});

Breadcrumbs::for('admin.transaksi.edit', function ($trail, $id) {
    $trail->parent('admin.transaksi.index');
    $trail->push(__('backend.transaksi.labels.edit'), route('admin.transaksi.edit', $id));
});

Breadcrumbs::for('admin.transaksi.dataagen', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Transaksi', route('admin.transaksi.dataagen'));
});
