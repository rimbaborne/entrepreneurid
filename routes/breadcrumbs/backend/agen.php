<?php

Breadcrumbs::for('admin.agens.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_agens.labels.management'), route('admin.agens.index'));
});

Breadcrumbs::for('admin.agens.create', function ($trail) {
    $trail->parent('admin.agens.index');
    $trail->push(__('backend_agens.labels.create'), route('admin.agens.create'));
});

Breadcrumbs::for('admin.agens.show', function ($trail, $id) {
    $trail->parent('admin.agens.index');
    $trail->push(__('backend_agens.labels.view'), route('admin.agens.show', $id));
});

Breadcrumbs::for('admin.agens.edit', function ($trail, $id) {
    $trail->parent('admin.agens.index');
    $trail->push(__('backend.agens.labels.edit'), route('admin.agens.edit', $id));
});

Breadcrumbs::for('admin.agens.deleted', function ($trail) {
    $trail->parent('admin.agens.index');
    $trail->push(__('backend_agens.labels.deleted'), route('admin.agens.deleted'));
});

Breadcrumbs::for('admin.agens.yajra', function ($trail) {
    $trail->parent('admin.agens.index');
    $trail->push(__('backend_agens.labels.index'), route('admin.agens.yajra'));
});

Breadcrumbs::for('admin.agens.agenlama', function ($trail) {
    $trail->parent('admin.agens.index');
    $trail->push(__('backend_agens.labels.index'), route('admin.agens.agenlama'));
});

Breadcrumbs::for('admin.agens.password', function ($trail) {
    $trail->parent('admin.agens.index');
    $trail->push(__('backend_agens.labels.index'), route('admin.agens.password'));
});

Breadcrumbs::for('admin.agens.subagen', function ($trail) {
    $trail->parent('admin.agens.index');
    $trail->push('Sub Agen', route('admin.agens.subagen'));
});
