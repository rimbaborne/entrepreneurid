<?php

Breadcrumbs::for('admin.events.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Data Event', route('admin.events.index'));
});

Breadcrumbs::for('admin.events.create', function ($trail) {
    $trail->parent('admin.events.index');
    $trail->push('Buat Event', route('admin.events.create'));
});

Breadcrumbs::for('admin.events.show', function ($trail, $id) {
    $trail->parent('admin.events.index');
    $trail->push('Event', route('admin.events.show', $id));
});

Breadcrumbs::for('admin.events.edit', function ($trail) {
    $trail->parent('admin.events.index');
    $trail->push('Edit Event', route('admin.events.edit'));
});

Breadcrumbs::for('admin.events.deleted', function ($trail) {
    $trail->parent('admin.events.index');
    $trail->push('Event Dihapus', route('admin.events.deleted'));
});

Breadcrumbs::for('admin.events.notifikasi', function ($trail) {
    $trail->parent('admin.events.index');
    $trail->push('Notifikasi Event', route('admin.events.notifikasi'));
});

Breadcrumbs::for('admin.events.leaderboard', function ($trail) {
    $trail->parent('admin.events.index');
    $trail->push('Data Leaderboard', route('admin.events.leaderboard'));
});

Breadcrumbs::for('admin.events.slideagen', function ($trail) {
    $trail->parent('admin.events.index');
    $trail->push('Dashboard Image Slide Agen', route('admin.events.slideagen'));
});
