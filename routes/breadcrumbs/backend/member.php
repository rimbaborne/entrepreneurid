<?php

Breadcrumbs::for('admin.members.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_members.labels.management'), route('admin.members.index'));
});

Breadcrumbs::for('admin.members.create', function ($trail) {
    $trail->parent('admin.members.index');
    $trail->push(__('backend_members.labels.create'), route('admin.members.create'));
});

Breadcrumbs::for('admin.members.show', function ($trail, $id) {
    $trail->parent('admin.members.index');
    $trail->push(__('backend_members.labels.view'), route('admin.members.show', $id));
});

Breadcrumbs::for('admin.members.edit', function ($trail, $id) {
    $trail->parent('admin.members.index');
    $trail->push(__('backend.members.labels.edit'), route('admin.members.edit', $id));
});

Breadcrumbs::for('admin.members.deleted', function ($trail) {
    $trail->parent('admin.members.index');
    $trail->push(__('backend_members.labels.deleted'), route('admin.members.deleted'));
});

Breadcrumbs::for('admin.members.koc', function ($trail) {
    $trail->parent('admin.members.index');
    $trail->push('Kelas Online Copywriting', route('admin.members.koc'));
});

Breadcrumbs::for('admin.members.koc2', function ($trail) {
    $trail->parent('admin.members.index');
    $trail->push('Kelas Online Copywriting Batch 2', route('admin.members.koc2'));
});

Breadcrumbs::for('admin.members.koc.praktikum', function ($trail) {
    $trail->parent('admin.members.koc');
    $trail->push('Praktikum', route('admin.members.koc.praktikum'));
});

Breadcrumbs::for('admin.members.mic', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push('Mentoring Instant Copywriting', route('admin.members.mic'));
});

Breadcrumbs::for('admin.members.mic.advisor', function ($trail) {
    $trail->parent('admin.members.mic');
    $trail->push('Advisor', route('admin.members.mic.advisor'));
});
