<?php

Breadcrumbs::for('admin.biodata.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_biodata.labels.management'), route('admin.biodata.index'));
});

Breadcrumbs::for('admin.biodata.create', function ($trail) {
    $trail->parent('admin.biodata.index');
    $trail->push(__('backend_biodata.labels.create'), route('admin.biodata.create'));
});

Breadcrumbs::for('admin.biodata.show', function ($trail, $id) {
    $trail->parent('admin.biodata.index');
    $trail->push(__('backend_biodata.labels.view'), route('admin.biodata.show', $id));
});

Breadcrumbs::for('admin.biodata.edit', function ($trail, $id) {
    $trail->parent('admin.biodata.index');
    $trail->push('Edit Data', route('admin.biodata.edit', $id));
});

Breadcrumbs::for('admin.biodata.deleted', function (    $trail) {
    $trail->parent('admin.biodata.index');
    $trail->push(__('backend_biodata.labels.deleted'), route('admin.biodata.deleted'));
});

Breadcrumbs::for('admin.biodata.cnl.resi', function ($trail) {
    $trail->parent('admin.biodata.index');
    $trail->push('Resi CNL', route('admin.biodata.cnl.resi'));
});
