<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';

require __DIR__.'/biodatum.php';
require __DIR__.'/agen.php';
require __DIR__.'/member.php';

require __DIR__.'/poster.php';
require __DIR__.'/produk.php';
require __DIR__.'/event.php';
require __DIR__.'/transaksi.php';
