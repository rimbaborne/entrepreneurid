<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Api\TransactionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::middleware('apikey')->group(function () {
    Route::post('transaction/create', [TransactionController::class, 'create']);
    Route::get('transaction/show/{uuid}', [TransactionController::class, 'show']);
});