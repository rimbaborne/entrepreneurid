<?php

use App\Http\Controllers\LanguageController;
use App\Models\Auth\Agen;
/*
 * Global Routes
 * Routes that are used between both frontend and backend.
 */

// Switch between the included languages
Route::get('lang/{lang}', [LanguageController::class, 'swap']);

require_once 'agen.php';

// Route::group(['domain' => 'agen-eid.test'], $agenRoutes);
Route::group(['domain' => 'dashboard.agen-entrepreneurid.com'], $agenRoutes);

// Route::domain('admin.entrepreneurid.org')->group(function () {
//         Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
//             include_route_files(__DIR__.'/backend/');
//         });

//         Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
//             include_route_files(__DIR__.'/frontend/');
//         });
// });

// Route::domain('konfirmasi.agen-entrepreneurid.com')->group(function () {
//     Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
//         include_route_files(__DIR__.'/frontend/');
//     });
// });

// Route::domain('konfirmasi.mentoringinstantcopywriting.com')->group(function () {
//     Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
//         include_route_files(__DIR__.'/frontend/');
//     });
// });

// Route::domain('konfirmasi.kelasfbhacking.com')->group(function () {
//     Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
//         include_route_files(__DIR__.'/frontend/');
//     });
// });

// Route::domain('konfirmasi.mentoringsuperreseller.com')->group(function () {
//     Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
//         include_route_files(__DIR__.'/frontend/');
//     });
// });

// Route::domain('app.copywritingnextlevel.com')->group(function () {
//     Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
//         include_route_files(__DIR__.'/frontend/');
//     });
// });

// Route::domain('app.mentoringinstantcopywriting.com')->group(function () {
//     Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
//         include_route_files(__DIR__.'/frontend/');
//     });
// });

// Route::domain('eid-rev.test')->group(function () {
//         Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
//             include_route_files(__DIR__.'/backend/');
//         });

//         Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
//             include_route_files(__DIR__.'/frontend/');
//         });
// });

Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
                include_route_files(__DIR__.'/backend/');
            });

Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    include_route_files(__DIR__.'/frontend/');
});

