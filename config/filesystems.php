<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => [
        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        'bukti-transfer-24-AGENBARU' => [
            'driver' => 'local',
            'root' => public_path('../../../public_html/agen-entrepreneurid.com/afiliasi/app/public/bukti-transfer-24-AGENBARU'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        'bukti-transfer-25-KSO' => [
            'driver' => 'local',
            'root' => public_path('../../../public_html/kelas100orderan.com/app/public/bukti-transfer-25-KSO'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        'bukti-transfer-26-MOM' => [
            'driver' => 'local',
            'root' => public_path('../../../public_html/mentoringorganicmarketing.com/upload/public/bukti-transfer-26-MOM'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        'bukti-transfer-27-KOC' => [
            'driver' => 'local',
            'root' => public_path('../../../public_html/kelasonlinecopywriting.com/member/public/bukti-transfer-27-KOC'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        'bukti-transfer-28-KDW' => [
            'driver' => 'local',
            'root' => public_path('../../../public_html/kelasdatabasewa.com/upload/public/bukti-transfer-27-KOC'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        'bukti-transfer-29-CNL' => [
            'driver' => 'local',
            'root' => public_path('../../../public_html/copywritingnextlevel.com/app/public/bukti-transfer-29-CNL'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        'bukti-transfer-tes' => [
            'driver' => 'local',
            'root' => public_path('app/public/bukti-transfer-26-MOM'),
            'url' => env('APP_URL') . '/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
        ],
    ],

    'links' => [
        public_path('storage') => storage_path('app/public'),
        public_path('images') => storage_path('app/images'),
    ],
];
