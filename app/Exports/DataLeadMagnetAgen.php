<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Auth;
use App\Models\MemberEbook;

class DataLeadMagnetAgen implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    public function query()
    {
        return MemberEbook::query()->where('ref', Auth::guard('agen')->user()->id);
    }

    public function map($leadmagnet) : array {
        return [
            $leadmagnet->nama ?? null,
            $leadmagnet->email ?? null,
            $leadmagnet->registrasi ?? null,
        ];
    }

    public function headings() : array {
        return [
           'Nama',
           'Email',
           'Waktu Daftar',
        ] ;
    }
}
