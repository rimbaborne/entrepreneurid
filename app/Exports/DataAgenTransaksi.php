<?php

namespace App\Exports;

use App\Models\Transaction;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Facades\Auth;

class DataAgenTransaksi implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    public function query()
    {
        return Transaction::query()->where('id_agen', Auth::guard('agen')->user()->id);
    }

    public function jenis($jenis)
    {
        if ($jenis == 1) {
            return 'MANUAL';
        } elseif ($jenis == 2) {
            return 'AFILIASI';
        } else {
            return null;
        }
    }

    public function status($status)
    {
        if ($status == 1) {
            return 'BELUM AKTIF';
        } elseif ($status == 2) {
            return 'MENUNGGU KONFIRMASI';
        } elseif ($status == 3) {
            return 'AKTIF';
        } else {
            return null;
        }
    }

    public function map($transaksi) : array {
        return [
            $transaksi->event->id_event ?? null,
            $transaksi->event->produk->nama ?? null,
            $transaksi->nama ?? null,
            $transaksi->panggilan ?? null,
            $transaksi->tgllahir ?? null,
            $transaksi->gender ?? null,
            $transaksi->nohp ?? null,
            $transaksi->email ?? null,
            $this->status($transaksi->status) ?? null,
            $this->jenis($transaksi->jenis) ?? null,
            $transaksi->total ?? null,
            $transaksi->created_at ?? null,
            $transaksi->waktu_upload ?? null,
            $transaksi->waktu_konfirmasi ?? null,
        ] ;
    }

    public function headings() : array {
        return [
           'Kode Event',
           'Nama Event',
           'Nama',
           'Panggilan',
           'Tanggal Lahir',
           'Gender',
           'Notelp',
           'Email',
           'Status',
           'Jenis',
           'Total',
           'Waktu Daftar',
           'Waktu Upload',
           'Waktu Konfirmasi',
        ] ;
    }
}
