<?php

namespace App\Exports;

use App\Models\Transaction;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DataTransaksiBuku implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    public function event(int $event)
    {
        $this->event = $event;

        return $this;
    }

    public function query()
    {
        return Transaction::query()->where('id_event', $this->event);
    }

    public function jenis($jenis)
    {
        if ($jenis == 1) {
            return 'MANUAL';
        } elseif ($jenis == 2) {
            return 'AFILIASI';
        } else {
            return null;
        }
    }

    public function status($status)
    {
        if ($status == 1) {
            return 'BELUM AKTIF';
        } elseif ($status == 2) {
            return 'MENUNGGU KONFIRMASI';
        } elseif ($status == 3) {
            return 'AKTIF';
        } else {
            return null;
        }
    }

    public function map($transaksi) : array {
        return [
            $transaksi->event->id_event ?? null,
            $transaksi->event->produk->nama ?? null,
            $transaksi->nama ?? null,
            $transaksi->panggilan ?? null,
            $transaksi->tgllahir ?? null,
            $transaksi->gender ?? null,
            $transaksi->nohp ?? null,
            $transaksi->email ?? null,
            $transaksi->book->alamat ?? null,
            $transaksi->book->kecamatan ?? null,
            $transaksi->book->kota ?? null,
            $transaksi->book->provinsi ?? null,
            $transaksi->book->kodepos ?? null,
            $transaksi->book->jumlah_buku ?? null,
            $transaksi->book->kurir ?? null,
            $transaksi->book->no_resi ?? null,
            $transaksi->book->ongkir ?? null,
            $transaksi->book->total ?? null,
            $transaksi->agen->name ?? null,
            $transaksi->agen->email ?? null,
            $this->status($transaksi->status) ?? null,
            $this->jenis($transaksi->jenis) ?? null,
            $transaksi->total ?? null,
            $transaksi->created_at ?? null,
            $transaksi->waktu_upload ?? null,
            $transaksi->waktu_konfirmasi ?? null,
        ] ;
    }

    public function headings() : array {
        return [
           'Kode Event',
           'Nama Event',
           'Nama',
           'Panggilan',
           'Tanggal Lahir',
           'Gender',
           'Notelp',
           'Email',
           'Alamat',
           'Kecamatan',
           'Kota',
           'Provinsi',
           'Kodepos',
           'Jumlah Buku',
           'Kurir',
           'No. Resi',
           'Ongkir',
           'Total Harga',
           'Nama Agen',
           'Email Agen',
           'Status',
           'Jenis',
           'Total',
           'Waktu Daftar',
           'Waktu Upload',
           'Waktu Konfirmasi',
        ] ;
    }
}
