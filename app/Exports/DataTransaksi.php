<?php

namespace App\Exports;

use App\Models\Transaction;
use Intervention\Image\Gd\Decoder;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DataTransaksi implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    public function event(int $event)
    {
        $this->event = $event;

        return $this;
    }

    public function query()
    {
        return Transaction::query()->where('id_event', $this->event);
    }

    public function jenis($jenis)
    {
        if ($jenis == 1) {
            return 'MANUAL';
        } elseif ($jenis == 2) {
            return 'AFILIASI';
        } else {
            return null;
        }
    }

    public function status($status)
    {
        if ($status == 1) {
            return 'BELUM AKTIF';
        } elseif ($status == 2) {
            return 'MENUNGGU KONFIRMASI';
        } elseif ($status == 3) {
            return 'AKTIF';
        } else {
            return null;
        }
    }

    public function r_order($idevent, $email, $nohp)
    {
        $this->e = $email;
        $this->n = $nohp;
        $dataro = Transaction::where('id_event', 'NOT LIKE', $idevent)
                                ->where(function ($query) {
                                    $query->where('email', '=', $this->e)
                                        ->orWhere('nohp', '=', $this->n);
                                })
                                ->aktif()
                                ->get();
        foreach($dataro as $data){

            $d[] = array(
                $data->event->id_event
            );
        }

        return $d ?? null;
    }

    public function map($transaksi) : array {
        return [
            $transaksi->event->id_event ?? null,
            $transaksi->event->produk->nama ?? null,
            $transaksi->nama ?? null,
            $transaksi->panggilan ?? null,
            $transaksi->tgllahir ?? null,
            $transaksi->gender ?? null,
            $transaksi->nohp ?? null,
            $transaksi->email ?? null,
            $transaksi->agen->name ?? null,
            $transaksi->agen->email ?? null,
            $transaksi->agen->subagen->email ?? null,
            $this->status($transaksi->status) ?? null,
            $this->jenis($transaksi->jenis) ?? null,
            $transaksi->total ?? null,
            $transaksi->created_at ?? null,
            $transaksi->waktu_upload ?? null,
            $transaksi->waktu_konfirmasi ?? null,
            $this->r_order($transaksi->id_event, $transaksi->email, $transaksi->nohp) ?? null,
        ] ;
    }

    public function headings() : array {
        return [
           'Kode Event',
           'Nama Event',
           'Nama',
           'Panggilan',
           'Tanggal Lahir',
           'Gender',
           'Notelp',
           'Email',
           'Nama Agen',
           'Email Agen',
           'Sub Agen Dari',
           'Status',
           'Jenis',
           'Total',
           'Waktu Daftar',
           'Waktu Upload',
           'Waktu Konfirmasi',
           'Repeat Order',
        ] ;
    }
}
