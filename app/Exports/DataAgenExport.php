<?php

namespace App\Exports;

use App\Models\Biodatum;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
// use Maatwebsite\Excel\Concerns\FromCollection;

class DataAgenExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements WithCustomValueBinder, FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    public function produk(int $produk)
    {
        $this->produk = $produk;

        return $this;
    }

    public function query()
    {
        return Biodatum::query()->where(function($query) {
                            $query->where('status_user', '1')
                            ->where('aktif', '2');
                        })
                        ->orWhere(function($query) {
                            $query->where('aktif', '=', '2')
                                ->where('idprodukreg', '=', '24');
                        });
    }

    // public function collection()
    // {
    //     return Biodatum::with('pemesanan')->where('idprodukreg', 25)->get();
    // }

    public function map($agen) : array {
        return [
            $agen->nama,
            $agen->panggilan,
            $agen->email,
            strval($agen->no_telp),
            $agen->gender,
            $agen->nama_rek,
            $agen->bank_rek,
            strval($agen->rek_rek),
            $agen->kota,
            Carbon::parse($agen->tgl_daftar)->isoFormat('DD MMMM YYYY'),
        ] ;
    }

    public function headings() : array {
        return [
           'Nama Lengkap Agen',
           'Nama Panggilan',
           'Email',
           'Nomor Telepon',
           'Gender',
           'Nama Rekening',
           'Bank',
           'Nomor Rekening',
           'Domisili',
           'Terdaftar',
        ] ;
    }
}
