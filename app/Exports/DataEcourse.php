<?php

namespace App\Exports;

use App\Models\Biodatum;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Arr;
// use Maatwebsite\Excel\Concerns\FromCollection;

class DataEcourse implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    public function produk(int $produk)
    {
        $this->produk = $produk;

        return $this;
    }

    public function query()
    {
        return Biodatum::query()->with('agen')->with('pemesanan')->where('idprodukreg', $this->produk);
    }

    // public function collection()
    // {
    //     return Biodatum::with('pemesanan')->where('idprodukreg', 25)->get();
    // }

    public function map($biodata) : array {
        return [
            $biodata->nama,
            $biodata->panggilan,
            $biodata->gender,
            $biodata->no_telp,
            $biodata->email,
            Arr::get($biodata->agen, 'nama'),
            Arr::get($biodata->agen, 'email'),
            $biodata->status_user,
            $biodata->aktif,
            $biodata->tgl_daftar,
            $biodata->alamat,
            $biodata->provinsi,
            $biodata->kota,
            $biodata->kecamatan,
            $biodata->kodepos,
            $biodata->kurir,
            $biodata->noresi,
            $biodata->jumlah,
            $biodata->ongkir,
            $biodata->total,
            $biodata->rek_bayar,
            $biodata->pemesanan->tglpesan ?? '',
            $biodata->waktukonfirmasi,
            $biodata->idprodukreg,
        ] ;
    }

    public function headings() : array {
        return [
           'Nama Customer',
           'Panggilan Customer',
           'Gender Customer',
           'Notelp Customer',
           'Email Customer',
           'Nama Agen',
           'Email Agen',
           'Status Customer',
           'Aktif',
           'Alamat',
           'Provinsi',
           'Kota',
           'Kecamatan',
           'Kodepos',
           'Kurir',
           'No Resi',
           'Jumlah Buku',
           'Ongkir',
           'Total',
           'Rek. Transfer',
           'Waktu Daftar',
           'Waktu Upload',
           'Waktu Konfirmasi',
           'Kode Produk',
        ] ;
    }
}
