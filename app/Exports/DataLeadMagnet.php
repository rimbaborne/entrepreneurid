<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\Models\Auth\Agen;
use App\Models\MemberEbook;

class DataLeadMagnet implements FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    public function query()
    {
        return MemberEbook::query();
    }

    public function agens_($iddagen)
    {
        $data = Agen::find($iddagen);
        $h = $data->name.' - '.$data->email;

        return  $h;
    }

    public function map($leadmagnet) : array {
        return [
            $leadmagnet->nama ?? null,
            $leadmagnet->email ?? null,
            $leadmagnet->registrasi ?? null,
            Agen::find($leadmagnet->ref) ? $this->agens_($leadmagnet->ref) : null,
        ];
    }

    public function headings() : array {
        return [
           'Nama',
           'Email',
           'Waktu Daftar',
           'Agen',
        ] ;
    }
}
