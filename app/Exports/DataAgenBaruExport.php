<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use App\Models\Auth\Agen;
// use Maatwebsite\Excel\Concerns\FromCollection;

class DataAgenBaruExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements WithCustomValueBinder, FromQuery, WithHeadings, WithMapping, ShouldAutoSize
{
    use Exportable;

    public function query()
    {
        return Agen::query()->where('status_aktif', 1);
    }

    public function map($agen) : array {
        return [
            $agen->name,
            $agen->email,
            strval($agen->notelp),
            $agen->gender,
            $agen->nama_rek,
            $agen->bank_rek,
            strval($agen->no_rek),
            $agen->kota,
            Carbon::parse($agen->created_at)->isoFormat('DD MMMM YYYY'),
            $agen->subagen->email ?? null,
        ] ;
    }

    public function headings() : array {
        return [
           'Nama Lengkap Agen',
           'Email',
           'Nomor Telepon',
           'Gender',
           'Nama Rekening',
           'Bank',
           'Nomor Rekening',
           'Domisili',
           'Terdaftar',
           'Sub Agen Dari',
        ] ;
    }
}
