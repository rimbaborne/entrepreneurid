<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PraktikumsNilaibabs extends Model
{
    protected $fillable = [
        'id_praktikum_bab',
        'user_praktikum_nilai',
        'deskripsi_praktikum_nilai',
        'nilai_praktikum_nilai',
        'status_praktikum_nilai'
    ];
}