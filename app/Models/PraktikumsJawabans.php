<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PraktikumsJawabans extends Model
{
    protected $fillable = [
        'id_praktikum_pertanyaan',
        'user_praktikum_jawaban',
        'deskripsi_praktikum_jawaban',
        'jenis_praktikum_jawaban',
        'nilai_praktikum_jawaban',
        'status_praktikum_jawaban'
    ];
}