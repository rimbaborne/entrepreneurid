<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SlideAgen extends Model
{
    protected $fillable = [
        'nama',
        'gambar',
        'url',
        'sort',
        'aktif',
    ];
}
