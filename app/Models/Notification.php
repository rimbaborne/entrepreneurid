<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'id_event',
        'email_pendaftaran',
        'email_upload_pembayaran',
        'email_konfirmasi_pembayaran',
        'email_upload_ulang',
        'wa_pendaftaran',
        'wa_konfirmasi_pembayaran',
        'wa_belum_upload',
        'wa_upload_ulang',
        'status',
        'keterangan'
    ];
}
