<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Event;
use App\Models\BookTransactions;
use App\Models\Auth\Agen;
use Illuminate\Database\Eloquent\Builder;

class Transaction extends Model
{
    protected $fillable = [
        'uuid',
        'id_event',
        'id_agen',
        'email',
        'nama',
        'nohp',
        'panggilan',
        'tgllahir',
        'gender',
        'domisili',
        'konfirmasi',
        'tolaktf',
        'status',
        'jenis',
        'bukti_tf',
        'waktu_upload',
        'waktu_konfirmasi',
        'waktu_tolaktf',
        'total',
        'komisi',
        'kodeunik',
        'Keterangan'
    ];

    public function event()
    {
        return $this->hasOne(Event::class, 'id', 'id_event');
    }

    public function book()
    {
        return $this->belongsTo(BookTransactions::class, 'id', 'id_transaction');
    }

    public function agen()
    {
        return $this->belongsTo(Agen::class, 'id_agen', 'id');
    }

    public function childEmail()
    {
        return $this->hasOne(Transaction::class, 'email', 'email')->where('id_event', '!=', $this->i_);
    }

    public function childNohp()
    {
        return $this->hasOne(Transaction::class, 'nohp', 'nohp')->where('id_event', '!=', $this->i_);
    }

    public function rorder($idevent, $email, $nohp)
    {
        $this->e = $email;
        $this->n = $nohp;
        $dataro = Transaction::where('id_event', '!=', $idevent)
                                ->where(function ($query) {
                                    $query->where('email', '=', $this->e)
                                        ->orWhere('nohp', '=', $this->n);
                                })
                                ->get();
        foreach($dataro as $data){
            if ($data->agen) {
                $d[] = array(
                    'idevent'    => $data->event->id_event,
                    'id_event'   => $data->id_event,
                    'nama_agen'  => $data->agen->name,
                    'email_agen' => $data->agen->email,
                    'created_at' => $data->created_at,
                    'idagen'     => $data->id_agen,
                    'email'      => $data->email,
                );
            } else {
                $d[] = $d[] = array(
                    'idevent'    => '-',
                    'id_event'   => '-',
                    'nama_agen'  => '-',
                    'email_agen' => '-',
                    'created_at' => '-',
                    'idagen'     => '-',
                    'email'      => '-',
                );
            }

        }

        return $d ?? null;
    }

    // ekspetasi rumit
    // public function scopeRorder($query, $roder, $idevent, $email, $nohp)
    // {
    //     $this->i_ = $idevent ?? null;
    //     $this->e_ = $email ?? null;
    //     $this->n_ = $nohp ?? null;
    //     return $query->when($roder, function ($q) {
    //                 return $q->whereHas('child', function (Builder $qu) {
    //                     $qu->where('id_event', 'NOT LIKE',  $this->i_)
    //                         ->where(function ($que) {
    //                             $que->where('email', '=', $this->e_)
    //                                 ->orWhere('nohp', '=', $this->n_);
    //                         })
    //                         ->aktif();
    //                 });
    //             });
    // }

    public function scopeRoderr($query, $roder, $event)
    {
        $this->i_ = $event;
        return $query->when($roder, function ($q) {
                    return $q->whereHas('childEmail')
                                ->orWhereHas('childNohp');
                });
    }

    public function scopeWaktuAwal($query, $awal)
    {
        return $query->when($awal, function ($q, $awal) {
                    return $q->where('created_at', '=>', $awal);
                });
    }

    public function scopeWaktuAkhir($query, $akhir)
    {
        return $query->when($akhir, function ($q, $akhir) {
                    return $q->where('created_at', '=<', $akhir);
                });
    }

    public function scopeOfEvent($query, $event)
    {
        return $query->when($event, function ($q, $event) {
            return $q->where('id_event', '=', $event);
        });
    }

    public function scopeAfiliasi($query)
    {
        return $query->where('jenis', '=', 2);
    }

    public function scopeManual($query)
    {
        return $query->where('jenis', '=', 1);
    }

    public function scopeBelumAktif($query)
    {
        return $query->where('status', '=', 1);
    }

    public function scopeMenungguKonfirmasi($query)
    {
        return $query->where('status', '=', 2);
    }

    public function scopeAktif($query)
    {
        return $query->where('status', '=', 3);
    }

    public function scopeDataAgen($query, $idagen)
    {
        return $query->where('id_agen', '=', $idagen);
    }
}
