<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Produk;
use App\Models\Notification;
use App\Models\Transaction;

class Event extends Model
{
    protected $fillable = [
        'produk_id',
        'id_event',
        'open',
        'publish',
        'harga',
        'komisi',
        'open_jv',
        'publish_jv',
        'komisi_jv',
        'gambar',
        'link_sales',
        'link_form',
        'kode_akses',
        'start',
        'end',
        'keterangan',
        'leaderboard',
    ];

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id');
    }

    public function notifikasi()
    {
        return $this->hasOne(Notification::class, 'id_event', 'id');
    }

    public function transaksi()
    {
        return $this->hasMany(Transaction::class, 'id_event', 'id');
    }

    public function scopePublish($query)
    {
        return $query->where('publish', '=', 1)->where('leaderboard', '=', true);
    }
}
