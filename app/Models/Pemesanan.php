<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pemesanan extends Model
{
    public $table = 'pemesanan';
    protected $fillable = [
        'idcustomer',
        'idagen',
        'idproduk',
        'valid',
        'buktitf',
        'tglpesan',
    ];
}
