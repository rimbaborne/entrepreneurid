<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Traits\Attribute\BiodatumAttribute;
use App\Models\Pemesanan;

class Biodatum extends Model
{
    use BiodatumAttribute,
        SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idagenbaru',
        'nama',
        'email',
        'passwordd',
        'no_telp',
        'telegram',
        'tgl_daftar',
        'alamat',
        'kota',
        'status_user',
        'ref',
        'aktif',
        'closing',
        'panggilan',
        'username',
        'tgllahir',
        'gender',
        'nama_rek',
        'rek_rek',
        'bank_rek',
        'idprodukreg',
        'provinsi',
        'kecamatan',
        'kodepos',
        'noresi',
        'berat',
        'jumlah',
        'kurir',
        'harga',
        'ongkir',
        'total',
        'rek_bayar',
        'manual',
        'tolaktf',
        'waktutolaktf',
        'waktukonfirmasi',
        'kodeunik',
    ];

    public function children()
    {
        return $this->hasMany(Biodatum::class, 'ref');
    }

    public function parent()
    {
        return $this->belongsTo(Biodatum::class, 'ref');
    }

    public function agen()
    {
        return $this->belongsTo(Biodatum::class, 'ref');
    }

    public function pemesanan() {
        return $this->hasOne(Pemesanan::class, 'idcustomer');
    }
}
