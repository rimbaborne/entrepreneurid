<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberMicAdvisor extends Model
{
    public $table = 'member_mic_advisor';

    protected $fillable = [
        'member',
        'user',
        'status',
        'advisor',
    ];
}
