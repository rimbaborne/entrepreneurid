<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberEbook extends Model
{
    public $table = 'member_ebook';

    protected $fillable = [
        'nama',
        'email',
        'registrasi',
        'ref',
    ];
}
