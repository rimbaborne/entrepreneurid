<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookTransactions extends Model
{
    public $table = 'book_transactions';

    protected $fillable = [
        'uuid',
        'id_transaction',
        'alamat',
        'kecamatan',
        'kota',
        'provinsi',
        'negara',
        'kodepos',
        'alamat_pengiriman',
        'kota_pengiriman',
        'kurir',
        'harga',
        'ongkir',
        'jumlah_buku',
        'waktu_pengiriman',
        'total',
        'no_resi',
        'tanggal_pengiriman',
    ];
}
