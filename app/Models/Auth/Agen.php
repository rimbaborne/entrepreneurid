<?php

namespace App\Models\Auth;

use App\Notifications\Agen\Auth\ResetPassword;
use App\Notifications\Agen\Auth\VerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Transaction;

class Agen extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'kode_notelp',
        'notelp',
        'username',
        'tgl_lahir',
        'alamat',
        'kota',
        'gender',
        'nama_rek',
        'no_rek',
        'bank_rek',
        'status_agen',
        'sub_agen_id',
        'status_aktif',
        'foto',
        'produk'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    public function subagen()
    {
        return $this->belongsTo($this, 'status_agen');
    }

    public function totaltransaksisubagen($id)
    {
        $total = Transaction::where('id_agen', $id)->where('status', '=', 3)->count();
        return $total;
    }

    public function scopeTotalTransaksiAktif($query)
    {
        return $query->where('status', '=', 3)->count();
    }
}
