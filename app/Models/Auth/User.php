<?php

namespace App\Models\Auth;

use App\Models\Auth\Traits\Scope\UserScope;
use App\Models\Auth\Traits\Method\UserMethod;
use App\Models\Auth\Traits\Attribute\UserAttribute;
use App\Models\Auth\Traits\Relationship\UserRelationship;
use App\Models\PraktikumsNilaibabs;
use App\Models\PraktikumsJawabans;


/**
 * Class User.
 */
class User extends BaseUser
{
    use UserAttribute,
        UserMethod,
        UserRelationship,
        UserScope;

    public function praktikumnilai()
    {
        return $this->hasMany(PraktikumsNilaibabs::class, 'user_praktikum_nilai');
    }

    public function praktikumjawaban()
    {
        return $this->hasMany(PraktikumsJawabans::class, 'user_praktikum_jawaban');
    }
}