<?php

namespace App\Listeners\Backend\Biodatum;

/**
 * Class BiodatumEventListener.
 */
class BiodatumEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->biodatum->nama;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->biodatum->nama;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->biodatum->nama;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Biodatum\BiodatumCreated::class,
            'App\Listeners\Backend\Biodatum\BiodatumEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Biodatum\BiodatumUpdated::class,
            'App\Listeners\Backend\Biodatum\BiodatumEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Biodatum\BiodatumDeleted::class,
            'App\Listeners\Backend\Biodatum\BiodatumEventListener@onDeleted'
        );
    }
}
