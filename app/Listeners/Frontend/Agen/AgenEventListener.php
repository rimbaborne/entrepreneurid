<?php

namespace App\Listeners\Frontend\Agen;

/**
 * Class AgenEventListener.
 */
class AgenEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->agen->id_agen;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->agen->id_agen;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->agen->id_agen;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Agen\AgenCreated::class,
            'App\Listeners\Backend\Agen\AgenEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Agen\AgenUpdated::class,
            'App\Listeners\Backend\Agen\AgenEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Agen\AgenDeleted::class,
            'App\Listeners\Backend\Agen\AgenEventListener@onDeleted'
        );
    }
}
