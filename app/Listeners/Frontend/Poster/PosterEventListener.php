<?php

namespace App\Listeners\Frontend\Poster;

/**
 * Class PosterEventListener.
 */
class PosterEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->poster->nama;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->poster->nama;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->poster->nama;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Poster\PosterCreated::class,
            'App\Listeners\Backend\Poster\PosterEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Poster\PosterUpdated::class,
            'App\Listeners\Backend\Poster\PosterEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Poster\PosterDeleted::class,
            'App\Listeners\Backend\Poster\PosterEventListener@onDeleted'
        );
    }
}
