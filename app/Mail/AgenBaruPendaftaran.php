<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AgenBaruPendaftaran extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address  = 'kontak@entrepreneurid.co';
        $subject  = '[Konfirmasi Bukti Pendaftaran Agen entrepreneurID disini]';
        $name     = 'Tim entrepreneurID';
        $nominal  = '96.000' ;
        return $this->view('frontend.mail.AgenBaruPendaftaran')
            ->from($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with([
                'nama'     => $this->data['nama'],
                'email'    => $this->data['email'],
                'nohp'     => $this->data['no_telp'],
                'nominal'  => $nominal,
            ]);
    }
}
