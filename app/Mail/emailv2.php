<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class emailv2 extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        // $this->data['email'] = 'tes@tes.tes'; // Digunakan ketika edit pakai Mailaeclipse untuk default value
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email     = $this->data['email'];
        $subject   = $this->data['subject'];
        $name      = $this->data['name'];
        return $this->view('backend.mail.emailv2')
            ->from($email, $name)
            ->replyTo($email, $name)
            ->subject($subject)
            ->with([
                'isiemail'    => $this->data['isiemail'] ?? 'Maaf Terjadi Kesalahan.',
            ]);
    }
}
