<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CNLPendaftaran extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address  = 'kontak@entrepreneurid.co';
        $subject  = '[Konfirmasi Bukti Pemesanan CNL disini]';
        $name     = 'Tim entrepreneurID';
        return $this->view('backend.mail.CNLPendaftaran')
            ->from($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with([
                'nama'     => $this->data['nama'],
                'email'    => $this->data['email'],
                'nohp'     => $this->data['no_telp'],
                'alamat'   => $this->data['alamat'],
                'jumlah'   => $this->data['jumlah'],
                'kurir'    => $this->data['kurir'],
                'total'    => $this->data['total'],
                'kodeunik' => $this->data['kodeunik'],
            ]);
    }
}
