<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class KonfirmasiTransfer extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        // kondisi if harus di coment smentara untuk dapat diedit
        $idproduk = $this->data->idprodukreg;
        // $idproduk = 18;
        if ($idproduk == 6) {
            $produk = 'Copywriting Next Level';
        } elseif ($idproduk == 4) {
            $produk = 'Pendaftaran Agen';
        } elseif ($idproduk == 9) {
            $produk = 'Peta Bisnis Online';
        } elseif ($idproduk == 10) {
            $produk = 'Instant Copywriting';
        } elseif ($idproduk == 21) {
            $produk = 'Copywriting Next Level';
        } elseif ($idproduk == 11) {
            $produk = 'Mentoring Sales Funnel';
        } elseif ($idproduk == 12) {
            $produk = 'Kelas Online Copywriting';
        } elseif ($idproduk == 15) {
            $produk = 'Kelas Online Copywriting';
        } elseif ($idproduk == 17) {
            $produk = 'WhatsApp Master Closing';
        } elseif ($idproduk == 19) {
            $produk = 'Pendaftaran Agen Resmi';
        } elseif ($idproduk == 5) {
            $produk = 'Kelas Facebook Hacking';
        } elseif ($idproduk == 3) {
            $produk = 'Reseller Formula';
        } elseif ($idproduk == 18) {
            $produk = 'WhatsApp Master Closing';
        } elseif ($idproduk == 22) {
            $produk = 'Kelas Online Copywriting';
        } elseif ($idproduk == 23) {
            $produk = 'Mentoring Instant Copywriting';
        } elseif ($idproduk == 24) {
            $produk = 'Agen Resmi entrepreneurID';
        } elseif ($idproduk == 31) {
            $produk = 'Kelas 100 Orderan';
        } elseif ($idproduk == 26) {
            $produk = 'Mentoring Organic Marketing';
        } elseif ($idproduk == 27) {
            $produk = 'Kelas Online Copywriting';
        } elseif ($idproduk == 28) {
            $produk = 'Kelas Database WA';
        } elseif ($idproduk == 29) {
            $produk = 'Copywriting Next Level';
        } elseif ($idproduk == 30) {
            $produk = 'Agen Resmi entrepreneurID';
        } elseif ($idproduk == 33) {
            $produk = 'Mentoring Instant Copywriting';
        } elseif ($idproduk == 34) {
            $produk = 'Kelas Facebook Hacking';
        } elseif ($idproduk == 35) {
            $produk = 'Mentoring Super Reseller';
        } elseif ($idproduk == 39) {
            $produk = 'Agen Resmi entrepreneurID';
        } else {
            $produk = ' -Produk Tidak Terbaca- ';
        }

        $address = 'kontak@entrepreneurid.co';
        $subject = 'Selamat Anda Adalah Peserta ' . $produk;
        $name = 'Tim entrepreneurID';

        if ($idproduk == 23) {
            return $this->view('backend.mail.MICKonfirmasi')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } elseif ($idproduk == 27) {
            return $this->view('backend.mail.KOCKonfirmasiTransfer')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } elseif ($idproduk == 28) {
            return $this->view('backend.mail.KDWKonfirmasiTransfer')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } elseif ($idproduk == 29) {
            $subject = '[Penting ! Untuk Customer Buku Copywriting Next Level]';

            return $this->view('backend.mail.CNLKonfirmasiTransfer')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } elseif ($idproduk == 18) {
            return $this->view('backend.mail.WMCKonfirmasi')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } elseif ($idproduk == 5) {
            return $this->view('backend.mail.KFHKonfirmasi')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } elseif ($idproduk == 24) {
            $address = 'pusat@entrepreneurid.co';
            return $this->view('backend.mail.AgenBaruKonfirmasi')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } elseif ($idproduk == 31) {
            $subject = '[Penting ! Untuk Peserta Kelas 100 Orderan]';

            return $this->view('backend.mail.KSOKonfirmasiTransfer')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } elseif ($idproduk == 26) {
            $subject = '[Penting ! Untuk Peserta Mentoring Organic Marketing]';

            return $this->view('backend.mail.MOMKonfirmasiTransfer')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } elseif ($idproduk == 30) {
            $address = 'pusat@entrepreneurid.co';
            $name = 'entrepreneurID Pusat';
            $subject = '[Selamat ! Anda adalah Agen Resmi entrepreneurID]';

            return $this->view('backend.mail.AgenBaruKonfirmasi')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } elseif ($idproduk == 33) {
            return $this->view('backend.mail.MICKonfirmasiTransfer')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } elseif ($idproduk == 34) {
            return $this->view('backend.mail.KFHKonfirmasiTransfer')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } elseif ($idproduk == 35) {
            return $this->view('backend.mail.MSRKonfirmasiTransfer')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } elseif ($idproduk == 39) {
            $address = 'pusat@entrepreneurid.co';
            $name = 'entrepreneurID Pusat';
            $subject = '[Selamat ! Anda adalah Agen Resmi entrepreneurID]';

            return $this->view('backend.mail.AgenBaruKonfirmasi')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        } else {
            return $this->view('backend.mail.konfirmasiMaster')
                ->from($address, $name)
                ->replyTo($address, $name)
                ->subject($subject)
                ->with(['nama' => $this->data['nama'], 'produk' => $produk]);
        }
    }
}
