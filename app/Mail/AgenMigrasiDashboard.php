<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AgenMigrasiDashboard extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address  = 'pusat@entrepreneurid.co';
        $subject  = '[Migrasi Dashboard Anda Berhasil]';
        $name     = 'Tim entrepreneurID';
        return $this->view('backend.mail.AgenMigrasiDashboard')
            ->from($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with([
                'nama' => $this->data['name'],
            ]);
    }
}
