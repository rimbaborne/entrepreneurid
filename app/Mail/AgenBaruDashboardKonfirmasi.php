<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AgenBaruDashboardKonfirmasi extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address  = 'pusat@entrepreneurid.co';
        $subject  = '[Akun Afiliasi Anda sudah diaktifkan]';
        $name     = 'Tim entrepreneurID';
        return $this->view('backend.mail.AgenBaruDashboardKonfirmasi')
            ->from($address, $name)
            ->replyTo($address, $name)
            ->subject($subject)
            ->with([
                'nama'     => $this->data['name'],
            ]);
    }
}
