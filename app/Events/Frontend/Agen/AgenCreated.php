<?php

namespace App\Events\Frontend\Agen;

use Illuminate\Queue\SerializesModels;

/**
 * Class AgenCreated.
 */
class AgenCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $agens;

    /**
     * @param $agens
     */
    public function __construct($agens)
    {
        $this->agens = $agens;
    }
}
