<?php

namespace App\Events\Frontend\Poster;

use Illuminate\Queue\SerializesModels;

/**
 * Class PosterUpdated.
 */
class PosterUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $posters;

    /**
     * @param $posters
     */
    public function __construct($posters)
    {
        $this->posters = $posters;
    }
}
