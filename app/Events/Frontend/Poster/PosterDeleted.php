<?php

namespace App\Events\Frontend\Poster;

use Illuminate\Queue\SerializesModels;

/**
 * Class PosterDeleted.
 */
class PosterDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $posters;

    /**
     * @param $posters
     */
    public function __construct($posters)
    {
        $this->posters = $posters;
    }
}
