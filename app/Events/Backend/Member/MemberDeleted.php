<?php

namespace App\Events\Backend\Member;

use Illuminate\Queue\SerializesModels;

/**
 * Class MemberDeleted.
 */
class MemberDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $members;

    /**
     * @param $members
     */
    public function __construct($members)
    {
        $this->members = $members;
    }
}
