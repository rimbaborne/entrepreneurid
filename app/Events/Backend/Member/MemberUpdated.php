<?php

namespace App\Events\Backend\Member;

use Illuminate\Queue\SerializesModels;

/**
 * Class MemberUpdated.
 */
class MemberUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $members;

    /**
     * @param $members
     */
    public function __construct($members)
    {
        $this->members = $members;
    }
}
