<?php

namespace App\Events\Backend\Biodatum;

use Illuminate\Queue\SerializesModels;

/**
 * Class BiodatumDeleted.
 */
class BiodatumDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $biodata;

    /**
     * @param $biodata
     */
    public function __construct($biodata)
    {
        $this->biodata = $biodata;
    }
}
