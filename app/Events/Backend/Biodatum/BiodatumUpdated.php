<?php

namespace App\Events\Backend\Biodatum;

use Illuminate\Queue\SerializesModels;

/**
 * Class BiodatumUpdated.
 */
class BiodatumUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $biodata;

    /**
     * @param $biodata
     */
    public function __construct($biodata)
    {
        $this->biodata = $biodata;
    }
}
