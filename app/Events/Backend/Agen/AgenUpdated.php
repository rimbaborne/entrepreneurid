<?php

namespace App\Events\Backend\Agen;

use Illuminate\Queue\SerializesModels;

/**
 * Class AgenUpdated.
 */
class AgenUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $agens;

    /**
     * @param $agens
     */
    public function __construct($agens)
    {
        $this->agens = $agens;
    }
}
