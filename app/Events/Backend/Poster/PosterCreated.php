<?php

namespace App\Events\Backend\Poster;

use Illuminate\Queue\SerializesModels;

/**
 * Class PosterCreated.
 */
class PosterCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $posters;

    /**
     * @param $posters
     */
    public function __construct($posters)
    {
        $this->posters = $posters;
    }
}
