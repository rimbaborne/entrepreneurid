<?php

namespace App\Repositories\Backend;

use App\Models\Agen;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class AgenRepository.
 */
class AgenRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Agen::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Agen
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Agen
    {
        return DB::transaction(function () use ($data) {
            $agen = parent::create([
                'id_agen' => $data['id_agen'],
            ]);

            if ($agen) {
                return $agen;
            }

            throw new GeneralException(__('backend_agens.exceptions.create_error'));
        });
    }

    /**
     * @param Agen  $agen
     * @param array     $data
     *
     * @return Agen
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Agen $agen, array $data) : Agen
    {
        return DB::transaction(function () use ($agen, $data) {
            if ($agen->update([
                'id_agen' => $data['id_agen'],
            ])) {

                return $agen;
            }

            throw new GeneralException(__('backend_agens.exceptions.update_error'));
        });
    }

    /**
     * @param Agen $agen
     *
     * @return Agen
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Agen $agen) : Agen
    {
        if (is_null($agen->deleted_at)) {
            throw new GeneralException(__('backend_agens.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($agen) {
            if ($agen->forceDelete()) {
                return $agen;
            }

            throw new GeneralException(__('backend_agens.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Agen $agen
     *
     * @return Agen
     * @throws GeneralException
     */
    public function restore(Agen $agen) : Agen
    {
        if (is_null($agen->deleted_at)) {
            throw new GeneralException(__('backend_agens.exceptions.cant_restore'));
        }

        if ($agen->restore()) {
            return $agen;
        }

        throw new GeneralException(__('backend_agens.exceptions.restore_error'));
    }
}
