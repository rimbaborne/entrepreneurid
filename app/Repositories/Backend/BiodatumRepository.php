<?php

namespace App\Repositories\Backend;

use App\Models\Biodatum;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class BiodatumRepository.
 */
class BiodatumRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Biodatum::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc'): LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Biodatum
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data): Biodatum
    {
        return DB::transaction(function () use ($data) {
            $biodatum = parent::create([
                'idprodukreg' => $data['produk'],
                'ref'         => $data['ref'],
                'nama'        => $data['nama'],
                'email'       => $data['email'],
                'no_telp'     => $data['no_telp'],
                'panggilan'      => $data['panggilan'],
                'kota'        => $data['kota'],
                // 'jumlah'      => $data['jumlah'],
                // 'ongkir'      => $data['ongkir'],
                // 'total'       => $data['total'],
                'status_user' => 2,
                'aktif'       => 0,
            ]);

            if ($biodatum) {
                return $biodatum;
            }

            throw new GeneralException(__('backend_biodata.exceptions.create_error'));
        });
    }

    /**
     * @param Biodatum  $biodatum
     * @param array     $data
     *
     * @return Biodatum
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Biodatum $biodatum, array $data): Biodatum
    {
        return DB::transaction(function () use ($biodatum, $data) {
            if ($biodatum->update([
                'nama' => $data['nama'],
                'email' => $data['email'],
                'no_telp' => $data['no_telp'],
                // 'alamat' => $data['alamat'],
                'kota' => $data['kota'],
                'panggilan' => $data['panggilan'],
                'tgllahir' => $data['tgllahir'],
                'gender' => $data['gender'],
            ])) {

                return $biodatum;
            }

            throw new GeneralException(__('backend_biodata.exceptions.update_error'));
        });
    }

    /**
     * @param Biodatum $biodatum
     *
     * @return Biodatum
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Biodatum $biodatum): Biodatum
    {
        if (is_null($biodatum->deleted_at)) {
            throw new GeneralException(__('backend_biodata.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($biodatum) {
            if ($biodatum->forceDelete()) {
                return $biodatum;
            }

            throw new GeneralException(__('backend_biodata.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Biodatum $biodatum
     *
     * @return Biodatum
     * @throws GeneralException
     */
    public function restore(Biodatum $biodatum): Biodatum
    {
        if (is_null($biodatum->deleted_at)) {
            throw new GeneralException(__('backend_biodata.exceptions.cant_restore'));
        }

        if ($biodatum->restore()) {
            return $biodatum;
        }

        throw new GeneralException(__('backend_biodata.exceptions.restore_error'));
    }
}
