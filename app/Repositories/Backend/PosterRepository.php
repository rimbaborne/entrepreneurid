<?php

namespace App\Repositories\Backend;

use App\Models\Poster;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class PosterRepository.
 */
class PosterRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Poster::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Poster
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Poster
    {
        return DB::transaction(function () use ($data) {
            $poster = parent::create([
                'nama' => $data['nama'],
            ]);

            if ($poster) {
                return $poster;
            }

            throw new GeneralException(__('backend_posters.exceptions.create_error'));
        });
    }

    /**
     * @param Poster  $poster
     * @param array     $data
     *
     * @return Poster
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Poster $poster, array $data) : Poster
    {
        return DB::transaction(function () use ($poster, $data) {
            if ($poster->update([
                'nama' => $data['nama'],
            ])) {

                return $poster;
            }

            throw new GeneralException(__('backend_posters.exceptions.update_error'));
        });
    }

    /**
     * @param Poster $poster
     *
     * @return Poster
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Poster $poster) : Poster
    {
        if (is_null($poster->deleted_at)) {
            throw new GeneralException(__('backend_posters.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($poster) {
            if ($poster->forceDelete()) {
                return $poster;
            }

            throw new GeneralException(__('backend_posters.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Poster $poster
     *
     * @return Poster
     * @throws GeneralException
     */
    public function restore(Poster $poster) : Poster
    {
        if (is_null($poster->deleted_at)) {
            throw new GeneralException(__('backend_posters.exceptions.cant_restore'));
        }

        if ($poster->restore()) {
            return $poster;
        }

        throw new GeneralException(__('backend_posters.exceptions.restore_error'));
    }
}
