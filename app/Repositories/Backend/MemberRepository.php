<?php

namespace App\Repositories\Backend;

use App\Models\Member;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class MemberRepository.
 */
class MemberRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Member::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Member
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Member
    {
        return DB::transaction(function () use ($data) {
            $member = parent::create([
                'produk' => $data['produk'],
            ]);

            if ($member) {
                return $member;
            }

            throw new GeneralException(__('backend_members.exceptions.create_error'));
        });
    }

    /**
     * @param Member  $member
     * @param array     $data
     *
     * @return Member
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Member $member, array $data) : Member
    {
        return DB::transaction(function () use ($member, $data) {
            if ($member->update([
                'produk' => $data['produk'],
            ])) {

                return $member;
            }

            throw new GeneralException(__('backend_members.exceptions.update_error'));
        });
    }

    /**
     * @param Member $member
     *
     * @return Member
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Member $member) : Member
    {
        if (is_null($member->deleted_at)) {
            throw new GeneralException(__('backend_members.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($member) {
            if ($member->forceDelete()) {
                return $member;
            }

            throw new GeneralException(__('backend_members.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Member $member
     *
     * @return Member
     * @throws GeneralException
     */
    public function restore(Member $member) : Member
    {
        if (is_null($member->deleted_at)) {
            throw new GeneralException(__('backend_members.exceptions.cant_restore'));
        }

        if ($member->restore()) {
            return $member;
        }

        throw new GeneralException(__('backend_members.exceptions.restore_error'));
    }
}
