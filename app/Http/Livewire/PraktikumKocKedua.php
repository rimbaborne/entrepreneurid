<?php

namespace App\Http\Livewire;

use App\Models\PraktikumsJawabans;
use App\Models\PraktikumsNilaibabs;
use Livewire\Component;
use Illuminate\Support\Facades\DB;

class PraktikumKocKedua extends Component
{
    public $jawaban1bagian1 = null;
    public $jawaban1bagian2 = null;
    public $jawaban1bagian3 = null;
    public $jawaban1bagian4 = null;
    public $jawaban1bagian5 = null;
    public $djawaban1bagian1 = null;
    public $djawaban1bagian2 = null;
    public $djawaban1bagian3 = null;
    public $djawaban1bagian4 = null;
    public $djawaban1bagian5 = null;

    public function ceknilai() {
        $nilaibabs = new PraktikumsNilaibabs;

        $ceknilai  = $nilaibabs->where('id_praktikum_bab', 2)->where('user_praktikum_nilai', auth()->user()->id)->first();
        if (isset($ceknilai)) {
            $ceknilai->update([
                'status_praktikum_nilai' => 'Menunggu Koreksi',
            ]);
        } else {
            $nilaibabs->create([
                'id_praktikum_bab' => 2,
                'user_praktikum_nilai' => auth()->user()->id,
                'status_praktikum_nilai' => 'Menunggu Koreksi',
            ]);
        }
    }

    public function bagiansatu() {
        $this->ceknilai();

        $jawabans  = new PraktikumsJawabans;

        $data1  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 12)->first();

        if (isset($data1)) {
            $data1->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian1 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian1 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'12'
                        ]);
        }
        session()->flash('satu');

    }

    public function bagiandua() {
        $this->ceknilai();

        $jawabans  = new PraktikumsJawabans;

        $data2  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 13)->first();

        if (isset($data2)) {
            $data2->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian2 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian2 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'13'
                        ]);
        }
        session()->flash('dua');

    }

    public function bagiantiga() {
        $this->ceknilai();

        $jawabans  = new PraktikumsJawabans;

        $data3  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 14)->first();

        if (isset($data3)) {
            $data3->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian3 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian3 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'14'
                        ]);
        }
        session()->flash('tiga');

    }

    public function bagianempat() {
        $this->ceknilai();

        $jawabans  = new PraktikumsJawabans;

        $data4  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 15)->first();

        if (isset($data4)) {
            $data4->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian4 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian4 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'15'
                        ]);
        }
        session()->flash('empat');

    }

    public function bagianlima() {
        $this->ceknilai();

        $jawabans  = new PraktikumsJawabans;

        $data5  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 16)->first();

        if (isset($data5)) {
            $data5->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian5 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian5 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'16'
                        ]);
        }
        session()->flash('lima');

    }


    public function mount(){
        $jawabanpraktikum = new PraktikumsJawabans;
        $datajawaban1bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 12)->first();
        $datajawaban1bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 13)->first();
        $datajawaban1bagian3 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 14)->first();
        $datajawaban1bagian4 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 15)->first();
        $datajawaban1bagian5 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 16)->first();

        $this->jawaban1bagian1 = $datajawaban1bagian1->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban1bagian2 = $datajawaban1bagian2->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban1bagian3 = $datajawaban1bagian3->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban1bagian4 = $datajawaban1bagian4->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban1bagian5 = $datajawaban1bagian5->deskripsi_praktikum_jawaban ?? null;

        $this->djawaban1bagian1 = $datajawaban1bagian1;
        $this->djawaban1bagian2 = $datajawaban1bagian2;
        $this->djawaban1bagian3 = $datajawaban1bagian3;
        $this->djawaban1bagian4 = $datajawaban1bagian4;
        $this->djawaban1bagian5 = $datajawaban1bagian5;
    }
    public function render()
    {
        $jawabanpraktikum = new PraktikumsJawabans;
        $datajawaban1bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 12)->first();
        $datajawaban1bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 13)->first();
        $datajawaban1bagian3 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 14)->first();
        $datajawaban1bagian4 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 15)->first();
        $datajawaban1bagian5 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 16)->first();

        $this->djawaban1bagian1 = $datajawaban1bagian1;
        $this->djawaban1bagian2 = $datajawaban1bagian2;
        $this->djawaban1bagian3 = $datajawaban1bagian3;
        $this->djawaban1bagian4 = $datajawaban1bagian4;
        $this->djawaban1bagian5 = $datajawaban1bagian5;

        return view('livewire.praktikum-koc-kedua');
    }
}
