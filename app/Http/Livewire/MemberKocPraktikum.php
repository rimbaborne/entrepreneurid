<?php

namespace App\Http\Livewire;

use App\Models\PraktikumsJawabans;
use App\Models\PraktikumsNilaibabs;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Request;

class MemberKocPraktikum extends Component
{
    public $iduser           = '';
    public $statusnilai      = '';
    public $deskripsinilai   = '';
    public $statusnilaihasil = '';
    public $waktustatusnilai = '';
    public $jawaban1bagian1  = null;
    public $jawaban2bagian1  = null;
    public $jawaban3bagian1  = null;
    public $jawaban4bagian1  = null;
    public $jawaban5bagian1  = null;
    public $jawaban6bagian1  = null;
    public $jawaban1bagian2  = null;
    public $jawaban2bagian2  = null;
    public $jawaban3bagian2  = null;
    public $jawaban4bagian2  = null;
    public $jawaban1bagian3  = null;
    public $jawaban1bagian4  = null;
    public $jawaban1bagian5  = null;
    public $jawaban1bagian6  = null;
    public $djawaban1bagian1 = null;
    public $djawaban2bagian1 = null;
    public $djawaban3bagian1 = null;
    public $djawaban4bagian1 = null;
    public $djawaban5bagian1 = null;
    public $djawaban6bagian1 = null;
    public $djawaban1bagian2 = null;
    public $djawaban2bagian2 = null;
    public $djawaban3bagian2 = null;
    public $djawaban4bagian2 = null;
    public $djawaban1bagian3 = null;
    public $djawaban1bagian4 = null;
    public $djawaban1bagian5 = null;
    public $djawaban1bagian6 = null;
    public $jawaban2bagian3 = null;
    public $jawaban2bagian4 = null;
    public $jawaban2bagian5 = null;
    public $wab13 = null;
    public $p1j13 = null;
    public $p2j13 = null;
    public $p3j13 = null;
    public $wab14 = null;
    public $p1j14 = null;
    public $p2j14 = null;
    public $p3j14 = null;
    public $p4j14 = null;
    public $wab15 = null;
    public $p1j15 = null;
    public $p2j15 = null;
    public $p3j15 = null;
    public $p4j15 = null;
    public $p5j15 = null;
    public $p6j15 = null;

    public function penilaian(){
        $datanilai  = PraktikumsNilaibabs::where('user_praktikum_nilai', request()->query('id'))->where('id_praktikum_bab', request()->query('ke'))->first();

        if (isset($datanilai)) {
            PraktikumsNilaibabs::where('user_praktikum_nilai', request()->query('id'))->where('id_praktikum_bab', request()->query('ke'))
                        ->update([
                            'status_praktikum_nilai'=>$this->statusnilai ?? ' ',
                            'deskripsi_praktikum_nilai'=>$this->deskripsinilai ?? ' ',
                        ]);
        } else {
            PraktikumsNilaibabs::where('user_praktikum_nilai', request()->query('id'))->where('id_praktikum_bab', request()->query('ke'))
                        ->create([
                            'status_praktikum_nilai'=>$this->statusnilai ?? ' ',
                            'deskripsi_praktikum_nilai'=>$this->deskripsinilai ?? ' ',
                            'user_praktikum_nilai'=>request()->query('id'),
                            'id_praktikum_bab'=>'1'
                        ]);
        }

        $this->statusnilaihasil = $this->statusnilai;
    }

    public function mount(){
        $jawabanpraktikum = new PraktikumsJawabans;

        if ( request()->query('ke') === '1') {
            $datajawaban1bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 1)->first();
            $datajawaban2bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 2)->first();
            $datajawaban3bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 3)->first();
            $datajawaban4bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 4)->first();
            $datajawaban5bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 5)->first();
            $datajawaban6bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 6)->first();
            $datajawaban1bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 7)->first();
            $datajawaban2bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 8)->first();
            $datajawaban3bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 9)->first();
            $datajawaban4bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 10)->first();
            $datajawaban1bagian3 = $jawabanpraktikum->where('user_praktikum_jawaban',request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 11)->first();

            $this->jawaban1bagian1 = $datajawaban1bagian1->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban2bagian1 = $datajawaban2bagian1->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban3bagian1 = $datajawaban3bagian1->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban4bagian1 = $datajawaban4bagian1->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban5bagian1 = $datajawaban5bagian1->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban6bagian1 = $datajawaban6bagian1->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban1bagian2 = $datajawaban1bagian2->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban2bagian2 = $datajawaban2bagian2->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban3bagian2 = $datajawaban3bagian2->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban4bagian2 = $datajawaban4bagian2->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban1bagian3 = $datajawaban1bagian3->deskripsi_praktikum_jawaban ?? null;

            $this->djawaban1bagian1 = $datajawaban1bagian1;
            $this->djawaban1bagian2 = $datajawaban1bagian2;
            $this->djawaban1bagian3 = $datajawaban1bagian3;

        } elseif ( request()->query('ke') === '2') {

            $datajawaban1bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban', request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 12)->first();
            $datajawaban1bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban', request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 13)->first();
            $datajawaban1bagian3 = $jawabanpraktikum->where('user_praktikum_jawaban', request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 14)->first();
            $datajawaban1bagian4 = $jawabanpraktikum->where('user_praktikum_jawaban', request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 15)->first();
            $datajawaban1bagian5 = $jawabanpraktikum->where('user_praktikum_jawaban', request()->query('id'))
                                                    ->where('id_praktikum_pertanyaan', 16)->first();

            $this->jawaban1bagian1 = $datajawaban1bagian1->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban1bagian2 = $datajawaban1bagian2->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban1bagian3 = $datajawaban1bagian3->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban1bagian4 = $datajawaban1bagian4->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban1bagian5 = $datajawaban1bagian5->deskripsi_praktikum_jawaban ?? null;

            $this->djawaban1bagian1 = $datajawaban1bagian1;
            $this->djawaban1bagian2 = $datajawaban1bagian2;
            $this->djawaban1bagian3 = $datajawaban1bagian3;
            $this->djawaban1bagian4 = $datajawaban1bagian4;
            $this->djawaban1bagian5 = $datajawaban1bagian5;
        } elseif ( request()->query('ke') === '3') {
            $datajawaban1bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',
            request()->query('id'))->where('id_praktikum_pertanyaan', 17)->first();
            $datajawaban1bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',
            request()->query('id'))->where('id_praktikum_pertanyaan', 18)->first();
            $datajawaban2bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',
            request()->query('id'))->where('id_praktikum_pertanyaan', 19)->first();
            $datajawaban1bagian3 = $jawabanpraktikum->where('user_praktikum_jawaban',
            request()->query('id'))->where('id_praktikum_pertanyaan', 20)->first();
            $datajawaban2bagian3 = $jawabanpraktikum->where('user_praktikum_jawaban',
            request()->query('id'))->where('id_praktikum_pertanyaan', 21)->first();
            $datajawaban1bagian4 = $jawabanpraktikum->where('user_praktikum_jawaban',
            request()->query('id'))->where('id_praktikum_pertanyaan', 22)->first();
            $datajawaban2bagian4 = $jawabanpraktikum->where('user_praktikum_jawaban',
            request()->query('id'))->where('id_praktikum_pertanyaan', 23)->first();
            $datajawaban1bagian5 = $jawabanpraktikum->where('user_praktikum_jawaban',
            request()->query('id'))->where('id_praktikum_pertanyaan', 24)->first();
            $datajawaban2bagian5 = $jawabanpraktikum->where('user_praktikum_jawaban',
            request()->query('id'))->where('id_praktikum_pertanyaan', 25)->first();
            $datajawaban1bagian6 = $jawabanpraktikum->where('user_praktikum_jawaban',
            request()->query('id'))->where('id_praktikum_pertanyaan', 26)->first();

            $this->jawaban1bagian1 = $datajawaban1bagian1->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban1bagian2 = $datajawaban1bagian2->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban2bagian2 = $datajawaban2bagian2->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban1bagian3 = $datajawaban1bagian3->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban2bagian3 = $datajawaban2bagian3->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban1bagian4 = $datajawaban1bagian4->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban2bagian4 = $datajawaban2bagian4->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban1bagian5 = $datajawaban1bagian5->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban2bagian5 = $datajawaban2bagian5->deskripsi_praktikum_jawaban ?? null;
            $this->jawaban1bagian6 = $datajawaban1bagian6->deskripsi_praktikum_jawaban ?? null;


            $this->djawaban1bagian1 = $datajawaban1bagian1;
            $this->djawaban1bagian2 = $datajawaban1bagian2;
            $this->djawaban1bagian3 = $datajawaban1bagian3;
            $this->djawaban1bagian4 = $datajawaban1bagian4;
            $this->djawaban1bagian5 = $datajawaban1bagian5;
            $this->djawaban1bagian6 = $datajawaban1bagian6;

            $this->p1j13 = strstr($this->jawaban1bagian3,'Delection=1') ? true : false;
            $this->p2j13 = strstr($this->jawaban1bagian3,'Generalitation=1') ? true : false;
            $this->p3j13 = strstr($this->jawaban1bagian3,'Distortion=1') ? true : false;

            $this->p1j14 = strstr($this->jawaban1bagian4,'Kebutuhan=1') ? true : false;
            $this->p2j14 = strstr($this->jawaban1bagian4,'Waktu=1') ? true : false;
            $this->p3j14 = strstr($this->jawaban1bagian4,'Harga=1') ? true : false;
            $this->p4j14 = strstr($this->jawaban1bagian4,'Kualitas=1') ? true : false;

            $this->p1j15 = strstr($this->jawaban1bagian5,'Embedded-Command=1') ? true : false;
            $this->p2j15 = strstr($this->jawaban1bagian5,'Embedded-Question=1') ? true : false;
            $this->p3j15 = strstr($this->jawaban1bagian5,'Question-Tag=1') ? true : false;
            $this->p4j15 = strstr($this->jawaban1bagian5,'Negative-Command=1') ? true : false;
            $this->p5j15 = strstr($this->jawaban1bagian5,'Converstation-Postulates=1') ? true : false;
            $this->p6j15 = strstr($this->jawaban1bagian5,'Ambiguity=1') ? true : false;

            $this->jawaban1bagian3 = ['Delection' => $this->p1j13, 'Generalitation' => $this->p2j13, 'Distortion' => $this->p3j13];
            $this->jawaban1bagian4 = ['Kebutuhan' => $this->p1j14, 'Waktu' => $this->p2j14, 'Harga' => $this->p3j14, 'Kualitas' => $this->p4j14];
            $this->jawaban1bagian5 = ['Embedded-Command' => $this->p1j15, 'Embedded-Question' => $this->p2j15, 'Question-Tag' => $this->p3j15, 'Negative-Command' => $this->p4j15, 'Converstation-Postulates' => $this->p5j15, 'Ambiguity' => $this->p6j15];

        }



        $datanilai = PraktikumsNilaibabs::where('user_praktikum_nilai',request()->query('id'))->where('id_praktikum_bab', request()->query('ke'))->first();
        $this->statusnilaihasil = $datanilai->status_praktikum_nilai ?? null;
        $this->deskripsinilai   = $datanilai->deskripsi_praktikum_nilai ?? null;
        $this->statusnilai      = $datanilai->status_praktikum_nilai ?? null;

    }

    public function render()
    {
        $datanilai = PraktikumsNilaibabs::where('user_praktikum_nilai',request()->query('id'))->where('id_praktikum_bab', request()->query('ke'))->first();
        $this->waktustatusnilai = $datanilai;

        $this->iduser = request()->query('id');
        return view('livewire.member-koc-praktikum',
        [
            'memberkocpraktikum' => \App\Models\Auth\User::where('id', $this->iduser)->first(),
        ]
    );
    }
}
