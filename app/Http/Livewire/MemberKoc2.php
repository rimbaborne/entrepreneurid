<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Database\Eloquent\Builder;


class MemberKoc2 extends Component
{
    use WithPagination;

    public $search        = '';
    public $searchnama    = '';
    public $searchemail   = '';
    public $perPage       = 10;
    public $sortAsc       = true;
    public $sortField     = 'created_at';
    public $praktikum     = '1';
    public $status        = '';
    public $urutkanstatus = '';

    public function sortBy($field)
    {
        $this->urutkanstatus = '';

        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
        $this->sortField = $field;
    }

    public function render()
    {
        return view('livewire.member-koc',
        [
            'memberkoc' => \App\Models\Auth\User::where('produk', 'Kelas Online Copywriting')
                ->when($this->search, function ($query) {
                    return $query->where('first_name', 'like', '%' . $this->search . '%')->orWhere('email', 'like', '%' . $this->search . '%');
                })
                ->when($this->status, function ($query) {
                    if ($this->status === '1') {
                        return $query->whereDoesntHave('praktikumnilai', function (Builder $query) {
                            $query->where('id_praktikum_bab', $this->praktikum)->where('status_praktikum_nilai', 'Perlu Diulang');
                        });
                    } elseif ($this->status === '2') {
                        return $query->whereHas('praktikumjawaban');
                    } else {
                        return $query->whereHas('praktikumnilai', function (Builder $query) {
                            $query->where('id_praktikum_bab', $this->praktikum)->where('status_praktikum_nilai', $this->status);
                        });
                    }
                })
                ->when($this->status, function ($query) {
                    if ($this->status === '1') {
                        return $query->whereDoesntHave('praktikumnilai', function (Builder $query) {
                            $query->where('id_praktikum_bab', $this->praktikum)->where('status_praktikum_nilai', 'Lulus Praktikum');;
                        });
                    }
                })
                ->when($this->urutkanstatus, function ($query) {
                    if ($this->urutkanstatus === 'desc') {
                        return $query->with('praktikumnilai')->orderBy('updated_at', 'asc');
                    } elseif ($this->urutkanstatus === 'asc') {
                        return $query->with('praktikumnilai')->orderBy('updated_at', 'desc');
                    }
                })
                ->where('created_at', '>', '2020-11-28')
                ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
                ->paginate($this->perPage),
        ]);
    }
}
