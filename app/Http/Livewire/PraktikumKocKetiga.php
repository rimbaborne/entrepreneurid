<?php

namespace App\Http\Livewire;

use App\Models\PraktikumsJawabans;
use App\Models\PraktikumsNilaibabs;
use Livewire\Component;
use Illuminate\Support\Facades\DB;

class PraktikumKocKetiga extends Component
{
    public $jawaban1bagian1 = null;
    public $jawaban1bagian2 = null;
    public $jawaban1bagian3 = [];
    public $jawaban1bagian4 = [];
    public $jawaban1bagian5 = [];
    public $jawaban2bagian2 = null;
    public $jawaban2bagian3 = null;
    public $jawaban2bagian4 = null;
    public $jawaban2bagian5 = null;
    public $jawaban1bagian6 = null;
    public $djawaban1bagian1 = null;
    public $djawaban1bagian2 = null;
    public $djawaban1bagian3 = null;
    public $djawaban1bagian4 = null;
    public $djawaban1bagian5 = null;
    public $djawaban1bagian6 = null;
    public $wab13 = null;
    public $p1j13 = false;
    public $p2j13 = false;
    public $p3j13 = false;
    public $wab14 = null;
    public $p1j14 = false;
    public $p2j14 = false;
    public $p3j14 = false;
    public $p4j14 = false;
    public $wab15 = null;
    public $p1j15 = false;
    public $p2j15 = false;
    public $p3j15 = false;
    public $p4j15 = false;
    public $p5j15 = false;
    public $p6j15 = false;

    public function ceknilai() {
        $nilaibabs = new PraktikumsNilaibabs;

        $ceknilai  = $nilaibabs->where('id_praktikum_bab', 3)->where('user_praktikum_nilai', auth()->user()->id)->first();
        if (isset($ceknilai)) {
            $ceknilai->update([
                'status_praktikum_nilai' => 'Menunggu Koreksi',
            ]);
        } else {
            $nilaibabs->create([
                'id_praktikum_bab' => 3,
                'user_praktikum_nilai' => auth()->user()->id,
                'status_praktikum_nilai' => 'Menunggu Koreksi',
            ]);
        }
    }

    public function bagiansatu() {
        $this->ceknilai();

        $jawabans  = new PraktikumsJawabans;

        $data1  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 17)->first();

        if (isset($data1)) {
            $data1->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian1 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian1 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'17'
                        ]);
        }
        session()->flash('satu');

    }

    public function bagiandua() {
        $this->ceknilai();

        $jawabans  = new PraktikumsJawabans;

        $data2  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 18)->first();
        $data3  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 19)->first();

        if (isset($data2)) {
            $data2->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian2 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian2 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'18'
                        ]);
        }

        if (isset($data3)) {
            $data3->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban2bagian2 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban2bagian2 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'19'
                        ]);
        }
        session()->flash('dua');

    }

    public function bagiantiga() {
        $this->ceknilai();

        $jawabans  = new PraktikumsJawabans;

        $data4  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 20)->first();
        $data5  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 21)->first();

        $this->wab13 = null;
        foreach($this->jawaban1bagian3 as $key=> $ja13){
            $this->wab13 = $key.'='.$ja13.', '.$this->wab13;
        }

        if (isset($data4)) {
            $data4->update([
                            'deskripsi_praktikum_jawaban'=>$this->wab13 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->wab13 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'20'
                        ]);
        }

        if (isset($data5)) {
            $data5->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban2bagian3 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban2bagian3 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'21'
                        ]);
        }
        session()->flash('tiga');

    }

    public function bagianempat() {
        $this->ceknilai();

        $jawabans  = new PraktikumsJawabans;

        $data6  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 22)->first();
        $data7  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 23)->first();

        $this->wab14 = null;
            foreach($this->jawaban1bagian4 as $key=> $ja14){
                $this->wab14 = $key.'='.$ja14.', '.$this->wab14;
            }
        if (isset($data6)) {
            $data6->update([
                            'deskripsi_praktikum_jawaban'=>$this->wab14 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->wab14 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'22'
                        ]);
        }

        if (isset($data7)) {
            $data7->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban2bagian4 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban2bagian4 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'23'
                        ]);
        }
        session()->flash('empat');

    }

    public function bagianlima() {
        $this->ceknilai();

        $jawabans  = new PraktikumsJawabans;

        $data8  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 24)->first();
        $data9  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 25)->first();

        $this->wab15 = null;
        foreach($this->jawaban1bagian5 as $key=> $ja14){
            $this->wab15 = $key.'='.$ja14.', '.$this->wab15;
        }

        if (isset($data8)) {
            $data8->update([
                            'deskripsi_praktikum_jawaban'=>$this->wab15 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->wab15 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'24'
                        ]);
        }

        if (isset($data9)) {
            $data9->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban2bagian5 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban2bagian5 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'25'
                        ]);
        }
        session()->flash('lima');

    }

    public function bagianenam() {
        $this->ceknilai();

        $jawabans  = new PraktikumsJawabans;

        $data10  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 26)->first();

        if (isset($data10)) {
            $data10->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian6 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian6 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'26'
                        ]);
        }
        session()->flash('enam');

    }


    public function mount(){
        $jawabanpraktikum = new PraktikumsJawabans;
        $datajawaban1bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 17)->first();
        $datajawaban1bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 18)->first();
        $datajawaban2bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 19)->first();
        $datajawaban1bagian3 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 20)->first();
        $datajawaban2bagian3 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 21)->first();
        $datajawaban1bagian4 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 22)->first();
        $datajawaban2bagian4 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 23)->first();
        $datajawaban1bagian5 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 24)->first();
        $datajawaban2bagian5 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 25)->first();
        $datajawaban1bagian6 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 26)->first();

        $this->jawaban1bagian1 = $datajawaban1bagian1->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban1bagian2 = $datajawaban1bagian2->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban2bagian2 = $datajawaban2bagian2->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban1bagian3 = $datajawaban1bagian3->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban2bagian3 = $datajawaban2bagian3->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban1bagian4 = $datajawaban1bagian4->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban2bagian4 = $datajawaban2bagian4->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban1bagian5 = $datajawaban1bagian5->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban2bagian5 = $datajawaban2bagian5->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban1bagian6 = $datajawaban1bagian6->deskripsi_praktikum_jawaban ?? null;


        $this->djawaban1bagian1 = $datajawaban1bagian1;
        $this->djawaban1bagian2 = $datajawaban1bagian2;
        $this->djawaban1bagian3 = $datajawaban1bagian3;
        $this->djawaban1bagian4 = $datajawaban1bagian4;
        $this->djawaban1bagian5 = $datajawaban1bagian5;
        $this->djawaban1bagian6 = $datajawaban1bagian6;

        $this->p1j13 = strstr($this->jawaban1bagian3,'Delection=1') ? true : false;
        $this->p2j13 = strstr($this->jawaban1bagian3,'Generalitation=1') ? true : false;
        $this->p3j13 = strstr($this->jawaban1bagian3,'Distortion=1') ? true : false;

        $this->p1j14 = strstr($this->jawaban1bagian4,'Kebutuhan=1') ? true : false;
        $this->p2j14 = strstr($this->jawaban1bagian4,'Waktu=1') ? true : false;
        $this->p3j14 = strstr($this->jawaban1bagian4,'Harga=1') ? true : false;
        $this->p4j14 = strstr($this->jawaban1bagian4,'Kualitas=1') ? true : false;

        $this->p1j15 = strstr($this->jawaban1bagian5,'Embedded-Command=1') ? true : false;
        $this->p2j15 = strstr($this->jawaban1bagian5,'Embedded-Question=1') ? true : false;
        $this->p3j15 = strstr($this->jawaban1bagian5,'Question-Tag=1') ? true : false;
        $this->p4j15 = strstr($this->jawaban1bagian5,'Negative-Command=1') ? true : false;
        $this->p5j15 = strstr($this->jawaban1bagian5,'Converstation-Postulates=1') ? true : false;
        $this->p6j15 = strstr($this->jawaban1bagian5,'Ambiguity=1') ? true : false;

        $this->jawaban1bagian3 = ['Delection' => $this->p1j13, 'Generalitation' => $this->p2j13, 'Distortion' => $this->p3j13];
        $this->jawaban1bagian4 = ['Kebutuhan' => $this->p1j14, 'Waktu' => $this->p2j14, 'Harga' => $this->p3j14, 'Kualitas' => $this->p4j14];
        $this->jawaban1bagian5 = ['Embedded-Command' => $this->p1j15, 'Embedded-Question' => $this->p2j15, 'Question-Tag' => $this->p3j15, 'Negative-Command' => $this->p4j15, 'Converstation-Postulates' => $this->p5j15, 'Ambiguity' => $this->p6j15];

    }
    public function render()
    {
        $jawabanpraktikum = new PraktikumsJawabans;
        $datajawaban1bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 17)->first();
        $datajawaban1bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 18)->first();
        $datajawaban1bagian3 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 20)->first();
        $datajawaban1bagian4 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 22)->first();
        $datajawaban1bagian5 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 24)->first();
        $datajawaban1bagian6 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 26)->first();

        $this->djawaban1bagian1 = $datajawaban1bagian1;
        $this->djawaban1bagian2 = $datajawaban1bagian2;
        $this->djawaban1bagian3 = $datajawaban1bagian3;
        $this->djawaban1bagian4 = $datajawaban1bagian4;
        $this->djawaban1bagian5 = $datajawaban1bagian5;
        $this->djawaban1bagian6 = $datajawaban1bagian6;

        return view('livewire.praktikum-koc-ketiga');
    }
}
