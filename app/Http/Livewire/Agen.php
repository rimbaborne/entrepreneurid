<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Database\Eloquent\Builder;

class Agen extends Component
{
    use WithPagination;

    public $search        = '';
    public $searchnama    = '';
    public $searchemail   = '';
    public $perPage       = 10;
    public $sortAsc       = true;
    public $sortField     = 'created_at';
    public $searchproduk  = '';


    public function sortBy($field)
    {
        $this->urutkanstatus = '';

        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
        $this->sortField = $field;
    }

    public function render()
    {
        return view(
            'livewire.agen',
            [

                'agens' => \App\Models\Biodatum::where('status_user', '=', '1')
                    ->when($this->search, function ($query) {
                        return $query->where('nama', 'like', '%' . $this->search . '%');
                    })
                    ->when($this->searchemail, function ($query) {
                        return $query->where('email', 'like', '%' . $this->searchemail . '%');
                    })
                    ->when($this->searchproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->searchproduk);
                    })
                    ->orderBy($this->sortField, $this->sortAsc ? 'desc' : 'asc')
                    ->paginate($this->perPage),
            ]
        );
    }
}
