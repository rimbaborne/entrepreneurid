<?php

namespace App\Http\Livewire;

use App\Models\PraktikumsJawabans;
use App\Models\PraktikumsNilaibabs;
use Livewire\Component;
use Illuminate\Support\Facades\DB;
// use App\Models\PraktikumsJawabans;

class PraktikumKocPertama extends Component
{
    public $jawaban1bagian1 = null;
    public $jawaban2bagian1 = null;
    public $jawaban3bagian1 = null;
    public $jawaban4bagian1 = null;
    public $jawaban5bagian1 = null;
    public $jawaban6bagian1 = null;
    public $jawaban1bagian2 = null;
    public $jawaban2bagian2 = null;
    public $jawaban3bagian2 = null;
    public $jawaban4bagian2 = null;
    public $jawaban1bagian3 = null;
    public $djawaban1bagian1 = null;
    public $djawaban2bagian1 = null;
    public $djawaban3bagian1 = null;
    public $djawaban4bagian1 = null;
    public $djawaban5bagian1 = null;
    public $djawaban6bagian1 = null;
    public $djawaban1bagian2 = null;
    public $djawaban2bagian2 = null;
    public $djawaban3bagian2 = null;
    public $djawaban4bagian2 = null;
    public $djawaban1bagian3 = null;

    public function ceknilai() {
        $nilaibabs = new PraktikumsNilaibabs;

        $ceknilai  = $nilaibabs->where('id_praktikum_bab', 1)->where('user_praktikum_nilai', auth()->user()->id)->first();
        if (isset($ceknilai)) {
            $ceknilai->update([ 'status_praktikum_nilai' => 'Menunggu Koreksi' ]);
        } else {
            $nilaibabs->create([
                'id_praktikum_bab' => 1,
                'user_praktikum_nilai' => auth()->user()->id,
                'status_praktikum_nilai' => 'Menunggu Koreksi',
            ]);
        }
    }

    public function bagiansatu() {
        $this->ceknilai();

        $jawabans  = new PraktikumsJawabans;

        $data1  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 1)->first();
        $data2  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 2)->first();
        $data3  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 3)->first();
        $data4  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 4)->first();
        $data5  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 5)->first();
        $data6  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 6)->first();

        if (isset($data1)) {
            $data1->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian1 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian1 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'1'
                        ]);
        }

        if (isset($data2)) {
            $data2->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban2bagian1 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban2bagian1 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'2'
                        ]);
        }

        if (isset($data3)) {
            $data3->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban3bagian1 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban3bagian1 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'3'
                        ]);
        }

        if (isset($data4)) {
            $data4->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban4bagian1 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban4bagian1 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'4'
                        ]);
        }

        if (isset($data5)) {
            $data5->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban5bagian1 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban5bagian1 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'5'
                        ]);
        }

        if (isset($data6)) {
            $data6->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban6bagian1 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban6bagian1 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'6'
                        ]);
        }
        session()->flash('satu');
    }

    public function bagiandua() {
        $this->ceknilai();

        $jawabans = new PraktikumsJawabans;

        $data7   = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 7)->first();
        $data8   = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 8)->first();
        $data9   = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 9)->first();
        $data10  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 10)->first();

        if (isset($data7)) {
            $data7->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian2 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian2 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'7'
                        ]);
        }

        if (isset($data8)) {
            $data8->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban2bagian2 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban2bagian2 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'8'
                        ]);
        }

        if (isset($data9)) {
            $data9->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban3bagian2 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban3bagian2 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'9'
                        ]);
        }

        if (isset($data10)) {
            $data10->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban4bagian2 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban4bagian2 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'10'
                        ]);
        }
        session()->flash('dua');
    }

    public function bagiantiga() {
        $this->ceknilai();

        $jawabans = new PraktikumsJawabans;

        $data11  = $jawabans->where('user_praktikum_jawaban', auth()->user()->id)->where('id_praktikum_pertanyaan', 11)->first();

        if (isset($data11)) {
            $data11->update([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian3 ?? ' ',
                        ]);
        } else {
            $jawabans->create([
                            'deskripsi_praktikum_jawaban'=>$this->jawaban1bagian3 ?? ' ',
                            'user_praktikum_jawaban'=>auth()->user()->id,
                            'id_praktikum_pertanyaan'=>'11'
                        ]);
        }
        session()->flash('tiga');

    }

    public function mount(){
        $jawabanpraktikum = new PraktikumsJawabans;
        $datajawaban1bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 1)->first();
        $datajawaban2bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 2)->first();
        $datajawaban3bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 3)->first();
        $datajawaban4bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 4)->first();
        $datajawaban5bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 5)->first();
        $datajawaban6bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 6)->first();
        $datajawaban1bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 7)->first();
        $datajawaban2bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 8)->first();
        $datajawaban3bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 9)->first();
        $datajawaban4bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 10)->first();
        $datajawaban1bagian3 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 11)->first();

        $this->jawaban1bagian1 = $datajawaban1bagian1->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban2bagian1 = $datajawaban2bagian1->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban3bagian1 = $datajawaban3bagian1->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban4bagian1 = $datajawaban4bagian1->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban5bagian1 = $datajawaban5bagian1->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban6bagian1 = $datajawaban6bagian1->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban1bagian2 = $datajawaban1bagian2->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban2bagian2 = $datajawaban2bagian2->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban3bagian2 = $datajawaban3bagian2->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban4bagian2 = $datajawaban4bagian2->deskripsi_praktikum_jawaban ?? null;
        $this->jawaban1bagian3 = $datajawaban1bagian3->deskripsi_praktikum_jawaban ?? null;

        $this->djawaban1bagian1 = $datajawaban1bagian1;
        $this->djawaban2bagian1 = $datajawaban2bagian1;
        $this->djawaban3bagian1 = $datajawaban3bagian1;
        $this->djawaban4bagian1 = $datajawaban4bagian1;
        $this->djawaban5bagian1 = $datajawaban5bagian1;
        $this->djawaban6bagian1 = $datajawaban6bagian1;
        $this->djawaban1bagian2 = $datajawaban1bagian2;
        $this->djawaban2bagian2 = $datajawaban2bagian2;
        $this->djawaban3bagian2 = $datajawaban3bagian2;
        $this->djawaban4bagian2 = $datajawaban4bagian2;
        $this->djawaban1bagian3 = $datajawaban1bagian3;
    }
    public function render()
    {
        $jawabanpraktikum = new PraktikumsJawabans;
        $datajawaban1bagian1 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 1)->first();
        $datajawaban1bagian2 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 7)->first();
        $datajawaban1bagian3 = $jawabanpraktikum->where('user_praktikum_jawaban',
        auth()->user()->id)->where('id_praktikum_pertanyaan', 11)->first();

        $this->djawaban1bagian1 = $datajawaban1bagian1;
        $this->djawaban1bagian2 = $datajawaban1bagian2;
        $this->djawaban1bagian3 = $datajawaban1bagian3;

        return view('livewire.praktikum-koc-pertama');
    }
}
