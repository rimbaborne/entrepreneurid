<?php

namespace App\Http\Livewire;

use App\Exports\DataEcourse;
use App\Mail\KonfirmasiTransfer;
use Illuminate\Support\Facades\Mail;
use App\Models\Biodatum;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Database\Eloquent\Builder;
use Throwable;
use Excel;
use Illuminate\Support\Facades\DB;

class Biodata extends Component
{
    use WithPagination;
    public $user_id      = '';
    public $nama         = '';
    public $email        = '';
    public $noresi       = '';
    public $search       = '';
    public $searchproduk = '';
    public $searchuser   = '';
    public $searchaktif  = '';
    public $searchemail  = '';
    public $searchagen   = '';
    public $statusTanda  = '2';
    public $perPage      = 10;
    public $sortAsc      = true;
    public $sortField    = 'created_at';
    public $count        = 0;
    public $chartproduk  = '39';
    public $updateMode   = false;
    public $waktuevent   = '2021-07-30 23:59:59';

    // public $uid, $unama, $uemail, $upanggilan, $unohp, $ugender, $utgllahir, $ukota;

    // public function editdata($id)
    // {
    //     $data = Biodatum::findOrFail($id);
    //     $this->unama      = $data->nama;
    //     $this->uemail     = $data->email;
    //     $this->upanggilan = $data->panggilan;
    //     $this->unohp      = $data->no_telp;
    //     $this->ugender    = $data->gender;
    //     $this->utgllahir  = $data->tgllahir;
    //     $this->ukota      = $data->kota;
    // }

    // public function updatedata($id)
    // {
    //     try {
    //         $data = Biodatum::findOrFail($id);
    //         $data->update([
    //             'nama'      => $this->unama,
    //             'email'     => $this->uemail,
    //             'panggilan' => $this->upanggilan,
    //             'no_telp'   => $this->unohp,
    //             'gender'    => $this->ugender,
    //             'tgllahir'  => $this->utgllahir,
    //             'kota'      => $this->ukota,
    //             'updated_at' => Carbon::now()
    //         ]);
    //         session()->flash('notif-berhasil', $data->nama . ' - ' . $data->email . ', Data Berhasil Diupdate !');
    //     } catch (Throwable $th) {
    //         session()->flash('notif-gagal', $data->nama . ' - ' . $data->email . ', Terjadi Kesalahan Update !');
    //     }
    // }
    protected function clearFormNoresi()
    {
        $this->noresi = '';
        $this->nama = '';
    }

    protected function cancelFormNoresi()
    {
        $this->updateMode = false;
        $this->clearFormNoresi();
    }

    public function editNoresi($id)
    {
        $this->updateMode = true;
        $data = Biodatum::where('id',$id)->first();
        $this->user_id = $id;
        $this->nama = $data->nama;
        $this->email = $data->email;
    }


    public function updateNoresi()
    {
        try {
            $this->updateMode = true;
            $data = Biodatum::findOrFail($this->user_id);
            $data->update([
                'noresi' => $this->noresi
            ]);
            $this->cancelFormNoresi();
            $this->emit('updateresi');
            try {
                $apikey = 'xEhX7sF1wh7KFdVXZx5AfJj7wn6krW8m';
                $phone = '+62' . $data->no_telp;
                $message =
                    '*Info Resi Pengiriman Buku CNL*

Halo ' . $data->nama . ' 😊🙏
Selamat ya pemesanan buku Copywriting Next Level Anda sudah dalam pengiriman, dengan data pemesanan :

Nomor Resi : ' . $data->noresi . '
Ekspedisi : ' . $data->kurir . '

Untuk cek resi silahkan bisa dicek ke bit.ly/resicnl2021
Jika Anda punya pertanyaan, silahkan balas pesan ini. Dengan senang hati kami akan membantu. 😊🙏

Salam,

*Tim entrepreneurID*';

                $url = 'https://api.wanotif.id/v1/send';

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_HEADER, 0);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, array(
                    'Apikey'    => $apikey,
                    'Phone'     => $phone,
                    'Message'   => $message,
                ));
                $response = curl_exec($curl);
                curl_close($curl);

                session()->flash('notif-berhasil', $data->nama . ' - ' . $data->email . ', Nomor Resi Berhasil Ditambahkan !');
            } catch (Throwable $th) {
                session()->flash('notif-gagal', $data->nama . ' - ' . $data->email . ',  Nomor Resi Tidak Terupdate !. Silahkan Diulangi');
            }
        } catch (Throwable $e) {
            // report($e);
            // return false;
            session()->flash('notif-gagal', 'Terjadi Kesalahan. Konfirmasi Gagal !');
        }
    }

    public function konfirmasi($id)
    {
        try {
            $data = Biodatum::findOrFail($id);
            $data->update([
                'aktif' => 2,
                'waktukonfirmasi' => Carbon::now()
            ]);
            try {
                Mail::to($data->email)->send(new KonfirmasiTransfer($data));
                $apikey = '8c073ed79bf7f3796cf8e98a288ba69f6db5847d6ff364a2';

                if ($data->idprodukreg == 35) {
                $phone = '+62' . $data->no_telp;
                $message =
                'Dear ' . $data->nama . '
Ini adalah pesan spesial yang hanya dikirim kepada peserta *Mentoring Super Reseller (MSR)*.

Pertama-tama, kami ucapkan selamat ya karena telah mendaftar kursus online ini. 🤝

Sebagai salah satu peserta MSR, silahkan simak informasi berikut. 😊

*1. JADWAL MENTORING*
Mulai Hari Senin, Anda akan mendapat bimbingan via email selama 7 Hari.
Waktu bimbingan adalah *pukul 20.00 WIB.*
Harap siapkan catatan saat proses belajar nanti, agar hasil yang Anda dapatkan maksimal.

*2. AKSES MEMBER AREA*
Selain bimbingan, Anda juga bisa mengakses fasilitas MSR lainnya, seperti
✅ Member Area Peserta
✅ Akses Grup diskusi, dan
✅ Konsultasi Langsung

Untuk mengakses fasilitas pendukung, silahkan aktifkan member area Anda melalui link https://mentoringsuperreseller.com/member-area/registrasi/
_(Saat mengaktifkan Member Area, mohon gunakan email yang Anda gunakan saat pendaftaran, bukan email lainnya)_

*3. SUPPORT CONTACT*
Jika Anda menemui kendala, butuh bantuan atau tidak mendapatkan email apapun saat waktu bimbingan dimulai, maka silahkan hubungi Customer Support kami melalui chat WhatsApp di nomor 0857-8757-2580 atau langsung klik 👉 bit.ly/CS-eID

Sekali lagi selamat karena telah menjadi peserta Mentoring Super Reseller.
Semoga ini jadi wasilah untuk pertumbuhan bisnis Anda, aamiin. 😇

Salam,

_*Tim entrepreneurID*_';

                $url='http://116.203.191.58/api/send_message';
                $data = array(
                    "phone_no"  => $phone,
                    "key"		=> $apikey,
                    "message"	=> $message,
                    "skip_link"	=> True // This optional for skip snapshot of link in message
                );
                $data_string = json_encode($data);

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_VERBOSE, 0);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                curl_setopt($ch, CURLOPT_TIMEOUT, 360);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
                );
                // echo $res=curl_exec($ch);
                $res=curl_exec($ch);
                curl_close($ch);
                session()->flash('notif-berhasil', 'Berhasil Dikonfirmasi eMAIL & WA Produk MSR!');

                } elseif ($data->idprodukreg == 18) {
                    $phone = '+62' . $data->no_telp;
                    $message =
                    'Dear ' . $data->nama . '
Ini adalah pesan spesial yang hanya dikirim kepada peserta WA Master Closing (WMC).

Pertama-tama, kami ucapkan selamat ya karena telah mendaftar program ini. 🤝

Sebagai salah satu peserta WMC, silahkan simak informasi berikut. 😊

1. AKSES MEMBER AREA
Materi program WMC berada dimember area, untuk mengakses silahkan aktifkan member area Anda melalui link wamasterclosing.com/registrasi
(Saat mengaktifkan Member Area, mohon gunakan email yang Anda gunakan saat pendaftaran, bukan email lainnya)

2. SUPPORT CONTACT
Jika Anda menemui kendala dan  butuh bantuan, maka silahkan hubungi Customer Support kami melalui chat WhatsApp di nomor 0857-8757-2580 atau langsung klik 👉 bit.ly/CS-eID

Sekali lagi selamat karena telah menjadi peserta WA Master Closing.
Semoga ini jadi wasilah untuk pertumbuhan bisnis Anda, aamiin. 😇

Salam,

*Tim entrepreneurID*';

                    $url='http://116.203.191.58/api/send_message';
                    $data = array(
                        "phone_no"  => $phone,
                        "key"		=> $apikey,
                        "message"	=> $message,
                        "skip_link"	=> True // This optional for skip snapshot of link in message
                    );
                    $data_string = json_encode($data);

                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_VERBOSE, 0);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 360);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                    );
                    // echo $res=curl_exec($ch);
                    $res=curl_exec($ch);
                    curl_close($ch);
                    session()->flash('notif-berhasil', 'Berhasil Dikonfirmasi eMAIL & WA Produk WMC!');

                } elseif ($data->idprodukreg == 39) {
                    $phone = '+62' . $data->no_telp;
                    $message =
                    'Dear ' . $data->nama . '
Ini adalah pesan spesial yang hanya dikirim kepada Agen Resmi entrepreneurID terdaftar.

Pertama-tama, kami ucapkan selamat ya 🤝

Sebagai salah satu Agen entrepreneurID, silahkan ikuti arahan berikut. 😊

*1. Hubungi Fasilitator Agen*

Simpan nomor 0821-5089-6665 di Hp Anda dengan nama Mba Wulan.

Mba Wulan adalah fasilitator agen entrepreneurID yang akan mengurus seluruh kebutuhan Anda selama aktif sebagai Agen entrepreneurID. Mba Wulan juga yang nantinya akan memasukan Anda ke grup belajar untuk Agen entrepreneurID dan channel info terupdate untuk Agen.

Karena itu, silahkan langsung hubungi Mba Wulan via WA dengan format berikut
_*Aktivasi Agen baru*_
_*Nama Panggilan :*_
_*Alamat Email yang didaftarakan :*_
_*Nomor Telegram :*_

Jika sebelumnya Anda sudah melakukan hal ini, maka silahkan abaikan. Tapi jika Anda belum melakukannya, silahkan langsung hubungi Mba Wulan sekarang.

*2. Pasang Alarm di Jadwal Penting*

Ada 3 proses penting yang akan Anda lalui setelah ini. Karena itu, silahkan pasang alarm di jadwal-jadwal dibawah ini.

*Terima Email Regulasi*
_Jadwal : Senin, Tanggal 13 September, Mulai Pukul 10.00 WIB._

Saat ini proses pendaftaran Agen entrepreneurID sedang berlangsung sampai tanggal 11 September, karena itu akses masuk grup untuk agen baru, akan dibuka setelah pendaftaran selesai.

Jika Anda tidak mendapat email diwaktu tersebut, silahkan hubungi Mba Wulan untuk membantu Anda.

*Masuk Grup Agen*
_Jadwal : Senin, Tanggal 13 September, Mulai Pukul 10.00 WIB._

Saat ini proses pendaftaran Agen entrepreneurID sedang berlangsung sampai tanggal 11 September, karena itu akses masuk grup untuk agen baru, akan dibuka setelah pendaftaran selesai.

Ditanggal yang disebutkan diatas, silahkan hubungi Mba Wulan lagi agar bisa masuk ke grupnya.

*Mulai Bimbingan*
_Jadwal : Mulai Hari Selasa, Tanggal 14 September, Mulai Pukul 19.00 WIB_

Seperti yang disebutkan dipendaftaran, Anda akan dapat bimbingan dari nol tentang apa yang harus dilakukan agar bisa memiliki tambahan penghasilan dari program agen ini. Bimbingan akan dimulai sesuai jadwal yang disebutkan.

Kami menyarankan Anda untuk mengikuti sesi bimbingan Agen baru, agar hasil yang Anda dapatkan lebih maksimal. 🙂

Jika ada yang ingin ditanyakan tentang isi pesan ini, silahkan hubungi Mba Wulan ya.

Sekali lagi kami ucapkan selamat datang ke dalam keluarga besar entrepreneurID. Semoga ini jadi wasilah kebaikan untuk Anda dan orang yang Anda sayangi. 😊

Regards,

_*Tim entrepreneurID Pusat*_';

                    $url='http://116.203.191.58/api/send_message';
                    $data = array(
                        "phone_no"  => $phone,
                        "key"		=> $apikey,
                        "message"	=> $message,
                        "skip_link"	=> True // This optional for skip snapshot of link in message
                    );
                    $data_string = json_encode($data);

                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_VERBOSE, 0);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 360);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                    );
                    // echo $res=curl_exec($ch);
                    $res=curl_exec($ch);
                    curl_close($ch);
                    session()->flash('notif-berhasil', 'Berhasil Dikonfirmasi eMAIL & WA Produk Agen Baru!');

                } else {
                    session()->flash('notif-berhasil', $data->nama . ' - ' . $data->email . ', Berhasil Dikonfirmasi !');
                }

            } catch (Throwable $th) {
                session()->flash('notif-gagal', $data->nama . ' - ' . $data->email . ', Email Tidak Terkirim !. Silahkan Diulangi');
            }
        } catch (Throwable $e) {
            // report($e);
            // return false;
            session()->flash('notif-gagal', 'Terjadi Kesalahan. Konfirmasi Gagal !');
        }
    }

    public function transferulang($id)
    {
        try {
            $data = Biodatum::findOrFail($id);
            $data->update([
                'aktif' => 2,
                'waktukonfirmasi' => Carbon::now()
            ]);
            try {
                Mail::to($data->email)->send(new KonfirmasiTransfer($data));

                session()->flash('notif-berhasil', $data->nama . ' - ' . $data->email . ', Email Pengingat Bukti Transfer Salah Berhasil Dikirim !');
            } catch (Throwable $th) {
                session()->flash('notif-gagal', $data->nama . ' - ' . $data->email . ', Email Tidak Terkirim !. Silahkan Diulangi');
            }
        } catch (Throwable $e) {
            // report($e);
            // return false;
            session()->flash('notif-gagal', 'Terjadi Kesalahan. Konfirmasi Gagal !');
        }
    }

    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
        $this->sortField = $field;
    }

    public function render()
    {
        return view(
            'livewire.biodata',
            [
                'biodatas' => \App\Models\Biodatum::
                // where('status_user', '!=', '1')
                //     ->
                    when($this->search, function ($query) {
                        return $query->whereRaw('LOWER(nama) LIKE ? ', '%' . strtolower($this->search) . '%');
                    })
                    ->when($this->searchemail, function ($query) {
                        return $query->whereRaw('LOWER(email) LIKE ? ', '%' . strtolower($this->searchemail) . '%');
                    })
                    ->when($this->searchproduk, function ($query) {
                        if ($this->searchproduk === '5') {
                            return $query->where('idprodukreg', '=', '5')->where('created_at', '>', '2020-07-28 12:00:00');
                        } else {
                            return $query->where('idprodukreg', '=', $this->searchproduk);
                        }
                    })
                    ->when($this->searchproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->searchproduk);
                    })
                    ->when($this->searchagen, function ($query) {
                        return $query->whereHas('parent', function (Builder $query) {
                            $query->where('nama', 'like', '%' . $this->searchagen . '%');
                        });
                    })
                    ->when($this->searchuser, function ($query) {
                        if ($this->searchuser === '99') {
                            return $query->where('status_user', '=', 0);
                        } else {
                            return $query->where('status_user', '=', $this->searchuser);
                        }
                    })
                    ->when($this->searchaktif, function ($query) {
                        if ($this->searchaktif === '99') {
                            return $query->where('aktif', '=', 0);
                        } else {
                            return $query->where('aktif', '=', $this->searchaktif);
                        }
                    })
                    ->orderBy($this->sortField, $this->sortAsc ? 'desc' : 'asc')
                    ->paginate($this->perPage),

                'konfirmasi'      => \App\Models\Biodatum::offset(0)->limit(7)->orderBy('waktukonfirmasi', 'desc')->get(),
                'belumaktif'      => \App\Models\Biodatum::where('aktif', '=', 0)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->paginate(2),
                'belumkonfirmasi' => \App\Models\Biodatum::where('aktif', '=', 1)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->paginate(2),
                'aktif'           => \App\Models\Biodatum::where('aktif', '=', 2)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->paginate(2),
                //
                'afiliasi'           => \App\Models\Biodatum::where('status_user', '=', 0)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->count(),
                'manual'           => \App\Models\Biodatum::where('status_user', '=', 2)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->count(),
                //
                'bkaf'           => \App\Models\Biodatum::where('status_user', '=', 0)
                    ->where('aktif', '=', 0)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->count(),
                'bkma'           => \App\Models\Biodatum::where('status_user', '=', 2)
                    ->where('aktif', '=', 0)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->count(),
                'beaf'           => \App\Models\Biodatum::where('status_user', '=', 0)
                    ->where('aktif', '=', 1)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->count(),
                'bema'           => \App\Models\Biodatum::where('status_user', '=', 2)
                    ->where('aktif', '=', 1)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->count(),
                'akaf'           => \App\Models\Biodatum::where('status_user', '=', 0)
                    ->where('aktif', '=', 2)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->count(),
                'akma'           => \App\Models\Biodatum::where('status_user', '=', 2)
                    ->where('aktif', '=', 2)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->count(),
                'agen'           => \App\Models\Biodatum::
                    select('ref')
                    ->groupBy('ref')
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->get()
                    ->count(),
                'agena'           => \App\Models\Biodatum::
                    select('ref')
                    ->groupBy('ref')
                    ->where('aktif', '=', 0)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->get()
                    ->count(),
                'agenb'           => \App\Models\Biodatum::
                    select('ref')
                    ->groupBy('ref')
                    ->where('aktif', '=', 1)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->get()
                    ->count(),
                'agenc'           => \App\Models\Biodatum::
                    select('ref')
                    ->groupBy('ref')
                    ->where('aktif', '=', 2)
                    ->when($this->chartproduk, function ($query) {
                        return $query->where('idprodukreg', '=', $this->chartproduk);
                    })
                    ->where('created_at', '>=', $this->waktuevent)
                    ->get()
                    ->count(),

                // JUMLAH BUKU
                // 'buku_belumaktif'      => \App\Models\Biodatum::where('aktif', '=', 0)
                //     ->when($this->chartproduk, function ($query) {
                //         return $query->where('idprodukreg', '=', $this->chartproduk);
                //     })
                //     ->sum('jumlah'),
                // 'buku_belumkonfirmasi' => \App\Models\Biodatum::where('aktif', '=', 1)
                //     ->when($this->chartproduk, function ($query) {
                //         return $query->where('idprodukreg', '=', $this->chartproduk);
                //     })
                //     ->sum('jumlah'),
                // 'buku_aktif'           => \App\Models\Biodatum::where('aktif', '=', 2)
                //     ->when($this->chartproduk, function ($query) {
                //         return $query->where('idprodukreg', '=', $this->chartproduk);
                //     })
                //     ->sum('jumlah'),
                // //
                // 'buku_afiliasi'           => \App\Models\Biodatum::where('status_user', '=', 0)
                //     ->when($this->chartproduk, function ($query) {
                //         return $query->where('idprodukreg', '=', $this->chartproduk);
                //     })

                //     ->sum('jumlah'),
                // 'buku_manual'           => \App\Models\Biodatum::where('status_user', '=', 2)
                //     ->when($this->chartproduk, function ($query) {
                //         return $query->where('idprodukreg', '=', $this->chartproduk);
                //     })

                //     ->sum('jumlah'),
                // //
                // 'buku_bkaf'           => \App\Models\Biodatum::where('status_user', '=', 0)
                //     ->where('aktif', '=', 0)
                //     ->when($this->chartproduk, function ($query) {
                //         return $query->where('idprodukreg', '=', $this->chartproduk);
                //     })

                //     ->sum('jumlah'),
                // 'buku_bkma'           => \App\Models\Biodatum::where('status_user', '=', 2)
                //     ->where('aktif', '=', 0)
                //     ->when($this->chartproduk, function ($query) {
                //         return $query->where('idprodukreg', '=', $this->chartproduk);
                //     })

                //     ->sum('jumlah'),
                // 'buku_beaf'           => \App\Models\Biodatum::where('status_user', '=', 0)
                //     ->where('aktif', '=', 1)
                //     ->when($this->chartproduk, function ($query) {
                //         return $query->where('idprodukreg', '=', $this->chartproduk);
                //     })

                //     ->sum('jumlah'),
                // 'buku_bema'           => \App\Models\Biodatum::where('status_user', '=', 2)
                //     ->where('aktif', '=', 1)
                //     ->when($this->chartproduk, function ($query) {
                //         return $query->where('idprodukreg', '=', $this->chartproduk);
                //     })

                //     ->sum('jumlah'),
                // 'buku_akaf'           => \App\Models\Biodatum::where('status_user', '=', 0)
                //     ->where('aktif', '=', 2)
                //     ->when($this->chartproduk, function ($query) {
                //         return $query->where('idprodukreg', '=', $this->chartproduk);
                //     })

                //     ->sum('jumlah'),
                // 'buku_akma'           => \App\Models\Biodatum::where('status_user', '=', 2)
                //     ->where('aktif', '=', 2)
                //     ->when($this->chartproduk, function ($query) {
                //         return $query->where('idprodukreg', '=', $this->chartproduk);
                //     })

                //     ->sum('jumlah'),
            ]
        );
    }
}
