<?php

namespace App\Http\Controllers\Agen;

use App\Http\Controllers\Controller;
use App\Models\Auth\Agen;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Image;

class ProfileController extends Controller
{

    // protected $redirectTo = '/agen/login';
    protected $redirectTo = '/login';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('agen.auth:agen');
    }

    public function index()
    {
        return view('agen.page.profile.index');
    }

    public function updateprofile(Request $request)
    {

        $agen = Agen::find(Auth::guard('agen')->user()->id);

        $agen->name        = $request->input('nama');
        $agen->kode_notelp = $request->input('kode_notelp');
        $agen->notelp      = $request->input('notelp');
        $agen->bank_rek    = $request->input('bank_rek');
        $agen->no_rek      = $request->input('no_rek');
        $agen->nama_rek    = $request->input('nama_rek');

        $agen->save();

        notify()->success('Profile Berhasil Diupdate !');

        return redirect()->back();
    }

    public function password()
    {
        return view('agen.page.profile.password');
    }

    public function updatepassword(Request $request)
    {
        $agen = Agen::find(Auth::guard('agen')->user()->id);
        $agen->password = Hash::make($request->input('password'));
        $agen->save();

        notify()->success('Password Berhasil Diupdate !');

        return redirect()->to('/profile');
    }

    public function editfoto()
    {
        return view('agen.page.profile.edit-foto');
    }

    public function updateeditfoto(Request $request)
    {
        $foto         = Image::make($request->file('image'));
        $namafoto     = Auth::guard('agen')->user()->email.'.'.$request->file('image')->getClientOriginalExtension();
        $lokasifoto   = public_path('../../../public_html/agen-entrepreneurid.com/dashboard/foto/');
        $foto->resize(500, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $foto->save($lokasifoto.$namafoto);

        $agen = Agen::find(Auth::guard('agen')->user()->id);
        $agen->foto = $namafoto;
        $agen->save();

        notify()->success('Foto Profile Berhasil Di Update !');

        return redirect()->to('/profile');
    }
}
