<?php

namespace App\Http\Controllers\Agen;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Event;
use Illuminate\Support\Carbon;
use Str;
use Session;
use Image;
use App\Models\Transaction;
use App\Exports\DataAgenTransaksi;
use App\Models\BookTransactions;
use App\Exports\DataLeadMagnetAgen;

class ProdukController extends Controller
{
    protected $redirectTo = '/login';

    public function __construct()
    {
        $this->middleware('agen.auth:agen');
    }

    public function ekspedisi($dataUrl, $id, $type)
    {
        $id_ = $id != null ? $id : "";

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => "https://pro.rajaongkir.com/api/".$dataUrl.$id_,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => array(
                "key:7d23bffcefc9680802db1d10ba43eb67"
            ),
        ));
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            $result = $type ? json_decode($response, true) : json_decode($response);
            return $result;
        }
    }

    public function cekongkir()
    {
        if (request()->get('cari') == 'kota') {
            $datakota = $this->ekspedisi('city?province=', request()->get('provinsi'), true);

            $n[] = ['datakota' => "<option> Silahkan Pilih Kota/Kabupaten ..</option>"];

            foreach($datakota['rajaongkir']['results'] as $data){
                $n[] = ['datakota' => "<option data-kodepos='".$data['postal_code']."' value='".$data['city_id']."'>".$data['city_name']."</option>"];
            }
            return $n;

        } elseif (request()->get('cari') == 'kecamatan') {
            $datakecamatan = $this->ekspedisi('subdistrict?city=', request()->get('kota'), true);

            $n[] = ['datakecamatan' => "<option> Silahkan Pilih Kecamatan ..</option>"];

            foreach($datakecamatan['rajaongkir']['results'] as $data){
                $n[] = ['datakecamatan' => "<option value='".$data['subdistrict_id']."'>".$data['subdistrict_name']."</option>"];
            }
            return $n;

        } elseif (request()->get('cari') == 'ongkir') {
            $kurir = ['tiki', 'jne', 'pos', 'jnt'];
            for ($i=0; $i < 4; $i++) {
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL 			=> "https://pro.rajaongkir.com/api/cost",
                    CURLOPT_RETURNTRANSFER 	=> true,
                    CURLOPT_ENCODING 		=> "",
                    CURLOPT_MAXREDIRS 		=> 10,
                    CURLOPT_TIMEOUT 		=> 30,
                    CURLOPT_HTTP_VERSION 	=> CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST 	=> "POST",
                    CURLOPT_POSTFIELDS 		=> "origin=19&originType=city&destination=".request()->get('kec_id')."&destinationType=subdistrict&weight=1&courier=".$kurir[$i]."", //origin = 16 -> BALIKPAPAN
                    CURLOPT_HTTPHEADER 		=> array(
                        "content-type: application/x-www-form-urlencoded",
                        "key:7d23bffcefc9680802db1d10ba43eb67" //pro key rajaongkir
                    ),
                ));
                $response 	= curl_exec($curl);
                $err 		= curl_error($curl);
                curl_close($curl);
                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    $dataongkir = json_decode($response, true);
                }

                foreach($dataongkir['rajaongkir']['results'] as $key => $datajenisongkir){
                    foreach($datajenisongkir['costs'] as $databiaya){
                        foreach($databiaya['cost'] as $data_){
                            $n[] = ['dataongkir' => "<option value='".
                                                    $datajenisongkir['code'].",".
                                                    $databiaya['service'].",".
                                                    $data_['value'].",".
                                                    $data_['etd']."'>".
                                                    "<strong class='text-uppercase'>".
                                                        $datajenisongkir['code']." ".$databiaya['service'].
                                                    "</strong>".
                                                    ": Rp. ".$data_['value']." - ".
                                                    $data_['etd']." "."Hari".
                                                    "</option>"];
                        }
                    }
                }
                $ongkir[] = $dataongkir;
            }
            return $n;
        }
    }

    public function index()
    {
        if (!null == Auth::guard('agen')->user()->status_agen) {
            if (!null == Auth::guard('agen')->user()->produk) {
                $produkevent = Event::where('publish_jv', 1)
                            ->where('harga', '>', 0)
                            ->orderBy('end', 'desc')
                            ->paginate(21);
            } else {
                $produkevent = Event::where('publish_jv', 21)
                            ->where('harga', '>', 0)
                            ->orderBy('end', 'desc')
                            ->paginate(21);
            }
            return view('agen.page.produk.index', compact('produkevent'));
        } else {
            $produkevent = Event::where('publish', 1)
                            ->where('harga', '>', 0)
                            ->orderBy('end', 'desc')
                            ->paginate(21);
            return view('agen.page.produk.index', compact('produkevent'));
        }

    }

    public function leadmagnet()
    {
        if (!null == Auth::guard('agen')->user()->status_agen) {
            if (!null == Auth::guard('agen')->user()->produk) {
                $produkevent = Event::where('publish_jv', 1)
                            ->where('harga', '=', 0)
                            ->orderBy('end', 'desc')
                            ->paginate(21);
            } else {
                $produkevent = Event::where('publish_jv', 21)
                            ->where('harga', '=', 0)
                            ->orderBy('end', 'desc')
                            ->paginate(21);
            }
            return view('agen.page.produk.lead-magnet', compact('produkevent'));
        } else {
            $produkevent = Event::where('publish', 1)
                            ->where('harga', '=', 0)
                            ->orderBy('end', 'desc')
                            ->paginate(21);
            return view('agen.page.produk.lead-magnet', compact('produkevent'));
        }

    }

    public function pesan()
    {
        $produk = Event::where('uuid', request()->id)->first();

        //verifikasi
        if ($produk == null) {
            notify()->error('Produk Tidak Valid !');
            return redirect()->route('agen.page.produk.index');
        } else {
            if ($produk->open === 0 || Carbon::now() < $produk->start || Carbon::now() > $produk->end) {
                notify()->error('Produk Tidak Valid !');
                return redirect()->route('agen.page.produk.index');
            }
        }

        //jenis produk
        if ($produk->produk->jenis == 'ECOURSE') {
            return view('agen.page.produk.pesan', compact('produk'));
        } elseif ($produk->produk->jenis == 'BUKU') {
            $dataprovinsi = $this->ekspedisi('province', null, false);
            return view('agen.page.produk.pesan-buku', compact('produk', 'dataprovinsi'));
        }
    }

    public function pesansimpan()
    {
        //verifikasi produk
        $produk = Event::where('uuid', request()->id)->first();
        if ($produk == null) {
            notify()->error('Produk Tidak Valid !');
            return redirect()->route('agen.page.produk.index');
        } else {
            if ($produk->open === 0 || Carbon::now() < $produk->start || Carbon::now() > $produk->end) {
                notify()->error('Produk Tidak Valid !');
                return redirect()->route('agen.page.produk.index');
            }
        }

        //generelasi no wa
        $nohp = request()->nohp;
        if (substr($nohp, 0, 1) === '0') {
            $nohp = substr($nohp, 1);
        } elseif (substr($nohp, 0, 2) === '62') {
            $nohp = substr($nohp, 2);
        } elseif (substr($nohp, 0, 3) === '+62') {
            $nohp = substr($nohp, 3);
        } else {
            $nohp = $nohp;
        }

        // cek data yang sama
        $cekdataemailnohp = Transaction::where('id_event', $produk->id)->where('email', request()->email)->where('nohp', $nohp)->first();
        $cekdataemail     = Transaction::where('id_event', $produk->id)->where('email', request()->email)->first();
        $cekdatanohp      = Transaction::where('id_event', $produk->id)->where('nohp', $nohp)->first();

        if (!null == $cekdataemailnohp) {
            return redirect()->back()->withInput()->withFlashDanger('Maaf, <strong>Email '.request()->email.' dan No Handphone '.$nohp.' </strong> Sudah digunakan Di Produk ini !');
        }
        if (!null == $cekdataemail) {
            return redirect()->back()->withInput()->withFlashDanger('Maaf, <strong>Email '.request()->email.'</strong>  Sudah digunakan Di Produk ini !');
        }
        if (!null == $cekdatanohp) {
            return redirect()->back()->withInput()->withFlashDanger('Maaf, <strong>No Handphone '.$nohp.'</strong>  Sudah digunakan Di Produk ini !');
        }
        $kodeunik = 0;

        $data            = new Transaction;
        $data->uuid      = Str::uuid();
        $data->id_event  = $produk->id;
        $data->id_agen   = Auth::guard('agen')->user()->id;
        $data->nama      = strtoupper(request()->nama);
        $data->panggilan = request()->panggilan;
        $data->gender    = request()->jeniskelamin;
        $data->kode_nohp = request()->kode_nohp;
        $data->nohp      = $nohp;
        $data->tgllahir  = request()->tgl.'-'.request()->bln.'-'.request()->thn;
        $data->email     = request()->email;
        $data->domisili  = request()->kota;
        $data->total     = $produk->harga + $kodeunik;
        $data->kodeunik  = $kodeunik;
        $data->jenis     = 1; // TRANSAKSI MANUAL - AGEN INPUT
        $data->status    = 1; // 1 BELUM AKTIF - 2 MENUNGGU KONFIRMASI - 3 AKTIF
        $data->save();

        //jika produk buku
        if ($produk->produk->jenis == 'BUKU') {
            $ongkir_                    = explode(',', request()->input('ongkir'));
            $kota_                      = $this->ekspedisi('city?id=', request()->get('kota'), true);
            $provinsi_                  = $this->ekspedisi('province?id=', request()->get('provinsi'), true);
            $kecamatan_                 = $this->ekspedisi('subdistrict?id=', request()->get('kecamatan'), true);

            $pesanbuku                  = new BookTransactions;
            $pesanbuku->id_transaction  = $data->id;
            $pesanbuku->uuid            = Str::uuid();
            $pesanbuku->kota            = strtoupper($kota_['rajaongkir']['results']['city_name']);
            $pesanbuku->provinsi        = strtoupper($provinsi_['rajaongkir']['results']['province']);
            $pesanbuku->kecamatan       = strtoupper($kecamatan_['rajaongkir']['results']['subdistrict_name']);
            $pesanbuku->negara          = 'INDONESIA';
            $pesanbuku->kota_pengiriman = 'BALIKPAPAN';
            $pesanbuku->kodepos         = request()->input('kodepos');
            $pesanbuku->alamat          = strtoupper(request()->input('alamat'));
            $pesanbuku->jumlah_buku     = request()->input('jumlah');
            $pesanbuku->harga           = request()->input('harga');
            $pesanbuku->ongkir          = request()->input('hargaongkir');
            $pesanbuku->total           = request()->input('total');
            $pesanbuku->kurir           = strtoupper($ongkir_[0].' '.$ongkir_[1]);
            $pesanbuku->save();
        }

        notify()->success('Data Berhasil Ditambahkan :)');
        return redirect()->route('agen.page.produk.transaksi');
    }

    public function transaksi()
    {
        $transaksi = Transaction::where('id_agen', Auth::guard('agen')->user()->id)
                                ->when(request()->get('nama'), function ($query) {
                                    if( request()->get('nama') != null) {
                                        return $query->whereRaw('LOWER(nama) LIKE ? ', '%' . strtolower(request()->get('nama')) . '%');
                                    }
                                })
                                ->when(request()->get('email'), function ($query) {
                                    if( request()->get('email') != null) {
                                        return $query->whereRaw('LOWER(email) LIKE ? ', '%' . strtolower(request()->get('email')) . '%');
                                    }
                                })
                                ->when(request()->get('produk'), function ($query) {
                                    if( request()->get('produk') != null) {
                                        return $query->where('id_event', request()->get('produk'));
                                    }
                                })
                                ->when(request()->get('status'), function ($query) {
                                    if( request()->get('status') != null) {
                                        return $query->where('status', request()->get('status'));
                                    }
                                })
                                ->when(request()->get('jenis'), function ($query) {
                                    if( request()->get('jenis') != null) {
                                        return $query->where('jenis', request()->get('jenis'));
                                    }
                                })
                                ->orderBy('created_at', 'desc')
                                ->paginate(5);

        $belumaktif = Transaction::where('id_agen', Auth::guard('agen')->user()->id)->where('status', 1)->count();
        $konfirmasi = Transaction::where('id_agen', Auth::guard('agen')->user()->id)->where('status', 2)->count();
        $aktif      = Transaction::where('id_agen', Auth::guard('agen')->user()->id)->where('status', 3)->count();
        $total      = Transaction::where('id_agen', Auth::guard('agen')->user()->id)->count();

        $produklist = Transaction::select('id_event')
                                ->groupBy('id_event')
                                ->where('id_agen', Auth::guard('agen')->user()->id)
                                ->get();
        // dd($produklist);
        if (request()->metode == 'hapus') {
            $cekdata = Transaction::where('id_agen', Auth::guard('agen')->user()->id)
                                    ->where('status', 1)
                                    ->where('id', request()->id)
                                    ->first();
            if (!null == $cekdata) {
                $cekdata->delete();
                notify()->success('Data Berhasil Dihapus !');
                return redirect()->back();
            } else {
                notify()->error('Data Tidak Valid !');
                return redirect()->back();
            }
        }

        return view('agen.page.produk.transaksi', compact('transaksi', 'belumaktif', 'konfirmasi', 'aktif', 'total', 'produklist'));
    }

    public function pembayaran()
    {
        $data = Transaction::where('uuid', request()->id)->first();

        $generate = $data->email . '-' . data_get($data->event, 'id_event') . '-' . Str::random(5);
        Session::put('sesiupload', $generate);
        Session::put('idevent', $data->id_event);
        //verifikasi
        if (request()->id == null) {
            return redirect()->route('agen.page.produk.transaksi');
        } elseif ($data == null) {
            return redirect()->route('agen.page.produk.transaksi');
        }
        return view('agen.page.produk.pembayaran', compact('data'));
    }

    public function pembayaranupload()
    {
        request()->validate([
            'filepond' => 'required|image|max:10240',
        ]);
        $file_bukti_transfer      = request()->file('filepond');
        Session::put('filebuktitransfer', Session::get('sesiupload') . '.' . $file_bukti_transfer->getClientOriginalExtension()); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
        $buktitf         = Image::make($file_bukti_transfer);
        if ($_SERVER['HTTP_HOST'] == 'dashboard.agen-entrepreneurid.com') {
            $lokasibuktitf   = public_path('../../../public_html/agen-entrepreneurid.com/dashboard/app/public/bukti-transfer/');
        } else {
            $lokasibuktitf   = public_path('/app/public/bukti-transfer/');
        }
        $buktitf->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $buktitf->save($lokasibuktitf.Session::get('filebuktitransfer'));
    }

    public function pembayaransimpan()
    {
        $data               = Transaction::where('uuid', request()->id)->first();
        $data->bukti_tf     = Session::get('filebuktitransfer');
        $data->waktu_upload = Carbon::now();
        $data->status       = 2;
        $data->keterangan   = request()->rekening;
        $data->save();

        notify()->success('Bukti Transfer Berhasil Diupload :)');
        return redirect()->route('agen.page.produk.transaksi');

    }

    public function pembayaranuploadafiliasi()
    {
        request()->validate([
            'filepond' => 'required|image|max:10240',
        ]);
        $event = Event::find(Session::put('idevent'));
        $path  = strval('../../../public_html/'.$event->produk->domain.'/konfirmasi/app/public/bukti-transfer/');
        $file_bukti_transfer      = request()->file('filepond');
        Session::put('filebuktitransfer', Session::get('sesiupload') . '.' . $file_bukti_transfer->getClientOriginalExtension()); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
        $buktitf         = Image::make($file_bukti_transfer);
        $lokasibuktitf   = public_path($path);
        $buktitf->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $buktitf->save($lokasibuktitf.Session::get('filebuktitransfer'));
    }

    public function transaksilama()
    {
        $transaksi = Transaction::where('id_agen', Auth::guard('agen')->user()->id)
                                ->when(request()->get('nama'), function ($query) {
                                    if( request()->get('nama') != null) {
                                        return $query->whereRaw('LOWER(nama) LIKE ? ', '%' . strtolower(request()->get('nama')) . '%');
                                    }
                                })
                                ->when(request()->get('email'), function ($query) {
                                    if( request()->get('email') != null) {
                                        return $query->whereRaw('LOWER(email) LIKE ? ', '%' . strtolower(request()->get('email')) . '%');
                                    }
                                })
                                ->when(request()->get('produk'), function ($query) {
                                    if( request()->get('produk') != null) {
                                        return $query->where('id_event', request()->get('produk'));
                                    }
                                })
                                ->when(request()->get('status'), function ($query) {
                                    if( request()->get('status') != null) {
                                        return $query->where('status', request()->get('status'));
                                    }
                                })
                                ->when(request()->get('jenis'), function ($query) {
                                    if( request()->get('jenis') != null) {
                                        return $query->where('jenis', request()->get('jenis'));
                                    }
                                })
                                ->orderBy('created_at', 'desc')
                                ->paginate(5);

        return view('agen.page.produk.transaksi-lama', compact('transaksi'));
    }

    public function transaksiexport(){
        return (new DataAgenTransaksi)->download('Data Transaksi - Agen '.Auth::guard('agen')->user()->name.'.xlsx');
    }

    public function leadmagnetexport(){
        return (new DataLeadMagnetAgen)->download('Data Produk Lead Magnet - Agen '.Auth::guard('agen')->user()->name.'.xlsx');
    }
}
