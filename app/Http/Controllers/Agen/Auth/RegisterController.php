<?php

namespace App\Http\Controllers\Agen\Auth;

use App\Models\Auth\Agen;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Mail\AgenMigrasiDashboard;
use App\Mail\AgenBaruDashboard;
use Illuminate\Support\Facades\Mail;
use Session;
use App\Models\Event;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new admins as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/agen';
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('agen.guest:agen');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => ['required', 'string', 'max:255'],
            'notelp'   => ['required', 'string', 'max:14', 'unique:agens'],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:agens'],
            'username' => ['required', 'string', 'max:100', 'unique:agens'],
            'password' => ['required', 'string', 'min:4', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Models\Auth\Agen
     */
    protected function create(array $data)
    {
        $cekeventaktif = Event::where('open_jv', 1)->first();

        if (!empty($data['agenbaru'])) {
            // Mail::to($data['email'])->send(new AgenBaruDashboard($data));
            return Agen::create([
                'name'              => ucwords($data['name']),
                'email'             => strtolower($data['email']),
                'username'          => strtolower($data['username']),
                'password'          => Hash::make($data['password']),
                'kode_notelp'       => $data['kode_notelp'] ?? 62,
                'notelp'            => $data['notelp'],
                'nama_rek'          => strtoupper($data['nama_rek']) ?? NULL,
                'no_rek'            => strtoupper($data['no_rek']) ?? NULL,
                'bank_rek'          => strtoupper($data['bank_rek']) ?? NULL,
                'gender'            => $data['gender'],
                'tgl_lahir'         => empty($data['tgl']) ? null : $data['tgl'] . '-' . $data['bln'] . '-' . $data['thn'],
                'kota'              => empty($data['kota']) ? null : strtoupper($data['kota']),
                'alamat'            => empty($data['alamat']) ? null : $data['alamat'],
                'email_verified_at' => Carbon::now(),
                'status_agen'       => $data['ref'] ?? null, // Daftar Sub Agen - 1
                'status_aktif'      => 0,
                'produk'            => $cekeventaktif->id ?? 0,
            ]);
        } else {
            Mail::to($data['email'])->send(new AgenMigrasiDashboard($data));
            return Agen::create([
                'id'                => $data['kode'],
                'name'              => $data['name'],
                'email'             => $data['email'],
                'username'          => $data['username'],
                'password'          => Hash::make($data['password']),
                'kode_notelp'       => $data['kode_notelp'] ?? 62,
                'notelp'            => $data['notelp'],
                'nama_rek'          => $data['nama_rek'] ?? NULL,
                'no_rek'            => $data['no_rek'] ?? NULL,
                'bank_rek'          => $data['bank_rek'] ?? NULL,
                'gender'            => $data['gender'],
                'tgl_lahir'         => $data['tgllahir'],
                'kota'              => $data['kota'] ?? NULL,
                'alamat'            => $data['alamat'] ?? NULL,
                'status_aktif'      => $data['agenbaru'] ?? 1,
                'email_verified_at' => Carbon::now(),
            ]);
        }
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $cekdata = DB::table('biodata')
                    ->where('id', request()->kode)
                    ->where('email', request()->email)
                    ->where('passwordd', request()->token)
                    ->first();
        $cekuseragen = Agen::where('email', request()->email)->first();

        if ($cekdata == null) {
            return redirect('/');
        } else {
            if ($cekuseragen != null) {
                session()->put('flash_success', 'Akun Anda Sudah Terdaftar Di Dashboard Ini ');
                return redirect()->to('/login?em='.$cekuseragen->email);
            } else {
                if ($cekdata->aktif == 2 && $cekdata->status_user == 1) {
                    return view('agen.auth.register', compact('cekdata'));
                } else {
                    return redirect('https://entrepreneurid.co/');
                }
            }

        }

    }

    public function daftar()
    {
        return view('agen.auth.daftar');
    }

    public function daftarsub()
    {
        return view('agen.auth.daftar-sub');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('agen');
    }

}
