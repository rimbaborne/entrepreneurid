<?php

namespace App\Http\Controllers\Agen;

use App\Http\Controllers\Controller;
use App\Models\Auth\Agen;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use UxWeb\SweetAlert\SweetAlert;
use Illuminate\Support\Facades\Hash;
use App\Models\Event;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use App\Models\SlideAgen;

class HomeController extends Controller
{

    // protected $redirectTo = '/agen/login';
    protected $redirectTo = '/login';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('agen.auth:agen');
    }

    /**
     * Show the Agen dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {

        $slideaktif = SlideAgen::where('aktif', '=', 1)->orderBy('sort', 'asc')->paginate(10, ['*'], 'aktif_page');

        if (!null == Auth::guard('agen')->user()->status_agen) {
            return redirect()->route('agen.page.profile');
        } else {
            return view('agen.home', compact('slideaktif'));
        }

    }

    public function amunisi()
    {
        return view('agen.page.amunisi');
    }

    public function informasi()
    {
        return view('agen.page.informasi');
    }

    public function leaderboard()
    {
        if (!null == Auth::guard('agen')->user()->status_agen) {
            return redirect()->route('agen.page.profile');
        } else {
            if (request()->event) {
                $event = Event::where('uuid', '=', request()->event)->first();
            } else {
                $event = Event::publish()->latest('created_at')->first();
            }
            if ($event) {
                $dataevent = Event::orderBy('created_at', 'desc')->publish()->get();
                $total = Transaction::aktif()
                                ->where('id_event', '=', $event->id)
                                ->count();
                $leaderboard = Transaction::aktif()
                                ->where('id_event', '=', $event->id)
                                ->select('id_agen', DB::raw('COUNT(*) as jumlah '))
                                ->where('id_agen', '!=', 32579) // dewangga
                                ->where('id_agen', '!=', 54608) // wulan
                                ->where('id_agen', '!=', 100001) // wulan
                                // ->where('id_agen', '!=', 15635) // gagang
                                ->groupBy('id_agen')
                                ->orderBy('jumlah', 'desc')
                                ->paginate(10); // 10 Besar
                return view('agen.page.leaderboard', compact('leaderboard', 'dataevent', 'event'));
            } else {
                $event = null;
                return view('agen.page.leaderboard', compact('event'));
            }
        }

    }

    public function menuagen()
    {
        return view('agen.page.menu-agen');
    }

    public function password()
    {
        return view('agen.page.password');
    }

    public function subagen()
    {
        $dataagen = Agen::where('status_agen',  Auth::guard('agen')->user()->id)
                        ->when(request()->get('nama'), function ($query) {
                            if( request()->get('nama') != null) {
                                return $query->whereRaw('LOWER(name) LIKE ? ', '%' . strtolower(request()->get('nama')) . '%');
                            }
                        })
                        ->when(request()->get('email'), function ($query) {
                            if( request()->get('email') != null) {
                                return $query->whereRaw('LOWER(email) LIKE ? ', '%' . strtolower(request()->get('email')) . '%');
                            }
                        })
                        ->paginate(10);
        return view('agen.page.subagen', compact('dataagen'));
    }
}
