<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
use Illuminate\Support\Str;

class TransactionController extends Controller
{
    public function create(Request $request)
    {
        $customer = new Transaction;
        $customer->uuid = Str::uuid();
        $customer->id_event = $request->input('id_event');
        $customer->id_agen = $request->input('id_agen');
        $customer->nama = strtoupper($request->input('nama'));
        $customer->email = strtolower($request->input('email'));
        $customer->kode_nohp = $request->input('kode_nohp');
        $customer->nohp = $request->input('nohp');
        // $customer->panggilan = $request->input('panggilan');
        // $customer->domisili = strtoupper($request->input('domisili'));
        // $customer->tgllahir = $request->input('tgllahir');
        // $customer->gender = $request->input('gender');
        $customer->status = '1'; //BELUM AKTIF
        $customer->jenis = '2'; //AFILIASI
        $customer->kodeunik = 0;
        $customer->total = $request->input('total');
        $customer->save();

        return response()->json(['message' => 'Created successfully', 'data' => $customer], 201);
    }

    public function show($uuid)
    {
        $customer = Transaction::where('uuid', $uuid)->first();

        if (!$customer) {
            return response()->json(['error' => 'Customer not found'], 404);
        }

        return response()->json(['data' => $customer], 200);
    }
}

