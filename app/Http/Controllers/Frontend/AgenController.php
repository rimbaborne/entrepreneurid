<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\Agen;
use App\Repositories\Frontend\Agen\AgenRepository;
use App\Http\Requests\Frontend\Agen\ManageAgenRequest;
use App\Http\Requests\Frontend\Agen\StoreAgenRequest;
use App\Http\Requests\Frontend\Agen\UpdateAgenRequest;

use App\Events\Frontend\Agen\AgenCreated;
use App\Events\Frontend\Agen\AgenUpdated;
use App\Events\Frontend\Agen\AgenDeleted;

class AgenController extends Controller
{
    /**
     * @var AgenRepository
     */
    protected $agenRepository;

    /**
     * AgenController constructor.
     *
     * @param AgenRepository $agenRepository
     */
    public function __construct(AgenRepository $agenRepository)
    {
        $this->agenRepository = $agenRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageAgenRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageAgenRequest $request)
    {
        return view('frontend.agen.index')
            ->withagens($this->agenRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManageAgenRequest    $request
     *
     * @return mixed
     */
    public function create(ManageAgenRequest $request)
    {
        return view('frontend.agen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAgenRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreAgenRequest $request)
    {
        $this->agenRepository->create($request->only(
            'id_agen'
        ));

        // Fire create event (AgenCreated)
        event(new AgenCreated($request));

        return redirect()->route('frontend.agens.index')
            ->withFlashSuccess(__('frontend_agens.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManageAgenRequest  $request
     * @param Agen               $agen
     *
     * @return mixed
     */
    public function show(ManageAgenRequest $request, Agen $agen)
    {
        return view('frontend.agen.show')->withAgen($agen);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageAgenRequest $request
     * @param Agen              $agen
     *
     * @return mixed
     */
    public function edit(ManageAgenRequest $request, Agen $agen)
    {
        return view('frontend.agen.edit')->withAgen($agen);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAgenRequest  $request
     * @param Agen               $agen
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateAgenRequest $request, Agen $agen)
    {
        $this->agenRepository->update($agen, $request->only(
            'id_agen'
        ));

        // Fire update event (AgenUpdated)
        event(new AgenUpdated($request));

        return redirect()->route('frontend.agens.index')
            ->withFlashSuccess(__('frontend_agens.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageAgenRequest $request
     * @param Agen              $agen
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageAgenRequest $request, Agen $agen)
    {
        $this->agenRepository->deleteById($agen->id);

        // Fire delete event (AgenDeleted)
        event(new AgenDeleted($request));

        return redirect()->route('frontend.agens.deleted')
            ->withFlashSuccess(__('frontend_agens.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManageAgenRequest $request
     * @param Agen              $deletedAgen
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageAgenRequest $request, Agen $deletedAgen)
    {
        $this->agenRepository->forceDelete($deletedAgen);

        return redirect()->route('frontend.agens.deleted')
            ->withFlashSuccess(__('frontend_agens.alerts.deleted_permanently'));
    }

    /**
     * @param ManageAgenRequest $request
     * @param Agen              $deletedAgen
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageAgenRequest $request, Agen $deletedAgen)
    {
        $this->agenRepository->restore($deletedAgen);

        return redirect()->route('frontend.agens.index')
            ->withFlashSuccess(__('frontend_agens.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManageAgenRequest $request
     *
     * @return mixed
     */
    public function deleted(ManageAgenRequest $request)
    {
        return view('frontend.agen.deleted')
            ->withagens($this->agenRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
