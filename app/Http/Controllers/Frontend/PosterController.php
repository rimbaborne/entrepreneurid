<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\Poster;
use App\Repositories\Frontend\Poster\PosterRepository;
use App\Http\Requests\Frontend\Poster\ManagePosterRequest;
use App\Http\Requests\Frontend\Poster\StorePosterRequest;
use App\Http\Requests\Frontend\Poster\UpdatePosterRequest;

use App\Events\Frontend\Poster\PosterCreated;
use App\Events\Frontend\Poster\PosterUpdated;
use App\Events\Frontend\Poster\PosterDeleted;

class PosterController extends Controller
{
    /**
     * @var PosterRepository
     */
    protected $posterRepository;

    /**
     * PosterController constructor.
     *
     * @param PosterRepository $posterRepository
     */
    public function __construct(PosterRepository $posterRepository)
    {
        $this->posterRepository = $posterRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManagePosterRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posters = Poster::paginate(10);
        return view('frontend.poster.index', compact('posters'));
        // return view('frontend.poster.index')
        //     ->withposters($this->posterRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManagePosterRequest    $request
     *
     * @return mixed
     */
    public function create(ManagePosterRequest $request)
    {
        return view('frontend.poster.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePosterRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StorePosterRequest $request)
    {
        $this->posterRepository->create($request->only(
            'nama'
        ));

        // Fire create event (PosterCreated)
        event(new PosterCreated($request));

        return redirect()->route('frontend.posters.index')
            ->withFlashSuccess(__('frontend_posters.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManagePosterRequest  $request
     * @param Poster               $poster
     *
     * @return mixed
     */
    public function show(ManagePosterRequest $request, Poster $poster)
    {
        return view('frontend.poster.show')->withPoster($poster);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManagePosterRequest $request
     * @param Poster              $poster
     *
     * @return mixed
     */
    public function edit(ManagePosterRequest $request, Poster $poster)
    {
        return view('frontend.poster.edit')->withPoster($poster);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePosterRequest  $request
     * @param Poster               $poster
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdatePosterRequest $request, Poster $poster)
    {
        $this->posterRepository->update($poster, $request->only(
            'nama'
        ));

        // Fire update event (PosterUpdated)
        event(new PosterUpdated($request));

        return redirect()->route('frontend.posters.index')
            ->withFlashSuccess(__('frontend_posters.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManagePosterRequest $request
     * @param Poster              $poster
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManagePosterRequest $request, Poster $poster)
    {
        $this->posterRepository->deleteById($poster->id);

        // Fire delete event (PosterDeleted)
        event(new PosterDeleted($request));

        return redirect()->route('frontend.posters.deleted')
            ->withFlashSuccess(__('frontend_posters.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManagePosterRequest $request
     * @param Poster              $deletedPoster
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManagePosterRequest $request, Poster $deletedPoster)
    {
        $this->posterRepository->forceDelete($deletedPoster);

        return redirect()->route('frontend.posters.deleted')
            ->withFlashSuccess(__('frontend_posters.alerts.deleted_permanently'));
    }

    /**
     * @param ManagePosterRequest $request
     * @param Poster              $deletedPoster
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManagePosterRequest $request, Poster $deletedPoster)
    {
        $this->posterRepository->restore($deletedPoster);

        return redirect()->route('frontend.posters.index')
            ->withFlashSuccess(__('frontend_posters.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManagePosterRequest $request
     *
     * @return mixed
     */
    public function deleted(ManagePosterRequest $request)
    {
        return view('frontend.poster.deleted')
            ->withposters($this->posterRepository->getDeletedPaginated(25, 'id', 'asc'));
    }

    public function desain()
    {
        return view('frontend.poster.desain');
    }
}
