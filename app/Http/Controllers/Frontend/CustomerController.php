<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Transaction;
use Illuminate\Support\Facades\Http;
use Carbon\Carbon;
use Throwable;
use Illuminate\Support\Facades\Mail;
use App\Mail\emailv2;
use Session;
use Str;
use Image;
use App\Models\BookTransactions;

class CustomerController extends Controller
{
    public function __construct(){
        $this->nama          = request()->nama ?? null;
        $this->email         = request()->email ?? null;
        $this->panggilan     = request()->panggilan ?? null;
        $this->gender        = request()->gender ?? null;
        $this->tgllahir      = request()->tgl . '-' . request()->bln . '-' . request()->thn ?? null;
        $this->kode_nohp     = request()->kode_nohp ?? null;
        $this->nohp          = $this->nohp(request()->nohp) ?? null;
        $this->kota          = request()->kota ?? null;
        $this->ref           = request()->ref ?? null;
        $this->total         = $this->total ?? null;
        $this->ekspedisi     = $this->ekspedisi ?? null;
        $this->jmlh_buku     = $this->jmlh_buku ?? null;
        $this->alamat        = $this->alamat ?? null;
        $this->kodeunik      = 0; //rand(2,600);
        $this->cekeventaktif = empty(request()->manual)
                               ?
                               Event::where('link_form', 'like', '%'.request()->getHost().'%')
                                    ->where('open', 1)
                                    ->where('start', '<', Carbon::now())
                                    ->where('end', '>', Carbon::now())
                                    ->first()
                               :
                               Event::where('link_form', 'like', '%'.request()->getHost().'%')->first()
                               ;
        $this->cekeventgratis = Event::where('link_form', 'like', '%'.request()->getHost().'%')
                                    ->where('open', 1)
                                    ->where('harga', '=', 0)
                                    ->first()
                                ??
                                null
                               ;
        $this->cekeventmanual = Event::where('link_form', 'like', '%'.request()->getHost().'%')->first();
        $this->cekeventaktifinvoice = Event::where('link_form', 'like', '%'.request()->getHost().'%')
                                            ->first();
    }

    public function nohp($nomor){
        if (substr($nomor, 0, 1) === '0') {
            return substr($nomor, 1);
        } elseif (substr($nomor, 0, 2) === '62') {
            return substr($nomor, 2);
        } elseif (substr($nomor, 0, 3) === '+62') {
            return substr($nomor, 3);
        } else {
            return $nomor;
        }
    }

    public function convertnotif($isi)
    {
        $notifikasi = str_replace('{Nama}', $this->nama, $isi);
        $notifikasi = str_replace('{E-mail}', $this->email, $notifikasi);
        $notifikasi = str_replace('{Panggilan}', $this->panggilan, $notifikasi);
        $notifikasi = str_replace('{Gender}', $this->gender, $notifikasi);
        $notifikasi = str_replace('{No. Telepon}', $this->kode_nohp.$this->nohp, $notifikasi);
        $notifikasi = str_replace('{Kota}', $this->kota, $notifikasi);
        $notifikasi = str_replace('{Nominal Transfer}', $this->total, $notifikasi);
        $notifikasi = str_replace('{Ekspedisi}', $this->ekspedisi, $notifikasi);
        $notifikasi = str_replace('{Jumlah Buku}', $this->jmlh_buku, $notifikasi);
        $notifikasi = str_replace('{Alamat}', $this->alamat, $notifikasi);

        return $notifikasi;
    }

    public function notifemail($penerima, $isiemail)
    {
        $dataemail = json_decode($isiemail);
        $data = [
            'name'     => $dataemail->name,
            'email'    => $dataemail->email,
            'subject'  => $dataemail->subject,
            'isiemail' => $this->convertnotif($dataemail->isi),
        ];

        Mail::to($penerima)->send(new emailv2($data));
    }

    public function notifwa($nomorhp, $isipesan)
    {
        $datawa = json_decode($isipesan);

        $apikey     = env('WAHA_API_KEY');
        $url        = env('WAHA_API_URL');
        $sessionApi = env('WAHA_API_SESSION');
        $requestApi = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Accept'       => 'application/json',
            'X-Api-Key'    => $apikey,
        ]);

        // SOP based on https://waha.devlike.pro/docs/overview/how-to-avoid-blocking/

        try {
            #1 Send Seen
            $requestApi->post($url.'/api/sendSeen', [ "session" => $sessionApi, "chatId"  => $nomorhp.'@c.us', ]);

            #2 Start Typing
            $requestApi->post($url.'/api/startTyping', [ "session" => $sessionApi, "chatId"  => $nomorhp.'@c.us', ]);

            sleep(1); // jeda seolah olah ngetik

            #3 Stop Typing
            $requestApi->post($url.'/api/stopTyping', [ "session" => $sessionApi, "chatId"  => $nomorhp.'@c.us', ]);

            #4 Send Message
            $requestApi->post($url.'/api/sendText', [
                "session" => $sessionApi,
                "chatId"  => $nomorhp.'@c.us',
                "text"    => $this->convertnotif($datawa->isi),
            ]);
        } catch (Throwable $th) {
            throw $th;
        }
    }

    public function ekspedisi($dataUrl, $id, $type)
    {
        $id_ = $id != null ? $id : "";

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => "https://pro.rajaongkir.com/api/".$dataUrl.$id_,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => array(
                "key:7d23bffcefc9680802db1d10ba43eb67"
            ),
        ));
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            $result = $type ? json_decode($response, true) : json_decode($response);
            return $result;
        }
    }

    public function cekongkir()
    {
        if (request()->get('cari') == 'kota') {
            $datakota = $this->ekspedisi('city?province=', request()->get('provinsi'), true);

            $n[] = ['datakota' => "<option> Silahkan Pilih Kota/Kabupaten ..</option>"];

            foreach($datakota['rajaongkir']['results'] as $data){
                $n[] = ['datakota' => "<option data-kodepos='".$data['postal_code']."' value='".$data['city_id']."'>".$data['city_name']."</option>"];
            }
            return $n;

        } elseif (request()->get('cari') == 'kecamatan') {
            $datakecamatan = $this->ekspedisi('subdistrict?city=', request()->get('kota'), true);

            $n[] = ['datakecamatan' => "<option> Silahkan Pilih Kecamatan ..</option>"];

            foreach($datakecamatan['rajaongkir']['results'] as $data){
                $n[] = ['datakecamatan' => "<option value='".$data['subdistrict_id']."'>".$data['subdistrict_name']."</option>"];
            }
            return $n;

        } elseif (request()->get('cari') == 'ongkir') {
            $kurir = ['tiki', 'jne', 'pos', 'jnt'];
            for ($i=0; $i < 4; $i++) {
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL 			=> "https://pro.rajaongkir.com/api/cost",
                    CURLOPT_RETURNTRANSFER 	=> true,
                    CURLOPT_ENCODING 		=> "",
                    CURLOPT_MAXREDIRS 		=> 10,
                    CURLOPT_TIMEOUT 		=> 30,
                    CURLOPT_HTTP_VERSION 	=> CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST 	=> "POST",
                    CURLOPT_POSTFIELDS 		=> "origin=19&originType=city&destination=".request()->get('kec_id')."&destinationType=subdistrict&weight=1&courier=".$kurir[$i]."", //origin = 16 -> BALIKPAPAN
                    CURLOPT_HTTPHEADER 		=> array(
                        "content-type: application/x-www-form-urlencoded",
                        "key:7d23bffcefc9680802db1d10ba43eb67" //pro key rajaongkir
                    ),
                ));
                $response 	= curl_exec($curl);
                $err 		= curl_error($curl);
                curl_close($curl);
                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    $dataongkir = json_decode($response, true);
                }

                foreach($dataongkir['rajaongkir']['results'] as $key => $datajenisongkir){
                    foreach($datajenisongkir['costs'] as $databiaya){
                        foreach($databiaya['cost'] as $data_){
                            $n[] = ['dataongkir' => "<option value='".
                                                    $datajenisongkir['code'].",".
                                                    $databiaya['service'].",".
                                                    $data_['value'].",".
                                                    $data_['etd']."'>".
                                                    "<strong class='text-uppercase'>".
                                                        $datajenisongkir['code']." ".$databiaya['service'].
                                                    "</strong>".
                                                    ": Rp. ".$data_['value']." - ".
                                                    $data_['etd']." "."Hari".
                                                    "</option>"];
                        }
                    }
                }
                $ongkir[] = $dataongkir;
            }
            return $n;
        }
    }

    public function pemesanan()
    {
        $ref           = $this->ref;
        $cekeventaktif = $this->cekeventaktif;

        if ($this->cekeventaktif) {
            if ($this->cekeventaktif->produk->jenis == 'ECOURSE') {
                return view('frontend.user.customer.v2.pemesanan', compact('ref', 'cekeventaktif'));
            } elseif ($this->cekeventaktif->produk->jenis == 'BUKU') {
                $dataprovinsi = $this->ekspedisi('province', null, false);
                return view('frontend.user.customer.v2.pemesanan-buku', compact('ref', 'cekeventaktif', 'dataprovinsi'));
            }
        } else {
            return redirect()->to('https://'.$this->cekeventaktifinvoice->produk->domain);
        }
    }

    public function pesanmanual()
    {
        $ref           = $this->ref;
        $cekeventaktif = $this->cekeventmanual;

        if ($this->cekeventmanual->produk->jenis == 'ECOURSE') {
            return view('frontend.user.customer.v2.pemesanan', compact('ref', 'cekeventaktif'));
        } elseif ($this->cekeventmanual->produk->jenis == 'BUKU') {
            $dataprovinsi = $this->ekspedisi('province', null, false);
            return view('frontend.user.customer.v2.pemesanan-buku', compact('ref', 'cekeventaktif', 'dataprovinsi'));
        }
    }

    public function simpanpemesanan()
    {
        $cekemail = Transaction::
                where([
                    ['id_event', $this->cekeventaktif->id],
                    ['email', $this->email],
                ])->first();

        $ceknohp = Transaction::
                where([
                    ['id_event', $this->cekeventaktif->id],
                    ['nohp', $this->nohp],
                ])->first();

        if ($cekemail) {
            return redirect()->back()->withFlashDanger('Email sudah terdaftar diproduk yang sama.')->withInput();
        } elseif($ceknohp) {
            return redirect()->back()->withFlashDanger('Nomor Handphone sudah terdaftar diproduk yang sama.')->withInput();
        } else {

            // FUNGSI INI AKU SEBENERNYA AGA BINGUNG
            if ($this->cekeventgratis != null) {
                $cekdata = Transaction::ofEvent($this->cekeventgratis->id)
                                         ->where('email', strtolower($this->email))
                                         ->orWhere('nohp', $this->nohp)
                                         ->first();
                 if ($cekdata) {
                     $kode_agen = $cekdata->id_agen;
                 } else {
                    $kode_agen = $this->ref;
                 }
            } else {
                $kode_agen = $this->ref;
            }

            try {
                $customer              = new Transaction;
                $customer->uuid        = Str::uuid();
                $customer->id_event    = $this->cekeventaktif->id;
                $customer->id_agen     = $kode_agen;
                $customer->nama        = strtoupper($this->nama);
                $customer->email       = strtolower($this->email);
                $customer->kode_nohp   = $this->kode_nohp;
                $customer->nohp        = $this->nohp;
                $customer->panggilan   = $this->panggilan;
                $customer->domisili    = strtoupper($this->kota);
                $customer->tgllahir    = $this->tgllahir;
                $customer->gender      = $this->gender;
                $customer->status      = '1'; //BELUM AKTIF
                $customer->jenis       = '2'; //AFILIASI
                $customer->kodeunik    = $this->kodeunik;
                $customer->total       = $this->cekeventaktif->harga + $this->kodeunik;
                $customer->save();

                if($this->cekeventaktif->harga == 0 || $this->cekeventaktif->harga == '0'){
                    $customer->status = '2';
                    $customer->save();
                }

                if ($this->cekeventaktif->produk->jenis == 'BUKU') {
                    $ongkir_                    = explode(',', request()->input('ongkir'));
                    $kota_                      = $this->ekspedisi('city?id=', request()->get('kota'), true);
                    $provinsi_                  = $this->ekspedisi('province?id=', request()->get('provinsi'), true);
                    $kecamatan_                 = $this->ekspedisi('subdistrict?id=', request()->get('kecamatan'), true);

                    $pesanbuku                  = new BookTransactions;
                    $pesanbuku->id_transaction  = $customer->id;
                    $pesanbuku->uuid            = Str::uuid();
                    $pesanbuku->kota            = strtoupper($kota_['rajaongkir']['results']['city_name']);
                    $pesanbuku->provinsi        = strtoupper($provinsi_['rajaongkir']['results']['province']);
                    $pesanbuku->kecamatan       = strtoupper($kecamatan_['rajaongkir']['results']['subdistrict_name']);
                    $pesanbuku->negara          = 'INDONESIA';
                    $pesanbuku->kota_pengiriman = 'BALIKPAPAN';
                    $pesanbuku->kodepos         = request()->input('kodepos');
                    $pesanbuku->alamat          = strtoupper(request()->input('alamat'));
                    $pesanbuku->jumlah_buku     = request()->input('jumlah');
                    $pesanbuku->harga           = request()->input('harga');
                    $pesanbuku->ongkir          = request()->input('hargaongkir');
                    $pesanbuku->total           = request()->input('total');
                    $pesanbuku->kurir           = strtoupper($ongkir_[0].' '.$ongkir_[1]);
                    $pesanbuku->save();
                }

                $konfirmasi          = Transaction::find($customer->id);
                $this->total         = $konfirmasi->event->produk->jenis === 'BUKU' ? $konfirmasi->book->total : $konfirmasi->total;
                $this->ekspedisi     = $konfirmasi->event->produk->jenis === 'BUKU' ? $konfirmasi->book->kurir : null;
                $this->jmlh_buku     = $konfirmasi->event->produk->jenis === 'BUKU' ? $konfirmasi->book->jumlah_buku : null;
                $this->alamat        = $konfirmasi->event->produk->jenis === 'BUKU' ? $konfirmasi->book->alamat.', '.$konfirmasi->book->kecamatan.', '.$konfirmasi->book->kota.', '.$konfirmasi->book->provinsi : $konfirmasi->alamat;

                  if($this->cekeventaktif->komisi != '-'){
                      $this->notifwa($this->kode_nohp.$this->nohp, $this->cekeventaktif->notifikasi->wa_pendaftaran);
                 }

                // return redirect()->to('/pemesanan-selesai')->withFlashSuccess($this->nama . ' - ' . $this->email . ', Pendaftaran Berhasil !<br/> Periksa Notifikasi Whatsapp Anda. <br/>Terima kasih.<br><a href="/daftar?ref=' . $this->ref . '">Kembali Ke form Registrasi</a>');


                try {
                    if($this->cekeventaktif->komisi != '-'){
                        $this->notifemail($this->email, $this->cekeventaktif->notifikasi->email_pendaftaran);
                    }

                    return redirect()->to('/pemesanan-selesai')->withFlashSuccess($this->nama . ' - ' . $this->email . ', Pendaftaran Berhasil !<br/> Periksa E-mail Anda. <br/>Terima kasih.<br><a href="/daftar?ref=' . $this->ref . '">Kembali Ke form Registrasi</a>');
                } catch (Throwable $th) {
                    return redirect()->to('/pemesanan-selesai')->withFlashDanger($this->nama . ' - ' . $this->email . ', Terjadi Kesalahan Pengiriman E-mail. Namun, data berhasil Terdaftar<br/>Terima kasih.<br><a href="/daftar?ref=' . $this->ref . '">Kembali Ke form Registrasi</a>');
                }
            } catch (Throwable $td) {
                return redirect()->back()->withFlashDanger('Pendaftaran Gagal ! Terjadi kesalahan, mohon ulangi. <br>Terima Kasih')->withInput();
            }
        }
    }

    public function pemesananselesai()
    {
        $cekeventaktif = $this->cekeventaktif;

        return view('frontend.user.customer.v2.pemesanan-selesai', compact('cekeventaktif'));
    }

    public function invoice()
    {
        $cekeventaktif = $this->cekeventaktif;

        $data    = '';
        $status  = '';
        $wstatus = '';

        if (isset($this->email)) {
            $data = Transaction::where('email', $this->email)->where('id_event', $cekeventaktif->id)->first();
            if ($data) {
                $generate = $data->email . '-' . $cekeventaktif->id_event . '-' . Str::random(5);
                Session::put('sesiupload', $generate);
                // dd($data);

                if ($data->status == '1') {
                    $status  = 'BELUM UPLOAD';
                    $wstatus = 'danger';
                } elseif ($data->status == '2') {
                    $status  = 'MENUNGGU KONFIRMASI';
                    $wstatus = 'warning';
                } elseif ($data->status == '3') {
                    $status  = 'AKTIF';
                    $wstatus = 'info';
                }
            } else {
                $data = "0";
            }
        }

        if ($cekeventaktif) {
            return view('frontend.user.customer.v2.invoice', compact('cekeventaktif', 'data', 'status', 'wstatus'));
        } else {
            return redirect()->to('https://'.$this->cekeventaktifinvoice->produk->domain);
        }
    }

    public function uploadinvoice()
    {
        request()->validate([
            'filepond' => 'required|image|max:10240',
        ]);
        $file_bukti_transfer      = request()->file('filepond');
        Session::put('filebuktitransfer', Session::get('sesiupload') . '.' . $file_bukti_transfer->getClientOriginalExtension()); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
        $buktitf         = Image::make($file_bukti_transfer);
        if ($this->cekeventaktifinvoice) {
            if(strpos($this->cekeventaktifinvoice->link_form, 'app.') !== false){
                $lokasibuktitf   = public_path('../../../public_html/'.$this->cekeventaktifinvoice->produk->domain.'/app/app/public/bukti-transfer/');
            } else if(strpos($this->cekeventaktifinvoice->link_form, 'konfirmasi.') !== false){
                $lokasibuktitf   = public_path('../../../public_html/'.$this->cekeventaktifinvoice->produk->domain.'/konfirmasi/app/public/bukti-transfer/');
            }
        } else {
            $lokasibuktitf   = public_path('/app/public/bukti-transfer/');
        }
        $buktitf->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $buktitf->save($lokasibuktitf.Session::get('filebuktitransfer'));
    }

    public function simpaninvoice()
    {
        try {
            $data               = Transaction::find(request()->id);
            $data->bukti_tf     = Session::get('filebuktitransfer');
            $data->waktu_upload = Carbon::now();
            $data->status       = 2;
            $data->keterangan   = request()->rekening;
            $data->save();

            $this->nama  = $data->nama;

            $this->notifemail($data->email, $this->cekeventaktifinvoice->notifikasi->email_upload_pembayaran);

            return redirect()->back()->withFlashSuccess('Terima Kasih !, Anda berhasil upload pembayaran. Silahkan Periksa Email Anda. Kami Akan segera mengkonfirmasinya.');
        }catch (Throwable $td) {
            return redirect()->back()->withInput()->withFlashDanger('Maaf, Terjadi kesalahan. Mohon diulangi. Terima Kasih');

        }

    }
}
