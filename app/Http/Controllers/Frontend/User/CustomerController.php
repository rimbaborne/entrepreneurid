<?php

namespace App\Http\Controllers\Frontend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class CustomerController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function kocpraktikumpertama()
    {
        return view('frontend.user.customer.koc.praktikum-pertama');
    }

    public function kocpraktikumkedua()
    {
        return view('frontend.user.customer.koc.praktikum-kedua');
    }

    public function kocpraktikumketiga()
    {
        return view('frontend.user.customer.koc.praktikum-ketiga');
    }

    public function kocsertifikat(Request $request)
    {
        $email = $request->input('email');
        if (!isset($email)) {
            return redirect()->route('frontend.user.dashboard');
        }

        // try {
            $users     = DB::table('users')->where('email', $email)->first();


            $penilaian = DB::table('praktikums_nilaibabs')
                        ->where('id_praktikum_bab', 3)
                        ->where('user_praktikum_nilai', $users->id)
                        ->where('status_praktikum_nilai', 'Lulus Praktikum')
                        ->first();

            $valid     = DB::table('praktikums_nilaibabs')
                        ->where('user_praktikum_nilai', $users->id)
                        ->where('status_praktikum_nilai', 'Lulus Praktikum')
                        ->count();

            if ($valid == 3) {
                $nama    = $users->first_name;
                $tanggal = $penilaian->updated_at;
                $waktu   = \Carbon\Carbon::create($penilaian->updated_at)->format('d F Y');
                return view('frontend.user.customer.koc.sertifikat', compact('nama', 'tanggal', 'waktu'));
            }
        // } catch (\Throwable $th) {
        //     return redirect()->route('frontend.user.dashboard');
        // }

    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
    }
}
