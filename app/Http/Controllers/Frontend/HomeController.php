<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Http;
use App\Mail\AgenBaruPendaftaran;
use App\Mail\AgenBaruPembayaran;
use App\Mail\KSOPembayaran;
use App\Mail\KSOPendaftaran;
use App\Mail\KFHPembayaran;
use App\Mail\MICPembayaran;
use App\Mail\MICPendaftaran;
use App\Mail\MOMPembayaran;
use App\Mail\MOMPendaftaran;
use App\Mail\KOCPembayaran;
use App\Mail\KOCPendaftaran;
use App\Mail\CNLPembayaran;
use App\Mail\CNLPendaftaran;
use App\Mail\MSRPembayaran;
use App\Mail\MSRPendaftaran;
use Throwable;
use Illuminate\Support\Carbon;
use App\Models\Biodatum;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Models\Pemesanan;
use DB;
use Image;
use App\Models\Poster;
use UxWeb\SweetAlert\SweetAlert;
use App\Models\MemberMicAdvisor;
/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.index');
    }

    public function tes()
    {
        return view('frontend.user.modul.tes');
    }

    public function selesai()
    {
        return view('frontend.user.modul.selesai');
    }

    public function daftar(Request $request)
    {
        $ref          = $request->input('ref') ?? '0';
        $kodeunik     = rand(10, 500);
        // dd($dataprovinsi);
        // $dataprovinsi = Http::get('https://api.rajaongkir.com/starter/province');

        if ($_SERVER['HTTP_HOST'] == 'konfirmasi.agen-entrepreneurid.com' || $_SERVER['HTTP_HOST'] == 'eid.test') {
            return view('frontend.user.modul.daftar', compact('ref'));
        } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.kelas100orderan.com') {
            return view('frontend.user.modul.daftar', compact('ref'));
        } elseif ($_SERVER['HTTP_HOST'] == 'upload.mentoringorganicmarketing.com') {
            return view('frontend.user.modul.daftar', compact('ref'));
        } elseif ($_SERVER['HTTP_HOST'] == 'member.kelasonlinecopywriting.com'){
            return view('frontend.user.modul.daftar', compact('ref'));
        } elseif ($_SERVER['HTTP_HOST'] == 'upload.kelasdatabasewa.com') {
            return view('frontend.user.modul.daftar', compact('ref'));
        } elseif ($_SERVER['HTTP_HOST'] == 'app.copywritingnextlevel.com') {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://pro.rajaongkir.com/api/province",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key:7d23bffcefc9680802db1d10ba43eb67"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $dataprovinsi = json_decode($response);

            if(request()->get('order') == 'manual'){
                return view('frontend.user.customer.cnl.pesan-manual', compact('ref', 'dataprovinsi', 'kodeunik'));
            } else {
                return view('frontend.user.customer.cnl.pesan', compact('ref', 'dataprovinsi', 'kodeunik'));
            }
        } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringinstantcopywriting.com') {
            return view('frontend.user.modul.daftar', compact('ref'));
        } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringsuperreseller.com') {
            return view('frontend.user.modul.daftar', compact('ref'));
        }
        return view('frontend.user.customer.cnl.pesan', compact('ref', 'dataprovinsi', 'kodeunik'));
    }

    public function simpan(Request $request)
    {
        $nama         = $request->input('nama');
        $panggilan    = $request->input('panggilan');
        $jeniskelamin = $request->input('jeniskelamin');
        $nohp         = $request->input('nohp');
        $tgllahir     = $request->input('tgl') . '-' . $request->input('bln') . '-' . $request->input('thn');
        $email        = $request->input('email');
        $kota         = $request->input('kota');
        $ref          = $request->input('ref');
        $kodeunik     = null;
        $tgldaftar    = Carbon::now();

        if (substr($nohp, 0, 1) === '0') {
            $nohp = substr($nohp, 1);
        } elseif (substr($nohp, 0, 2) === '62') {
            $nohp = substr($nohp, 2);
        } elseif (substr($nohp, 0, 3) === '+62') {
            $nohp = substr($nohp, 3);
        } else {
            $nohp = $nohp;
        }

        if ($_SERVER['HTTP_HOST'] == 'konfirmasi.agen-entrepreneurid.com' || $_SERVER['HTTP_HOST'] == 'eid.test') {

            $cekemail = DB::table('biodata')
                ->where([
                    ['idprodukreg', '39'],
                    ['email', $email],
                ])
                ->orWhere([
                    ['idprodukreg', '39'],
                    ['no_telp', $nohp],
                ])
                ->first();

            if (is_null($cekemail)) {
                // try {
                $iddata = DB::table('biodata')->insertGetId(
                    [
                        'nama'        => $nama,
                        'email'       => $email,
                        'no_telp'     => $nohp,
                        'panggilan'   => $panggilan,
                        'kota'        => $kota,
                        'tgllahir'    => $tgllahir,
                        'gender'      => $jeniskelamin,
                        'ref'         => $ref,
                        'status_user' => '0',
                        'aktif'       => '0',
                        'tgl_daftar'  => $tgldaftar,
                        'idprodukreg' => '39', // Produk Agen Baru 2020
                        'kodeunik'    => $kodeunik,
                        'total'       => 96000,
                    ]
                );

                $apikey = '8c073ed79bf7f3796cf8e98a288ba69f6db5847d6ff364a2';
                $phone = '+62' . $nohp;
                $message =
                    'Halo ' . $nama . ' 😊🙏

Pesan ini Anda terima karena beberapa waktu lalu telah mengisi form pendaftaran *Agen Resmi entrepreneurID*

Agar Anda segera terdaftar sebagai Agen Resmi, maka silahkan transfer biaya pendaftaran *sebesar Rp. 96.000* ke salah satu rekening berikut :

Mandiri = 1360007600528
BCA = 1911488766
BRI = 012101009502532
BNI = 1238568920

Seluruh rekening barusan, atas nama *Wulandari Tri*

Setelah Anda menyelesaikan pendaftaran, langsung konfirmasi bukti transfernya ke link ini https://konfirmasi.agen-entrepreneurid.com/pembayaran

Jika Anda punya pertanyaan, silahkan balas pesan ini. Dengan senang hati kami akan membantu. 🙂

Salam,

_*Tim entrepreneurID*_';


                $url='http://116.203.191.58/api/send_message';
                $data = array(
                    "phone_no"  => $phone,
                    "key"		=> $apikey,
                    "message"	=> $message,
                    "skip_link"	=> True // This optional for skip snapshot of link in message
                );
                $data_string = json_encode($data);

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_VERBOSE, 0);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                curl_setopt($ch, CURLOPT_TIMEOUT, 360);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
                );
                echo $res=curl_exec($ch);
                curl_close($ch);

                $data = Biodatum::findOrFail($iddata);


                    Mail::to($data->email)->send(new AgenBaruPendaftaran($data));

                    return redirect()->to('/selesai')->withFlashSuccess($data->nama . ' - ' . $data->email . ', Pendaftaran Berhasil !<br/> Periksa E-mail Anda. <br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                // } catch (Throwable $th) {
                //     return redirect()->to('/selesai')->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                // }
            }

            return redirect()->back()->withFlashDanger('Pendaftaran Gagal ! E-mail / No. Telepon sudah terdaftar. <br>Terima Kasih')->withInput();
        }
        elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.kelas100orderan.com') {

            $cekemail = DB::table('biodata')
                ->where([
                    ['idprodukreg', '31'], //25
                    ['email', $email],
                ])
                ->orWhere([
                    ['idprodukreg', '31'],
                    ['no_telp', $nohp],
                ])
                ->first();

            if (is_null($cekemail)) {

                $iddata = DB::table('biodata')->insertGetId(
                    [
                        'nama'        => $nama,
                        'email'       => $email,
                        'no_telp'     => $nohp,
                        'panggilan'   => $panggilan,
                        'kota'        => $kota,
                        'tgllahir'    => $tgllahir,
                        'gender'      => $jeniskelamin,
                        'ref'         => $ref,
                        'status_user' => '0',
                        'aktif'       => '0',
                        'tgl_daftar'  => $tgldaftar,
                        'idprodukreg' => '31', // Produk Agen Baru 2020
                        'kodeunik'    => null,
                    ]
                );



                $apikey = '8c073ed79bf7f3796cf8e98a288ba69f6db5847d6ff364a2';
                $phone = '+62' . $nohp;
                $message =
                    'Halo ' . $nama . ' 😊🙏

                Pesan ini Anda terima karena beberapa waktu lalu telah mengisi form pendaftaran *Kelas 100 Orderan*.

                Setelah menjadi peserta dikelas tersebut, maka Anda berhak masuk grup belajarnya, punya kesempatan konsultasi dan bisa mengikuti seluruh materi bimbingannya.

                Agar Anda segera terdaftar sebagai Peserta resmi, maka silahkan transfer biaya pendaftaran *sebesar Rp. 55.000* ke salah satu rekening berikut :

                Mandiri = 1360007600528
                BCA = 1911488766
                BRI = 012101009502532
                BNI = 1238568920

                Seluruh rekening barusan, atas nama *Wulandari Tri*

                Setelah Anda menyelesaikan pendaftaran, langsung konfirmasi bukti transfernya ke link ini https://konfirmasi.kelas100orderan.com/pembayaran

                Jika Anda punya pertanyaan, silahkan balas pesan ini. Dengan senang hati kami akan membantu. 🙂

                Salam,

                *Tim entrepreneurID*';

                $url='http://116.203.191.58/api/send_message';
                $data = array(
                    "phone_no"  => $phone,
                    "key"		=> $apikey,
                    "message"	=> $message,
                    "skip_link"	=> True // This optional for skip snapshot of link in message
                );
                $data_string = json_encode($data);

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_VERBOSE, 0);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                curl_setopt($ch, CURLOPT_TIMEOUT, 360);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
                );
                echo $res=curl_exec($ch);
                curl_close($ch);

                $data = Biodatum::findOrFail($iddata);

                try {
                    Mail::to($data->email)->send(new KSOPendaftaran($data));

                    return redirect()->to('/selesai')->withFlashSuccess($data->nama . ' - ' . $data->email . ', Pendaftaran Berhasil !<br/> Periksa E-mail Anda. <br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                } catch (Throwable $th) {
                    return redirect()->to('/selesai')->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                }
            }

            return redirect()->back()->withFlashDanger('Pendaftaran Gagal ! E-mail / No. Telepon sudah terdaftar. <br>Terima Kasih')->withInput();
        }
        elseif ($_SERVER['HTTP_HOST'] == 'upload.mentoringorganicmarketing.com') {

            $cekemail = DB::table('biodata')
                ->where([
                    ['idprodukreg', '26'],
                    ['email', $email],
                ])
                ->orWhere([
                    ['idprodukreg', '26'],
                    ['no_telp', $nohp],
                ])
                ->first();

            if (is_null($cekemail)) {

                $iddata = DB::table('biodata')->insertGetId(
                    [
                        'nama'        => $nama,
                        'email'       => $email,
                        'no_telp'     => $nohp,
                        'panggilan'   => $panggilan,
                        'kota'        => $kota,
                        'tgllahir'    => $tgllahir,
                        'gender'      => $jeniskelamin,
                        'ref'         => $ref,
                        'status_user' => '0',
                        'aktif'       => '0',
                        'tgl_daftar'  => $tgldaftar,
                        'idprodukreg' => '26', // Produk Agen Baru 2020
                        'kodeunik'    => $kodeunik,
                    ]
                );



                $apikey = 'xEhX7sF1wh7KFdVXZx5AfJj7wn6krW8m';
                $phone = '+62' . $nohp;
                $message =
                    'Halo ' . $nama . ' 😊🙏

                Pesan ini Anda terima karena beberapa waktu lalu telah mengisi form pendaftaran *Mentoring Organic Marketing*.

                Setelah menjadi peserta dikelas tersebut, maka Anda berhak masuk grup belajarnya, punya kesempatan konsultasi dan bisa mengikuti seluruh materi bimbingannya.

                Agar Anda segera terdaftar sebagai Peserta resmi, maka silahkan transfer biaya pendaftaran *sebesar Rp. ' . 99000 + $kodeunik . '* ke salah satu rekening berikut :

                Mandiri = 1360007600528
                BCA = 1911488766
                BRI = 012101009502532
                BNI = 1238568920

                Seluruh rekening barusan, atas nama *Wulandari Tri*

                Setelah Anda menyelesaikan pendaftaran, langsung konfirmasi bukti transfernya ke link ini https://upload.mentoringorganicmarketing.com/pembayaran

                Jika Anda punya pertanyaan, silahkan balas pesan ini. Dengan senang hati kami akan membantu. 🙂

                Salam,

                *Tim entrepreneurID*';

                $url = 'https://api.wanotif.id/v1/send';

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_HEADER, 0);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, array(
                    'Apikey'    => $apikey,
                    'Phone'     => $phone,
                    'Message'   => $message,
                ));
                $response = curl_exec($curl);
                curl_close($curl);

                $data = Biodatum::findOrFail($iddata);

                try {
                    Mail::to($data->email)->send(new MOMPendaftaran($data));

                    return redirect()->to('/selesai')->withFlashSuccess($data->nama . ' - ' . $data->email . ', Pendaftaran Berhasil !<br/> Periksa E-mail Anda. <br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                } catch (Throwable $th) {
                    return redirect()->to('/selesai')->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                }
            }

            return redirect()->back()->withFlashDanger('Pendaftaran Gagal ! E-mail / No. Telepon sudah terdaftar. <br>Terima Kasih')->withInput();
        }
        elseif ($_SERVER['HTTP_HOST'] == 'member.kelasonlinecopywriting.com') {

            $cekemail = DB::table('biodata')
                ->where([
                    ['idprodukreg', '27'],
                    ['email', $email],
                ])
                ->orWhere([
                    ['idprodukreg', '27'],
                    ['no_telp', $nohp],
                ])
                ->first();

            if (is_null($cekemail)) {

                $iddata = DB::table('biodata')->insertGetId(
                    [
                        'nama'        => $nama,
                        'email'       => $email,
                        'no_telp'     => $nohp,
                        'panggilan'   => $panggilan,
                        'kota'        => $kota,
                        'tgllahir'    => $tgllahir,
                        'gender'      => $jeniskelamin,
                        'ref'         => $ref,
                        'status_user' => '0',
                        'aktif'       => '0',
                        'tgl_daftar'  => $tgldaftar,
                        'idprodukreg' => '27', // KOC Late 2020
                        'kodeunik'    => $kodeunik,
                    ]
                );



                $apikey = 'xEhX7sF1wh7KFdVXZx5AfJj7wn6krW8m';
                $phone = '+62' . $nohp;
                $message =
                    'Halo ' . $nama . ' 😊🙏

                Pesan ini Anda terima karena beberapa waktu lalu telah mengisi form pendaftaran *Kelas Online Copywriting*.

                Setelah menjadi peserta dikelas tersebut, maka Anda berhak masuk grup belajarnya, punya kesempatan konsultasi dan bisa mengikuti seluruh materi bimbingannya.

                Agar Anda segera terdaftar sebagai Peserta resmi, maka silahkan transfer biaya pendaftaran *sebesar Rp. ' . 55000 + $kodeunik . '* ke salah satu rekening berikut :

                Mandiri = 1360007600528
                BCA = 1911488766
                BRI = 012101009502532
                BNI = 1238568920

                Seluruh rekening barusan, atas nama *Wulandari Tri*

                Setelah Anda menyelesaikan pendaftaran, langsung konfirmasi bukti transfernya ke link ini https://member.kelasonlinecopywriting.com/pembayaran

                Jika Anda punya pertanyaan, silahkan balas pesan ini. Dengan senang hati kami akan membantu. 🙂

                Salam,

                *Tim entrepreneurID*';

                $url = 'https://api.wanotif.id/v1/send';

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_HEADER, 0);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, array(
                    'Apikey'    => $apikey,
                    'Phone'     => $phone,
                    'Message'   => $message,
                ));
                $response = curl_exec($curl);
                curl_close($curl);

                $data = Biodatum::findOrFail($iddata);

                try {
                    Mail::to($data->email)->send(new KOCPendaftaran($data));

                    return redirect()->to('/selesai')->withFlashSuccess($data->nama . ' - ' . $data->email . ', Pendaftaran Berhasil !<br/> Periksa E-mail Anda. <br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                } catch (Throwable $th) {
                    return redirect()->to('/selesai')->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                }
            }

            return redirect()->back()->withFlashDanger('Pendaftaran Gagal ! E-mail / No. Telepon sudah terdaftar. <br>Terima Kasih')->withInput();
        }
        elseif ($_SERVER['HTTP_HOST'] == 'upload.kelasdatabasewa.com') {

            $cekemail = DB::table('biodata')
                ->where([
                    ['idprodukreg', '28'],
                    ['email', $email],
                ])
                ->orWhere([
                    ['idprodukreg', '28'],
                    ['no_telp', $nohp],
                ])
                ->first();

            if (is_null($cekemail)) {

                $iddata = DB::table('biodata')->insertGetId(
                    [
                        'nama'        => $nama,
                        'email'       => $email,
                        'no_telp'     => $nohp,
                        'panggilan'   => $panggilan,
                        'kota'        => $kota,
                        'tgllahir'    => $tgllahir,
                        'gender'      => $jeniskelamin,
                        'ref'         => $ref,
                        'status_user' => '0',
                        'aktif'       => '0',
                        'tgl_daftar'  => $tgldaftar,
                        'idprodukreg' => '28', // KOC Late 2020
                        'kodeunik'    => $kodeunik,
                    ]
                );



                $apikey = 'xEhX7sF1wh7KFdVXZx5AfJj7wn6krW8m';
                $phone = '+62' . $nohp;
                $message =
                    'Halo ' . $nama . ' 😊🙏

                Pesan ini Anda terima karena beberapa waktu lalu telah mengisi form pendaftaran *Kelas Database WA*.

                Setelah menjadi peserta dikelas tersebut, maka Anda berhak masuk grup belajarnya, punya kesempatan konsultasi dan bisa mengikuti seluruh materi bimbingannya.

                Agar Anda segera terdaftar sebagai Peserta resmi, maka silahkan transfer biaya pendaftaran *sebesar Rp. ' . 55000 + $kodeunik . '* ke salah satu rekening berikut :

                Mandiri = 1360007600528
                BCA = 1911488766
                BRI = 012101009502532
                BNI = 1238568920

                Seluruh rekening barusan, atas nama *Wulandari Tri*

                Setelah Anda menyelesaikan pendaftaran, langsung konfirmasi bukti transfernya ke link ini https://upload.kelasdatabasewa.com/pembayaran

                Jika Anda punya pertanyaan, silahkan balas pesan ini. Dengan senang hati kami akan membantu. 🙂

                Salam,

                *Tim entrepreneurID*';

                $url = 'https://api.wanotif.id/v1/send';

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_HEADER, 0);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, array(
                    'Apikey'    => $apikey,
                    'Phone'     => $phone,
                    'Message'   => $message,
                ));
                $response = curl_exec($curl);
                curl_close($curl);

                $data = Biodatum::findOrFail($iddata);

                try {
                    Mail::to($data->email)->send(new KOCPendaftaran($data));

                    return redirect()->to('/selesai')->withFlashSuccess($data->nama . ' - ' . $data->email . ', Pendaftaran Berhasil !<br/> Periksa E-mail Anda. <br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                } catch (Throwable $th) {
                    return redirect()->to('/selesai')->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                }
            }

            return redirect()->back()->withFlashDanger('Pendaftaran Gagal ! E-mail / No. Telepon sudah terdaftar. <br>Terima Kasih')->withInput();
        }
        elseif ($_SERVER['HTTP_HOST'] == 'app.copywritingnextlevel.com') {


            $ongkir_ = explode(',',$request->input('ongkir'));
            $idp = request()->input('provinsi');
            $provinsi = curl_init();
            curl_setopt_array($provinsi, array(
                CURLOPT_URL => "https://pro.rajaongkir.com/api/province?id=$idp",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key:7d23bffcefc9680802db1d10ba43eb67"
                ),
            ));
            $provinsi_ = json_decode(curl_exec($provinsi), true);
            curl_close($provinsi);

            $idc = request()->input('kota');
            $kota = curl_init();
            curl_setopt_array($kota, array(
                CURLOPT_URL => "https://pro.rajaongkir.com/api/city?id=$idc",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key:7d23bffcefc9680802db1d10ba43eb67"
                ),
            ));
            $kota_ = json_decode(curl_exec($kota), true);
            curl_close($kota);

            $idk = request()->input('kecamatan');
            $kecamatan = curl_init();
            curl_setopt_array($kecamatan, array(
                CURLOPT_URL => "https://pro.rajaongkir.com/api/subdistrict?id=$idk",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key:7d23bffcefc9680802db1d10ba43eb67"
                ),
            ));
            $kecamatan_ = json_decode(curl_exec($kecamatan), true);
            curl_close($kecamatan);



            $kota         = $kota_['rajaongkir']['results']['city_name'];
            $provinsi     = $provinsi_['rajaongkir']['results']['province'];
            $kecamatan    = $kecamatan_['rajaongkir']['results']['subdistrict_name'];
            $kodepos      = $request->input('kodepos');
            $alamat       = $request->input('alamat');
            $jumlah       = $request->input('jumlah');
            $harga        = $request->input('harga');
            $ongkir       = $request->input('hargaongkir');
            $total        = $request->input('total');
            $kurir        = $ongkir_[0].' '.$ongkir_[1];
            $kodeunik     = $request->input('kodeunik');


            $cekemail = DB::table('biodata')
                ->where([
                    ['idprodukreg', '29'],
                    ['email', $email],
                ])
                ->orWhere([
                    ['idprodukreg', '29'],
                    ['no_telp', $nohp],
                ])
                ->first();

            if (is_null($cekemail)) {

                $iddata = DB::table('biodata')->insertGetId(
                    [
                        'nama'        => $nama,
                        'email'       => $email,
                        'no_telp'     => $nohp,
                        'panggilan'   => $panggilan,
                        'kota'        => $kota,
                        'tgllahir'    => $tgllahir,
                        'gender'      => $jeniskelamin,
                        'ref'         => $ref,
                        'status_user' => request()->get('order') == 'manual' ? '2' : '0',
                        'aktif'       => '0',
                        'tgl_daftar'  => $tgldaftar,
                        'idprodukreg' => '29', // CNL Late 2020
                        'kodeunik'    => $kodeunik,

                        'alamat'      => $alamat,
                        'provinsi'    => $provinsi,
                        'kecamatan'   => $kecamatan,
                        'kodepos'     => $kodepos,
                        'jumlah'      => $jumlah,
                        'kurir'       => $kurir,
                        'harga'       => $harga,
                        'ongkir'      => $ongkir,
                        'total'       => $total,
                    ]
                );

                $data = Biodatum::findOrFail($iddata);

                if (request()->get('order') == 'manual') {
                    try {
                        return redirect()->to('/selesai')->withFlashSuccess($data->nama . ' - ' . $data->email . ', Pemesanan Berhasil !<br/> Periksa Dashboard Agen Anda. <br/>Terima kasih.<br><a href="/pesan?order=manual&ref=' . $data->ref . '">Kembali Ke form Pemesanan</a>');
                    } catch (Throwable $th) {
                        return redirect()->to('/selesai')->withFlashDanger($data->nama . ' - ' . $data->email . ', Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.<br><a href="/pesan?order=manual&ref=' . $data->ref . '">Kembali Ke form Pemesanan</a>');
                    }
                }


                $apikey = '8c073ed79bf7f3796cf8e98a288ba69f6db5847d6ff364a2';
                $phone = '+62' . $nohp;
                $message =
                    'Halo ' . $nama . ' 😊🙏

Pesan ini Anda terima karena beberapa waktu lalu telah mengisi form pemesanan buku *Copywriting Next Level*.

Agar pemesanan Anda bisa segera kami proses, maka silahkan transfer total pemesanan *sebesar Rp. ' . $total . '* ke salah satu rekening berikut :

Mandiri = 1360007600528
BCA = 1911488766
BRI = 012101009502532
BNI = 1238568920

Seluruh rekening barusan, atas nama *Wulandari Tri*

Setelah Anda menyelesaikan pendaftaran, langsung konfirmasi bukti transfernya ke link ini https://app.copywritingnextlevel.com/pembayaran

Jika Anda punya pertanyaan, silahkan balas pesan ini. Dengan senang hati kami akan membantu. 🙂

Salam,

*Tim entrepreneurID*';

                // $url = 'https://api.wanotif.id/v1/send';

                // $curl = curl_init();
                // curl_setopt($curl, CURLOPT_URL, $url);
                // curl_setopt($curl, CURLOPT_HEADER, 0);
                // curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
                // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                // curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                // curl_setopt($curl, CURLOPT_POST, 1);
                // curl_setopt($curl, CURLOPT_POSTFIELDS, array(
                //     'Apikey'    => $apikey,
                //     'Phone'     => $phone,
                //     'Message'   => $message,
                // ));
                // $response = curl_exec($curl);
                // curl_close($curl);

                $url='http://116.203.191.58/api/send_message';
                $data = array(
                    "phone_no"  => $phone,
                    "key"		=> $apikey,
                    "message"	=> $message,
                    "skip_link"	=> True // This optional for skip snapshot of link in message
                );
                $data_string = json_encode($data);

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_VERBOSE, 0);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                curl_setopt($ch, CURLOPT_TIMEOUT, 360);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
                );
                echo $res=curl_exec($ch);
                curl_close($ch);

                $data = Biodatum::findOrFail($iddata);

                try {
                    Mail::to($data->email)->send(new CNLPendaftaran($data));

                    return redirect()->to('/selesai')->withFlashSuccess($data->nama . ' - ' . $data->email . ', Pemesanan Berhasil !<br/> Periksa E-mail Anda. <br/>Terima kasih.<br><a href="/pesan?ref=' . $data->ref . '">Kembali Ke form Pemesanan</a>');
                } catch (Throwable $th) {
                    return redirect()->to('/selesai')->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.<br><a href="/pesan?ref=' . $data->ref . '">Kembali Ke form Pemesanan</a>');
                }
            }

            return redirect()->back()->withFlashDanger('Pendaftaran Gagal ! E-mail / No. Telepon sudah terdaftar. <br>Terima Kasih')->withInput();
        }
        elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringinstantcopywriting.com') {

            $cekemail = DB::table('biodata')
                ->where([
                    ['idprodukreg', '33'], //25
                    ['email', $email],
                ])
                ->orWhere([
                    ['idprodukreg', '33'],
                    ['no_telp', $nohp],
                ])
                ->first();

            if (is_null($cekemail)) {

                $iddata = DB::table('biodata')->insertGetId(
                    [
                        'nama'        => $nama,
                        'email'       => $email,
                        'no_telp'     => $nohp,
                        'panggilan'   => $panggilan,
                        'kota'        => $kota,
                        'tgllahir'    => $tgllahir,
                        'gender'      => $jeniskelamin,
                        'ref'         => $ref,
                        'status_user' => '0',
                        'aktif'       => '0',
                        'tgl_daftar'  => $tgldaftar,
                        'idprodukreg' => '33', // Produk Agen Baru 2020
                        'kodeunik'    => null,
                    ]
                );



                $apikey = '8c073ed79bf7f3796cf8e98a288ba69f6db5847d6ff364a2';
                $phone = '+62' . $nohp;
                $message =
                    'Halo ' . $nama . ' 😊🙏

Pesan ini Anda terima karena beberapa waktu lalu telah mengisi form pendaftaran *Mentoring Instant Copywriting*.

Setelah menjadi peserta dikelas tersebut, maka Anda berhak masuk grup belajarnya, punya kesempatan konsultasi dan bisa mengikuti seluruh materi bimbingannya.

Agar Anda segera terdaftar sebagai Peserta resmi, maka silahkan transfer biaya pendaftaran *sebesar Rp. 149.000* ke salah satu rekening berikut :

Mandiri = 1360007600528
BCA = 1911488766
BRI = 012101009502532
BNI = 1238568920

Seluruh rekening barusan, atas nama *Wulandari Tri*

Setelah Anda menyelesaikan pendaftaran, langsung konfirmasi bukti transfernya ke link ini https://konfirmasi.mentoringinstantcopywriting.com/pembayaran

Jika Anda punya pertanyaan, silahkan balas pesan ini. Dengan senang hati kami akan membantu. 🙂

Salam,

*Tim entrepreneurID*';

                $url='http://116.203.191.58/api/send_message';
                $data = array(
                    "phone_no"  => $phone,
                    "key"		=> $apikey,
                    "message"	=> $message,
                    "skip_link"	=> True // This optional for skip snapshot of link in message
                );
                $data_string = json_encode($data);

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_VERBOSE, 0);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                curl_setopt($ch, CURLOPT_TIMEOUT, 360);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
                );
                echo $res=curl_exec($ch);
                curl_close($ch);

                $data = Biodatum::findOrFail($iddata);

                try {
                    Mail::to($data->email)->send(new MICPendaftaran($data));

                    return redirect()->to('/selesai')->withFlashSuccess($data->nama . ' - ' . $data->email . ', Pendaftaran Berhasil !<br/> Periksa E-mail Anda. <br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                } catch (Throwable $th) {
                    return redirect()->to('/selesai')->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                }
            }

            return redirect()->back()->withFlashDanger('Pendaftaran Gagal ! E-mail / No. Telepon sudah terdaftar. <br>Terima Kasih')->withInput();
        }
        elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringsuperreseller.com') {

            $cekemail = DB::table('biodata')
                ->where([
                    ['idprodukreg', '35'], //25
                    ['email', $email],
                ])
                ->orWhere([
                    ['idprodukreg', '35'],
                    ['no_telp', $nohp],
                ])
                ->first();

            if (is_null($cekemail)) {

                $iddata = DB::table('biodata')->insertGetId(
                    [
                        'nama'        => $nama,
                        'email'       => $email,
                        'no_telp'     => $nohp,
                        'panggilan'   => $panggilan,
                        'kota'        => $kota,
                        'tgllahir'    => $tgllahir,
                        'gender'      => $jeniskelamin,
                        'ref'         => $ref,
                        'status_user' => '0',
                        'aktif'       => '0',
                        'tgl_daftar'  => $tgldaftar,
                        'idprodukreg' => '35',
                        'kodeunik'    => null,
                    ]
                );



                $apikey = '8c073ed79bf7f3796cf8e98a288ba69f6db5847d6ff364a2';
                $phone = '+62' . $nohp;
                $message =
                    'Halo ' . $nama . ' 😊🙏

Pesan ini Anda terima karena beberapa waktu lalu telah mengisi form pendaftaran *Mentoring Super Reseller*.

Setelah menjadi peserta dikelas tersebut, maka Anda berhak masuk grup belajarnya, punya kesempatan konsultasi dan bisa mengikuti seluruh materi bimbingannya.

Agar Anda segera terdaftar sebagai Peserta resmi, maka silahkan transfer biaya pendaftaran *sebesar Rp. 93.000* ke salah satu rekening berikut :

Mandiri = 1360007600528
BCA = 1911488766
BRI = 012101009502532
BNI = 1238568920

Seluruh rekening barusan, atas nama *Wulandari Tri*

Setelah Anda menyelesaikan pendaftaran, langsung konfirmasi bukti transfernya ke link ini https://konfirmasi.mentoringsuperreseller.com/pembayaran

Jika Anda punya pertanyaan, silahkan balas pesan ini. Dengan senang hati kami akan membantu. 🙂

Salam,

*Tim entrepreneurID*';

                $url='http://116.203.191.58/api/send_message';
                $data = array(
                    "phone_no"  => $phone,
                    "key"		=> $apikey,
                    "message"	=> $message,
                    "skip_link"	=> True // This optional for skip snapshot of link in message
                );
                $data_string = json_encode($data);

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_VERBOSE, 0);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
                curl_setopt($ch, CURLOPT_TIMEOUT, 360);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
                );
                echo $res=curl_exec($ch);
                curl_close($ch);

                $data = Biodatum::findOrFail($iddata);

                try {
                    Mail::to($data->email)->send(new MSRPendaftaran($data));

                    return redirect()->to('/selesai')->withFlashSuccess($data->nama . ' - ' . $data->email . ', Pendaftaran Berhasil !<br/> Periksa E-mail Anda. <br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                } catch (Throwable $th) {
                    return redirect()->to('/selesai')->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.<br><a href="/daftar?ref=' . $data->ref . '">Kembali Ke form Registrasi</a>');
                }
            }

            return redirect()->back()->withFlashDanger('Pendaftaran Gagal ! E-mail / No. Telepon sudah terdaftar. <br>Terima Kasih')->withInput();
        }

        return redirect()->route('frontend.index');
    }

    public function pembayaran(Request $request)
    {
        $email      = $request->input('email') ?? NULL;
        $sesidaftar = $email . '-' . Carbon::now() . '-' . Str::random(5);
        Session::put('sesidaftar', $sesidaftar);

        if ($_SERVER['HTTP_HOST'] == 'konfirmasi.agen-entrepreneurid.com') {
            $data    = '';
            $status  = '';
            $wstatus = '';

            if (isset($email)) {
                $data = DB::table('biodata')->where('email', $email)->where('idprodukreg', '39')->first();

                if (isset($data)) {
                    if ($data->aktif == '0') {
                        $status  = 'BELUM UPLOAD';
                        $wstatus = 'danger';
                    } elseif ($data->aktif == '1') {
                        $status  = 'MENUNGGU KONFIRMASI';
                        $wstatus = 'warning';
                    } elseif ($data->aktif == '2') {
                        $status  = 'AKTIF';
                        $wstatus = 'info';
                    }
                } else {
                    $data = '0';
                }
            }

            return view('frontend.user.modul.pembayaran', compact('email', 'data', 'status', 'wstatus'));
        } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.kelas100orderan.com' ) {
            $data    = '';
            $status  = '';
            $wstatus = '';

            if (isset($email)) {
                $data = DB::table('biodata')->where('email', $email)->where('idprodukreg', '31')->first();

                if (isset($data)) {
                    if ($data->aktif == '0') {
                        $status  = 'BELUM UPLOAD';
                        $wstatus = 'danger';
                    } elseif ($data->aktif == '1') {
                        $status  = 'MENUNGGU KONFIRMASI';
                        $wstatus = 'warning';
                    } elseif ($data->aktif == '2') {
                        $status  = 'AKTIF';
                        $wstatus = 'info';
                    }
                } else {
                    $data = '0';
                }
            }

            return view('frontend.user.modul.pembayaran', compact('email', 'data', 'status', 'wstatus'));
        } elseif ($_SERVER['HTTP_HOST'] == 'upload.mentoringorganicmarketing.com') {
            $data    = '';
            $status  = '';
            $wstatus = '';

            if (isset($email)) {
                $data = DB::table('biodata')->where('email', $email)->where('idprodukreg', '26')->first();

                if (isset($data)) {
                    if ($data->aktif == '0') {
                        $status  = 'BELUM UPLOAD';
                        $wstatus = 'danger';
                    } elseif ($data->aktif == '1') {
                        $status  = 'MENUNGGU KONFIRMASI';
                        $wstatus = 'warning';
                    } elseif ($data->aktif == '2') {
                        $status  = 'AKTIF';
                        $wstatus = 'info';
                    }
                } else {
                    $data = '0';
                }
            }

            return view('frontend.user.modul.pembayaran', compact('email', 'data', 'status', 'wstatus'));
        } elseif ($_SERVER['HTTP_HOST'] == 'member.kelasonlinecopywriting.com') {
            $data    = '';
            $status  = '';
            $wstatus = '';

            if (isset($email)) {
                $data = DB::table('biodata')->where('email', $email)->where('idprodukreg', '27')->first();

                if (isset($data)) {
                    if ($data->aktif == '0') {
                        $status  = 'BELUM UPLOAD';
                        $wstatus = 'danger';
                    } elseif ($data->aktif == '1') {
                        $status  = 'MENUNGGU KONFIRMASI';
                        $wstatus = 'warning';
                    } elseif ($data->aktif == '2') {
                        $status  = 'AKTIF';
                        $wstatus = 'info';
                    }
                } else {
                    $data = '0';
                }
            }

            return view('frontend.user.modul.pembayaran', compact('email', 'data', 'status', 'wstatus'));
        } elseif ($_SERVER['HTTP_HOST'] == 'upload.kelasdatabasewa.com') {
            $data    = '';
            $status  = '';
            $wstatus = '';

            if (isset($email)) {
                $data = DB::table('biodata')->where('email', $email)->where('idprodukreg', '28')->first();

                if (isset($data)) {
                    if ($data->aktif == '0') {
                        $status  = 'BELUM UPLOAD';
                        $wstatus = 'danger';
                    } elseif ($data->aktif == '1') {
                        $status  = 'MENUNGGU KONFIRMASI';
                        $wstatus = 'warning';
                    } elseif ($data->aktif == '2') {
                        $status  = 'AKTIF';
                        $wstatus = 'info';
                    }
                } else {
                    $data = '0';
                }
            }

            return view('frontend.user.modul.pembayaran', compact('email', 'data', 'status', 'wstatus'));
        } elseif ($_SERVER['HTTP_HOST'] == 'app.copywritingnextlevel.com' || $_SERVER['HTTP_HOST'] == 'eid.test') {
            $data    = '';
            $status  = '';
            $wstatus = '';

            if (isset($email)) {
                $data = DB::table('biodata')->where('email', $email)->where('idprodukreg', '29')->first();

                if (isset($data)) {
                    if ($data->aktif == '0') {
                        $status  = 'BELUM UPLOAD';
                        $wstatus = 'danger';
                    } elseif ($data->aktif == '1') {
                        $status  = 'MENUNGGU KONFIRMASI';
                        $wstatus = 'warning';
                    } elseif ($data->aktif == '2') {
                        $status  = 'AKTIF';
                        $wstatus = 'info';
                    }
                } else {
                    $data = '0';
                }
            }

            return view('frontend.user.modul.pembayaran', compact('email', 'data', 'status', 'wstatus'));
        } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringinstantcopywriting.com' ) {
            $data    = '';
            $status  = '';
            $wstatus = '';

            if (isset($email)) {
                $data = DB::table('biodata')->where('email', $email)->where('idprodukreg', '33')->first();

                if (isset($data)) {
                    if ($data->aktif == '0') {
                        $status  = 'BELUM UPLOAD';
                        $wstatus = 'danger';
                    } elseif ($data->aktif == '1') {
                        $status  = 'MENUNGGU KONFIRMASI';
                        $wstatus = 'warning';
                    } elseif ($data->aktif == '2') {
                        $status  = 'AKTIF';
                        $wstatus = 'info';
                    }
                } else {
                    $data = '0';
                }
            }

            return view('frontend.user.modul.pembayaran', compact('email', 'data', 'status', 'wstatus'));
        } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.kelasfbhacking.com' ) {
            $data    = '';
            $status  = '';
            $wstatus = '';

            if (isset($email)) {
                $data = DB::table('biodata')->where('email', $email)->where('idprodukreg', '34')->first();

                if (isset($data)) {
                    if ($data->aktif == '0') {
                        $status  = 'BELUM UPLOAD';
                        $wstatus = 'danger';
                    } elseif ($data->aktif == '1') {
                        $status  = 'MENUNGGU KONFIRMASI';
                        $wstatus = 'warning';
                    } elseif ($data->aktif == '2') {
                        $status  = 'AKTIF';
                        $wstatus = 'info';
                    }
                } else {
                    $data = '0';
                }
            }

            return view('frontend.user.modul.pembayaran', compact('email', 'data', 'status', 'wstatus'));
        } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringsuperreseller.com' ) {
            $data    = '';
            $status  = '';
            $wstatus = '';

            if (isset($email)) {
                $data = DB::table('biodata')->where('email', $email)->where('idprodukreg', '35')->first();

                if (isset($data)) {
                    if ($data->aktif == '0') {
                        $status  = 'BELUM UPLOAD';
                        $wstatus = 'danger';
                    } elseif ($data->aktif == '1') {
                        $status  = 'MENUNGGU KONFIRMASI';
                        $wstatus = 'warning';
                    } elseif ($data->aktif == '2') {
                        $status  = 'AKTIF';
                        $wstatus = 'info';
                    }
                } else {
                    $data = '0';
                }
            }

            return view('frontend.user.modul.pembayaran', compact('email', 'data', 'status', 'wstatus'));
        }

        return redirect()->route('frontend.index');
    }

    public function uploadbuktitransfer(Request $request)
    {
        $request->validate([
            'filepond' => 'required|image|max:10240',
        ]);

        $file_bukti_transfer      = $request->file('filepond');

        if ($_SERVER['HTTP_HOST'] == 'konfirmasi.agen-entrepreneurid.com') {

            Session::put('filebuktitransfer', Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension()); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
            $buktitf         = Image::make($file_bukti_transfer);
            $lokasibuktitf   = public_path('../../../public_html/agen-entrepreneurid.com/konfirmasi/public/bukti-transfer-39-AGEN-BARU/');
            $buktitf->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $buktitf->save($lokasibuktitf.Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension());

        } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.kelas100orderan.com') {
            // $nama_file_bukti_transfer = '25-' . Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension();
            // Session::put('filebuktitransfer', $nama_file_bukti_transfer); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
            // Storage::disk('bukti-transfer-25-KSO')->put($nama_file_bukti_transfer, File::get($file_bukti_transfer));

            Session::put('filebuktitransfer', Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension()); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
            $buktitf         = Image::make($file_bukti_transfer);
            $lokasibuktitf   = public_path('../../../public_html/kelas100orderan.com/konfirmasi/public/bukti-transfer-31-KSO/');
            $buktitf->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $buktitf->save($lokasibuktitf.Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension());
        } elseif ($_SERVER['HTTP_HOST'] == 'upload.mentoringorganicmarketing.com') {
            $nama_file_bukti_transfer = '26-' . Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension();
            Session::put('filebuktitransfer', $nama_file_bukti_transfer); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
            Storage::disk('bukti-transfer-26-MOM')->put($nama_file_bukti_transfer, File::get($file_bukti_transfer));
        } elseif ($_SERVER['HTTP_HOST'] == 'member.kelasonlinecopywriting.com') {
            $nama_file_bukti_transfer = '27-' . Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension();
            Session::put('filebuktitransfer', $nama_file_bukti_transfer); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
            Storage::disk('bukti-transfer-27-KOC')->put($nama_file_bukti_transfer, File::get($file_bukti_transfer));
        } elseif ($_SERVER['HTTP_HOST'] == 'upload.kelasdatabasewa.com') {
            $nama_file_bukti_transfer = '28-' . Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension();
            Session::put('filebuktitransfer', $nama_file_bukti_transfer); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
            Storage::disk('bukti-transfer-28-KDW')->put($nama_file_bukti_transfer, File::get($file_bukti_transfer));
        } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringinstantcopywriting.com') {
            // $nama_file_bukti_transfer = '29-' . Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension();
            Session::put('filebuktitransfer', Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension()); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
            // Storage::disk('bukti-transfer-29-CNL')->put($nama_file_bukti_transfer, File::get($file_bukti_transfer));
            $buktitf         = Image::make($file_bukti_transfer);
            $lokasibuktitf   = public_path('../../../public_html/mentoringinstantcopywriting.com/konfirmasi/public/bukti-transfer-33-MIC/');
            $buktitf->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $buktitf->save($lokasibuktitf.Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension());

        } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.kelas100orderan.com') {
            // $nama_file_bukti_transfer = '25-' . Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension();
            // Session::put('filebuktitransfer', $nama_file_bukti_transfer); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
            // Storage::disk('bukti-transfer-25-KSO')->put($nama_file_bukti_transfer, File::get($file_bukti_transfer));

            Session::put('filebuktitransfer', Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension()); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
            $buktitf         = Image::make($file_bukti_transfer);
            $lokasibuktitf   = public_path('../../../public_html/kelas100orderan.com/konfirmasi/public/bukti-transfer-31-KSO/');
            $buktitf->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $buktitf->save($lokasibuktitf.Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension());
        } elseif ($_SERVER['HTTP_HOST'] == 'app.copywritingnextlevel.com') {

            Session::put('filebuktitransfer', Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension()); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
            $buktitf         = Image::make($file_bukti_transfer);
            $lokasibuktitf   = public_path('../../../public_html/copywritingnextlevel.com/app/public/bukti-transfer-29-CNL/');
            $buktitf->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $buktitf->save($lokasibuktitf.Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension());
        } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.kelasfbhacking.com') {
            Session::put('filebuktitransfer', Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension()); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
            $buktitf         = Image::make($file_bukti_transfer);
            $lokasibuktitf   = public_path('../../../public_html/kelasfbhacking.com/konfirmasi/public/bukti-transfer-34-KFH/');
            $lokasibuktitf_  = public_path('../../../public_html/mentoringinstantcopywriting.com/konfirmasi/public/bukti-transfer-34-KFH/');
            $buktitf->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $buktitf->save($lokasibuktitf.Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension());
            //disimpan di mic, krna route web kebaca di domain mic
            $buktitf->save($lokasibuktitf_.Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension());
        } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringsuperreseller.com') {
            Session::put('filebuktitransfer', Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension()); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
            $buktitf         = Image::make($file_bukti_transfer);
            $lokasibuktitf   = public_path('../../../public_html/mentoringsuperreseller.com/konfirmasi/public/bukti-transfer-35-MSR/');
            $buktitf->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $buktitf->save($lokasibuktitf.Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension());
        }else {
            // $nama_file_bukti_transfer = 'TES-' . Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension();
            Session::put('filebuktitransfer', Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension());
            // Storage::disk('bukti-transfer-tes')->put($nama_file_bukti_transfer, File::get($file_bukti_transfer));

            $buktitf         = Image::make($file_bukti_transfer);
            $lokasibuktitf   = public_path('app/public/');
            $buktitf->resize(400, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $buktitf->save($lokasibuktitf.Session::get('sesidaftar') . '.' . $file_bukti_transfer->getClientOriginalExtension());

            // thumbnail
            // $thumbnailPath  = public_path('app/public/bukti-transfer-26-MOM/thumbnail/');
            // $buktitf->save($thumbnailPath.time().$originalImage->getClientOriginalName());
        }
    }

    public function simpanpembayaran(Request $request)
    {
        // try {

            $id          = $request->input('id');
            $data        = Biodatum::findOrFail($id);
            $filebuktitf = Session::get('filebuktitransfer');

            $data->aktif = '1';
            $data->save();
            if ($_SERVER['HTTP_HOST'] == 'konfirmasi.agen-entrepreneurid.com') {
                DB::table('pemesanan')->insert([
                    'idcustomer' => $data->id,
                    'idagen'     => $data->ref,
                    'valid'      => 'WAITING CONFIRMATION',
                    'buktitf'    => Session::get('filebuktitransfer'),
                    'idproduk'   => '39',
                ]);

                try {
                    Mail::to($data->email)->send(new AgenBaruPembayaran($data));

                    return redirect()->back()->withFlashSuccess($data->nama . ' - ' . $data->email . ', Terima Kasih. <br>Bukti Transfer Anda Berhasil Diupload !<br/> Kurang dari 1 x 24 Jam Anda akan menerima konfirmasi e-mail dari kami.');
                } catch (Throwable $th) {
                    return redirect()->back()->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.');
                }
            } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.kelas100orderan.com') {
                DB::table('pemesanan')->insert([
                    'idcustomer' => $data->id,
                    'idagen'     => $data->ref,
                    'valid'      => 'WAITING CONFIRMATION',
                    'buktitf'    => Session::get('filebuktitransfer'),
                    'idproduk'   => 31,
                ]);

                try {
                    Mail::to($data->email)->send(new KSOPembayaran($data));

                    return redirect()->back()->withFlashSuccess($data->nama . ' - ' . $data->email . ', Terima Kasih. <br>Bukti Transfer Anda Berhasil Diupload !<br/> Kurang dari 1 x 24 Jam Anda akan menerima konfirmasi e-mail dari kami.');
                } catch (Throwable $th) {
                    return redirect()->back()->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.');
                }
            } elseif ($_SERVER['HTTP_HOST'] == 'upload.mentoringorganicmarketing.com') {
                DB::table('pemesanan')->insert([
                    'idcustomer' => $data->id,
                    'idagen'     => $data->ref,
                    'valid'      => 'WAITING CONFIRMATION',
                    'buktitf'    => Session::get('filebuktitransfer'),
                    'idproduk'   => 26,
                ]);

                try {
                    Mail::to($data->email)->send(new MOMPembayaran($data));

                    return redirect()->back()->withFlashSuccess($data->nama . ' - ' . $data->email . ', Terima Kasih. <br>Bukti Transfer Anda Berhasil Diupload !<br/> Kurang dari 1 x 24 Jam Anda akan menerima konfirmasi e-mail dari kami.');
                } catch (Throwable $th) {
                    return redirect()->back()->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.');
                }
            } elseif ($_SERVER['HTTP_HOST'] == 'member.kelasonlinecopywriting.com') {
                DB::table('pemesanan')->insert([
                    'idcustomer' => $data->id,
                    'idagen'     => $data->ref,
                    'valid'      => 'WAITING CONFIRMATION',
                    'buktitf'    => Session::get('filebuktitransfer'),
                    'idproduk'   => 27,
                ]);

                try {
                    Mail::to($data->email)->send(new KOCPembayaran($data));

                    return redirect()->back()->withFlashSuccess($data->nama . ' - ' . $data->email . ', Terima Kasih. <br>Bukti Transfer Anda Berhasil Diupload !<br/> Kurang dari 1 x 24 Jam Anda akan menerima konfirmasi e-mail dari kami.');
                } catch (Throwable $th) {
                    return redirect()->back()->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.');
                }
            } elseif ($_SERVER['HTTP_HOST'] == 'upload.kelasdatabasewa.com') {
                DB::table('pemesanan')->insert([
                    'idcustomer' => $data->id,
                    'idagen'     => $data->ref,
                    'valid'      => 'WAITING CONFIRMATION',
                    'buktitf'    => Session::get('filebuktitransfer'),
                    'idproduk'   => 28,
                ]);

                try {
                    Mail::to($data->email)->send(new KOCPembayaran($data));

                    return redirect()->back()->withFlashSuccess($data->nama . ' - ' . $data->email . ', Terima Kasih. <br>Bukti Transfer Anda Berhasil Diupload !<br/> Kurang dari 1 x 24 Jam Anda akan menerima konfirmasi e-mail dari kami.');
                } catch (Throwable $th) {
                    return redirect()->back()->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.');
                }
            } elseif ($_SERVER['HTTP_HOST'] == 'app.copywritingnextlevel.com') {
                DB::table('pemesanan')->insert([
                    'idcustomer' => $data->id,
                    'idagen'     => $data->ref,
                    'valid'      => 'WAITING CONFIRMATION',
                    'buktitf'    => Session::get('filebuktitransfer'),
                    'idproduk'   => 29,
                ]);
                $data_    = Biodatum::findOrFail($data->id);
                $data_->rek_bayar = request()->get('rekening');
                $data_->save();

                try {
                    Mail::to($data->email)->send(new CNLPembayaran($data));

                    return redirect()->back()->withFlashSuccess($data->nama . ' - ' . $data->email . ', Terima Kasih. <br>Bukti Transfer Anda Berhasil Diupload !<br/> Kurang dari 1 x 24 Jam Anda akan menerima konfirmasi e-mail dari kami.');
                } catch (Throwable $th) {
                    return redirect()->back()->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.');
                }
            } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringinstantcopywriting.com') {
                DB::table('pemesanan')->insert([
                    'idcustomer' => $data->id,
                    'idagen'     => $data->ref,
                    'valid'      => 'WAITING CONFIRMATION',
                    'buktitf'    => Session::get('filebuktitransfer'),
                    'idproduk'   => 33,
                ]);

                try {
                    Mail::to($data->email)->send(new MICPembayaran($data));

                    return redirect()->back()->withFlashSuccess($data->nama . ' - ' . $data->email . ', Terima Kasih. <br>Bukti Transfer Anda Berhasil Diupload !<br/> Kurang dari 1 x 24 Jam Anda akan menerima konfirmasi e-mail dari kami.');
                } catch (Throwable $th) {
                    return redirect()->back()->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.');
                }
            } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.kelasfbhacking.com') {
                DB::table('pemesanan')->insert([
                    'idcustomer' => $data->id,
                    'idagen'     => $data->ref,
                    'valid'      => 'WAITING CONFIRMATION',
                    'buktitf'    => $filebuktitf,
                    'idproduk'   => 34,
                ]);

                // try {
                //     Mail::to($data->email)->send(new KFHPembayaran($data));

                    return redirect()->back()->withFlashSuccess($data->nama . ' - ' . $data->email . ', Terima Kasih. <br>Bukti Transfer Anda Berhasil Diupload !<br/> Kurang dari 1 x 24 Jam Anda akan menerima konfirmasi e-mail dari kami.');
                // } catch (Throwable $th) {
                //     return redirect()->back()->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.');
                // }
            } elseif ($_SERVER['HTTP_HOST'] == 'konfirmasi.mentoringsuperreseller.com' || $_SERVER['HTTP_HOST'] == 'eid.test') {
                DB::table('pemesanan')->insert([
                    'idcustomer' => $data->id,
                    'idagen'     => $data->ref,
                    'valid'      => 'WAITING CONFIRMATION',
                    'buktitf'    => $filebuktitf,
                    'idproduk'   => 35,
                ]);

                try {
                    Mail::to($data->email)->send(new MSRPembayaran($data));

                    return redirect()->back()->withFlashSuccess($data->nama . ' - ' . $data->email . ', Terima Kasih. <br>Bukti Transfer Anda Berhasil Diupload !<br/> Kurang dari 1 x 24 Jam Anda akan menerima konfirmasi e-mail dari kami.');
                } catch (Throwable $th) {
                    return redirect()->back()->withFlashDanger($data->nama . ' - ' . $data->email . ', Email Tidak Terkirim. Terjadi Kesalahan. Mohon Diulangi !<br/>Terima kasih.');
                }
            }

        // } catch (Throwable $th) {
        //     return redirect()->route('frontend.pembayaran.simpan')->withFlashDanger($th);
        // }
    }

    public function transferulang(Request $request)
    {
        return view('frontend.user.modul.transfer-ulang');
    }

    public function cekongkir(Request $request)
    {
        if (request()->get('cari') == 'kota') {
            $provinsi = request()->get('provinsi');

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://pro.rajaongkir.com/api/city?province=$provinsi",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "key:7d23bffcefc9680802db1d10ba43eb67"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            $datakota = json_decode($response, true);

            $n[] = ['datakota' => "<option> Silahkan Pilih Kota/Kabupaten ..</option>"];

            foreach($datakota['rajaongkir']['results'] as $data){
                $n[] = ['datakota' => "<option value='".$data['city_id']."'>".$data['city_name']."</option>"];
            }
            return $n;

            // $datakota = RajaOngkirFacade::Kota()->byProvinsi(request()->get('provinsi'))->get();
            // $datakota = RajaOngkir::Kota()->dariProvinsi(request()->get('provinsi'))->get();
            // $n[] = ['datakota' => "<option> Silahkan Pilih Kota/Kabupaten ..</option>"];
            // foreach($datakota as $data){
            //     $n[] = ['datakota' => "<option value='".$data['city_id']."'>".$data['city_name']."</option>"];
            // }
            // return $n;
        } elseif (request()->get('cari') == 'kecamatan') {
            $city_id = request()->get('kota');

            $curl = curl_init();
            curl_setopt_array($curl, array(
                    CURLOPT_URL            => "https://pro.rajaongkir.com/api/subdistrict?city=$city_id",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING       => "",
                    CURLOPT_MAXREDIRS      => 10,
                    CURLOPT_TIMEOUT        => 30,
                    CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST  => "GET",
                    CURLOPT_HTTPHEADER     => array( "key:7d23bffcefc9680802db1d10ba43eb67"), //pro key rajaongkir
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $datakecamatan = json_decode($response, true);

            }

            $n[] = ['datakecamatan' => "<option> Silahkan Pilih Kecamatan ..</option>"];

            foreach($datakecamatan['rajaongkir']['results'] as $data){
                $n[] = ['datakecamatan' => "<option value='".$data['subdistrict_id']."'>".$data['subdistrict_name']."</option>"];
            }
            return $n;

        } elseif (request()->get('cari') == 'ongkir') {
            $kurir = ['tiki', 'jne', 'pos', 'jnt'];
            for ($i=0; $i < 4; $i++) {
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL 			=> "https://pro.rajaongkir.com/api/cost",
                    CURLOPT_RETURNTRANSFER 	=> true,
                    CURLOPT_ENCODING 		=> "",
                    CURLOPT_MAXREDIRS 		=> 10,
                    CURLOPT_TIMEOUT 		=> 30,
                    CURLOPT_HTTP_VERSION 	=> CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST 	=> "POST",
                    CURLOPT_POSTFIELDS 		=> "origin=19&originType=city&destination=".request()->get('kec_id')."&destinationType=subdistrict&weight=1&courier=".$kurir[$i]."", //origin = 16 -> BALIKPAPAN
                    CURLOPT_HTTPHEADER 		=> array(
                        "content-type: application/x-www-form-urlencoded",
                        "key:7d23bffcefc9680802db1d10ba43eb67" //pro key rajaongkir
                    ),
                ));

                $response 	= curl_exec($curl);
                $err 		= curl_error($curl);
                curl_close($curl);
                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    $dataongkir = json_decode($response, true);
                }

                foreach($dataongkir['rajaongkir']['results'] as $key => $datajenisongkir){
                    foreach($datajenisongkir['costs'] as $databiaya){
                        foreach($databiaya['cost'] as $data_){
                            $n[] = ['dataongkir' => "<option value='".$datajenisongkir['code'].",".
                                                                    $databiaya['service'].",".
                                                                    $data_['value'].",".
                                                                    $data_['etd']."'>".

                                                                    "<strong class='text-uppercase'>".
                                                                        $datajenisongkir['code']." ".$databiaya['service'].
                                                                    "</strong>".
                                                                    ": Rp. ".$data_['value']." - ".
                                                                    $data_['etd']." "."Hari".
                                                    "</option>"];
                        }
                    }
                }

                $ongkir[] = $dataongkir;
            }

            return $n;
        }

    }
    public function cekresi(Request $request)
    {
        if (null !== request()->get('email')){

            $data = Biodatum::where('idprodukreg', '29')->where('email', request()->get('email'))->first();

            return view('frontend.user.customer.cnl.cekresi', compact('data'));
        } else {
            return view('frontend.user.customer.cnl.cekresi');
        }
    }

    public function poster(Request $request)
    {
        $posters = Poster::paginate(12);
        return view('frontend.poster.index', compact('posters'));
    }

    public function micadvisor(Request $request)
    {
        $data = DB::table('member_MIC')->where('id', request()->sesi)->where('password', request()->token)->first();
        $advisor = MemberMicAdvisor::where('member', request()->sesi)->orderBy('created_at','desc')->paginate(3);

        if (request()->info == 'delete') {
            try {
                $advs = MemberMicAdvisor::find(request()->id);
                $advs->delete();
                alert()->info('Berhasil Dihapus')->autoclose(5000);
            } catch (\Throwable $th) {
                alert()->error('Mohon Ulangi, Terima Kasih', 'Terjadi Kesalahan')->autoclose(5000);
            }
            return redirect()->back();

        }
        if (null != request()->advisor) {

            try {
                $advs = new MemberMicAdvisor();
                $advs->member  = request()->sesi;
                $advs->advisor = request()->advisor;
                $advs->save();

                $update = DB::table('member_MIC')->where('id', request()->sesi)
                                                ->update(['status_advisor' => 'Menunggu Koreksi']);

                alert()->success('Harap Menunggu dan cek secara berkala ya ', 'Konsultasi Anda Berhasil Terkirim')->autoclose(5000);
            } catch (\Throwable $th) {
                alert()->error('Mohon Ulangi, Terima Kasih', 'Terjadi Kesalahan')->autoclose(5000);
            }
            return redirect()->back();
         }

        return view('frontend.user.customer.mic.advisor', compact('advisor', 'data'));
    }


}
