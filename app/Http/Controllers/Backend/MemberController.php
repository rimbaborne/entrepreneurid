<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use Illuminate\Support\Facades\DB;
use App\Models\MemberMicAdvisor;

use App\Models\Member;
use App\Repositories\Backend\MemberRepository;
use App\Http\Requests\Backend\Member\ManageMemberRequest;
use App\Http\Requests\Backend\Member\StoreMemberRequest;
use App\Http\Requests\Backend\Member\UpdateMemberRequest;

use App\Events\Backend\Member\MemberCreated;
use App\Events\Backend\Member\MemberUpdated;
use App\Events\Backend\Member\MemberDeleted;

class MemberController extends Controller
{
    /**
     * @var MemberRepository
     */
    protected $memberRepository;

    /**
     * MemberController constructor.
     *
     * @param MemberRepository $memberRepository
     */
    public function __construct(MemberRepository $memberRepository)
    {
        $this->memberRepository = $memberRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageMemberRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageMemberRequest $request)
    {
        return view('backend.member.index');
            // ->withmembers($this->memberRepository->getActivePaginated(25, 'id', 'asc'));
    }

    public function koc(Request $request)
    {
        return view('backend.member.koc');
    }

    public function koc2(Request $request)
    {
        return view('backend.member.koc2');
    }

    public function kocpraktikum(Request $request)
    {
        return view('backend.member.koc-praktikum');
    }


    // MIC
    public function mic(Request $request)
    {
        $peserta = DB::table('member_MIC')
                        ->when(request()->cari, function ($query) {
                                return $query->whereRaw('LOWER(nama) LIKE ? ', '%' . strtolower(request()->cari) . '%')
                                             ->orWhereRaw('LOWER(email) LIKE ? ', '%' . strtolower(request()->cari) . '%');
                        })
                        ->when(request()->status, function ($query) {
                            if( request()->status != 'Semua') {
                                return $query->where('status_advisor', '=', request()->status);
                            }
                        })
                        ->paginate(10);
        return view('backend.member.mic.peserta', compact('peserta'));
    }

    public function micadvisor(Request $request)
    {
        $data = DB::table('member_MIC')->where('id', request()->sesi)->where('password', request()->token)->first();
        $advisor = MemberMicAdvisor::where('member', request()->sesi)->orderBy('created_at','desc')->paginate(3);

        if (request()->info == 'delete') {
            try {
                $advs = MemberMicAdvisor::find(request()->id);
                $advs->delete();
                alert()->info('Berhasil Dihapus')->autoclose(5000);
            } catch (\Throwable $th) {
                alert()->error('Mohon Ulangi, Terima Kasih', 'Terjadi Kesalahan')->autoclose(5000);
            }
            return redirect()->back();

        }
        if (null != request()->advisor) {

            try {
                $advs = new MemberMicAdvisor();
                $advs->member  = request()->sesi;
                $advs->advisor = request()->advisor;
                $advs->user    = 'admin';
                $advs->save();

                $update = DB::table('member_MIC')->where('id', request()->sesi)
                                                ->update(['status_advisor' => 'Sudah Dibalas']);

                alert()->success('Berhasil Terkirim')->autoclose(5000);
            } catch (\Throwable $th) {
                alert()->error('Mohon Ulangi, Terima Kasih', 'Terjadi Kesalahan')->autoclose(5000);
            }
            return redirect()->back();
         }

        return view('backend.member.mic.advisor', compact('data', 'advisor'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @param ManageMemberRequest    $request
     *
     * @return mixed
     */
    public function create(ManageMemberRequest $request)
    {
        return view('backend.member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreMemberRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreMemberRequest $request)
    {
        $this->memberRepository->create($request->only(
            'produk'
        ));

        // Fire create event (MemberCreated)
        event(new MemberCreated($request));

        return redirect()->route('admin.members.index')
            ->withFlashSuccess(__('backend_members.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManageMemberRequest  $request
     * @param Member               $member
     *
     * @return mixed
     */
    public function show(ManageMemberRequest $request, Member $member)
    {
        return view('backend.member.show')->withMember($member);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageMemberRequest $request
     * @param Member              $member
     *
     * @return mixed
     */
    public function edit(ManageMemberRequest $request, Member $member)
    {
        return view('backend.member.edit')->withMember($member);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateMemberRequest  $request
     * @param Member               $member
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateMemberRequest $request, Member $member)
    {
        $this->memberRepository->update($member, $request->only(
            'produk'
        ));

        // Fire update event (MemberUpdated)
        event(new MemberUpdated($request));

        return redirect()->route('admin.members.index')
            ->withFlashSuccess(__('backend_members.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageMemberRequest $request
     * @param Member              $member
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageMemberRequest $request, Member $member)
    {
        $this->memberRepository->deleteById($member->id);

        // Fire delete event (MemberDeleted)
        event(new MemberDeleted($request));

        return redirect()->route('admin.members.deleted')
            ->withFlashSuccess(__('backend_members.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManageMemberRequest $request
     * @param Member              $deletedMember
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageMemberRequest $request, Member $deletedMember)
    {
        $this->memberRepository->forceDelete($deletedMember);

        return redirect()->route('admin.members.deleted')
            ->withFlashSuccess(__('backend_members.alerts.deleted_permanently'));
    }

    /**
     * @param ManageMemberRequest $request
     * @param Member              $deletedMember
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageMemberRequest $request, Member $deletedMember)
    {
        $this->memberRepository->restore($deletedMember);

        return redirect()->route('admin.members.index')
            ->withFlashSuccess(__('backend_members.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManageMemberRequest $request
     *
     * @return mixed
     */
    public function deleted(ManageMemberRequest $request)
    {
        return view('backend.member.deleted')
            ->withmembers($this->memberRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
