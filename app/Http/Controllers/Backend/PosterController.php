<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\Poster;
use App\Repositories\Backend\PosterRepository;
use App\Http\Requests\Backend\Poster\ManagePosterRequest;
use App\Http\Requests\Backend\Poster\StorePosterRequest;
use App\Http\Requests\Backend\Poster\UpdatePosterRequest;

use App\Events\Backend\Poster\PosterCreated;
use App\Events\Backend\Poster\PosterUpdated;
use App\Events\Backend\Poster\PosterDeleted;

use Image;
use Str;

class PosterController extends Controller
{
    /**
     * @var PosterRepository
     */
    protected $posterRepository;

    /**
     * PosterController constructor.
     *
     * @param PosterRepository $posterRepository
     */
    public function __construct(PosterRepository $posterRepository)
    {
        $this->posterRepository = $posterRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManagePosterRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManagePosterRequest $request)
    {
        if(request()->get('status') == 'hapus'){
            Poster::where('id', request()->get('id'))->delete();
            return redirect()->back()
                    ->withFlashSuccess('Berhasil Dihapus!');
        }
        return view('backend.poster.index')->withposters($this->posterRepository->getActivePaginated(12, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManagePosterRequest    $request
     *
     * @return mixed
     */
    public function create(ManagePosterRequest $request)
    {
        return view('backend.poster.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePosterRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    // public function store(StorePosterRequest $request)
    public function store()
    {
        // $this->posterRepository->create($request->only(
        //     'nama',
        //     'kategori',
        //     'file',
        //     'jenis',
        //     'data',
        //     'deskripsi',
        // ));

        // // Fire create event (PosterCreated)
        // event(new PosterCreated($request));

        $poster         = Image::make(request()->file('file'));
        $lokasiposter   = public_path('/poster/');
        $lokasicnl      = public_path('../../../public_html/copywritingnextlevel.com/app/poster/');

        $poster->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $namaposter = Str::random(10) . '.' . request()->file('file')->getClientOriginalExtension();
        $poster->save($lokasiposter.$namaposter);
        $poster->save($lokasicnl.$namaposter);


        $poster = Poster::create([
            'link' => request()->get('link'),
            'file' => $namaposter,
        ]);

        return redirect()->back()->withFlashSuccess('Upload Berhasil !');
    }

    /**
     * Display the specified resource.
     *
     * @param ManagePosterRequest  $request
     * @param Poster               $poster
     *
     * @return mixed
     */
    public function show(ManagePosterRequest $request, Poster $poster)
    {
        return view('backend.poster.show')->withPoster($poster);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManagePosterRequest $request
     * @param Poster              $poster
     *
     * @return mixed
     */
    public function edit(ManagePosterRequest $request, Poster $poster)
    {
        return view('backend.poster.edit')->withPoster($poster);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePosterRequest  $request
     * @param Poster               $poster
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdatePosterRequest $request, Poster $poster)
    {
        $this->posterRepository->update($poster, $request->only(
            'nama'
        ));

        // Fire update event (PosterUpdated)
        event(new PosterUpdated($request));

        return redirect()->route('admin.posters.index')
            ->withFlashSuccess(__('backend_posters.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManagePosterRequest $request
     * @param Poster              $poster
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManagePosterRequest $request, Poster $poster)
    {
        $this->posterRepository->deleteById($poster->id);

        // Fire delete event (PosterDeleted)
        event(new PosterDeleted($request));

        return redirect()->route('admin.posters.deleted')
            ->withFlashSuccess(__('backend_posters.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManagePosterRequest $request
     * @param Poster              $deletedPoster
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManagePosterRequest $request, Poster $deletedPoster)
    {
        $this->posterRepository->forceDelete($deletedPoster);

        return redirect()->route('admin.posters.deleted')
            ->withFlashSuccess(__('backend_posters.alerts.deleted_permanently'));
    }

    /**
     * @param ManagePosterRequest $request
     * @param Poster              $deletedPoster
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManagePosterRequest $request, Poster $deletedPoster)
    {
        $this->posterRepository->restore($deletedPoster);

        return redirect()->route('admin.posters.index')
            ->withFlashSuccess(__('backend_posters.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManagePosterRequest $request
     *
     * @return mixed
     */
    public function deleted(ManagePosterRequest $request)
    {
        return view('backend.poster.deleted')
            ->withposters($this->posterRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
