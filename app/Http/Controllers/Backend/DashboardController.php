<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Event;
use Illuminate\Support\Carbon;
use App\Models\Transaction;
use Carbon\CarbonPeriod;
use App\Models\SlideAgen;
use Image;
use Str;
/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    public function __construct(){

        $this->event         = request()->event;
        $this->cekeventaktif = !null == $this->event
                                ?
                                Event::find($this->event)
                                :
                                Event::latest('created_at')->first();

        $this->awal          = request()->awal ?? $this->cekeventaktif->start;
        $this->akhir         = request()->akhir ?? $this->cekeventaktif->end;
    }

    public function index()
    {
        $dataevent = $this->cekeventaktif;
        $idevent   = $this->cekeventaktif->id;

        // $transaksi  = Transaction::ofEvent($idevent);
        // $konfirmasi = $transaksi->offset(0)->limit(7)->orderBy('waktu_konfirmasi', 'desc')->get();

        // DATA PERHITUNGAN STATISTIK
        $belumaktif         = Transaction::ofEvent($idevent)->belumAktif()->count();
        $menunggukonfirmasi = Transaction::ofEvent($idevent)->menungguKonfirmasi()->count();
        $aktif              = Transaction::ofEvent($idevent)->aktif()->count();

        $afiliasi = Transaction::ofEvent($idevent)->afiliasi()->count();
        $manual   = Transaction::ofEvent($idevent)->manual()->count();

        $baaf = Transaction::ofEvent($idevent)->afiliasi()->belumAktif()->count();
        $bama = Transaction::ofEvent($idevent)->manual()->belumAktif()->count();
        $mkaf = Transaction::ofEvent($idevent)->afiliasi()->menungguKonfirmasi()->count();
        $mkma = Transaction::ofEvent($idevent)->manual()->menungguKonfirmasi()->count();
        $akaf = Transaction::ofEvent($idevent)->afiliasi()->aktif()->count();
        $akma = Transaction::ofEvent($idevent)->manual()->aktif()->count();

        $agen   = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->get()->count();
        $agenba = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->belumAktif()->get()->count();
        $agenmk = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->menungguKonfirmasi()->get()->count();
        $agenak = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->aktif()->get()->count();

        $agbaaf = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->afiliasi()->belumAktif()->get()->count();
        $agbama = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->manual()->belumAktif()->get()->count();
        $agmkaf = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->afiliasi()->menungguKonfirmasi()->get()->count();
        $agmkma = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->manual()->menungguKonfirmasi()->get()->count();
        $agakaf = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->afiliasi()->aktif()->get()->count();
        $agakma = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->manual()->aktif()->get()->count();

        $data = array(
            'afiliasi'                          => $afiliasi,
            'manual'                            => $manual,
            'belum_aktif'                       => $belumaktif,
            'menunggu_konfirmasi'               => $menunggukonfirmasi,
            'aktif'                             => $aktif,
            'belum_aktif_afiliasi'              => $baaf,
            'belum_aktif_manual'                => $bama,
            'menunggu_konfirmasi_afiliasi'      => $mkaf,
            'menunggu_konfirmasi_manual'        => $mkma,
            'aktif_afiliasi'                    => $akaf,
            'aktif_manual'                      => $akma,
            'agen'                              => $agen,
            'agen_closing_belum_aktif'          => $agenba,
            'agen_closing_menunggu_konfirmasi'  => $agenmk,
            'agen_closing_aktif'                => $agenak,
            'agen_belum_aktif_afiliasi'         => $agbaaf,
            'agen_belum_aktif_manual'           => $agbama,
            'agen_menunggu_konfirmasi_afiliasi' => $agmkaf,
            'agen_menunggu_konfirmasi_manual'   => $agmkma,
            'agen_aktif_afiliasi'               => $agakaf,
            'agen_aktif_manual'                 => $agakma,
        );

        // dd($data['manual']);

        // DATA STATISTIK DARI SISTEM BARU

        $data_tr = CarbonPeriod::create(Carbon::now()->subdays(120), Carbon::now());

        foreach ($data_tr as $key => $date){
            $data_tanggal_sta_[] = $date->format('d');
            $data_total_tran_[]  = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->count();
            $data_tran_afi_[]    = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->afiliasi()->count();
            $data_tran_man_[]    = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->manual()->count();
            $data_tran_akt_[]    = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->aktif()->count();
            $data_total_agen_[]  = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->select('id_agen')->groupBy('id_agen')->get()->count();
        }

        $tanggal_sta_ = str_replace('"', '', json_encode($data_tanggal_sta_));
        $total_tran_  = str_replace('"', '', json_encode($data_total_tran_));
        $tran_afi_    = str_replace('"', '', json_encode($data_tran_afi_));
        $tran_man_    = str_replace('"', '', json_encode($data_tran_man_));
        $tran_akt_    = str_replace('"', '', json_encode($data_tran_akt_));
        $total_agen_  = str_replace('"', '', json_encode($data_total_agen_));

        return view('backend.dashboard', compact('total_agen_','tran_man_','tran_akt_','tran_afi_','total_tran_','tanggal_sta_','data', 'dataevent'));
    }

    public function slideagen()
    {
        if (request()->metode == 'delete') {
            $delete = SlideAgen::find(request()->id);
            $delete->delete();

            return redirect()->back()->withFlashSuccess('Gambar Berhasil Di Hapus !');
        }

        if (request()->metode == 'update') {
            $update      = SlideAgen::find(request()->id);
            $update->url = request()->url;
            $update->save();

            return redirect()->back()->withFlashSuccess('URL Gambar Berhasil Di Perbarui !');
        }

        if (request()->metode == 'aktif') {
            $aktif        = SlideAgen::find(request()->id);
            $aktif->aktif = 1;
            $aktif->save();

            return redirect()->back()->withFlashSuccess('Gambar Berhasil Di Aktifkan !');
        }

        if (request()->metode == 'nonaktif') {
            $nonaktif        = SlideAgen::find(request()->id);
            $nonaktif->aktif = 0;
            $nonaktif->save();

            return redirect()->back()->withFlashSuccess('Gambar Berhasil Di Nonaktifkan !');
        }

        if (request()->metode == 'urutan') {
            for ($i=0; $i < count(request()->id); $i++) {
                $urutan        = SlideAgen::find(request()->id[$i]);
                $urutan->sort = request()->urutan[$i];
                $urutan->save();
            }

            return redirect()->back()->withFlashSuccess('Berhasil Di Perbaruhi !');
        }

        $slideaktif = SlideAgen::where('aktif', '=', 1)->orderBy('sort', 'asc')->paginate(10, ['*'], 'aktif_page');
        $slidedata  = SlideAgen::orderBy('created_at', 'desc')-> paginate(10, ['*'], 'data_page');

        return view('backend.slideagen.index', compact('slidedata', 'slideaktif'));
    }

    public function slideagentambah()
    {
        $tambah = new SlideAgen;

        $gambar         = Image::make(request()->file('gambar'));
        $namagambar     = Str::slug(request()->nama).'.'.request()->file('gambar')->getClientOriginalExtension();
        $lokasigambar   = public_path('../../../public_html/agen-entrepreneurid.com/dashboard/img/slide/');
        $lokasigambar2  = public_path('../../../public_html/entrepreneurid.org/admin/img/slide/');
        // $lokasigambar3  = public_path('/img/slide/');
        $gambar->resize(800, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $gambar->save($lokasigambar.$namagambar);
        $gambar->save($lokasigambar2.$namagambar);
        // $gambar->save($lokasigambar3.$namagambar);

        $tambah->nama   = request()->nama;
        $tambah->url    = request()->url;
        $tambah->gambar = $namagambar;
        $tambah->save();

        return redirect()->back()->withFlashSuccess('Gambar Berhasil Di Upload !');
    }

}
