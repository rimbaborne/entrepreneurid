<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Notification;
use App\Models\Produk;
use App\Models\Transaction;
use DB;
use Str;
use Image;

class EventController extends Controller
{

    public function index()
    {
        $dataevent = Event::orderBy('created_at', 'desc')->paginate(3);

        if (request()->mode == 'open') {
            $event = Event::find(request()->id);
            $event->open = request()->status;
            $event->save();
            return redirect()->back()->withFlashSuccess('Berhasil Diperbaruhi !');
        }

        if (request()->mode == 'publish') {
            $event = Event::find(request()->id);
            $event->publish = request()->status;
            $event->save();
            return redirect()->back()->withFlashSuccess('Berhasil Diperbaruhi !');
        }

        return view('backend.event.index', compact('dataevent'));
    }

    public function create()
    {
        $dataproduk = Produk::where('status', 'AKTIF')->get();

        $produk  = null;
        $kode    = null;
        $idevent = null;
        if (request()->produk) {
            $produk     = Produk::find(request()->produk ?? null);
            $kode       = Event::where('id_event', 'like', $produk->inisial.'%')->count() + 1;
            $idevent    = $produk->inisial.'-'.\Carbon\Carbon::now()->year.'-'.$kode;
        }
        return view('backend.event.create', compact('dataproduk', 'produk', 'idevent'));
    }

    public function store(Request $request)
    {
        $gambar         = Image::make($request->file('gambar'));
        $namagambar     = request()->idevent.'.'.$request->file('gambar')->getClientOriginalExtension();
        $lokasigambar   = public_path('../../../public_html/agen-entrepreneurid.com/dashboard/img/produk/');
        $lokasigambar2  = public_path('../../../public_html/entrepreneurid.org/admin/img/produk/');
        $gambar->resize(500, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $gambar->save($lokasigambar.$namagambar);
        $gambar->save($lokasigambar2.$namagambar);

        $tambahevent = new Event;

        $tambahevent->uuid       = Str::uuid();
        $tambahevent->produk_id  = request()->produkid;
        $tambahevent->id_event   = request()->idevent;
        $tambahevent->open       = request()->open;
        $tambahevent->publish    = request()->publish;
        $tambahevent->start      = request()->start;
        $tambahevent->end        = request()->end;
        $tambahevent->komisi     = request()->komisi;
        $tambahevent->harga      = request()->harga;
        $tambahevent->link_sales = request()->linksales;
        $tambahevent->link_form  = request()->linkform;
        $tambahevent->gambar     = $namagambar;
        $tambahevent->kode_akses = strtoupper(Str::random(5));

        $tambahevent->save();

        return redirect()->to('/admin/events')->withFlashSuccess('Event Berhasil Dibuat !');
    }

    public function edit()
    {
        $data = Event::find(request()->id);
        return view('backend.event.edit', compact('data'));
    }

    public function update()
    {
        $data             = Event::find(request()->id);
        $data->harga      = request()->harga;
        $data->komisi     = request()->komisi;
        $data->komisi_jv  = request()->komisi_jv;
        $data->start      = request()->start;
        $data->end        = request()->end;
        $data->link_sales = request()->linksales;
        $data->link_form  = request()->linkform;
        $data->open       = request()->open;
        $data->publish    = request()->publish;
        $data->open_jv    = request()->open_jv;
        $data->publish_jv = request()->publish_jv;
        $data->save();

        return redirect()->route('admin.events.index')->withFlashSuccess('Berhasil Di Update!');
    }

    public function notifikasi()
    {
        $data = Event::find(request()->id);
        $notif = Notification::where('id_event', request()->id)->first();
        if ( $notif == null) {
            $buatnotif = new Notification;
            $buatnotif->id_event = request()->id;
            $buatnotif->save();
        }

        if (request()->notif == null) {
            $datanotif = null;
        } else {
            $datanotif = Notification::where('id_event', request()->id)->first();
        }

        return view('backend.event.notifikasi', compact('data', 'datanotif'));
    }

    public function notifikasisimpan()
    {
        $data = array(
            'email'   => request()->email ?? 'whatsapp',
            'subject' => request()->subject ?? 'whatsapp',
            'name'    => request()->name ?? 'whatsapp',
            'isi'     => request()->isi,
        );

        $notif = Notification::where('id_event', request()->id)
                    ->update([request()->jenisnotif => json_encode($data)]);
        // dd($notif);
        return redirect()->back()->withFlashSuccess('Penyimpanan Notif Berhasil !');

    }

    public function leaderboard()
    {
        $dataevent_ = Event::orderBy('leaderboard', 'desc')->orderBy('created_at', 'desc')->paginate(15);
        $dataevent = Event::orderBy('created_at', 'desc')->paginate(15);

        if (request()->metode == 'update') {
            $update = Event::find(request()->id);
            $update->leaderboard = request()->status;
            $update->save();

            return redirect()->back()->withFlashSuccess('BNerhasil Diperbaruhi !');
        }

        if (request()->event) {
            $event = Event::where('uuid', '=', request()->event)->first();
        } else {
            $event = Event::latest('created_at')->first();
        }
        if ($event) {
            $total = Transaction::aktif()
                            ->where('id_event', '=', $event->id)
                            ->count();
            $leaderboard = Transaction::aktif()
                            ->where('id_event', '=', $event->id)
                            ->select('id_agen','id_event', (DB::raw('COUNT(*) as jumlah ')))
                            ->groupBy('id_agen','id_event')
                            ->orderBy('jumlah', 'desc')
                            ->paginate($total);
            return view('backend.leaderboard.index', compact('leaderboard', 'dataevent', 'dataevent_', 'event'));
        } else {
            $event = null;
            $leaderboard = null;
            return view('backend.leaderboard.index', compact('leaderboard', 'dataevent', 'dataevent_', 'event'));
        }
    }
}
