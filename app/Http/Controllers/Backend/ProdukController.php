<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Produk;

class ProdukController extends Controller
{
    public function index()
    {
        $editproduk = null;
        if (request()->metode == 'buat') {
            $produk = new Produk;

            $produk->nama    = request()->nama;
            $produk->inisial = request()->inisial;
            $produk->domain  = request()->domain;
            $produk->jenis   = request()->jenis;
            $produk->status  = request()->status;

            $produk->save();
            return redirect()->back()->withFlashSuccess('Penambahan Berhasil !');
        }

        if (request()->metode == 'update') {
            $produk = Produk::find(request()->id);

            $produk->nama    = request()->nama;
            $produk->inisial = request()->inisial;
            $produk->domain  = request()->domain;
            $produk->jenis   = request()->jenis;

            $produk->save();
            return redirect()->route('admin.produks.index')->withFlashSuccess('Update Berhasil !');
        }
        if (request()->metode == 'edit') {
            $editproduk = Produk::find(request()->id);
        }

        if (request()->metode == 'hapus') {
            $hapusproduk = Produk::find(request()->id);
            $hapusproduk->delete();
            return redirect()->route('admin.produks.index')->withFlashSuccess('Berhasil Dihapus !');
        }

        $dataproduk = Produk::paginate(5);


        return view('backend.produk.index', compact('dataproduk', 'editproduk'));
    }

}
