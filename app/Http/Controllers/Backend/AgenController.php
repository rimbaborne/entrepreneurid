<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Hash;


use App\Models\Agen;
use App\Repositories\Backend\AgenRepository;
use App\Http\Requests\Backend\Agen\ManageAgenRequest;
use App\Http\Requests\Backend\Agen\StoreAgenRequest;
use App\Http\Requests\Backend\Agen\UpdateAgenRequest;

use App\Events\Backend\Agen\AgenCreated;
use App\Events\Backend\Agen\AgenUpdated;
use App\Events\Backend\Agen\AgenDeleted;
use App\Exports\DataAgenExport;

use App\Models\Biodatum;
use App\Models\Pemesanan;
use Illuminate\Support\Facades\DB;
use Datatables;
use Yajra\DataTables\Services\DataTable;
use App\Models\Auth\Agen as Agens;
use App\Mail\AgenBaruDashboardKonfirmasi;
use Illuminate\Support\Facades\Mail;
use App\Exports\DataAgenBaruExport;

class AgenController extends Controller
{
    /**
     * @var AgenRepository
     */
    protected $agenRepository;

    /**
     * AgenController constructor.
     *
     * @param AgenRepository $agenRepository
     */
    public function __construct(AgenRepository $agenRepository)
    {
        $this->agenRepository = $agenRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageAgenRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageAgenRequest $request)
    {
        $cari = request()->get('cari') ?? null;
        $dataagen = Agens::when($cari, function ($query, $cari) {
                            return $query->whereRaw('LOWER(name) LIKE ? ', '%' . strtolower($cari) . '%')
                                        ->orWhereRaw('LOWER(email) LIKE ? ', '%' . strtolower($cari) . '%')
                                        ->orWhereRaw('LOWER(notelp) LIKE ? ', '%' . strtolower($cari) . '%')
                                        ->orWhereRaw('LOWER(nama_rek) LIKE ? ', '%' . strtolower($cari) . '%')
                                        ->orWhereRaw('LOWER(no_rek) LIKE ? ', '%' . strtolower($cari) . '%')
                                        ->orWhereRaw('LOWER(bank_rek) LIKE ? ', '%' . strtolower($cari) . '%')
                                        ;
                            })
                        ->orderBy('created_at', 'desc')
                        ->whereNull('status_agen')
                        ->paginate(10);

        if (request()->aktif) {
            $agen = Agens::find(request()->id);
            $agen->status_aktif = request()->status;
            $agen->save();

            Mail::to($agen->email)->send(new AgenBaruDashboardKonfirmasi($agen));

            return redirect()->back()->withFlashSuccess($agen->email.', Status Agen Berhasil Diaktifkan !');
        }

        if (request()->metode == 'edit') {
            $agen               = Agens::find(request()->id);
            $agen->name         = request()->nama;
            $agen->email        = request()->email;
            $agen->gender       = request()->gender;
            $agen->tgl_lahir    = request()->tgllahir;
            $agen->kode_notelp  = request()->kodenotelp;
            $agen->notelp       = request()->notelp;
            $agen->nama_rek     = request()->namarek;
            $agen->bank_rek     = request()->bankrek;
            $agen->no_rek       = request()->norek;
            $agen->kota         = request()->kota;
            $agen->save();

            return redirect()->back()->withFlashSuccess($agen->email.', Data Berhasil Diupdate !');
        }

        if (request()->metode == 'password') {
            $agen               = Agens::find(request()->id);
            $agen->password     = Hash::make(request()->password);
            $agen->save();

            return redirect()->back()->withFlashSuccess($agen->email.', Password Berhasil Diupdate !');
        }

        return view('backend.agen.index', compact('dataagen'));
    }

    public function subagen(ManageAgenRequest $request)
    {
        $cari = request()->get('cari') ?? null;
        $dataagen = Agens::when($cari, function ($query, $cari) {
                            return $query->whereRaw('LOWER(name) LIKE ? ', '%' . strtolower($cari) . '%')
                                        ->orWhereRaw('LOWER(email) LIKE ? ', '%' . strtolower($cari) . '%')
                                        ->orWhereRaw('LOWER(notelp) LIKE ? ', '%' . strtolower($cari) . '%')
                                        ->orWhereRaw('LOWER(nama_rek) LIKE ? ', '%' . strtolower($cari) . '%')
                                        ->orWhereRaw('LOWER(no_rek) LIKE ? ', '%' . strtolower($cari) . '%')
                                        ->orWhereRaw('LOWER(bank_rek) LIKE ? ', '%' . strtolower($cari) . '%')
                                        ;
                            })
                            ->orderBy('created_at', 'desc')
                            ->whereNotNull('status_agen')
                            ->paginate(10);

        if (request()->aktif) {
            $agen = Agens::find(request()->id);
            $agen->status_aktif = request()->status;
            $agen->save();

            Mail::to($agen->email)->send(new AgenBaruDashboardKonfirmasi($agen));

            return redirect()->back()->withFlashSuccess($agen->email.', Status Agen Berhasil Diaktifkan !');
        }

        if (request()->aktifkanagen) {
            $agen = Agens::find(request()->id);
            $agen->status_agen = null;
            $agen->sub_agen_id = request()->aktifkanagen; // simpan backup id
            $agen->save();

            return redirect()->back()->withFlashSuccess($agen->email.', Sub Agen Berhasil Dijadikan Agen Resmi entrepreneurID !');
        }

        return view('backend.agen.sub-agen', compact('dataagen'));
    }

    public function password(ManageAgenRequest $request)
    {
        $agen = $agen = Agens::find(request()->id);
        if(request()->metode == 'update'){
            $agen->password = bcrypt(request()->password);
            $agen->save();
            return redirect()->back()->withFlashSuccess($agen->email.', Password Berhasil Diperbaruhi !');
        }
        return view('backend.agen.password', compact('agen'));
    }

    public function agenlama(ManageAgenRequest $request)
    {
        $cari = request()->get('cari') ?? null;

        // return view('backend.agen.index')
        //     ->withagens($this->agenRepository->getActivePaginated(25, 'id', 'asc'));
        if ( null != $cari) {
            $dataagen = Biodatum::
            where(function($query) {
                $query->where('status_user', '1')
                    ->where('status_user', '2');
            })
            ->where('aktif', '2')
            ->when($cari, function ($query, $cari) {
                return $query->whereRaw('LOWER(nama) LIKE ? ', '%' . strtolower($cari) . '%')
                            ->orWhereRaw('LOWER(email) LIKE ? ', '%' . strtolower($cari) . '%')
                            ->orWhereRaw('LOWER(no_telp) LIKE ? ', '%' . strtolower($cari) . '%')
                            ->orWhereRaw('LOWER(nama_rek) LIKE ? ', '%' . strtolower($cari) . '%')
                            ->orWhereRaw('LOWER(rek_rek) LIKE ? ', '%' . strtolower($cari) . '%')
                            ->orWhereRaw('LOWER(bank_rek) LIKE ? ', '%' . strtolower($cari) . '%')
                            ;
            })
            ->orderBy('id', 'asc')
            ->paginate(10);
        } else {
            $dataagen = Biodatum::
            where(function($query) {
                $query->where('status_user', '1')
                ->where('aktif', '2');
            })
            ->orWhere(function($query) {
                $query->where('aktif', '=', '2')
                    ->where('idprodukreg', '=', '24');
            })
            ->orderBy('id', 'asc')
            ->paginate(10);
        }



        return view('backend.agen.index-lama', compact('dataagen'));
    }

    public function export(Request $request)
    {
        return (new DataAgenExport)->download('Data Agen entrepreneurID.xlsx');
    }

    public function agenbaru(Request $request)
    {
        return (new DataAgenBaruExport)->download('Data Agen entrepreneurID.xlsx');
    }

    public function yajra(Request $request)
    {
        return view('backend.agen.index-yajra');
    }

    public function yajradata(Request $request)
    {
        return Datatables::of(Biodatum::latest()->get())->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param ManageAgenRequest    $request
     *
     * @return mixed
     */
    public function create(ManageAgenRequest $request)
    {
        return view('backend.agen.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAgenRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreAgenRequest $request)
    {
        $this->agenRepository->create($request->only(
            'id_agen'
        ));

        // Fire create event (AgenCreated)
        event(new AgenCreated($request));

        return redirect()->route('admin.agens.index')
            ->withFlashSuccess(__('backend_agens.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManageAgenRequest  $request
     * @param Agen               $agen
     *
     * @return mixed
     */
    public function show(ManageAgenRequest $request, Agen $agen)
    {
        return view('backend.agen.show')->withAgen($agen);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageAgenRequest $request
     * @param Agen              $agen
     *
     * @return mixed
     */
    public function edit(ManageAgenRequest $request, Agen $agen)
    {
        return view('backend.agen.edit')->withAgen($agen);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAgenRequest  $request
     * @param Agen               $agen
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateAgenRequest $request, Agen $agen)
    {
        $this->agenRepository->update($agen, $request->only(
            'id_agen'
        ));

        // Fire update event (AgenUpdated)
        event(new AgenUpdated($request));

        return redirect()->route('admin.agens.index')
            ->withFlashSuccess(__('backend_agens.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageAgenRequest $request
     * @param Agen              $agen
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageAgenRequest $request, Agen $agen)
    {
        $this->agenRepository->deleteById($agen->id);

        // Fire delete event (AgenDeleted)
        event(new AgenDeleted($request));

        return redirect()->route('admin.agens.deleted')
            ->withFlashSuccess(__('backend_agens.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManageAgenRequest $request
     * @param Agen              $deletedAgen
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageAgenRequest $request, Agen $deletedAgen)
    {
        $this->agenRepository->forceDelete($deletedAgen);

        return redirect()->route('admin.agens.deleted')
            ->withFlashSuccess(__('backend_agens.alerts.deleted_permanently'));
    }

    /**
     * @param ManageAgenRequest $request
     * @param Agen              $deletedAgen
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageAgenRequest $request, Agen $deletedAgen)
    {
        $this->agenRepository->restore($deletedAgen);

        return redirect()->route('admin.agens.index')
            ->withFlashSuccess(__('backend_agens.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManageAgenRequest $request
     *
     * @return mixed
     */
    public function deleted(ManageAgenRequest $request)
    {
        return view('backend.agen.deleted')
            ->withagens($this->agenRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
