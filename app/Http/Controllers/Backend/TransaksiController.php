<?php

namespace App\Http\Controllers\Backend;

use App\Exports\DataTransaksi;
use App\Exports\DataTransaksiBuku;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use App\Models\Event;
use App\Models\Transaction;
use Carbon\Carbon;
use Throwable;
use Illuminate\Support\Facades\Mail;
use App\Mail\emailv2;
use Str;
use Illuminate\Support\Facades\Session;
use Image;
use App\Models\Auth\Agen;
use DB;
use Carbon\CarbonPeriod;
use App\Models\BookTransactions;
use App\Exports\DataLeadMagnet;
use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{
    public function __construct(){
        $this->nama          = $this->nama ?? null;
        $this->email         = $this->email ?? null;
        $this->panggilan     = $this->panggilan ?? null;
        $this->gender        = $this->gender ?? null;
        $this->tgllahir      = $this->tgllahir  ?? null;
        $this->nohp          = $this->nohp ?? null;
        $this->kota          = $this->kota ?? null;
        $this->total         = $this->total ?? null;
        $this->ekspedisi     = $this->ekspedisi ?? null;
        $this->jmlh_buku     = $this->jmlh_buku ?? null;
        $this->alamat        = $this->alamat ?? null;
        $this->ref           = request()->ref ?? null;
        $this->perpage       = request()->perpage ?? 5;
        $this->kodeunik      = rand(2,600);
        $this->event         = request()->produk;
        $this->cekeventaktif = !null == $this->event
                                ?
                                Event::find($this->event)
                                :
                                Event::latest('created_at')->first();

        $this->awal          = request()->awal ?? $this->cekeventaktif->start;
        $this->akhir         = request()->akhir ?? $this->cekeventaktif->end;
    }

    public function convertnotif($isi)
    {
        $notifikasi = str_replace('{Nama}', $this->nama, $isi);
        $notifikasi = str_replace('{E-mail}', $this->email, $notifikasi);
        $notifikasi = str_replace('{Panggilan}', $this->panggilan, $notifikasi);
        $notifikasi = str_replace('{Gender}', $this->gender, $notifikasi);
        $notifikasi = str_replace('{No. Telepon}', $this->nohp, $notifikasi);
        $notifikasi = str_replace('{Kota}', $this->kota, $notifikasi);
        $notifikasi = str_replace('{Nominal Transfer}', $this->total, $notifikasi);
        $notifikasi = str_replace('{Ekspedisi}', $this->ekspedisi, $notifikasi);
        $notifikasi = str_replace('{Jumlah Buku}', $this->jmlh_buku, $notifikasi);
        $notifikasi = str_replace('{Alamat}', $this->alamat, $notifikasi);

        return $notifikasi;
    }

    public function notifemail($penerima, $isiemail)
    {
        $dataemail = json_decode($isiemail);
        $data = [
            'name'     => $dataemail->name,
            'email'    => $dataemail->email,
            'subject'  => $dataemail->subject,
            'isiemail' => $this->convertnotif($dataemail->isi),
        ];

        Mail::to($penerima)->send(new emailv2($data));
    }

    public function notifwa($nomorhp, $isipesan)
    {
        $datawa = json_decode($isipesan);

        $apikey     = env('WAHA_API_KEY');
        $url        = env('WAHA_API_URL');
        $sessionApi = env('WAHA_API_SESSION');
        $requestApi = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Accept'       => 'application/json',
            'X-Api-Key'    => $apikey,
        ]);

        // SOP based on https://waha.devlike.pro/docs/overview/how-to-avoid-blocking/

        try {
            #1 Send Seen
            $requestApi->post($url.'/api/sendSeen', [ "session" => $sessionApi, "chatId"  => $nomorhp.'@c.us', ]);

            #2 Start Typing
            $requestApi->post($url.'/api/startTyping', [ "session" => $sessionApi, "chatId"  => $nomorhp.'@c.us', ]);

            sleep(1); // jeda seolah olah ngetik

            #3 Stop Typing
            $requestApi->post($url.'/api/stopTyping', [ "session" => $sessionApi, "chatId"  => $nomorhp.'@c.us', ]);

            #4 Send Message
            $requestApi->post($url.'/api/sendText', [
                "session" => $sessionApi,
                "chatId"  => $nomorhp.'@c.us',
                "text"    => $this->convertnotif($datawa->isi),
            ]);
        } catch (Throwable $th) {
            throw $th;
        }
    }

    public function index()
    {
        $edittransaksi = null;
        if (request()->metode == 'buat') {
            $transaksi = new Transaction;
            $dataevent = Event::find(request()->produk);

            $transaksi->uuid        = Str::uuid();
            $transaksi->nama        = strtoupper(request()->nama);
            $transaksi->email       = strtolower(request()->email);
            $transaksi->panggilan   = request()->panggilan;
            $transaksi->kode_nohp   = request()->kode_nohp;
            $transaksi->nohp        = request()->nohp;
            $transaksi->gender      = request()->gender;
            $transaksi->tgllahir    = request()->tgllahir;
            $transaksi->id_agen     = request()->ref;
            $transaksi->id_event    = request()->produk;
            $transaksi->total       = $dataevent->harga;
            $transaksi->status      = 1;
            $transaksi->jenis       = 1;

            $transaksi->save();
            return redirect()->to('/admin/transaksi?nama='.$transaksi->email.'&produk='.$transaksi->id_event)->withFlashSuccess('Update Berhasil !');
        }

        if (request()->metode == 'update') {
            $transaksi = Transaction::find(request()->id);

            $transaksi->nama        = strtoupper(request()->nama);
            $transaksi->email       = strtolower(request()->email);
            $transaksi->panggilan   = request()->panggilan;
            $transaksi->kode_nohp   = request()->kode_nohp;
            $transaksi->nohp        = request()->nohp;
            $transaksi->gender      = request()->gender;
            $transaksi->tgllahir    = request()->tgllahir;

            $transaksi->save();
            return redirect()->to('/admin/transaksi?nama='.$transaksi->email.'&produk='.$transaksi->id_event)->withFlashSuccess('Update Berhasil !');
        }

        if (request()->metode == 'ganti-agen') {
            $transaksi = Transaction::find(request()->id);
            $transaksi->id_agen     = request()->ref;
            $transaksi->save();
            return redirect()->back()->withFlashSuccess('Update Agen Berhasil !');
        }

        if (request()->metode == 'edit') {
            $edittransaksi = Transaction::find(request()->id);
        }

        if (request()->metode == 'upload') {
            $edittransaksi = Transaction::find(request()->id);

            $generate = $edittransaksi->email . '-' . data_get($edittransaksi->event, 'id_event') . '-' . Str::random(5);
            Session::put('sesiupload', $generate);
            Session::put('domain', $edittransaksi->event->produk->domain);
            Session::put('jenis', $edittransaksi->jenis);
        }

        if (request()->metode == 'hapus') {
            $hapustransaksi = Transaction::find(request()->id);
            $hapustransaksi->delete();

            if ($hapustransaksi->event->produk->jenis == 'BUKU') {
                $hapusbooktransaksi = BookTransactions::where('id_transaction', request()->id)->first();
                $hapusbooktransaksi->delete();
            }

            return redirect()->back()
            ->withFlashSuccess('Data '.$hapustransaksi->event->produk->nama.'-'.$hapustransaksi->event->id_event.' atas nama <strong>'.
                                $hapustransaksi->nama.' - '.$hapustransaksi->email.'</strong> Berhasil Dihapus !');
        }

        if (request()->metode == 'reminder') {
            $data = Transaction::find(request()->id);

            if ($data) {
                $this->nama          = $data->nama ?? null;
                $this->email         = $data->email ?? null;
                $this->panggilan     = $data->panggilan ?? null;
                $this->gender        = $data->gender ?? null;
                $this->tgllahir      = $data->tgllahir  ?? null;
                $this->nohp          = $data->kode_nohp.$data->nohp ?? null;
                $this->notifemail($data->email, $data->event->notifikasi->email_upload_ulang);
                $this->notifwa($data->kode_nohp.$data->nohp, $data->event->notifikasi->wa_upload_ulang);
                return redirect()->back()
                        ->withFlashSuccess('Data '.$data->event->produk->nama.'-'.$data->event->id_event.' atas nama <strong>'.
                                $data->nama.' - '.$data->email.'</strong> Reminder Berhasil Dikirim !');
            } else {
                return redirect()->back()
                        ->withFlashDanger('Data '.$data->event->produk->nama.'-'.$data->event->id_event.' atas nama <strong>'.
                                $data->nama.' - '.$data->email.'</strong> Reminder Gagal Dikirim !');
            }

        }

        if (request()->metode == 'belum-upload') {
            $data = Transaction::find(request()->id);

            if ($data) {
                $this->nama = $data->nama;
                $this->notifwa($data->kode_nohp.$data->nohp, $data->event->notifikasi->wa_belum_upload);
                return redirect()->back()
                        ->withFlashSuccess('Data '.$data->event->produk->nama.'-'.$data->event->id_event.' atas nama <strong>'.
                                $data->nama.' - '.$data->email.'</strong> Notif Belum Upload Berhasil Dikirim !');
            } else {
                return redirect()->back()
                        ->withFlashDanger('Data '.$data->event->produk->nama.'-'.$data->event->id_event.' atas nama <strong>'.
                                $data->nama.' - '.$data->email.'</strong> Notif Belum Upload Gagal Dikirim !');
            }
        }

        if (request()->method == 'update-resi') {
            $data = BookTransactions::find(request()->id);
            $data->no_resi = request()->noresi;
            $data->save();

            return redirect()->back()->withFlashSuccess('No. Resi Berhasil Ditambahkan !');
        }

        if (request()->metode == 'konfirmasi') {

            try {
                $konfirmasi                   = Transaction::find(request()->id);
                $konfirmasi->status           = 3;
                $konfirmasi->waktu_konfirmasi = Carbon::now();
                $konfirmasi->save();

                $this->nama          = $konfirmasi->nama;
                $this->email         = $konfirmasi->email;
                $this->panggilan     = $konfirmasi->panggilan;
                $this->gender        = $konfirmasi->gender;
                $this->tgllahir      = $konfirmasi->tgllahir ;
                $this->nohp          = $konfirmasi->kode_nohp.$konfirmasi->nohp;
                $this->kota          = $konfirmasi->kota;
                $this->total         = $konfirmasi->event->produk->jenis === 'BUKU' ? $konfirmasi->book->total : $konfirmasi->total;
                $this->ekspedisi     = $konfirmasi->event->produk->jenis === 'BUKU' ? $konfirmasi->book->kurir : null;
                $this->jmlh_buku     = $konfirmasi->event->produk->jenis === 'BUKU' ? $konfirmasi->book->jumlah_buku : null;
                $this->alamat        = $konfirmasi->event->produk->jenis === 'BUKU' ? $konfirmasi->book->alamat.', '.$konfirmasi->book->kecamatan.', '.$konfirmasi->book->kota.', '.$konfirmasi->book->provinsi : $konfirmasi->alamat;


                // if($konfirmasi->event->produk->inisial == 'JOM') {
                // $tes_ = DB::table('member_JOM')->create([
                //         'nama'          => $this->nama,
                //         'email'         => $this->email,
                //         'password'      => Hash::make('online'),
                //         'notelp'        => $this->nohp,
                //     ]);
                // }

                 if($konfirmasi->event->komisi != '-'){
                     $this->notifwa($this->nohp, $konfirmasi->event->notifikasi->wa_konfirmasi_pembayaran);
                 }

                try {
                    $this->notifemail($konfirmasi->email, $konfirmasi->event->notifikasi->email_konfirmasi_pembayaran);

                    return redirect()->back()
                        ->withFlashSuccess('Data '.$konfirmasi->event->produk->nama.'-'.$konfirmasi->event->id_event.' atas nama <strong>'.
                        $konfirmasi->nama.' - '.$konfirmasi->email.'</strong> Berhasil Dikonfirmasi!');
                } catch (Throwable $th) {
                    return redirect()->back()
                        ->withFlashDanger('Terjadi Kesalahan. Data '.$konfirmasi->event->produk->nama.'-'.$konfirmasi->event->id_event.' atas nama <strong>'.
                        $konfirmasi->nama.' - '.$konfirmasi->email.'</strong> Notifikasi Tidak Terikirim !. Mohon kirim ulang.');
                }
            } catch (Throwable $td) {
                return redirect()->back()->withFlashDanger('Konfirmasi Gagal ! Terjadi kesalahan, mohon ulangi. <br>Terima Kasih');
            }
        }
        $datatransaksi = Transaction::
                when(request()->get('nama'), function ($query) {
                    if( request()->get('nama') != null) {
                        return $query->whereRaw('LOWER(nama) LIKE ? ', '%' . strtolower(request()->get('nama')) . '%')
                        ->orWhereRaw('LOWER(email) LIKE ? ', '%' . strtolower(request()->get('nama')) . '%');
                    }
                })

                ->when(request()->get('agen'), function ($query) {
                    if( request()->get('agen') != null) {
                        return $query->whereHas('agen', function ($query) {
                            return $query->whereRaw('LOWER(name) LIKE ? ', '%' . strtolower(request()->get('agen')) . '%')
                                        ->orWhereRaw('LOWER(email) LIKE ? ', '%' . strtolower(request()->get('agen')) . '%');
                        });
                    }
                })

                ->when(request()->get('produk'), function ($query) {
                    if( request()->get('produk') != null) {
                        return $query->where('id_event', request()->get('produk'));
                    }
                })
                ->when(request()->get('status'), function ($query) {
                    if( request()->get('status') != null) {
                        return $query->where('status', request()->get('status'));
                    }
                })
                ->when(request()->get('jenis'), function ($query) {
                    if( request()->get('jenis') != null) {
                        return $query->where('jenis', request()->get('jenis'));
                    }
                })
                ->when(request()->roder, function ($query) {
                    if( request()->roder == 1) {
                        return $query->roderr(request()->roder, request()->produk);
                    }
                })
                ->when(request()->get('start'), function ($query) {
                    if( request()->get('start') != null) {
                        return $query->whereDate('created_at', '>=', request()->get('start'))
                                    ->whereDate('created_at', '<=', request()->get('end'))
                                    ;
                    }
                })
                ->orderBy('created_at', 'desc')
                ->paginate($this->perpage);
                // dd( $datatransaksi)
        $belumaktif = Transaction::where('status', 1)->count();
        $konfirmasi = Transaction::where('status', 2)->count();
        $aktif      = Transaction::where('status', 3)->count();
        $total      = Transaction::count();

        $produklist = Transaction::select('id_event')
                ->groupBy('id_event')
                ->get();

        $dataevent = $this->cekeventaktif;
        $idevent   = $this->cekeventaktif->id;

        // DATA PERHITUNGAN EVENT
        $belumaktif         = Transaction::ofEvent($idevent)->belumAktif()->count();
        $menunggukonfirmasi = Transaction::ofEvent($idevent)->menungguKonfirmasi()->count();
        $aktif              = Transaction::ofEvent($idevent)->aktif()->count();

        $afiliasi = Transaction::ofEvent($idevent)->afiliasi()->count();
        $manual   = Transaction::ofEvent($idevent)->manual()->count();

        $baaf = Transaction::ofEvent($idevent)->afiliasi()->belumAktif()->count();
        $bama = Transaction::ofEvent($idevent)->manual()->belumAktif()->count();
        $mkaf = Transaction::ofEvent($idevent)->afiliasi()->menungguKonfirmasi()->count();
        $mkma = Transaction::ofEvent($idevent)->manual()->menungguKonfirmasi()->count();
        $akaf = Transaction::ofEvent($idevent)->afiliasi()->aktif()->count();
        $akma = Transaction::ofEvent($idevent)->manual()->aktif()->count();

        $agen   = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->get()->count();
        $agenba = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->belumAktif()->get()->count();
        $agenmk = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->menungguKonfirmasi()->get()->count();
        $agenak = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->aktif()->get()->count();

        $agbaaf = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->afiliasi()->belumAktif()->get()->count();
        $agbama = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->manual()->belumAktif()->get()->count();
        $agmkaf = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->afiliasi()->menungguKonfirmasi()->get()->count();
        $agmkma = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->manual()->menungguKonfirmasi()->get()->count();
        $agakaf = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->afiliasi()->aktif()->get()->count();
        $agakma = Transaction::ofEvent($idevent)->select('id_agen')->groupBy('id_agen')->manual()->aktif()->get()->count();

        $datasta = array(
            'afiliasi'                          => $afiliasi,
            'manual'                            => $manual,
            'belum_aktif'                       => $belumaktif,
            'menunggu_konfirmasi'               => $menunggukonfirmasi,
            'aktif'                             => $aktif,
            'belum_aktif_afiliasi'              => $baaf,
            'belum_aktif_manual'                => $bama,
            'menunggu_konfirmasi_afiliasi'      => $mkaf,
            'menunggu_konfirmasi_manual'        => $mkma,
            'aktif_afiliasi'                    => $akaf,
            'aktif_manual'                      => $akma,
            'agen'                              => $agen,
            'agen_closing_belum_aktif'          => $agenba,
            'agen_closing_menunggu_konfirmasi'  => $agenmk,
            'agen_closing_aktif'                => $agenak,
            'agen_belum_aktif_afiliasi'         => $agbaaf,
            'agen_belum_aktif_manual'           => $agbama,
            'agen_menunggu_konfirmasi_afiliasi' => $agmkaf,
            'agen_menunggu_konfirmasi_manual'   => $agmkma,
            'agen_aktif_afiliasi'               => $agakaf,
            'agen_aktif_manual'                 => $agakma,
        );

        // DATA PERHITUNGAN STATISTIK 10 HARI

        $tgl10hari = CarbonPeriod::create(Carbon::now()->subdays(10), Carbon::now());

        foreach ($tgl10hari as $key => $date){
            $data_tanggal_sta[] = $date->format('d');
            $data_total_tran[]  = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->count();
            $data_tran_afi[]    = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->afiliasi()->count();
            $data_tran_man[]    = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->manual()->count();
            $data_tran_akt[]    = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->aktif()->count();
            $data_total_agen[]  = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->select('id_agen')->groupBy('id_agen')->get()->count();
        }

        $tanggal_sta = str_replace('"', '', json_encode($data_tanggal_sta));
        $total_tran  = str_replace('"', '', json_encode($data_total_tran));
        $tran_afi    = str_replace('"', '', json_encode($data_tran_afi));
        $tran_man    = str_replace('"', '', json_encode($data_tran_man));
        $tran_akt    = str_replace('"', '', json_encode($data_tran_akt));
        $total_agen  = str_replace('"', '', json_encode($data_total_agen));

        // DATA STATISTIK DARI SISTEM BARU AWAL

        // $data_tr = CarbonPeriod::create(Carbon::now()->subdays(80), Carbon::now());

        // foreach ($data_tr as $key => $date){
        //     $data_tanggal_sta_[] = $date->format('d');
        //     $data_total_tran_[]  = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->count();
        //     $data_tran_afi_[]    = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->afiliasi()->count();
        //     $data_tran_man_[]    = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->manual()->count();
        //     $data_total_agen_[]  = Transaction::where('created_at', 'like', '%'.$date->format('Y-m-d').'%')->select('id_agen')->groupBy('id_agen')->get()->count();
        // }

        // $tanggal_sta_ = str_replace('"', '', json_encode($data_tanggal_sta_));
        // $total_tran_  = str_replace('"', '', json_encode($data_total_tran_));
        // $tran_afi_    = str_replace('"', '', json_encode($data_tran_afi_));
        // $tran_man_    = str_replace('"', '', json_encode($data_tran_man_));
        // $total_agen_  = str_replace('"', '', json_encode($data_total_agen_));

        return view('backend.transaksi.index',
            //    compact('total_agen_','tran_man_','tran_afi_','total_tran_','tanggal_sta_','total_agen','tran_man','tran_afi','total_tran','tanggal_sta','datasta', 'dataevent', 'datatransaksi', 'edittransaksi', 'belumaktif', 'konfirmasi', 'aktif', 'total', 'produklist'));
               compact('total_agen','tran_man','tran_akt','tran_afi','total_tran','tanggal_sta','datasta', 'dataevent', 'datatransaksi', 'edittransaksi', 'belumaktif', 'konfirmasi', 'aktif', 'total', 'produklist'));
    }

    public function exportdata()
    {
        $dataevent = Event::find(request()->event);

        if ($dataevent != null) {
            if ($dataevent->produk->jenis == 'BUKU') {
                return (new DataTransaksiBuku)->event($dataevent->id)->download('Data - '.$dataevent->id_event.'.xlsx');
            } else {
                return (new DataTransaksi)->event($dataevent->id)->download('Data - '.$dataevent->id_event.'.xlsx');
            }
        } else {
            return redirect()->back()->withFlashDanger('Terjadi Kesalahan. Data Tidak Ditemukan');
        }
    }

    public function uploadbuktitf()
    {
        request()->validate([
            'filepond' => 'required|image|max:10240',
        ]);
        $file_bukti_transfer      = request()->file('filepond');
        Session::push('filebuktitransfer', Session::get('sesiupload') . '.' . $file_bukti_transfer->getClientOriginalExtension()); //membuat sesi nama file agar sesuai dengan pemilik pendaftar
        $buktitf         = Image::make($file_bukti_transfer);
        if ($_SERVER['HTTP_HOST'] == 'eid.test') {
            $lokasibuktitf   = public_path('/app/public/bukti-transfer/');
        } elseif (Session::get('jenis') == 1) {
            $lokasibuktitf   = public_path('../../../public_html/agen-entrepreneurid.com/dashboard/app/public/bukti-transfer/');
        } elseif (Session::get('jenis') == 2) {
            $lokasibuktitf   = public_path('../../../public_html/'.Session::get('domain').'/konfirmasi/app/public/bukti-transfer/');
            $lokasibuktitf_  = public_path('../../../public_html/'.Session::get('domain').'/app/app/public/bukti-transfer/');
        }
        $buktitf->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $buktitf->save($lokasibuktitf.Session::get('filebuktitransfer'));
        $buktitf->save($lokasibuktitf_.Session::get('filebuktitransfer'));

    }

    public function updateuploadbuktitf()
    {
        $transaksi = Transaction::find(request()->id);

        $file_bukti_transfer      = request()->file('buktitf');
        $namafile = $transaksi->email.'-'.$transaksi->event->id_event.'-'.Str::random(5).'.'.$file_bukti_transfer->getClientOriginalExtension();

        $buktitf  = Image::make($file_bukti_transfer);

        if ($_SERVER['HTTP_HOST'] == 'eid.test') {
            $lokasibuktitf   = public_path('/app/public/bukti-transfer/');
        } else {
            if ($transaksi->jenis == 1) {
                $lokasibuktitf   = public_path('../../../public_html/agen-entrepreneurid.com/dashboard/app/public/bukti-transfer/');
                $lokasibuktitf_   = public_path('../../../public_html/'.$transaksi->event->produk->domain.'/app/app/public/bukti-transfer/');
            } elseif ($transaksi->jenis == 2) {
                $lokasibuktitf   = public_path('../../../public_html/'.$transaksi->event->produk->domain.'/konfirmasi/app/public/bukti-transfer/');
                $lokasibuktitf_   = public_path('../../../public_html/'.$transaksi->event->produk->domain.'/app/app/public/bukti-transfer/');
            }
        }

        $buktitf->resize(400, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $buktitf->save($lokasibuktitf.$namafile);
        $buktitf->save($lokasibuktitf_.$namafile);

        $transaksi->keterangan   = request()->rekening;
        $transaksi->bukti_tf     = $namafile;
        $transaksi->waktu_upload = Carbon::now();

        if ($transaksi->status != 3) {
            $transaksi->status = 2;
        }

        $transaksi->save();

        return redirect()->to('/admin/transaksi?nama='.$transaksi->email.'&produk='.$transaksi->id_event)->withFlashSuccess('Upload Bukti Transfer Berhasil !');
    }

    public function cariagen()
    {
    	$agens = [];

        if(request()->q){
            $agens  = Agen::select("id", "name", "email")
                    ->whereRaw('LOWER(name) LIKE ? ', '%' . strtolower(request()->q) . '%')
                    ->orWhereRaw('LOWER(email) LIKE ? ', '%' . strtolower(request()->q) . '%')
            		->get();
        }
        return response()->json($agens);
    }

    public function dataagen()
    {
    	$datatransaksi = Transaction::
                when(request()->get('nama'), function ($query) {
                    if( request()->get('nama') != null) {
                        return $query->whereRaw('LOWER(nama) LIKE ? ', '%' . strtolower(request()->get('nama')) . '%')
                        ->orWhereRaw('LOWER(email) LIKE ? ', '%' . strtolower(request()->get('nama')) . '%');
                    }
                })
                ->when(request()->get('produk'), function ($query) {
                    if( request()->get('produk') != null) {
                        return $query->where('id_event', request()->get('produk'));
                    }
                })
                ->when(request()->get('status'), function ($query) {
                    if( request()->get('status') != null) {
                        return $query->where('status', request()->get('status'));
                    }
                })
                ->when(request()->get('jenis'), function ($query) {
                    if( request()->get('jenis') != null) {
                        return $query->where('jenis', request()->get('jenis'));
                    }
                })
                ->where('id_agen', '=', request()->idagen)
                ->when(request()->roder, function ($query) {
                    if( request()->roder == 1) {
                        return $query->roderr(request()->roder, request()->produk);
                    }
                })
                ->orderBy('created_at', 'desc')
                ->paginate($this->perpage);

        $agen       = Agen::find(request()->idagen);
        $event      = Event::find(request()->produk);
        $produklist = Transaction::select('id_event')
                ->groupBy('id_event')
                ->get();

        // DATA PERHITUNGAN EVENT
        $idevent   = request()->produk;
        $idagen    = request()->idagen;

        $belumaktif         = Transaction::ofEvent($idevent)->dataAgen($idagen)->belumAktif()->count();
        $menunggukonfirmasi = Transaction::ofEvent($idevent)->dataAgen($idagen)->menungguKonfirmasi()->count();
        $aktif              = Transaction::ofEvent($idevent)->dataAgen($idagen)->aktif()->count();
        $afiliasi           = Transaction::ofEvent($idevent)->dataAgen($idagen)->afiliasi()->count();
        $manual             = Transaction::ofEvent($idevent)->dataAgen($idagen)->manual()->count();

        $datasta = array(
            'afiliasi'                 => $afiliasi,
            'manual'                   => $manual,
            'belum_aktif'              => $belumaktif,
            'menunggu_konfirmasi'      => $menunggukonfirmasi,
            'aktif'                    => $aktif
        );

        return view('backend.transaksi.data-agen', compact('datatransaksi', 'agen', 'produklist', 'event', 'datasta'));
    }

    public function leadmagnetexport(){
        return (new DataLeadMagnet)->download('Data Produk Lead Magnet.xlsx');
    }
}
