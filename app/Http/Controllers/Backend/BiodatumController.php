<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

use App\Models\Biodatum;
use App\Repositories\Backend\BiodatumRepository;
use App\Http\Requests\Backend\Biodatum\ManageBiodatumRequest;
use App\Http\Requests\Backend\Biodatum\StoreBiodatumRequest;
use App\Http\Requests\Backend\Biodatum\UpdateBiodatumRequest;

use App\Events\Backend\Biodatum\BiodatumCreated;
use App\Events\Backend\Biodatum\BiodatumUpdated;
use App\Events\Backend\Biodatum\BiodatumDeleted;
use App\Exports\DataEcourse;
use Excel;
use Throwable;

class BiodatumController extends Controller
{
    /**
     * @var BiodatumRepository
     */
    protected $biodatumRepository;

    /**
     * BiodatumController constructor.
     *
     * @param BiodatumRepository $biodatumRepository
     */
    public function __construct(BiodatumRepository $biodatumRepository)
    {
        $this->biodatumRepository = $biodatumRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageBiodatumRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageBiodatumRequest $request)
    {
        return view('backend.biodatum.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManageBiodatumRequest    $request
     *
     * @return mixed
     */
    public function create(ManageBiodatumRequest $request)
    {
        $agen = Biodatum::findOrFail(request()->get('ref'));
        return view('backend.biodatum.create', compact('agen'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBiodatumRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreBiodatumRequest $request)
    {
        $this->biodatumRepository->create($request->only(
            'produk',
            'ref',
            'nama',
            'email',
            'no_telp',
            'panggilan',
            'kota',
            // 'jumlah',
            // 'ongkir',
            // 'total',
        ));

        // Fire create event (BiodatumCreated)
        event(new BiodatumCreated($request));

        return redirect()->route('admin.biodata.index')
            ->withFlashSuccess('Tambah Data Berhasil !');
    }

    /**
     * Display the specified resource.
     *
     * @param ManageBiodatumRequest  $request
     * @param Biodatum               $biodatum
     *
     * @return mixed
     */
    public function show(ManageBiodatumRequest $request, Biodatum $biodatum)
    {
        return view('backend.biodatum.show')->withBiodatum($biodatum);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageBiodatumRequest $request
     * @param Biodatum              $biodatum
     *
     * @return mixed
     */
    public function edit(ManageBiodatumRequest $request, Biodatum $biodatum)
    {
        return view('backend.biodatum.edit')->withBiodatum($biodatum);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBiodatumRequest  $request
     * @param Biodatum               $biodatum
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateBiodatumRequest $request, Biodatum $biodatum)
    {
        $this->biodatumRepository->update($biodatum, $request->only(
            'nama',
            'email',
            'no_telp',
            // 'alamat',
            'kota',
            'panggilan',
            'tgllahir',
            'gender',
        ));

        // Fire update event (BiodatumUpdated)
        event(new BiodatumUpdated($request));

        return redirect()->route('admin.biodata.index')
            ->withFlashSuccess($biodatum->nama . ' - ' . $biodatum->email . ', Berhasil Diperbaruhi !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageBiodatumRequest $request
     * @param Biodatum              $biodatum
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageBiodatumRequest $request, Biodatum $biodatum)
    {
        $this->biodatumRepository->deleteById($biodatum->id);

        // Fire delete event (BiodatumDeleted)
        event(new BiodatumDeleted($request));

        return redirect()->route('admin.biodata.index')
            ->withFlashDanger($biodatum->nama . ' - ' . $biodatum->email . ', Berhasil Dihapus !');
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManageBiodatumRequest $request
     * @param Biodatum              $deletedBiodatum
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageBiodatumRequest $request, Biodatum $deletedBiodatum)
    {
        $this->biodatumRepository->forceDelete($deletedBiodatum);

        return redirect()->route('admin.biodata.deleted')
            ->withFlashSuccess(__('backend_biodata.alerts.deleted_permanently'));
    }

    /**
     * @param ManageBiodatumRequest $request
     * @param Biodatum              $deletedBiodatum
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageBiodatumRequest $request, Biodatum $deletedBiodatum)
    {
        $this->biodatumRepository->restore($deletedBiodatum);

        return redirect()->route('admin.biodata.index')
            ->withFlashSuccess(__('backend_biodata.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManageBiodatumRequest $request
     *
     * @return mixed
     */
    public function deleted(ManageBiodatumRequest $request)
    {
        return view('backend.biodatum.deleted')
            ->withbiodata($this->biodatumRepository->getDeletedPaginated(25, 'id', 'asc'));
    }

    function getdata(Request $request)
    {
        $idproduk = request('produk');
        $user     = request('user');

        $biodatas = Biodatum::select(['id', 'nama', 'email', 'no_telp', 'ref', 'gender', 'tgl_daftar', 'idprodukreg', 'status_user', 'aktif'])->get();
        // ->where('idprodukreg', $idproduk)
        // ->where('status_user', '=', $user)
        // ->where([
        //     ['idprodukreg', '=', $idproduk],
        //     ['status_user', '=', $user],
        // ]);
        // ->where('idprodukreg', '=', $idproduk);
        // ->where(function ($query) {
        //     $user     = request('user') ?? 0;

        //     $query->where('status_user', '=', $user);
        // });


        return Datatables::of($biodatas)
            // ->addColumn('action', function($biodata){
            //     // return '<a href="#" class="btn btn-sm btn-pill btn-danger delete" id="'.$biodata->id.'"> <i class="fas fa-trash"></i> Hapus </a>';
            //     if ($biodata->aktif == 0 ) {
            //         return '<a href="#" class="btn btn-sm btn-pill btn-light" id="'.$biodata->id.'">  Belum TF <i class="fas fa-close"></i></a>';
            //     } elseif ($biodata->aktif == 1 ) {
            //         return '<a href="#" class="btn btn-sm btn-pill btn-success" id="'.$biodata->id.'"> Konfirmasi <i class="fas fa-edit"></i></a>';
            //     } elseif ($biodata->aktif == 2 ) {
            //         return '<a href="#" class="btn btn-sm btn-pill btn-info" id="'.$biodata->id.'">  Aktif <i class="fas fa-check"></i></a>';
            //     }
            // })
            // ->addColumn('checkbox', '<input type="checkbox" name="biodata_checkbox[]" class="biodata_checkbox" value="{{ $id }}" />')
            // ->rawColumns(['checkbox','action'])
            // ->filter(function ($query) use ($request) {
            //     if ($request->has('statusaktif')) {
            //         $status = $request->get('statusaktif');
            //         if ($request->get('statusaktif') == 'belum-transfer') {
            //             $query->where('aktif', '=', '0');
            //         } elseif ($request->get('statusaktif') == 'konfirmasi'){
            //             $query->where('aktif', '=', '1');
            //         } elseif ($request->get('statusaktif') == 'aktif'){
            //             $query->where('aktif', '=', '2');
            //         }
            //     }
            // })
            // ->filter(function ($query) use ($request) {
            //     if ($request->has('produk')) {
            //         $query->where('idprodukreg', '=', "%{$request->get('produk')}%");
            //     }

            //     if ($request->has('statususer')) {
            //         $query->where('status_user', '=', "%{$request->get('statususer')}%");
            //     }

            //     if ($request->has('konfirmasi')) {
            //         $query->where('aktif', '=', "%{$request->get('konfirmasi')}%");
            //     }
            // })
            ->filter(function ($instance) use ($request) {
                if ($request->has('produk')) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['idprodukreg'], $request->get('produk')) ? true : false;
                    });
                }

                if ($request->has('statususer')) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['status_user'], $request->get('statususer')) ? true : false;
                    });
                }

                if ($request->has('konfirmasi')) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        return Str::contains($row['aktif'], $request->get('konfirmasi')) ? true : false;
                    });
                }
            })
            ->make(true);
    }

    function hapusdata(Request $request)
    {
        $biodatas = Biodatum::find($request->input('id'));
        if ($biodatas->delete()) {
            echo 'Data Terhapus';
        }
    }

    function hapusdatamasal(Request $request)
    {
        $biodata_id_array = $request->input('id');
        $biodatas = Biodatum::whereIn('id', $biodata_id_array);
        if ($biodatas->delete()) {
            echo 'Data Terhapus';
        }
    }

    public function updatedata(Request $request)

    {
        $biodatas = Biodatum::find($request->input('pk'));
        if ($biodatas->update([$request->input('name') => $request->input('value')])) {
            // notify()->success('Laravel Notify is awesome!');
            // session()->put('info','This is for info.');
            echo 'Data Diperbaruhi';
        }
        // Biodatum::find($request->pk)->update([$request->name => $request->value]);
        // return response()->json(['success'=>'done']);
    }

    public function teswa()
    {
        // METHOD POST
        // Pastikan phone menggunakan kode negara 62 di depannya
        $apikey = 'xEhX7sF1wh7KFdVXZx5AfJj7wn6krW8m';
        $phone = '628125144744';
        $message =
            'Halo (nama) 😊🙏

Pesan ini Anda terima karena beberapa waktu lalu telah mengisi form pendaftaran Agen entrepreneurID.

Setelah menjadi Agen resmi entrepreneurID nanti, maka Anda berhak mendapatkan bimbingan khusus Agen, komisi untuk setiap penjualan produk *entrepreneurID* dan fasilitas Agen entrepreneurID lainnya.

Agar Anda segera terdaftar sebagai Agen resmi, maka silahkan transfer biaya pendaftaran *sebesar Rp. ke salah satu rekening berikut :

Mandiri = 1360007600528
BCA = 1911488766
BRI = 012101009502532
BNI = 1238568920

Seluruh rekening barusan, atas nama *Wulandari Tri*

Setelah Anda menyelesaikan pendaftaran, langsung konfirmasi bukti transfernya ke link ini https://konfirmasi.agen-entrepreneurid.com/pembayaran

Jika Anda punya pertanyaan, silahkan balas pesan ini. Dengan senang hati kami akan membantu. 🙂

Salam,

*Tim entrepreneurID*';

        $url = 'https://api.wanotif.id/v1/send';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array(
            'Apikey'    => $apikey,
            'Phone'     => $phone,
            'Message'   => $message,
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        // dd($curl);

        return redirect()->route('admin.biodata.index')
            ->withFlashSuccess('Tes WA Selesai');
    }

    public function exportexcel(Request $request)
    {
        $produk = $request->get('produk') ?? null;

        if($produk == 6)
        {$namaproduk = "Mentoring Organic Marketing";}
        elseif($produk == 6)
        {$namaproduk = "Copywriting Next Level";}
        elseif($produk == 4)
        {$namaproduk = "Pendaftaran Agen";}
        elseif($produk == 9)
        {$namaproduk = "Peta Bisnis Online";}
        elseif($produk == 10)
        {$namaproduk = "Instant Copywriting";}
        elseif($produk == 21)
        {$namaproduk = "Copywriting Next Level";}
        elseif($produk == 11)
        {$namaproduk = "Mentoring Sales Funnel";}
        elseif($produk == 12)
        {$namaproduk = "Kelas Online Copywriting";}
        elseif($produk == 15)
        {$namaproduk = "Kelas Online Copywriting";}
        elseif($produk == 17)
        {$namaproduk = "WhatsApp Master Closing";}
        elseif($produk == 19)
        {$namaproduk = "Pendaftaran Agen Resmi";}
        elseif($produk == 5)
        {$namaproduk = "Kelas Facebook Hacking";}
        elseif($produk == 3)
        {$namaproduk = "Reseller Formula";}
        elseif($produk == 18)
        {$namaproduk = "WhatsApp Master Closing";}
        elseif($produk == 22)
        {$namaproduk = "Kelas Online Copywriting";}
        elseif($produk == 23)
        {$namaproduk = "Mentoring Instant Copywriting";}
        elseif($produk == 24)
        {$namaproduk = "Agen Resmi entrepreneurID";}
        elseif($produk == 25)
        {$namaproduk = "Kelas 100 Orderan";}
        elseif($produk == 29)
        {$namaproduk = "Copywriting Next Level";}
        else
        {$namaproduk = "Produk Tidak Dikenali";}

        if ($produk != null) {
            return (new DataEcourse)->produk($produk)->download('Data - '.$namaproduk.'.xlsx');
            // return Excel::download(new DataEcourse, 'users.xlsx');
        } else {
            return redirect()->back();
        }
    }

    public function cnlresi()
    {
        $data = Biodatum::where('idprodukreg', '29')
                        ->where('aktif', '!=', '1')
                        ->when(request()->get('kurir'), function ($query) {
                            if( request()->get('kurir') != 'all') {
                                return $query->where('kurir', 'like', '%'.request()->get('kurir').'%');
                            }
                        })
                        ->when(request()->get('nama'), function ($query) {
                            return $query->whereRaw('idprodukreg = 29 AND LOWER(nama) LIKE ? ', '%' . strtolower(request()->get('nama')) . '%')
                                        ->orWhereRaw('idprodukreg = 29 AND LOWER(email) LIKE ? ', '%' . strtolower(request()->get('nama')) . '%');
                        })
                        ->paginate(10);

        if(request()->get('proses') == 'resi'){
            try {
                $datapemesan = Biodatum::findOrFail(request()->get('id'));
                $datapemesan->update([
                    'noresi' => request()->get('noresi')
                ]);
                try {
                    $apikey = 'xEhX7sF1wh7KFdVXZx5AfJj7wn6krW8m';
                    $phone = '+62' . $datapemesan->no_telp;
                    $message =
                        '*Info Resi Pengiriman Buku CNL*

Halo ' . $datapemesan->nama . ' 😊🙏
Selamat ya pemesanan buku Copywriting Next Level Anda sudah dalam pengiriman, dengan data pemesanan :

Nomor Resi : ' . $datapemesan->noresi . '
Ekspedisi : ' . $datapemesan->kurir . '

Untuk cek data pemesanan silahkan bisa dicek ke bit.ly/resicnl2021
Jika Anda punya pertanyaan, silahkan balas pesan ini. Dengan senang hati kami akan membantu. 😊🙏

Salam,

*Tim entrepreneurID*';

                    $url = 'https://api.wanotif.id/v1/send';

                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $url);
                    curl_setopt($curl, CURLOPT_HEADER, 0);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
                    curl_setopt($curl, CURLOPT_POST, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, array(
                        'Apikey'    => $apikey,
                        'Phone'     => $phone,
                        'Message'   => $message,
                    ));
                    $response = curl_exec($curl);
                    curl_close($curl);

                    session()->flash('notif-berhasil', $datapemesan->nama . ' - ' . $datapemesan->email . ', Nomor Resi Berhasil Diperbaruhi !');
                    return redirect()->back();
                } catch (Throwable $th) {
                    session()->flash('notif-gagal', $datapemesan->nama . ' - ' . $datapemesan->email . ',  Nomor Resi Tidak Terupdate !. Silahkan Diulangi');
                    return redirect()->back();
                }
            } catch (Throwable $e) {
                // report($e);
                // return false;
                session()->flash('notif-gagal', 'Terjadi Kesalahan. Konfirmasi Gagal !');
                return redirect()->back();
            }
        }
        return view('backend.biodatum.cnl.resi', compact('data'));
    }
}
